<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Conductor__c</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Conga Conductor</label>
    <tab>About_Conga_Conductor</tab>
    <tab>Conductor__c</tab>
    <tab>Conductor_Setup</tab>
    <tab>Conductor_Dashboard</tab>
</CustomApplication>
