({
    doInit : function(component, event, helper) {
        var action = component.get("c.getCurrentAction");
        action.setCallback(this, function(response) {
            var state = response.getState();
            var action = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS") {
	            component.set("v.alert1", action.Alert1__c);
	            component.set("v.alert2", action.Alert2__c);
	            component.set("v.alert3", action.Alert3__c);
	            component.set("v.alert4", action.Alert4__c);
            } else {
                console.log('Problem getting alerts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        var action1 = component.get("c.getFilterMap");
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
	            var fMap = response.getReturnValue();
	            var temp;
            	var result = [];
	            for (var pung in fMap) {
	            	temp = fMap[pung];
	       			result.push({
	       				key: pung,
	       				value: temp
	       			});
	       		}
	            component.set("v.filterMap", result);
            } else {
                console.log('Problem getting filters, response state: ' + state);
            }
        });
        $A.enqueueAction(action1);    
    }
})