/* 
    Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-846 - Creation of the Lien Synopsis Section Component
*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLienAmtList");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lienAmts", response.getReturnValue());
            } else {
                console.log('Problem getting Lien Amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        helper.getLienStageLabels(component);
        helper.getLienSubstageLabels(component);
    }
})