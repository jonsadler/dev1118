/* 
    Updated: MJD - October 2017 for October Release
    Purpose: PD-890 - Portal Users Search for a Claimant

    Updated: lmsw - January 2018 for January Release
    Purpose: PD-1320 - Add Matter Report Links

*/
    ({
    getActionRecord : function(component,event,helper){
        var action = component.get("c.getAction");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.currentAction", response.getReturnValue());
                // Start PD-890 //
                var currentAction = component.get("v.currentAction");
                this.GetPortalMatterReportId(component);    //PD-1320
                var availableOnPortal = currentAction.Available_in_Portal__c;

                if(availableOnPortal==false){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                    "url": "/not-available/"
                    });
                    urlEvent.fire();
                }
                // End PD-890 //
            } else {
                console.log('Problem getting Action, response state: ' + state);
            }
        });
  
        $A.enqueueAction(action);
    },

    getUserLawFirmName : function(component,event,helper){
        var action = component.get("c.getCompanyName");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lawFirmName", response.getReturnValue());
            } else {
                console.log('Problem getting Users Law Firm Name, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);
    },
    // start PD-1320
    GetPortalMatterReportId : function(component,event,helper) {
        var action = component.get("c.getPortalMatterReportId");
        action.setParams({"action":component.get("v.currentAction")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.matterReportId", response.getReturnValue());
            } else {
                console.log('Problem getting Matter Report Id, response state: '+ state);
            }
        });
        $A.enqueueAction(action);
    }
    // end PD-1320
})