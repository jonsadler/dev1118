/*
    Created: Marc Dysart - July 2017 for July Release
    Purpose: PD-837 - Help Page Content Creation

History:

  Updated: lmsw - November 2017 for December Release
  Purpose: PD-1264 - Add Legal Terms link to FAQs
  
*/

({
	// expandAll : function(component, event, helper) {
 //       helper.toggleSection(component,event,'accordion-details-01');
 //       helper.toggleSection(component,event,'accordion-details-02');
 //       helper.toggleSection(component,event,'accordion-details-03');
 //    },
	sectionOne : function(component, event, helper) {
       helper.toggleSection(component,event,'accordion-details-01');
    },
    
   sectionTwo : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-02');
    },
   
   sectionThree : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-03');
   },
   
   sectionFour : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-04');
   },
   sectionFive : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-05');
   },
   sectionSix : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-06');
   },
   sectionSeven : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-07');
   },
   sectionEight : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-08');
   },
   sectionNine : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-09');
   },
   sectionTen : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-10');
   },
   sectionEleven : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-11');
   },
  sectionTwelve : function(component, event, helper) {
      helper.toggleSection(component,event,'accordion-details-12');
   },
})