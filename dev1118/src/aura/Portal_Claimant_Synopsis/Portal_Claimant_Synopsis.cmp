<!--  
	Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-842 - Creation of Claimant Synopsis Section Component

History:

    Updated: lmsw - August 2017
    Purpose: PD-862 Add Compensable Injuries to Claimant Synopsis
-->

<aura:component controller="Controller_Aura1" access="global" implements="forceCommunity:availableForAllPageTypes,force:hasRecordId"> 	
<aura:attribute name="claimant" type="Claimant__c" default="{'sobjectType':'Claimant__c'}"/>
<aura:attribute name="lastRelease" type="Release__c" default="{'sobjectType':'Release__c'}"/>
<aura:attribute name="injuries" type="list" default="{'sobjectType': 'Injury__c[]'}"/>
<aura:attribute name="injuryEndDate" type="boolean" default="false"/>
<aura:attribute name="component_description" type="String" default="Component Description" access="global"/>
<aura:handler name="init" value="{!this}" action="{!c.doInit}" />
	<div>
		
	 		 <div class="slds-grid slds-wrap claimant-synopsis top-synopsis">
				<div class="slds-size_1-of-1 slds-medium-size_1-of-4 slds-large-size_1-of-4">
					<p class="slds-text-heading--label synopsisHeader">Providio ID</p>
						<p>{!v.claimant.ProvidioID__c}</p>
					<p class="slds-text-heading--label synopsisHeader additional">SSN</p>
						<p>{!v.claimant.SSN__c}</p>
					<p class="slds-text-heading--label synopsisHeader additional">Gender</p>
						<p>{!v.claimant.Gender__c}</p>
				</div>

				<div class="slds-size_1-of-1 slds-medium-size_1-of-4 slds-large-size_1-of-4">
					<p class="slds-text-heading--label synopsisHeader">Address</p>
					<p>{!v.claimant.Address_1__c}</p>
					<aura:if isTrue="{!v.claimant.Address_2__c}"><p>{!v.claimant.Address_2__c}</p></aura:if>
					<p>{!v.claimant.City__c}
		              <aura:if isTrue="{!v.claimant.State__c != null}">,&nbsp;{!v.claimant.State__c}&nbsp;&nbsp;{!v.claimant.Zip__c}
		              </aura:if>
		            </p>
		            <p class="slds-text-heading--label synopsisHeader additional">Dates</p>
					<p> DOB:&nbsp;&nbsp;<ui:outputDate value="{!v.claimant.DOB__c}"/></p>
					<aura:if isTrue="{!v.claimant.DOD__c}">
						<p> DOD:&nbsp;&nbsp;<ui:outputDate value="{!v.claimant.DOD__c}"/></p>
					</aura:if>
				</div>
			
				<div class="slds-size_1-of-1 slds-medium-size_2-of-4 slds-large-size_2-of-4">
					<p class="slds-text-heading--label synopsisHeader">PLRP Elections</p>
					<div class="slds-grid">
						<div class="slds-col"><p>Opted in PLRP: {!v.claimant.Opted_in_PLRP__c}</p></div>
						<div class="slds-col"><p>Providio handling Private Liens: {!v.claimant.Providio_handling_private_liens__c}</p></div>
						
					</div>
					<div><br></br></div>
					<aura:if isTrue="{!and(v.claimant.Opted_in_PLRP__c == 'Yes',v.claimant.Providio_handling_private_liens__c == 'YES')}">
						<p>Providio handling Claimant’s Private and/or Medicare Advantage Liens via PLRP and, if one or more is not a participating PLRP plan, directly with the plan (plan information to be provided by Claimant’s counsel).</p>
					</aura:if>

					<aura:if isTrue="{!and(v.claimant.Opted_in_PLRP__c == 'No',v.claimant.Providio_handling_private_liens__c == 'YES')}">
						<p>Claimant has opted out of the PLRP.&nbsp;&nbsp;Providio handling Private and/or Medicare Advantage Liens only as directed/information provided by Claimant’s counsel. </p>
					</aura:if>

					<aura:if isTrue="{!and(v.claimant.Opted_in_PLRP__c == 'Yes',v.claimant.Providio_handling_private_liens__c == 'NO')}">
						<p>Providio handling Claimant’s Private and/or Medicare Advantage Liens via PLRP.&nbsp;&nbsp;However, if one or more is not a participating PLRP plan, Providio will not be handling the non-participating plan lien resolution.</p>
					</aura:if>

					<aura:if isTrue="{!and(v.claimant.Opted_in_PLRP__c == 'No',v.claimant.Providio_handling_private_liens__c == 'NO')}">
						<p>Claimant has opted out of the PLRP.&nbsp;&nbsp;Private and/or Medicare Advantage will also not be handled by Providio.</p>
					</aura:if>

					<!-- Setup up option 5 where:  the MSA doesn’t dictate a lien resolution method (PLRP vs. not) but the leadership or firm (if inventory only) doesn’t want to do a PLRP -->
					<aura:if isTrue="{!and(v.claimant.Opted_in_PLRP__c == 'N/A',v.claimant.Providio_handling_private_liens__c == 'NO')}">
						<p>No PLRP program.  Providio handling Private and/or Medicare Advantage Liens only as directed/information provided by Claimant’s counsel.</p>
					</aura:if>	
				</div>

				<!-- <div class="slds-size_1-of-1 slds-medium-size_1-of-5 slds-large-size_1-of-5">
					<p class="slds-text-heading_label synopsisHeader">Most Recent Disbursement</p>
					<aura:if isTrue="{!v.lastRelease.Name}">
						<p>{!v.lastRelease.Name} - {!v.lastRelease.Status__c} - {!v.lastRelease.Award__r.Name}</p>
						<aura:set attribute="else">
							<p>None</p>
						</aura:set>
					</aura:if>
				</div> -->
			</div>
		<div class="slds-grid slds-wrap claimant-synopsis">
			<div class="outer-section-break">
				<div class="section-break"></div>
			</div>
			<div class="slds-size_1-of-1 slds-medium-size_1-of-1 slds-large-size_1-of-1 synopsisHeader injury-title-section">
				<p class="slds-text-heading--label synopsisHeader injury-title">Compensable Injuries ({!v.injuries.length})</p>
			</div>
		</div>
		<aura:if isTrue="{!(v.injuries.length gt 0)}">
			<div class="slds-grid slds-wrap claimant-synopsis injuryTitle">
				<div class="{! (v.injuryEndDate) ? 'slds-size_1-of-1 slds-medium-size_1-of-3 slds-large-size_1-of-3 injuryTitle' : 'slds-size_1-of-1 slds-medium-size_1-of-2 slds-large-size_1-of-2 injuryTitle'}">
					<p class="slds-text-heading--label synopsisHeader">Description</p>
				</div>
				<div class="{! (v.injuryEndDate) ? 'slds-size_1-of-1 slds-medium-size_1-of-3 slds-large-size_1-of-3 injuryTitle' : 'slds-size_1-of-1 slds-medium-size_1-of-2 slds-large-size_1-of-2 injuryTitle'}">
					<p class="slds-text-heading--label synopsisHeader">Date of Loss</p>
				</div>
				<div class="{! (v.injuryEndDate) ? 'slds-size_1-of-1 slds-medium-size_1-of-3 slds-large-size_1-of-3 injuryTitle' : 'noEndDate'}">
					<p class="slds-text-heading--label synopsisHeader">Last Date of Treatment</p>
				</div>
			</div>
			<div class="slds-grid slds-wrap claimant-synopsis smallFont">
				<aura:iteration items="{!v.injuries}" var="injury">  
					<div class="{! (v.injuryEndDate) ? 'slds-size_1-of-1 slds-medium-size_1-of-3 slds-large-size_1-of-3 smallFont' : 'slds-size_1-of-1 slds-medium-size_1-of-2 slds-large-size_1-of-2 smallFont'}">
						<div title="{! injury.Injury_Description__c }">{! injury.Injury_Description__c }</div>
					</div>
					<div class="{! (v.injuryEndDate) ? 'slds-size_1-of-1 slds-medium-size_1-of-3 slds-large-size_1-of-3 smallFont' : 'slds-size_1-of-1 slds-medium-size_1-of-2 slds-large-size_1-of-2 smallFont'}">
						<div class="slds-truncate" title="{! injury.DOL__c }"><ui:outputDate value="{! injury.DOL__c }"/></div>
					</div>
					<div class="{! (v.injuryEndDate) ? 'slds-size_1-of-1 slds-medium-size_1-of-3 slds-large-size_1-of-3 smallFont' : 'noEndDate'}">
						<div class="slds-truncate" title="{! injury.DOL__c }"><ui:outputDate value="{! injury.End_Date__c }"/></div>
					</div>
					
				</aura:iteration>
			</div>
			<aura:set attribute="else">
				<div class="noinjuries claimant-synopsis">No Compensable Injuries have been identified</div>
			</aura:set>
		</aura:if>
	</div>
</aura:component>