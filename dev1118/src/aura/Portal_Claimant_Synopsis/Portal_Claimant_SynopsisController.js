/*
    Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-842 - Creation of Claimant Synopsis Section Component

History:

    Updated: lmsw - August 2017
    Purpose: PD-862 Add Compensable Injuries to Claimant Synopsis
*/
({

     doInit : function(component, event, helper) {
        var action = component.get("c.getInjuries2");
        action.setParams({"cId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.injuries", response.getReturnValue());
                // Take out the underscoresin the picklists
                var records = component.get("v.injuries");
                var injuries = [];
                var j = 0;
                for (var i=0; i<records.length; i++) {
                    var c= records[i];
                    if (c.Compensable__c=='True') {
                        injuries[j]=c;
                        j++;
                        if (c.End_Date__c) {
                            component.set("v.injuryEndDate", true )
                        }
                    }
                }
                component.set("v.injuries", injuries);             
            } else {
                console.log('Problem getting injuries, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    /*    var action = component.get("c.getlawfirmID");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lfID", response.getReturnValue());
    // This is to get the claimant using the lawfirm id and claimant id
                var action2 = component.get("c.getlfClaimant");
                action2.setParams({"lfId": component.get("v.lfID"),"cId": component.get("v.recordId")});
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if(component.isValid() && state === "SUCCESS") {
                        component.set("v.claimant", response.getReturnValue());
                    } else {
                        console.log('Problem getting lawfirm claimant, response state: ' + state);
                    }
                });
                $A.enqueueAction(action2);

            } else {
                console.log('Problem getting lawfirm ID, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);

        var action3 = component.get("c.getLastRelease");
        action3.setParams({"cId": component.get("v.recordId")});
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lastRelease", response.getReturnValue());
            } else {
                console.log('Problem getting Last Eligible Disbursement, response state: ' + state);
            }
        });
        $A.enqueueAction(action3);		*/
    }
})