/*
	Created: lmsw - December 2017 for January Release
	Purpose: PD-1281 Create custom component for the 
			 AwardLien Related list on the Lien Page.

History:

    Updated: lmsw - December 2017 for January Release
    Purpose: PD-1281 Modify to add multiple checkbox. 
*/
({
	doInit : function(component, event, helper) {
        helper.ClearRecordsAttributes(component);
        helper.GetAwardLiens(component);
		helper.GetLien(component);
		helper.CreateItems(component,event);
	},
    
     onCheck: function(component, evt) {
        var checkboxes = component.find("checkbox");
        var results=[];
        for(var i = 0; i < checkboxes.length; i++){
            // Get award Ids if Award has been checked
            if(checkboxes[i].get("v.value")){
                if(checkboxes[i].get("v.text")!=null)
                    results.push(checkboxes[i].get("v.text"));
            }
        }
        component.set("v.awardIDs", results);
     },

	openTabwithSubtab: function(component, event, helper) {
        var newId = event.currentTarget.getAttribute("id");
        var url = '/'+newId;
        var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                    "url": url
                    });
                    urlEvent.fire();
    },
    // Delete AwardLien
    deleteAL: function(component, event,helper) {
        helper.DeleteAwardLien(component);
    }, 
    // Create new AwardLien
    createAL: function(component, event,helper) {
        helper.CreateAwardLien(component);  
    },
    handleMenuSelect: function(component, event, helper) {
        var selected = event.getParam("value");
        if (selected == 'edit') 
            helper.EditAL(component, event, helper);
        else if (selected == 'delete') {
            var alID = event.getSource().get("v.name");
            component.set("v.deleteAL", alID);
            component.set('v.activeModal', 'deleteConfirm');
        }     
        $A.get("e.force:refreshView").fire();
    },
    newAwardLienClick: function(component, event, helper) {
        component.set('v.activeModal', 'newAwardLien');
        helper.ClearRecordsAttributes(component);
        helper.GetAwardLiens(component);
        helper.GetLien(component);
    },
    closeModal: function(component, event, helper) {
        component.set('v.activeModal', null);
    }
})