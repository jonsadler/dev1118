/*
	Created: lmsw - December 2017 for January Release
	Purpose: PD-1281 Create custom component for the 
			 AwardLien Related list on the Lien Page.

History:

    Updated: lmsw - December 2017 for January Release
    Purpose: PD-1281 Modify to add multiple checkbox. 
*/
({
	CreateItems: function(component,event){
		var items = [
			{label: "Delete", value: "delete"},
			{label: "Edit", value: "edit"}
		];
		component.set("v.actions", items);
	},
	CreateAwardLien: function(component,event,helper){
		// Getting AwardLien information
        var lId = component.get("v.recordId");  
        var awardIds = component.get("v.awardIDs"); 
        var action = component.get("c.createAwardLien");
        action.setParams({"lId": lId, "awardIds": awardIds });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS") {
                if(response.getReturnValue() === "FAIL")
                    this.ShowToastCreateError(component);
                $A.get("e.force:refreshView").fire();
                console.log('A new AwardLien has been created.');
            } else {
                console.log('Problem creating/saving AwardLien, response state: '+ state);
                this.ShowToastCreateError(component);
            }
            component.set('v.activeModal', null);
            component.set("v.awardId",null);
        });
        $A.enqueueAction(action); 
	},
	EditAL: function(component,event,helper) {
		var alID = event.getSource().get("v.name");
		var ere = $A.get("e.force:editRecord");
		ere.setParams({"recordId": alID});
		ere.fire();
		$A.get("e.force:refreshView").fire();
	},
	DeleteAwardLien: function(component,event,helper){
		var alID = component.get("v.deleteAL");
        var action = component.get("c.deleteAwardLien");
        action.setParams({"awardLienId": alID});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS") {
                if(response.getReturnValue() === "FAIL")
                    this.ShowToastDeleteError(component);
                $A.get("e.force:refreshView").fire();
            } else {
                console.log('Problem deleting AwardLien, response state: '+ state);
                this.ShowToastDeleteError(component);
            }
            component.set('v.activeModal', null);
            component.set('v.deleteAL', null);
        });
        $A.enqueueAction(action);
	},
	GetAwardLiens : function(component, event) {
		var action = component.get("c.getAwardLiensByLienId");
		action.setParams({"lienId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				component.set("v.awardLiens", response.getReturnValue());
			} else {
				console.log('Problem getting AwardLiens, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	GetLien : function(component,helper) {
		var action = component.get("c.getLien");
		action.setParams({"lId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				var l = response.getReturnValue();
				component.set("v.lien", l);
				component.set("v.claimant", l.Claimant__c);
				component.set("v.currentAction", l.Action__c);
				this.GetAwards(component);
				this.GetLinkedAwards(component);
				this.GetAwardsWOInjuries(component);
				this.GetAwardsW_OtherInjuries(component);
			} else {
				console.log('Problem getting Lien, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	GetAwards : function(component) {
		var action = component.get("c.getAwardsMap");
		action.setParams({"cId": component.get("v.claimant"), "lId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				var iMap = response.getReturnValue();
				var result = [];
				var temp = [];
				for(var iId in iMap) {
					temp = iMap[iId];
					result.push({
						key: iId,
						value: temp
					});
				}
				component.set("v.awards", result);
			} else {
				console.log('Problem getting Awards Map, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	GetLinkedAwards : function(component) {
		var action = component.get("c.getAwardLinkedMap");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				var iMap = response.getReturnValue();
				var result = [];
				var temp = [];
				for(var iId in iMap) {
					temp = iMap[iId];
					result.push({
						key: iId,
						value: temp
					});
				}
				component.set("v.linkedAwards", result);
			} else {
				console.log('Problem getting Linked Awards Map, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	GetAwardsWOInjuries : function(component) {
		var action = component.get("c.getAwardsWOInjuryMap");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				var iMap = response.getReturnValue();
				var result = [];
				var temp = [];
				for(var iId in iMap) {
					temp = iMap[iId];
					result.push({
						key: iId,
						value: temp
					});
				}
				component.set("v.awardsWOInjuries", result);
			} else {
				console.log('Problem getting Awards w/o Injuries Map, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	GetAwardsW_OtherInjuries : function(component) {
		var action = component.get("c.getAwardsW_anInjuryMap");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				var iMap = response.getReturnValue();
				var result = [];
				var temp = [];
				for(var iId in iMap) {
					temp = iMap[iId];
					result.push({
						key: iId,
						value: temp
					});
				}
				component.set("v.awardsW_OtherInjuries", result);
			} else {
				console.log('Problem getting Awards with other Injuries Map, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	ClearRecordsAttributes : function (component, helper) {
        // Make sure that awardIDs is cleared
        component.set('v.awardIDs', []);
    },
	ShowToastDeleteError : function(component, event, helper) {
    	var toastEvent = $A.get("e.force:ShowToast");
    	toastEvent.setParams({
    		"title": "Fail!",
    		"message": "Deleting an existing AwardLien was not successful.  Please contact a systems administrator with this information including the Lien Name.",
    		"type": "error",
    		"mode": "sticky"
    	});
    	toastEvent.fire();
    },
    ShowToastCreateError : function(component, event, helper) {
    	var toastEvent = $A.get("e.force:ShowToast");
    	toastEvent.setParams({
    		"title": "Fail!",
    		"message": "Creating a new AwardLien was not successful.  Please contact a systems administrator with this information including the Lien Name.",
    		"type": "error",
    		"mode": "sticky"
    	});
    	toastEvent.fire();
    }
})