({

    getClaimantRecord : function(component) {
        var action = component.get("c.getLien");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state ==="SUCCESS") {
                component.set("v.lien", response.getReturnValue());
                } else {
                console.log('Problem getting claimant, response state: ' + state);
            }
        });
        $A.enqueueAction(action);

        var action2 = component.get("c.getLienAmtList");
        var lienID = component.get("v.recordId");
        console.log('lienID :'+lienID);
        action2.setParams({lId: lienID});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.lienamts", response.getReturnValue());
                var lienamts = component.get("v.lienamts");
                console.log('lienamts:'+lienamts);
                
            } else {
                console.log('Problem getting lien amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);
    }

})