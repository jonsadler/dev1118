({
    handleInit : function (component, event, helper) {
        helper.getAwardLiens(component, helper);
    },

    awardNameClicked : function (component, event, helper) {
        var awardId       = event.currentTarget.getAttribute('data-awardId');
        var url           = '/' + awardId;
        var navigateEvent = $A.get('e.force:navigateToURL');

        navigateEvent.setParams({
            'url' : url
        });

        navigateEvent.fire();
    },

    newAwardLienClicked : function (component, event, helper) {
        var lienId            = component.get('v.recordId');
        var createRecordEvent = $A.get('e.force:createRecord');

        createRecordEvent.setParams({
            'entityApiName'      : 'AwardLien__c',
            'defaultFieldValues' : {
                'Lien__c' : lienId
            }
        });

        createRecordEvent.fire();
    },

    buttonMenuSelected : function (component, event, helper) {
        var selection           = event.getParam('value');
        var buttonMenuComponent = event.getSource();
        var awardLienId         = buttonMenuComponent.get('v.name');

        if (selection == 'delete') {
            component.set('v.activeModal', 'deleteConfirmation');
            component.set('v.deleteConfirmationAwardLienId', awardLienId);
        }
        else if (selection == 'edit') {
            var editRecordEvent = $A.get('e.force:editRecord');

            editRecordEvent.setParams({
                recordId : awardLienId
            });

            editRecordEvent.fire();
        }
    },

    deleteConfirmationCloseClicked : function (component, event, helper) {
        component.set('v.activeModal', null);
        component.set('v.deleteConfirmationAwardLienId', null);
    },

    deleteConfirmationCancelClicked : function (component, event, helper) {
        component.set('v.activeModal', null);
        component.set('v.deleteConfirmationAwardLienId', null);
    },

    deleteConfirmationDeleteClicked : function (component, event, helper) {
        var awardLienId = component.get('v.deleteConfirmationAwardLienId');

        helper.deleteAwardLien(component, helper, awardLienId);
    },

    handleForceShowToast : function(component, event, helper) {
        var eventMessage = event.getParam('message');

        if (!$A.util.isUndefinedOrNull(eventMessage) && eventMessage.includes('AwardLien')) {
            helper.getAwardLiens(component, helper);
        }
    },

})