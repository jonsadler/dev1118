({
    getAwardLiens : function (component, helper) {
        var lienId     = component.get('v.recordId');
        var apexMethod = component.get('c.getAwardLiensByLienId');

        apexMethod.setParams({
            lienId : lienId
        });

        apexMethod.setCallback(this, function getAwardLiensByLienIdCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var awardLiens = response.getReturnValue();

                component.set('v.awardLiens', awardLiens);
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (getAwardLiensByLienIdCallback) INCOMPLETE');
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (getAwardLiensByLienIdCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (getAwardLiensByLienIdCallback) ERROR; unknown error');
                }

            }
        });

        $A.enqueueAction(apexMethod);
    },

    deleteAwardLien : function (component, helper, awardLienId) {
        var apexMethod = component.get('c.deleteAwardLien');

        apexMethod.setParams({
            awardLienId : awardLienId
        });

        apexMethod.setCallback(this, function deleteAwardLienCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var awardLienDeleted = response.getReturnValue();

                if (awardLienDeleted) {
                    helper.getAwardLiens(component, helper);
                }
                else {
                    helper.showDeleteAwardLienErrorToast(component, helper);
                }
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (deleteAwardLienCallback) INCOMPLETE');

                helper.showDeleteAwardLienErrorToast(component, helper);
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (deleteAwardLienCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (deleteAwardLienCallback) ERROR; unknown error');
                }

                helper.showDeleteAwardLienErrorToast(component, helper);
            }

            component.set('v.activeModal', null);
            component.set('v.deleteConfirmationAwardLienId', null);
        });

        $A.enqueueAction(apexMethod);

    },

    showDeleteAwardLienErrorToast : function (component, helper) {
        var showToastEvent = $A.get('e.force:showToast');

        showToastEvent.setParams({
            title   : 'ERROR',
            message : 'Deleting an existing AwardLien was not successful.  Please contact a systems administrator with this information including the Lien Name.',
            type    : 'error',
            mode    : 'sticky'
        });

        showToastEvent.fire();
    },

})