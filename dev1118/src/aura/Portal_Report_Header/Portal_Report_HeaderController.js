/*
	Created: lmsw - January 2018 for January Release
	Purpose: PD-1320 - Add Previous page button
*/
({
	gotoPreviousPage : function(component, event, helper) {
		console.log("Go to Previous Page");
		window.history.back();
	    return false;
	}
})