/*  
	Created: lmsw - June 2017
    Purpose: PD-847 - Creation of the Lien Status Bar Component
    
*/
({
	doInit : function(component, event, helper) {
        var action = component.get("c.getLien");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state ==="SUCCESS") 
                component.set("v.lien", response.getReturnValue());
             else 
                console.log('Problem getting claimant, response state: ' + state);
        });
        $A.enqueueAction(action);
        
        var action2 = component.get("c.getStages");
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") 
                component.set("v.stages", response.getReturnValue());
             else 
                console.log('Problem getting stages, response state: ' + state);
        });
        $A.enqueueAction(action2);

        
        helper.getStageLabel(component);
        helper.getSubstageLabel(component);
        helper.getStageIndex(component);
    }
})