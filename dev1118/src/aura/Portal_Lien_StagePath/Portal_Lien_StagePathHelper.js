/*  
	Created: lmsw - June 2017
    Purpose: PD-847 - Creation of the Lien Status Bar Component
    
*/
({
    getStageLabel : function(component) {
    	var action = component.get("c.getLienStageLabel");
		action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") 
                component.set("v.stageLabel", response.getReturnValue());
             else 
                console.log('Problem getting stage label, response state: ' + state);
        });
        $A.enqueueAction(action);
    },
    getSubstageLabel : function(component) {
    	var action = component.get("c.getLienSubstageLabel");
		action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") 
                component.set("v.substageLabel", response.getReturnValue());
             else 
                console.log('Problem getting substage label, response state: ' + state);
        });
        $A.enqueueAction(action);
    },
	getStageIndex : function(component)	{
		var action = component.get("c.getLienStageIndex");
		action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") 
                component.set("v.stageIndex", response.getReturnValue());
             else 
                console.log('Problem getting stage index, response state: ' + state);
        });
        $A.enqueueAction(action);
	}
	
})