({

    doInit : function(component, event, helper) {
        var action = component.get("c.getLienAmtList");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.lienamts", response.getReturnValue());
                component.set("v.numlnas", component.get("v.lienamts").length);
            } else {
                console.log('Problem getting lien amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }

,
    createLienAmountModal: function(component, event, helper) {
        document.getElementById("backGroundSectionId").style.display = "block";
        document.getElementById("lienamtCreateId").style.display = "block";
    }
,
    createRecord : function (component, event, helper) {

        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Lien_Negotiated_Amounts__c"
        });
        createRecordEvent.fire();
        } 




, editLienAmount : function(component, event, helper) {
    var rowID = event.target.id;
    console.log('rowID = ' + rowID);
    var editRecordEvent = $A.get("e.force:editRecord");
    editRecordEvent.setParams({"recordId": rowID});
    editRecordEvent.fire();
    
} 
,
refresh : function(component, event, helper) {
    var action = cmp.get('c.getLienAmtList');
    action.setCallback(cmp,
        function(response) {
            var state = response.getState();
            if (state === 'SUCCESS'){
                $A.get('e.force:refreshView').fire();
            } else {
                 console.log('Problem refreshing lien amounts, response state: ' + state);
            }
        }
    );
    $A.enqueueAction(action);
}




})