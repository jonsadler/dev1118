/*
	Created: RAP - February 2017 for March Release
    Purpose: PD-159 - Action - Lien Summary Tab

History: 

    Updated: lmsw - July 2017
    Purpose: PD-642 - Modified to only Show the Types of Liens that Are Being Worked
*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getStageMetrics");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
	            var smMap = response.getReturnValue();
       			var result = [];
	            var stages = [];
	            var temp2 = [];
	            var temp3 = [];
	            for (var lienType in smMap) {
	            	stages = smMap[lienType]; // stages keyset is lien type
	            	var result2 = [];
	            	for (var stage in stages) {
		       			temp2 = stages[stage]; // temp2 keyset is stage
	            		temp3 = temp2['All'];
		       			result2.push({
		       				key: stage,
		       				value: temp3
		       			});
		       		}
	       			result.push({
	       				key: lienType,
	       				value: result2
	       			});
		       	}
	            component.set("v.liensByStage", result);
            } else {
                console.log('Problem getting stage metrics, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        helper.getLienMap(component);
        helper.getLienStageLabels(component);
        helper.getLienTypeLabels(component);
    }
})