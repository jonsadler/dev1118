/*
	Created: RAP - February 2017 for March Release
    Purpose: PD-159 - Action - Lien Summary Tab

History: 

    Updated: lmsw - July 2017
    Purpose: PD-642 - Modified to only Show the Types of Liens that Are Being Worked
*/
({
    getLienStageLabels : function (component) {
        var action = component.get("c.getLienStageMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lsMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in lsMap) {
                    temp = lsMap[plif];
                    result.push(temp);
                }
                component.set("v.lienStages", result);
            } else {
                console.log('Problem getting Lien Stage Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	},
	getLienTypeLabels : function (component) {
        var action = component.get("c.getLienTypeMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lsMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in lsMap) {
                    temp = lsMap[plif];
	       			result.push({
	       				key: plif,
	       				value: temp
	       			});
                }
                component.set("v.lienTypes", result);
            } else {
                console.log('Problem getting Lien Type Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	},
    getLienMap : function(component) {
        var action = component.get("c.getLienMetrics");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.lienMap", response.getReturnValue());
            } else {
                console.log('Problem getting lien map, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})