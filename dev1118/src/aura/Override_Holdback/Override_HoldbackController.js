/*
	Created: RAP - November 2017 for December Release
    Purpose: PD-1198 - Custom Holdback (Override Holdback)
*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLienAmtList");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lienamts", response.getReturnValue());
            } else {
                console.log('Problem getting lien amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);

        var action2 = component.get("c.getLien");
        action2.setParams({"lId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.lien", response.getReturnValue());
            } else {
                console.log('Problem getting lien, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action2);
    }, 
	editLienAmount : function(component, event, helper) {
	    var lienamountrowID = event.target.id;
	    var editRecordEvent = $A.get("e.force:editRecord");
	        editRecordEvent.setParams({
	      		"recordId": lienamountrowID
	   		});
	    editRecordEvent.fire();
    
    },
    showOverride : function(component, event, helper) {
        var cmpMsg = component.find("override-section");
        $A.util.removeClass(cmpMsg, 'hide');

        var overridebtn= component.find("override-btn-id");
        $A.util.addClass(overridebtn, 'hide');

    },
    hideOverride : function(component, event, helper) {
        var cmpMsg = component.find("override-section");
        $A.util.addClass(cmpMsg, 'hide');

        var overridebtn= component.find("override-btn-id");
        $A.util.removeClass(overridebtn, 'hide');
    },
    saveOverride : function(component, event, helper) {
        var lienId = component.get("v.recordId");  
        var description = component.find("description").get("v.value");
        var directedBy = component.find("directedBy").get("v.value");
        var which = component.get("v.value");
        var amount = which == 'amount' ? component.find("hbAmount").get("v.value") : null;
        var percent = which == 'percent' ? component.find("hbPercent").get("v.value") : null;

        var action4 = component.get("c.UpdateHB");
        action4.setParams({
            "lId"  : lienId,
            "description"  : description,
            "amount"  : amount,
            "percent" : percent,
            "directedBy" : directedBy
        });
        action4.setCallback(component, function(response) {
            var state = response.getState();
            var lien;
            var msg;
            var outcome = response.getReturnValue();
            for (var item in outcome) {
        		lien = outcome[item];
        		msg = item;
            }
            if (msg == 'Success') {
            	component.set("v.lien", lien);
	            $A.get("e.force:refreshView").fire();
	            var cmpMsg = component.find("override-section");
	            $A.util.addClass(cmpMsg, 'hide');
	            var overridebtn= component.find("override-btn-id");
	            $A.util.removeClass(overridebtn, 'hide');
            }
            else {
                helper.showLienUpdateErrorToast(component,msg);
                console.log('Problem saving lien, response: ' + msg);
            }
        });
        $A.enqueueAction(action4);
    },
    handleChange: function (component, event) {
        var changeValue = event.getParam("value");
        component.set("v.value", changeValue);
        if (changeValue == 'amount') {
	        var selection = component.find("amount");
	        $A.util.removeClass(selection, 'hide');
	        var nonselection= component.find("percent");
	        $A.util.addClass(nonselection, 'hide');
	    }
	    else {
	        var selection = component.find("percent");
	        $A.util.removeClass(selection, 'hide');
	        var nonselection= component.find("amount");
	        $A.util.addClass(nonselection, 'hide');
	    }
    }})