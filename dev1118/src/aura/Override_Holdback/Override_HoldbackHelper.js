({
    showLienUpdateErrorToast : function (component, msg) {
        var showToastEvent = $A.get('e.force:showToast');
        showToastEvent.setParams({
            title   : 'ERROR',
            message : msg,
            type    : 'error',
            mode    : 'sticky'
        });
        showToastEvent.fire();
    }    
})