({
    // Updated: Marc Dysart Oct 2017 for Oct Release
    // Purpose: PD-985 - Portal - Needs Attention Status not changing


	doInit : function(component, event, helper) {
    
       var action = component.get("c.getLFClaimants");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.claimants", response.getReturnValue());
                var records = component.get("v.claimants");
                var filter = [];
                var field=0;
                var plrp=0;
                for (var i=0; i<records.length; i++) {
                	var c = records[i];
                    console.log(c);
                    console.log(c.Needs_Attention_Field__c);
           			if (c.Needs_Attention_Field__c>0 ){
           				field++;
           			}
                    if(c.Needs_AttentionPLRP__c > 0){
                    	plrp++;
                    }
              
            } 
            component.set("v.NeedsAttentionFields", field);
            component.set("v.NeedsAttentionPLRP", plrp);

            } else {
                console.log('Problem getting claimants, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);
        
	}, 

    // Start PD-985
    setNeedsAttentionFieldFilter : function(component, event, helper) {

        var cmpEvent = component.getEvent("cmpEvent");
            cmpEvent.setParams({
                "claimantneedsattentionField": true
            }); 
            cmpEvent.fire();
        },
    setNeedsAttentionPLRPFilter : function(component, event, helper) {

        var cmpEvent = component.getEvent("cmpEvent");
            cmpEvent.setParams({
                "claimantneedsattentionPLRP": true
            }); 
            cmpEvent.fire();
        }

        // End PD-985
})