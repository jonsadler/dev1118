/* 
    Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-814 - Creation of Portal Header Component
*/

({
    chooseMatter: function(component, event) {
        var actionID = event.currentTarget.dataset.id;
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            "matterID": actionID
        }); 
        cmpEvent.fire();
        component.set("v.showMatterList",false);
        var matterlist = document.getElementById("matter-list");
        if (matterlist) {
            matterlist.classList.add("hide-matter-list");
        }

        var action = component.get("c.SetAction");
        action.setParams({"aId": actionID});
        action.setCallback(this, function(response) {
        var state = response.getState();
        if(component.isValid() && state === "SUCCESS") {
                console.log('This set the action ID to '+actionID);
             } else {
               console.log('Problem setting Action ID, response state: ' + state);
             }
           });
            $A.enqueueAction(action); 
    },
            
    homechosen: function(component, event) {

        var matterlist = document.getElementById("matter-list");
        if (matterlist) {
            matterlist.classList.remove("hide-matter-list");
        }

    },

    menu-click : function (component,event) {
        var matterlist = document.getElementById("matter-list");
        if (matterlist) {
            matterlist.classList.add("hide-matter-list");
        }
    }
})