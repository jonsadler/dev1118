({
    handleInit : function (component, event, helper) {
        helper.getNeedsAttentionRecordTypeMap(component, helper);
        helper.getClaimantNeedsAttentions(component, helper);
    },

    handleNeedsAttentionsChanged : function (component, event, helper) {
        helper.sortNeedsAttentions(component, helper);
    },

    handleForceShowToast : function(component, event, helper) {
        var eventMessage = event.getParam('message');

        if (!$A.util.isUndefinedOrNull(eventMessage) && eventMessage.includes('Needs Attention')) {
            helper.getClaimantNeedsAttentions(component, helper);
        }
    },

    newInternalNeedsAttentionClicked : function (component, event, helper) {
        var claimantId                         = component.get('v.recordId');
        var needsAttentionRecordTypeMap        = component.get('v.needsAttentionRecordTypeMap');
        var internalNeedsAttentionRecordTypeId = needsAttentionRecordTypeMap.Internal;

        if ($A.util.isUndefinedOrNull(internalNeedsAttentionRecordTypeId)) {
            console.log('ERROR: (newInternalNeedsAttentionClicked) Needs Attention record type of Internal is not defined.');
            return;
        }

        var createRecordEvent = $A.get('e.force:createRecord');

        createRecordEvent.setParams({
            'entityApiName'      : 'NeedsAttention__c',
            'recordTypeId'       : internalNeedsAttentionRecordTypeId,
            'defaultFieldValues' : {
                'Claimant__c' : claimantId
            }
        });

        createRecordEvent.fire();
    },

    newPortalNeedsAttentionClicked : function (component, event, helper) {
        var claimantId                       = component.get('v.recordId');
        var needsAttentionRecordTypeMap      = component.get('v.needsAttentionRecordTypeMap');
        var portalNeedsAttentionRecordTypeId = needsAttentionRecordTypeMap.Portal;

        if ($A.util.isUndefinedOrNull(portalNeedsAttentionRecordTypeId)) {
            console.log('ERROR: (newPortalNeedsAttentionClicked) Needs Attention record type of Portal is not defined.');
            return;
        }

        var createRecordEvent = $A.get('e.force:createRecord');

        createRecordEvent.setParams({
            'entityApiName'      : 'NeedsAttention__c',
            'recordTypeId'       : portalNeedsAttentionRecordTypeId,
            'defaultFieldValues' : {
                'Claimant__c' : claimantId
            }
        });

        createRecordEvent.fire();
    },

    buttonMenuSelected : function (component, event, helper) {
        var selection           = event.getParam('value');
        var buttonMenuComponent = event.getSource();
        var needsAttentionId    = buttonMenuComponent.get('v.name');

        if (selection == 'delete') {
            component.set('v.activeModal', 'deleteConfirmation');
            component.set('v.deleteConfirmationNeedsAttentionId', needsAttentionId);
        }
        else if (selection == 'edit') {
            var editRecordEvent = $A.get('e.force:editRecord');

            editRecordEvent.setParams({
                recordId : needsAttentionId
            });

            editRecordEvent.fire();
        }
    },

    deleteConfirmationCloseClicked : function (component, event, helper) {
        component.set('v.activeModal', null);
        component.set('v.deleteConfirmationNeedsAttentionId', null);
    },

    deleteConfirmationCancelClicked : function (component, event, helper) {
        component.set('v.activeModal', null);
        component.set('v.deleteConfirmationNeedsAttentionId', null);
    },

    deleteConfirmationDeleteClicked : function (component, event, helper) {
        var needsAttentionId = component.get('v.deleteConfirmationNeedsAttentionId');

        helper.deleteNeedsAttention(component, helper, needsAttentionId);
    },

    typeClicked : function (component, event, helper) {
        var needsAttentionId = event.currentTarget.getAttribute('data-needsAttentionId');
        var url              = '/' + needsAttentionId;
        var navigateEvent    = $A.get('e.force:navigateToURL');

        navigateEvent.setParams({
            'url' : url
        });

        navigateEvent.fire();
    },

})