({
    getNeedsAttentionRecordTypeMap : function (component, helper) {
        var apexMethod = component.get('c.getNeedsAttentionRecordTypeMap');

        apexMethod.setCallback(this, function getNeedsAttentionRecordTypeMapCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var needsAttentionRecordTypeMap = response.getReturnValue();

                component.set('v.needsAttentionRecordTypeMap', needsAttentionRecordTypeMap);
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (getNeedsAttentionRecordTypeMapCallback) INCOMPLETE');
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (getNeedsAttentionRecordTypeMapCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (getNeedsAttentionRecordTypeMapCallback) ERROR; unknown error');
                }

            }
        });

        $A.enqueueAction(apexMethod);
    },

    getClaimantNeedsAttentions : function (component, helper) {
        var claimantId = component.get('v.recordId');
        var apexMethod = component.get('c.getClaimantNeedsAttentions');

        apexMethod.setParams({
            claimantId : claimantId
        });

        apexMethod.setCallback(this, function getClaimantNeedsAttentionsCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var needsAttentions = response.getReturnValue();

                component.set('v.needsAttentions', needsAttentions);
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (getClaimantNeedsAttentionsCallback) INCOMPLETE');
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (getClaimantNeedsAttentionsCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (getClaimantNeedsAttentionsCallback) ERROR; unknown error');
                }

            }
        });

        $A.enqueueAction(apexMethod);
    },

    deleteNeedsAttention : function (component, helper, needsAttentionId) {
        var apexMethod = component.get('c.deleteNeedsAttention');

        apexMethod.setParams({
            needsAttentionId : needsAttentionId
        });

        apexMethod.setCallback(this, function deleteNeedsAttentionCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var needsAttentionDeleted = response.getReturnValue();

                if (needsAttentionDeleted) {
                    helper.getClaimantNeedsAttentions(component, helper);
                }
                else {
                    helper.showDeleteNeedsAttentionErrorToast(component, helper);
                }
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (deleteNeedsAttentionCallback) INCOMPLETE');

                helper.showDeleteNeedsAttentionErrorToast(component, helper);
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (deleteNeedsAttentionCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (deleteNeedsAttentionCallback) ERROR; unknown error');
                }

                helper.showDeleteNeedsAttentionErrorToast(component, helper);
            }

            component.set('v.activeModal', null);
            component.set('v.deleteConfirmationNeedsAttentionId', null);
        });

        $A.enqueueAction(apexMethod);

    },

    showDeleteNeedsAttentionErrorToast : function (component, helper) {
        var showToastEvent = $A.get('e.force:showToast');

        showToastEvent.setParams({
            title   : 'ERROR',
            message : 'Deleting an existing Needs Attention was not successful.  Please contact a systems administrator with this information including the Claimant Name.',
            type    : 'error',
            mode    : 'sticky'
        });

        showToastEvent.fire();
    },

    sortNeedsAttentions : function (component, helper) {
        // This function takes the list of Claimant Needs Attentions and sorts them
        // into separate attributes for:
        //     Internal / Current
        //     Internal / Other
        //     Portal / Current
        //     Portal / Other

        var needsAttentions                = component.get('v.needsAttentions');
        var internalNeedsAttentionsCurrent = [];
        var internalNeedsAttentionsOther   = [];
        var portalNeedsAttentionsCurrent   = [];
        var portalNeedsAttentionsOther     = [];

        for (var needsAttentionsIndex = 0; needsAttentionsIndex < needsAttentions.length; needsAttentionsIndex++) {
            var needsAttention = needsAttentions[needsAttentionsIndex];

            if (!$A.util.isUndefinedOrNull(needsAttention.RecordTypeId) && needsAttention.RecordType.Name == 'Portal') {
                // This is a portal Needs Attention

                if (needsAttention.Alert_Status__c == 'Resolved') {
                    // This is an 'Other' type of status
                    portalNeedsAttentionsOther.push(needsAttention);
                }
                else {
                    // This is a 'Current' type of status
                    portalNeedsAttentionsCurrent.push(needsAttention);
                }

            }
            else {
                // This is an internal Needs Attention

                if (needsAttention.Internal_Status__c == 'Resolved' || needsAttention.Internal_Status__c == 'Special Case - Ignore') {
                    // This is an 'Other' type of status
                    internalNeedsAttentionsOther.push(needsAttention);
                }
                else {
                    // This is a 'Current' type of status
                    internalNeedsAttentionsCurrent.push(needsAttention);
                }
            }
        }

        component.set('v.internalNeedsAttentionsCurrent', internalNeedsAttentionsCurrent);
        component.set('v.internalNeedsAttentionsOther',   internalNeedsAttentionsOther);
        component.set('v.portalNeedsAttentionsCurrent',   portalNeedsAttentionsCurrent);
        component.set('v.portalNeedsAttentionsOther',     portalNeedsAttentionsOther);
    },

})