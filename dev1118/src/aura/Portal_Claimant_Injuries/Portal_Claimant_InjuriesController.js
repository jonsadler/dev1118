/* 
	Created: lmsw - July 2017
    Purpose: PD-862 - Creation of the Injuries Component on the Claimant Info Tab
*/
({
    doInit : function(component, event, helper) {
       
        var action = component.get("c.getInjuries2");
        action.setParams({"cId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.injuries", response.getReturnValue());
                // Take out the underscoresin the picklists
                var records = component.get("v.injuries");
                console.log('records size: '+ records.length);
                var injuries = [];
                var j = 0;
            	for (var i=0; i<records.length; i++) {
            		var c= records[i];
            		if (c.Compensable__c=='True') {
            			injuries[j]=c;
            			j++;
            		}
            	}
            	component.set("v.injuries", injuries);             
            } else {
                console.log('Problem getting injuries, response state: ' + state);
            }
        });
        $A.enqueueAction(action);

    /*    var action2 = component.get("c.getAwardsByInjuryMap");
        action2.setParams({"cId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
        	var state = response.getState();
        	if(component.isValid() && state === "SUCCESS") {
        		var fMap = response.getReturnValue();
        		var result = [];
        		var temp = [];
        		for(var plif in fMap){
        			temp = fMap[plif];
        			result.push({
        				key: plif,
        				value: temp
        			});
        		}
        		component.set("v.awards", result);
        	} else {
        		console.log('Problem getting awardMap, response state: ' + state);
        	}
        });
		$A.enqueueAction(action2); */
    }

}