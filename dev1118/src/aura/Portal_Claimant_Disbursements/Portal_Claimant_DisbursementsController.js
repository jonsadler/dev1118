/* 
    Created: lmsw - June 2017 for June Release
    Purpose: PD-864 - Claimant Disbursements for the portal
*/
({
	doInit : function(component, event, helper) {
        console.log('doInit');
        var action = component.get("c.getDisbForClaimant");
        action.setParams({"cId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var dMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in dMap) {
                    temp = dMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }                
                component.set("v.releases", result);
            } else {
                console.log('Problem getting release Map, response state: ' + state);
            }
        });  
        $A.enqueueAction(action);  

        var action2 = component.get("c.getDisbAwards");
        action2.setParams({"cId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.awards", response.getReturnValue());
            } else {
                console.log('Problem getting awards, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);

        var action3 = component.get("c.getReleaseTotal");
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.totalRelease", response.getReturnValue());
            } else {
                console.log('Problem getting totalRelease, response state: ' + state);
            }
        });
        $A.enqueueAction(action3);
    }
        
})