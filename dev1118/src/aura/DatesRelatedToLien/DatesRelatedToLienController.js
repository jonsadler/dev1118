/* 
	Created: lmsw - January 2017 for March release
    Purpose: PD-400 - Dates Tab Component - Lien

History:

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-80 Fix Medicare_Model -> Medicare_Modeled, show div depending
    		 on Lien Type

*/
({
	doInit : function(component, event, helper) {
		var action = component.get("c.getLienEligibleDates");
		action.setParams({"lId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state === "SUCCESS") {
				component.set("v.lienDatesMap", response.getReturnValue());
			} else {
				console.log('Problem getting lien eligiblility dates, response state: ' + state);
			}
		});
		$A.enqueueAction(action);

		var action2 = component.get("c.getLien");
		action2.setParams({"lId": component.get("v.recordId")});
		action2.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state === "SUCCESS") {
				component.set("v.lien", response.getReturnValue());
				helper.hideDateMsg(component);
			} else {
				console.log('Problem getting lien, response state: '+ state);
			}
		});
		$A.enqueueAction(action2);
	}
})