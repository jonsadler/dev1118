/* 
	Created: lmsw - March 2017
    Purpose: PRODSUPT-80 Fix Medicare_Model -> Medicare_Modeled, show div depending
    		 on Lien Type

History: 

	Updated: lmsw - May 2017
	Purpose: PD-654 - Add dates for record lien type PLRP Claim Detail and PLRP Model
	         Modify Lien Type and change type from Part_c to Non-PLRP
	         (Medicare Global Model record type name = Medicare_Modeled)
	         (PLRP Claim Detail record type name = PLRP_Claims_Submission)
	         (PLRP_Model record type name = PLRP_Modeled)
*/
({
	hideDateMsg : function(component,event) {
		var lienType = component.get("v.lien.RecordType.DeveloperName");
		console.log("Lien Type: "+ lienType);
		var datesDiv = component.find("hasDates");
		var plrpDiv = component.find('plrpDates');
		var noDatesDiv = component.find("noDates");
		if((lienType == "Medicare_Modeled") || (lienType == "Non_PLRP")){
			console.log("Show Dates");
			$A.util.removeClass(datesDiv, 'slds-hide');
// PD-654
		} else if((lienType == "PLRP_Claims_Submission") || (lienType == "PLRP_Modeled")){
			console.log("Show Private Dates");
			$A.util.removeClass(plrpDiv, 'slds-hide');
		}else {
			console.log("Hide Dates");
			$A.util.removeClass(noDatesDiv, 'slds-hide');
		}
	}
})