({
    // ################################################################################################# createCardItems
    // This method iterates through the tile's liens, and creates the cards for them.
    //
    createCardItems : function (component, helper) {
        var action                                       = component.get('v.action');
        var lienType                                     = component.get('v.lienType');
        var lienStage                                    = component.get('v.lienStage');
        var liens                                        = component.get('v.liens');
        var configTileCards                              = component.get('v.configTileCards');
        var configReportLinks                            = component.get('v.configReportLinks');
        var cardsToCreate                                = configTileCards[lienType][lienStage];
        var storeSubstages                               = false;
        var storeLienholders                             = false;
        var storeSpecialsNoResponseIn30Days              = false;
        var storeSpecialsLienholderStateAgencies         = false;
        var storeSpecialsLienholdersByNumLiensDescending = false;
        var cardItems                                    = [];
        var lienInfoMap                                  = {
            'substages'   : {},
            'lienholders' : {},
            'specials'    : {
                'noResponseIn30Days'              : [],
                'lienholderStateAgencies'         : [],
                'lienholdersByNumLiensDescending' : []
            }
        };

        // If this type and stage don't have any cards to display, do
        // not create any card items.
        if ($A.util.isUndefinedOrNull(cardsToCreate)) {
            return;
        }

        // Iterate through the cards to create and set Booleans
        // for the logic we need to run.  We do this to avoid unnecessary
        // processing for card types we don't need to create.
        cardsToCreate.forEach(function (cardToCreate) {
            if (cardToCreate.indexOf('substage::') != -1) {
                storeSubstages = true;
            }

            if (cardToCreate.indexOf('lienholder::') != -1) {
                storeLienholders = true;
            }

            if (cardToCreate.indexOf('special::noResponseIn30Days') != -1) {
                storeSpecialsNoResponseIn30Days = true;
            }

            if (cardToCreate.indexOf('special::lienholderStateAgencies') != -1) {
                storeSpecialsLienholderStateAgencies = true;
            }

            if (cardToCreate.indexOf('special::lienholder1') != -1
                || cardToCreate.indexOf('special::lienholder2') != -1
                || cardToCreate.indexOf('special::lienholder3') != -1) {
                storeLienholders                             = true;
                storeSpecialsLienholdersByNumLiensDescending = true;
            }
        });

        // Iterate through the liens and populate the Lien Info Map based
        // on the Booleans we set above.
        liens.forEach(function (lien) {
            var lienSubstage = lien.substageLabel;
            var lienholder   = lien.Account__r.Name;

            // If we need to store liens based on their substage,
            // store the lien appropriately.
            if (storeSubstages && !$A.util.isUndefinedOrNull(lienSubstage)) {
                if ($A.util.isUndefinedOrNull(lienInfoMap.substages[lienSubstage])) {
                    lienInfoMap.substages[lienSubstage] = [];
                }

                lienInfoMap.substages[lienSubstage].push(lien);
            }

            // If we need to store liens based on their lienholder name,
            // store the lien appropriately.
            if (storeLienholders && !$A.util.isUndefinedOrNull(lienholder)) {
                if ($A.util.isUndefinedOrNull(lienInfoMap.lienholders[lienholder])) {
                    lienInfoMap.lienholders[lienholder] = [];
                }

                lienInfoMap.lienholders[lienholder].push(lien);
            }

            // If we need to store liens based on the lienholder being
            // a state agency, store the lien appropriately.
            if (storeSpecialsLienholderStateAgencies   &&
                !$A.util.isUndefinedOrNull(lienholder) &&
                lienType   == 'Medicaid'               &&
                lienholder != 'First Recovery Group'   &&
                lienholder != 'HMS') {

                lienInfoMap.specials.lienholderStateAgencies.push(lien);
            }

            // If we need to store liens based on them having no response
            // in 30 days, store the lien appropriately.
            if (storeSpecialsNoResponseIn30Days
                && lienStage == 'Submitted'
                && (lienSubstage == 'First Notice Sent' || lienSubstage == 'Second Notice Sent' || lienSubstage == 'Third Notice Sent')
                && lien.daysSinceLastNoticeSent__c >= 30) {

                lienInfoMap.specials.noResponseIn30Days.push(lien);
            }
        });

        // If we need to store liens based on the lienholder's number
        // of liens in descending order, then sort and store
        // the lienholder appropriately.
        if (storeSpecialsLienholdersByNumLiensDescending) {
            var lienholders     = Object.keys(lienInfoMap.lienholders);
            var lienholdersInfo = [];

            // Build a temporary list of lienholders with their number of liens
            lienholders.forEach(function (lienholder) {
                var lienholderInfo = {
                    'lienholder' : lienholder,
                    'numLiens'   : lienInfoMap.lienholders[lienholder].length
                };

                lienholdersInfo.push(lienholderInfo);
            });

            // Sort the list by:
            //     Number of liens (descending)
            //     Lienholder name (ascending)
            lienholdersInfo.sort(function (a, b) {
                if (a.numLiens < b.numLiens) return 1;
                if (a.numLiens > b.numLiens) return -1;

                // If records have the same number of liens, then
                // sort by the lienholder name
                if (a.lienholder < b.lienholder) return -1;
                if (a.lienholder > b.lienholder) return 1;
                return 0;
            });

            // Store the lienholders in the Lien Info Map
            lienholdersInfo.forEach(function (lienholderInfo) {
                var lienholder = lienholderInfo.lienholder;

                lienInfoMap.specials.lienholdersByNumLiensDescending.push(lienholder);
            });
        }

        // Iterate through the cards and create the card items
        cardsToCreate.forEach(function (cardToCreate) {
            // Split the card data into parameters.  See the comments in
            // the Action_LienInventory component's helper method
            // called "setConfigTileCards" for details on what
            // each parameter means.
            var parameters   = cardToCreate.split('::');
            var parameter1   = parameters[0];
            var parameter2   = parameters[1];
            var parameter3   = parameters[2];
            var cardItem     = {};

            // Create a card for a substage
            if (parameter1 == 'substage') {
                cardItem.category   = 'Substage = ' + parameter2;
                cardItem.label      = parameter2;
                cardItem.liens      = ($A.util.isUndefinedOrNull(lienInfoMap.substages[parameter2]) ? [] : lienInfoMap.substages[parameter2]);
                cardItem.reportLink = ($A.util.isUndefinedOrNull(lienInfoMap.substages[parameter2]) || lienInfoMap.substages[parameter2].length == 0 ? '' : configReportLinks.lienSubstage + 'fv0=' + action.Name + '&fv1=' + lienType + '&fv2=' + parameter2);
            }

            // Create a card for a lienholder
            if (parameter1 == 'lienholder') {
                cardItem.category   = 'Lienholder = ' + parameter2;
                cardItem.label      = parameter2;
                cardItem.liens      = ($A.util.isUndefinedOrNull(lienInfoMap.lienholders[parameter2]) ? [] : lienInfoMap.lienholders[parameter2]);
                cardItem.cardConfig = cardToCreate;
                cardItem.reportLink = '';
            }

            // Create a card for special / to be submitted
            if (parameter1 == 'special' && parameter2 == 'toBeSubmitted') {
                cardItem.category   = '';
                cardItem.label      = 'To Be Submitted';
                cardItem.liens      = (lienStage == 'To Be Submitted' ? liens : []);
                cardItem.cardConfig = cardToCreate;
                cardItem.reportLink = (liens.length == 0 ? '' : configReportLinks.lienStage + 'fv0=' + action.Name + '&fv1=' + lienType + '&fv2=' + lienStage);
            }

            // Create a card for special / submitted
            if (parameter1 == 'special' && parameter2 == 'submitted') {
                cardItem.category   = '';
                cardItem.label      = 'Submitted';
                cardItem.liens      = (lienStage == 'Submitted' ? liens : []);
                cardItem.cardConfig = cardToCreate;
                cardItem.reportLink = '';
            }

            // Create a card for special / no response in 30 days
            if (parameter1 == 'special' && parameter2 == 'noResponseIn30Days') {
                cardItem.category   = 'No Response in 30 Days';
                cardItem.label      = 'No Response in 30 Days';
                cardItem.liens      = ($A.util.isUndefinedOrNull(lienInfoMap.specials.noResponseIn30Days) ? [] : lienInfoMap.specials.noResponseIn30Days);
                cardItem.cardConfig = cardToCreate;
                cardItem.reportLink = ($A.util.isUndefinedOrNull(lienInfoMap.specials.noResponseIn30Days) || lienInfoMap.specials.noResponseIn30Days.length == 0 ? '' : configReportLinks.noResponseIn30Days + 'fv0=' + action.Name + '&fv1=' + lienType);
            }

            // Create a card for special / lienholder state agencies
            if (parameter1 == 'special' && parameter2 == 'lienholderStateAgencies') {
                cardItem.category   = 'State Agencies';
                cardItem.label      = 'State Agencies';
                cardItem.liens      = ($A.util.isUndefinedOrNull(lienInfoMap.specials.lienholderStateAgencies) ? [] : lienInfoMap.specials.lienholderStateAgencies);
                cardItem.cardConfig = cardToCreate;
                cardItem.reportLink = '';
            }

            // Create a card for special / lienholder1
            if (parameter1 == 'special'     &&
                parameter2 == 'lienholder1' &&
                !$A.util.isUndefinedOrNull(lienInfoMap.specials.lienholdersByNumLiensDescending[0])) {

                var lienholder = lienInfoMap.specials.lienholdersByNumLiensDescending[0];

                cardItem.category   = 'Lienholder = ' + lienholder;
                cardItem.label      = lienholder;
                cardItem.liens      = ($A.util.isUndefinedOrNull(lienInfoMap.lienholders[lienholder]) ? [] : lienInfoMap.lienholders[lienholder]);
                cardItem.cardConfig = cardToCreate;
                cardItem.reportLink = '';
            }

            // Create a card for special / lienholder2
            if (parameter1 == 'special'     &&
                parameter2 == 'lienholder2' &&
                !$A.util.isUndefinedOrNull(lienInfoMap.specials.lienholdersByNumLiensDescending[1])) {

                var lienholder = lienInfoMap.specials.lienholdersByNumLiensDescending[1];

                cardItem.category   = 'Lienholder = ' + lienholder;
                cardItem.label      = lienholder;
                cardItem.liens      = ($A.util.isUndefinedOrNull(lienInfoMap.lienholders[lienholder]) ? [] : lienInfoMap.lienholders[lienholder]);
                cardItem.cardConfig = cardToCreate;
                cardItem.reportLink = '';
            }

            // Create a card for special / lienholder3
            if (parameter1 == 'special'     &&
                parameter2 == 'lienholder3' &&
                !$A.util.isUndefinedOrNull(lienInfoMap.specials.lienholdersByNumLiensDescending[2])) {

                var lienholder = lienInfoMap.specials.lienholdersByNumLiensDescending[2];

                cardItem.category   = 'Lienholder = ' + lienholder;
                cardItem.label      = lienholder;
                cardItem.liens      = ($A.util.isUndefinedOrNull(lienInfoMap.lienholders[lienholder]) ? [] : lienInfoMap.lienholders[lienholder]);
                cardItem.cardConfig = cardToCreate;
                cardItem.reportLink = '';
            }

            // Add the new card to the list of card items
            cardItems.push(cardItem);
        });

        // Set the card items attribute that drives the creation of the cards
        component.set('v.cardItems', cardItems);
    },

})