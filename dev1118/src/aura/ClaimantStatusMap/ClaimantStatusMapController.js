/* 
    Created: lmsw - February 2017 for March Release
    Purpose: PD-348 - Display Claimant status for an action

History: 

	Updated: lmsw - December for December Release
	Purpose: PD-1296 - Fix display of total and percents in Action Synopsis
*/
({
	doInit : function(component, event, helper) {
        var key = component.get("v.key");
        var map = component.get("v.map");
        
        component.set("v.totalClaimants", map["TOTAL"]);
        component.set("v.value" , map[key]);  
        component.set("v.keyLabel", component.get("v.key"));     
          
        helper.getCPercent(component);
	}       
})