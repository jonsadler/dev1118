/*
    Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-863 - Creation of the Awards Tab Component on the Claimant Page

History:

    Updated: lmsw - November 2017 for December Release
    Purpose: PD-1268 - Change Fee language for CBF
*/
({
	GetClaimantInfo : function(component,event,helper) {
		var action = component.get("c.getClaimant");
		action.setParams({"cId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				component.set("v.claimant", response.getReturnValue());
			} else {
			console.log('Problem getting Claimant Information, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	}

	// showMoreDetails : function(section) {
	// 	var awardrows = component.get(section);
 //  		if (awardrows) {
 //        	component.set(section, false);
	//     } else {
	//     	component.set(section, true);
	//     }
	// }

	// showMoreAwardCosts : function(component,expandAll) {

	//     if(expandAll) {
	//     	component.set('v.awardCosts', false);
	//     } else {
	//     	component.set('v.awardCosts', true);
	//     }
	// },
	// showMoreLienFees : function(component, expandAll) {

	//     if(expandAll) {
	//     	component.set('v.lienFees', false);
	//     } else {
	//     	component.set('v.lienFees', true);
	//     }
	// },
})