/*
    Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-863 - Creation of the Awards Tab Component on the Claimant Page

History:

    Updated: lmsw - November 2017 for December Release
    Purpose: PD-1268 - Change Fee language for CBF
*/
({
	doInit : function(component, event, helper) {
        helper.GetClaimantInfo(component);
		var action1 = component.get("c.getAllFees");
        action1.setParams({"cId": component.get("v.recordId")});
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var fMap = response.getReturnValue(); 
                var temp = [];
                var fees = [];
                var feeMap = [];
                var feesTotal = 0;
                for (var plif in fMap) { // plif is a type/subtype combination
	                var middle = [];
                    temp = fMap[plif]; // temp is a map of Award IDs to Amounts
                	var feeTotal = 0;
                    for (var plig in temp) { // plig is an awardId
	                	vipl = temp[plig]; // vipl is an amount
                    	feeTotal += vipl;
	                	middle.push({
	                		key: plig,
	                		value: vipl
	                	});
	                }
					feesTotal += feeTotal;
                    fees.push({
	                	key: plif,
	                	value: middle
	                });
                    feeMap.push({
                        key: plif,
                        value: feeTotal                        
                    })
                }
                component.set("v.fees", fees);
                component.set("v.feesTotal", feesTotal);
                component.set("v.feeMap", feeMap);
            } else {
                console.log('Problem getting fees, response state: ' + state);
            }
        });
        $A.enqueueAction(action1);
        var action1a = component.get("c.getFeeTypes");
        action1a.setParams({"cId": component.get("v.recordId")});
        action1a.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var feeTypes = response.getReturnValue();
                var result = [];
                var temp;
                for (var plif in feeTypes) { // plif is a type/subtype pair
                    temp = feeTypes[plif]; // temp is plif with spaces instead of underscores
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.feeEnum", result);
            } else {
                console.log('Problem getting feeTypes, response state: ' + state);
            }
        });
        $A.enqueueAction(action1a);
        var action1b = component.get("c.getACDs");
        action1b.setParams({"cId": component.get("v.recordId")});
        action1b.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var temp = response.getReturnValue();
                var mapping;
                var acdTotal = 0;
                var acdsTotal = 0;
                var discTotal = 0;
                var acds = [];
                var result = [];
                var mapper = [];
                var totsMap = [];
                for (var acdType in temp) {
                    var first = true;
                    var mapitem = acdType.split('_');
                	var wamp = temp[acdType]; // wamp is a list of award Id / amount pairs
                    discTotal = 0;
                    acdTotal = 0;
	                var middle = [];
                    for (var blub in wamp) {
                        var wonk = wamp[blub];
                        middle.push({
	                        key: blub, // action Id
	                        value: wonk // amount
	                    });
                        if (first) {
                            mapper.push({
                                key: acdType,
                                value: mapitem.join(' ')
                            });
                            first = false;
                        }
                        if (acdType == 'Fee_Reduction') {
                            discTotal += wonk;
                        } else {
                           	acdTotal += wonk;
                           	acdsTotal += wonk;
                        }
                    }
                    totsMap.push({
                        key: acdType,
                        value: acdTotal
                    });
                    result.push({
                        key: acdType,
                        value: middle
                    });
                }
                component.set("v.totsMap", totsMap);
                component.set("v.acdTotal", acdTotal);
                component.set("v.acdsTotal", acdsTotal);
                component.set("v.acds", result);
                component.set("v.discTotal",discTotal);
                component.set("v.acdMap", mapper);
            } else {
                console.log('Problem getting acds, response state: ' + state);
            }
        });
        $A.enqueueAction(action1b);
        var action2 = component.get("c.getAwards");
        action2.setParams({"cId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.awards", response.getReturnValue());
                var awards = response.getReturnValue();
                var size = awards.length + 2;
                if (size > 2)
                    size += 1;
                var tdWidth = 1/size;
                var awTotal = 0;
                var feeTotal = 0;
                var hbTotal = 0;
                var discTotal = 0;
                var finalTotal = 0;
                var totalTotal = 0;
                var rTotal = 0;
                var result = [];
                var showRemains = false;
                for (var pleef in awards) {
                	var award = awards[pleef]; 
                	if (award.Amount__c != null);
	                    awTotal += award.Amount__c;
                	if (award.Fees_With_Discounts__c != null);
                        feeTotal += award.Fees_With_Discounts__c;
                    if (award.Amount_Final_Payable__c != null)
                        finalTotal += award.Amount_Final_Payable__c;
                	if (award.Amount_Applied__c != null);
                        hbTotal += award.Amount_Applied__c;
                	if (award.Amount_Remaining__c != null);
                        totalTotal += award.Amount_Remaining__c;
                    if (award.Claimant__r.Status__c == 'Cleared' || award.Claimant__r.Status__c == 'Released_to_a_Holdback')
                    	showRemains = true;
                	if (award.Amount_Disbursed__c != null);
	                    rTotal += award.Amount_Disbursed__c;
                    if (award.Distribution_Status__c == 'Not_Ready_to_be_Cleared')
                        currentBalance = false;
                }
                totalTotal -= feeTotal; 
                component.set("v.awardTotal", awTotal);
                component.set("v.feeTotal", feeTotal);
                component.set("v.totalTotal", totalTotal);
                component.set("v.tdWidth", tdWidth); 
                component.set("v.showRemains", showRemains); 
                component.set("v.rTotal", rTotal);
                component.set("v.currentBalance", currentBalance); 
            } else {
                console.log('Problem getting awards, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);
        var action3 = component.get("c.getLiensByAwardByType");
        action3.setParams({"cId": component.get("v.recordId")});
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var stage = response.getReturnValue() // map<type,map<awardId,map<recordType,amount>>>
                for (var type in stage) {
                	if (type == 'final') {
	                    temp = stage[type]; // map<recordType,map<awardId,amount>>
	                    var result = [];
	                    var finalPayable = 0.00;
	                    for (var rt in temp) {
	                    	var finalTotal = 0.00;
	                    	var lower = [];
		                	var pairs = temp[rt]; // map<awardId,amount>
	                    	for (var awardId in pairs) {
		                    	var amt = pairs[awardId];
		                    	finalTotal += Number(amt);
			                	lower.push({
			                		key: awardId,
			                		value: amt
			                	});
			                }
			                finalPayable += Number(finalTotal);
console.log('finalTotal = ' + finalTotal);
console.log('finalPayable = ' + finalPayable);
		                	lower.push({
		                		key: 'all',
		                		value: finalTotal
		                	});
			                result.push({
			                	key: rt,
			                	value: lower
			                });
		                }
                		component.set("v.lienMap", result);
                		component.set("v.finalTotal", finalPayable);
                	}
                	else {
	                    temp = stage[type]; // map<recordType,map<awardId,amount>>
	                    var result = [];
	                    var hbPayable = 0.00;
	                    for (var rt in temp) {
	                    	var hbTotal = 0.00;
	                    	var lower = [];
		                	var pairs = temp[rt]; // map<awardId,amount>
	                    	for (var awardId in pairs) {
		                    	var amt = pairs[awardId];
		                    	hbTotal += Number(amt);
			                	lower.push({
			                		key: awardId,
			                		value: amt
			                	});
			                }
			                hbPayable += Number(hbTotal);
console.log('hbTotal = ' + hbTotal);
console.log('hbPayable = ' + hbPayable);
		                	lower.push({
		                		key: 'all',
		                		value: hbTotal
		                	});
			                result.push({
			                	key: rt,
			                	value: lower
			                });
		                }
                		component.set("v.hbMap", result);
                		component.set("v.hbTotal", hbPayable);
                	}
                }
            } else {
                console.log('Problem getting awards, response state: ' + state);
            }
        });
        $A.enqueueAction(action3);
        var action4 = component.get("c.getReleases");
        action4.setParams({"cId": component.get("v.recordId")});
        action4.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var releases = response.getReturnValue();
                var result = [];
                var temp;
                var rTotal = 0;
                for (var awardId in releases) {
                    amt = releases[awardId];
                    rTotal += amt;
                    result.push({
                        key: awardId,
                        value: amt
                    });
                }
                component.set("v.releaseMap", result);
                component.set("v.rTotal", rTotal);
            } else {
                console.log('Problem getting releases, response state: ' + state);
            }
        });
        $A.enqueueAction(action4);
    },
    showMoreAwardCosts : function(component, event, helper) {
        var awardCosts = component.get('v.awardCosts');
        component.set('v.awardCosts', !awardCosts);
    },
    showMoreLienFees : function(component, event, helper) {
        var lienFees = component.get('v.lienFees');
        if (lienFees) {
            component.set('v.lienFees', false);
        } else {
            component.set('v.lienFees', true);
        }
    },
    showMorelienAmounts : function(component, event, helper) {
        var lienAmount = component.get('v.lienAmount');
        if (lienAmount) {
            component.set('v.lienAmount', false);
        } else {
            component.set('v.lienAmount', true);
        }
    },
    showMoreHoldbackAmounts : function(component, event, helper) {
        var holdbackAmount = component.get('v.holdbackAmount');
        if (holdbackAmount) {
            component.set('v.holdbackAmount', false);
        } else {
            component.set('v.holdbackAmount', true);
        }
    },
    expandAll : function(component, event, helper) {
        var expandAll = component.get('v.expand');
        if (expandAll) {
            component.set('v.expand', false);
            component.set('v.awardCosts', false);
            component.set('v.lienFees', false);
            component.set('v.lienAmount', false);
            component.set('v.holdbackAmount', false);
        } else {
            component.set('v.expand', true);
            component.set('v.awardCosts', true);
            component.set('v.lienFees', true);
            component.set('v.lienAmount', true);
            component.set('v.holdbackAmount', true);
        }
    },
// Start PD-1117
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
   },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){    
       component.set("v.Spinner", false);
    }
// End PD-1117
})