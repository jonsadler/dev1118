/*
	Created: lmsw - June 2017
    Purpose: PD-839 - Matter Synopsis for Portal
*/
({
    doInit : function(component, event, helper) {
        component.set('v.Spinner', true);
        // Action Lien Summary
        var action = component.get("c.getLienMetrics");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	var smMap = response.getReturnValue();
            	var result = [];
            	var temp = [];
            	for (var lienType in smMap) {
            		temp = smMap[lienType]; // temp is a list of values keyed by lientype
            		result.push({
            			key: lienType,
            			value:temp
            		});
            	}
	            component.set("v.lienInv", result);
                component.set('v.lienInvLoaded', true);
            } else {
                console.log('Problem getting lien map, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        helper.createChart(component);
        helper.createlawfirmChart(component);
        
    },

     // Start PD-1218
    handleRecordsLoaded : function (component, event, helper) {
        // var lienMapPendingLoaded        = component.get('v.lienMapPendingLoaded');
        var lienInvLoaded                = component.get('v.lienInvLoaded');
        var doughnutChartLoaded           = component.get('v.doughnutChartLoaded');

        // If all records have been loaded, don't show spinner. 
        if (lienInvLoaded &&
            doughnutChartLoaded
            ) {
                component.set("v.Spinner", false);  
            } else {
                component.set("v.Spinner", true);
        }      
    },
// End PD-1218
})