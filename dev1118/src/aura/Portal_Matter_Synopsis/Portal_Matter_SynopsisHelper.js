/*
	Created: lmsw - June 2017
    Purpose: PD-839 - Matter Synopsis for Portal

History:

    Updated: lmsw - July 2017
    Purpose: PD-839 - Modify Chart layout

    Updated: Marc Dysart - July 2017
    Purpose: PD-839 - Modify Chart layout

    Updated: Marc Dysart - Oct 2017
    Purpose: PD-977 - Portal - Donut Graph remove hover

    Updated: Marc Dysart - Oct 2017
    Purpose: PD-974 - Portal Matter page - graph title
*/
({
   createChart : function (component){
        var matter= component.get("v.currentActionName");
        console.log('Action Name: '+matter);
        var action = component.get("c.getReportJSON");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var jsonRetVal = JSON.parse(response.getReturnValue()); 
                component.set("v.total",jsonRetVal.chartTotal);
                
                var chartTitle = 'Claimants in Matter';
                var img = document.getElementById("stripe");  
                var c = document.getElementById("chart");
                var ctx = c.getContext("2d");
                var fillPattern = ctx.createPattern(img, 'repeat');
  
                
                var config = {
                    type: 'doughnut',
                    data: {
                        labels: jsonRetVal.chartLabels,
                        datasets: [{
                            data: jsonRetVal.chartData,
                            backgroundColor: ["#F7B52D", fillPattern, "#60336f"]
                        }]
                    },
                    options: {
                        legend: {
                            display: true,
                            position:'bottom',
                            onClick: function(event, legendItem) {},
                            labels: {
                                fontSize: 14,
                                boxWidth: 18,
                                padding: 10 }                        
                        },
                        title: {
                            display: true,
                            fontSize: 18,
                            fontWeight: "lighter",
                            fontColor: '#60336f',
                            padding: 10,
                            text: 'All Claimants in Matter'  /*PD-974 */
                        },
                        tooltips: {
                            enabled: false, /*PD-977*/
                            position: 'nearest',
                            backgroundColor: 'rgb(211,211,211,.5)',
                            bodyFontColor: 'rgb(0,0,0)',
                            bodyFontSize: 18,
                            displayColors: false,
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                }
                            }
                        }                   
                    }
                };
/*
                Chart.pluginService.register({
                    beforeRender: function (chart) {
                        if (chart.config.options.showAllTooltips) {
                            // create an array of tooltips
                            // we can't use the chart tooltip because there is only one tooltip per chart
                            chart.pluginTooltips = [];
                            chart.config.data.datasets.forEach(function (dataset, i) {
                                chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                                    chart.pluginTooltips.push(new Chart.Tooltip({
                                        _chart: chart.chart,
                                        _chartInstance: chart,
                                        _data: chart.data,
                                        _options: chart.options.tooltips,
                                        _active: [sector]
                                    }, chart));
                                });
                            });

                            // turn off normal tooltips
                            chart.options.tooltips.enabled = false;
                        }
                    },
                    afterDraw: function (chart, easing) {
                        if (chart.config.options.showAllTooltips) {
                            // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                            if (!chart.allTooltipsOnce) {
                                if (easing !== 1)
                                    return;
                                chart.allTooltipsOnce = true;
                            }

                            // turn on tooltips
                            chart.options.tooltips.enabled = true;
                            Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                                tooltip.initialize();
                                tooltip.update();
                            });
                            chart.options.tooltips.enabled = false;
                        }
                    }
                })
*/
                
                var doughnutChart = new Chart(ctx, config);
                component.set('v.doughnutChartLoaded', true);  /*PD-1218*/
               
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message on createReport: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
   },
   createlawfirmChart : function (component){
        var action = component.get("c.getLawfirmReportJSON");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var jsonRetVal = JSON.parse(response.getReturnValue()); 
                component.set("v.lawfirmtotal",jsonRetVal.chartTotal);
                
                var img = document.getElementById("stripe");  
                var c = document.getElementById("lawfirmchart");
                var ctx = c.getContext("2d");
                var fillPattern = ctx.createPattern(img, 'repeat');
  
                
                var config = {
                    type: 'doughnut',
                    data: {
                        labels: jsonRetVal.chartLabels,
                        datasets: [{
                            data: jsonRetVal.chartData,
                            backgroundColor: ["#F7B52D", fillPattern, "#60336f"]
                        }]
                    },
                    options: {
                        legend: {
                            display: true,
                            position:'bottom',
                            onClick: function(event, legendItem) {},
                            labels: {
                                fontSize: 14,
                                boxWidth: 18,
                                padding: 10 }                        
                        },
                        title: {
                            display: true,
                            fontSize: 18,
                            fontColor: '#60336f',
                            padding: 10,
                            text: 'Claimants of Law Firm'
                        },
                        tooltips: {
                            enabled: false,  /*PD-977*/
                            position: 'nearest',
                            backgroundColor: 'rgb(211,211,211,.5)',
                            bodyFontColor: 'rgb(0,0,0)',
                            bodyFontSize: 18,
                            displayColors: false,
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                }
                            }
                        }                   
                    }
                };
       
                var doughnutChart = new Chart(ctx, config); 
                component.set('v.lawfirmDoughnutChartLoaded', true);  /*PD-1218*/
               
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message on createReport: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
   }
})