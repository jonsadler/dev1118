/*
    Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-844 - Creation of Activity Component on Claimant Page 
*/
({
    doInit : function(component, event, helper) {
        var action3 = component.get("c.getClaimantActivities");
        action3.setParams({"cId": component.get("v.recordId")});
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.activities", response.getReturnValue());
            } else {
                console.log('Problem getting Activity Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action3);
        // Show 'More gActivities' button and Hide 'Less Activities' Button
        component.set('v.activityBtn', true);

    },

    showMoreActivities : function(component, event, helper) {
        var activites = document.getElementsByClassName("single-activity");
  
        for(var i = 0; i < activites.length; ++i){
            if (i > 3) {
            activites.item(i).classList.remove("hideActivity")
            }
        }
        component.set('v.activityBtn', false);
    },

    showLessActivities : function(component, event, helper) {
        var activites = document.getElementsByClassName("single-activity");

        for(var i = 0; i < activites.length; ++i){
            if (i > 3) {
            activites.item(i).classList.add("hideActivity")
            }
        }
        component.set('v.activityBtn', true);
    }
})