({
    doInit : function(component, event, helper) {
   
        helper.getFeeList(component);

        var action4 = component.get("c.getFeeSubtype");
        action4.setParams({"cId": component.get("v.recordId")});
        action4.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var sMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var stif in sMap) {
                    temp = sMap[stif];
                    result.push({
                        label: stif,
                        value: temp
                    });
                }
                component.set("v.feeSubtypeList", result);
            } else {
                console.log('Problem getting Fee Subtype picklist, response state: ' + state);
            }
        });
        $A.enqueueAction(action4);
        
		helper.CreateItems(component,event);
    },

	handleMenuSelect: function(cmp, event, helper) {
	    var selected = event.getParam("value");
	    if (selected == 'edit')
	    	helper.EditLRF(cmp, event, helper);
	    else if (selected == 'delete')
	    	helper.DeleteLRF(cmp, event, helper);
        $A.get("e.force:refreshView").fire();
	},

	openTabwithSubtab: function(component, event, helper) {
		var newId = event.currentTarget.getAttribute("id");
		var url = '/'+newId;
		var urlEvent = $A.get("e.force:navigateToURL");
		        urlEvent.setParams({
		            "url": url
		        });
		        urlEvent.fire();
    },

    handleForceShowToast : function(component, event, helper) {
       var eventMessage = event.getParam('message');
       if (!$A.util.isUndefinedOrNull(eventMessage) && eventMessage.includes('Lien Resolution Fee')) {
           helper.getFeeList(component);
       }
    },
})