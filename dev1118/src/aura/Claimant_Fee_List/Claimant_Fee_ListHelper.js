// Created: Marc Dysart - Sept 2017 for Sept Release
    // Purpose: PD-1024 - Fee Related List on Claimant Page
({
    EditLRF : function(component, event, helper) {
        var acdID = event.getSource().get("v.name");
        console.log('acdID: ' + acdID);
        var ere = $A.get("e.force:editRecord");
        ere.setParams({"recordId": acdID});
        ere.fire();
        $A.get("e.force:refreshView").fire();
    },

    DeleteLRF : function(component, event, helper) {
        var acdId = event.getSource().get("v.name");
        var action2 = component.get("c.DeleteACD");
        action2.setParams({"acdId": acdId});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	$A.get("e.force:refreshView").fire();
            } else {
                console.log('Problem deleting ACD, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);
    },
    
    CreateItems: function (cmp, event) {
        var items = [
            { label: "Edit", value: "edit" },
            // { label: "Delete", value: "delete" }
        ];
        cmp.set("v.actions", items);
    },

    getFeeList : function (component) {
      var action3 = component.get("c.getAllFeesForClaimant");
        action3.setParams({"cId": component.get("v.recordId")});
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
         
                var records = response.getReturnValue();
                var feeTotal = 0;
                var liens = [];
                for (var i=0; i<records.length; i++) {
                    var ft = records[i].Fee_Type__c;

                    var fs = records[i].Fee_Subtype__c;

                    // Determine how many liens have fees
                    var k=0;                    
                    var lien = records[i].Lien__c;
                    var lienslength= liens.length;
                    if (liens.length>0) {
                        for(var j=0; j<liens.length; j++) {
                            if(lien!=liens[j].Lien__c) {
                                liens[k] =records[i];
                                k++;
                            }
                        }
                    } else {
                        liens[k] = records[i];
                    }

                    // Determine the total fee amount
                    feeTotal += records[i].Fee_Amount__c;

                    if (ft.includes("_")||fs.includes("_")){
                        records[i].Fee_Type__c=ft.replace(/_/g," ");
                        records[i].Fee_Subtype__c=fs.replace(/_/g," ");
                    }
                } 
                component.set("v.liens", liens);
                component.set("v.feesTotal", feeTotal);
                component.set("v.newfeesTotal", feeTotal);
                component.set("v.fees", records);

            } else {
                console.log('Problem getting fees, response state: ' + state);
            }
        });
        $A.enqueueAction(action3);
    },
})