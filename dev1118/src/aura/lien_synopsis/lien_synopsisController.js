/*
History:

    Updated: lmsw - February 2017 for March Release
    Purpose: PD-393 Add synopsis to title.
             PRODSUPT-9

    Updated: cps - August 2017
	Purpose: PD-393 - Handle force:showToast event to allow for refreshing when
	         Lien Negotiated Amount records are inserted or updated.
	         NOTE: Moved getLienAmtList code from the doInit controller method
	         into its own helper method so it could be called from either
	         doInit or handleForceShowToast.
*/

({
    doInit : function(component, event, helper) {
        // PD-393 start (move getLienAmtList code from controller to helper method)
        helper.getLienAmtList(component);
        // PD-393 end

        helper.getLienStageLabels(component);
        helper.getLienSubstageLabels(component);
    },
   // Start PD-905
   openTabwithSubtab: function(component, event, helper) {
    var newId = event.currentTarget.getAttribute("id");
    var url = '/'+newId;
    var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                "url": url
                });
                urlEvent.fire();
   },
// PD-905

// PD-393 start
    handleForceShowToast : function(component, event, helper) {
        var eventMessage = event.getParam('message');

        if (!$A.util.isUndefinedOrNull(eventMessage) && eventMessage.includes('Lien Negotiated Amount')) {
            helper.getLienAmtList(component);
        }
    },
// PD-393 end
})