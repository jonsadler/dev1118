/*  Created: lmsw - January 2017 first draft
    Purpose: PD-168 - Title Section on the Claimant Detail Page

History:

	Updated: lmsw - February 2017 March Release
	Purpose: PD-168 - Refresh detail record when change the Claimant status

	Updated: lmsw - February 2017 for March Release
	Purpose: PRODSUPT-14 - remove "Send Email" button

	Updated: Marc Dysart - March 2017 for March Release
    Purpose: PD-443 - Breadcrumbs in Lien & Claimant pages

	Updated: RAP - June 2017 for June Release
	Purpose: techDebt - Claimant Status not showing correct value

	Updated: lmsw - Sept 2017 for Sept Release
	Purpose: PD-996  - Add notice to Title, handle force:showToast event 
             to allow refreshing.
 */
({
	doInit : function(component, event, helper) {
// start techDebt
/*		// Set up Claimant Status dropdown
		var action = component.get("c.getClaimantStatusPicklist");
		var inputsel = component.find("ClaimantStatusSelect");
		var opts=[];
		
		// Create Claimant Status Button options		
		action.setCallback(this, function(a) {
			for(var i=0; i<a.getReturnValue().length; i++) {
				opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
			}
			inputsel.set("v.options", opts);
		});
		$A.enqueueAction(action);
*/		
// end techDebt
		helper.getClaimantRecord(component);
		//helper.getNACount(component);
// start PD-443
		var action2 = component.get("c.getAction2");
        action2.setParams({"cId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.action", response.getReturnValue());
            } else {
                console.log('Problem getting Action, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);
        helper.getNACount(component);  //PD-996

// end PD-443
	},
// start PD-1308
    buttonMenuSelected : function (component, event, helper) {
        var selection           = event.getParam('value');
        var buttonMenuComponent = event.getSource();
        var needsAttentionId    = buttonMenuComponent.get('v.name');

        if (selection == 'edit') {
            var editRecordEvent = $A.get('e.force:editRecord');

            editRecordEvent.setParams({
                recordId : needsAttentionId
            });

            editRecordEvent.fire();
        }
        if (selection == 'drop') {
            console.log('selection = drop');
            component.set('v.activeModal1', 'dropConfirmation');
        }
        if (selection == 'move') {
            console.log('selection = move');
            component.set('v.activeModal2', 'moveConfirmation');
        }
    },
    DropClaimantCancelClicked : function (component, event, helper) {
        component.set('v.activeModal1', null);
    },
    MoveClaimantCancelClicked : function (component, event, helper) {
        component.set('v.activeModal2', null);
    },
    DropClaimantClicked : function (component, event, helper) {
        component.set('v.activeModal1', null);
		var actiona = component.get("c.DropClaimantFromAction");
        actiona.setParams({"cId": component.get("v.recordId")});
        actiona.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                if (response.getReturnValue() === 'SUCCESS') {
                    helper.showDropClaimantSuccessToast(component, event, helper);
                }
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (DropClaimantFromAction) INCOMPLETE');
                helper.showDropClaimantErrorToast(component, event, helper);
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (DropClaimantFromAction) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (DropClaimantFromAction) ERROR; unknown error');
                }
                helper.showDropClaimantErrorToast(component, event, helper);
            }
			$A.get("e.force:refreshView").fire();
        });
        $A.enqueueAction(actiona);
    },
    MoveClaimantClicked : function (component, event, helper) {
        component.set('v.activeModal2', null);
        console.log('Return button clicked');
		var actionb = component.get("c.ReturnClaimantToAction");
        actionb.setParams({"cId": component.get("v.recordId")});
        actionb.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                if (response.getReturnValue() === 'SUCCESS') {
                    helper.showMoveClaimantSuccessToast(component, event, helper);
                }
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (MoveClaimantToAction) INCOMPLETE');
                helper.showMoveClaimantErrorToast(component, event, helper);
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (MoveClaimantToAction) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (MoveClaimantToAction) ERROR; unknown error');
                }
                helper.showMoveClaimantErrorToast(component, event, helper);
            }
			$A.get("e.force:refreshView").fire();
        });
        $A.enqueueAction(actionb);
    },
	onStatusChange: function(component, event, helper) {
		// Change status on record depending on selection in picklist
		var claimantId = component.get("v.recordId");
		var selected = component.find("ClaimantStatusSelect").get("v.value");
		
		// change Claimant_status__c value = selected on record
		var action = component.get("c.updateClaimant");
		action.setParams({
			"cId" : claimantId,
			"status"  : selected 
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				component.set("v.claimant", response.getReturnValue());
				// Determine color of Status circle during selection
				helper.statusCircle(component);
			} else {
				console.log('Problem getting claimant, response state: ' + state);
			}
// Add refresh
			$A.get("e.force:refreshView").fire();
		});
		$A.enqueueAction(action);
	},
// start PRODSUPT-14
/*	sendEmail : function(component) {
		var lawFirmEmail = component.get("v.claimant.Law_Firm__r.Email__c");
		var claimantName = component.get("v.claimant.Name");
		var link = "mailto:"+lawFirmEmail+"?Subject=About%20"+claimantName;
		window.location.href = link;
	}, */
// end PRODSUPT-14
	editClaimant : function(component, event, helper) {
		var editRecordEvent = $A.get("e.force:editRecord");
		editRecordEvent.setParams({"recordId": component.get("v.claimant.Id")
		});
		editRecordEvent.fire();
	},
// Start PD-905
   openTabwithSubtab: function(component, event, helper) {
    var newId = event.currentTarget.getAttribute("id");
	    var url = '/'+newId;
	    var urlEvent = $A.get("e.force:navigateToURL");
	                urlEvent.setParams({
	                "url": url
	                });
	                urlEvent.fire();
},
// PD-905
// start PD-996
    handleForceShowToast : function(component, event, helper) {
        var eventMessage = event.getParam('message');

        if (!$A.util.isUndefinedOrNull(eventMessage) && eventMessage.includes('Needs Attention')) {
            helper.getNACount(component);
        }
    },
// end PD-996
})