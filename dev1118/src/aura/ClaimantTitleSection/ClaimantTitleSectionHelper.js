/*  Created: lmsw - January 2017 first draft
    Purpose: PD-168 - Title Section on the Claimant Detail Page

History:

	Updated: lmsw - February 2017 for March Release
	Purpose: PRODSUPT-1 - Modified for change of claimant status picklist

	Updated: lmsw - February 2017 for March Release
	Purpose: PD-168 - Modified for changes in .css, removed console.logs

	Updated: lmsw - February 2017 for March Release
	Purpose: PRODSUPT-1 Change Claimant Status: 'Partially Cleared'=> 'Released to a Holdback'

	Updated: lmsw - February 2017 for March Release
	Purpose: PRODSUPT-14 - remove "Send Email" button

	Updated: lmsw - Sept 2017 for Sept Release
	Purpose: PD-996  - Add notice to Title,  handle force:showToast event 
             to allow refreshing.

    Updated: lmsw - Septempber 2017 for September Release
    Purpose: PD-996 - Modify NA query

 */
({
	getClaimantRecord : function(component) {
		var action = component.get("c.getClaimant");
		action.setParams({"cId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				component.set("v.claimant", response.getReturnValue());
//				this.enableEmailButton(component);
				this.statusCircle(component);
			} else {
				console.log('Problem getting claimant, response state: ' + state);
			}
		});
		$A.enqueueAction(action);
	},
// Peer code review change - Modified logic for color of circle	
	statusCircle : function(component) {
		// Determine color of circle
		var selected = component.find("ClaimantStatusSelect").get("v.value");
// start PRODSUPT-1 PD-168
		selected = selected.replace(/ /g,"_");
		if(selected == "Cleared") {
			component.set("v.circleStyle", "defaultCircle greenCircle");
		} else if(selected == "Released_to_a_Holdback") {
			component.set("v.circleStyle", "defaultCircle yellowCircle");
		} else {
			component.set("v.circleStyle", "defaultCircle redCircle");
		} 
	},
	getNACount : function(component) {
		var action = component.get("c.getNACount");
		action.setParams({"recordId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state === "SUCCESS") {
				component.set("v.naCount", response.getReturnValue());
			} else {
				console.log("Problem getting Claimant Needs Attention count, response state: " + state);
			}
		});
		$A.enqueueAction(action);

	},
// start PD-1308
    showDropClaimantErrorToast : function (component, helper) {
        var showToastEvent = $A.get('e.force:showToast');
        showToastEvent.setParams({
            title   : 'ERROR',
            message : 'Dropping Claimant was not successful.  Please contact a systems administrator with this information including the Claimant Name.',
            type    : 'error',
            mode    : 'sticky'
        });
        showToastEvent.fire();
    },
    showDropClaimantSuccessToast : function (component, helper) {
        var showToastEvent = $A.get('e.force:showToast');
        showToastEvent.setParams({
            title   : 'SUCCESS',
            message : 'Claimant successfully dropped from action.',
            type    : 'success',
            duration: '100',
            mode    : 'dismissible'
        });
        showToastEvent.fire();
    },
    showMoveClaimantErrorToast : function (component, helper) {
        var showToastEvent = $A.get('e.force:showToast');
        showToastEvent.setParams({
            title   : 'ERROR',
            message : 'Returning Claimant to Action was not successful.  Please contact a systems administrator with this information including the Claimant Name.',
            type    : 'error',
            mode    : 'sticky'
        });
        showToastEvent.fire();
    },
    showMoveClaimantSuccessToast : function (component, helper) {
        var showToastEvent = $A.get('e.force:showToast');
        showToastEvent.setParams({
            title   : 'SUCCESS',
            message : 'Claimant successfully returned to action.',
            type    : 'success',
            duration: '100',
            mode    : 'dismissible'
        });
        showToastEvent.fire();
    }
// end PD-1308
// end PRODSUPT-1 PD-168
// start PRODSUPT-14
/*	enableEmailButton : function(component) {
		var lawFirmEmail = component.get("v.claimant.Law_Firm__r.Email__c");
		if(lawFirmEmail) { component.set("v.hasEmail","true"); }
			else{ component.set("v.hasEmail", "false");	}
	} */
// end PRODSUPT-14	
})