/* 
    Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-814 - Creation of Portal Header Component

History:

    Updated: lmsw - August 2017
    Purpose: PD-922 - Limit portal users without using permissions.

    Updated: MJD Dec 2017 for January Release
    Purpose: PD-1285 - Modify CSS to accommodate MS Edge / Internet Explorer

    Updated: lmsw - January 2018 for January Release
    Purpose: PD-1320 - Add Matter Report Links

*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLFActions");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.actions", response.getReturnValue());
                helper.GetPortalReports(component);     //PD-1320
            } else {
                console.log('Problem getting Actions, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);

        var action2 = component.get("c.getCurrentUser");
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.currentUser", response.getReturnValue());
                var temp = component.get("v.currentUser");
                var currentAction = temp[0].CurrentAction__c;
                
                component.set("v.currentAction", currentAction);
            } else {
                console.log('Problem getting Actions, response state: ' + state);
            }
        });     
        $A.enqueueAction(action2);

        // if (window.location.pathname!= '/s/') {
        //     component.set("v.showMatterList",false);
        // } else {
        //     component.set("v.showMatterList",true);
        // }
        helper.checkForInternetExplorer(component);  // Start PD-1285
    },
    
    handleComponentEvent : function(cmp, event) {
        var matterID = event.getParam("matterID");
        cmp.set("v.chosenMatter", matterID);
        cmp.set("v.currentAction", matterID);
        var matterName = event.getParam("matterName");
        cmp.set("v.chosenMatterName", matterName);
    },

    menu-click : function (component,event) {
        var matterlist = document.getElementById("matter-list");
        if (matterlist) {

            matterlist.classList.add("hide-matter-list");
        }
    }

})