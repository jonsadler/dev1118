/*  Updated: MJD Dec 2017 for January Release
    Purpose: PD-1285 - Modify CSS to accommodate MS Edge / Internet Explorer

    Updated: lmsw - January 2018 for January Release
    Purpose: PD-1320 - Add Matter Report links
*/

({
	  // Start PD-1285
    checkForInternetExplorer:  function(component,event,helper){
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer
            // I got the idea for the 3 lines above from this website:  https://stackoverflow.com/questions/19999388/check-if-user-is-using-ie-with-jquery */
            // I'm not clear what '/Trident.*rv\'' does //
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                 "title": "You are using the Internet Explorer browser which is not supported!",
               "message": "We recommend using Chrome for a better experience.",
                  "type": "error",
                  "mode": 'sticky',
             "duration" : 7000
            });

            toastEvent.fire();
        }  
    },
     // End PD-1285
    // start PD-1320
    GetPortalReports : function(component, event, helper){
      var action = component.get("c.getPortalReportMap");
      action.setParams({"actions": component.get("v.actions")});
      action.setCallback(this, function(response) {
        var state = response.getState();
        if(component.isValid() && state === "SUCCESS") {
          var reportMap = response.getReturnValue();
          var result = [];
          var reportId = [];
          for(var aName in reportMap) {
            reportId = reportMap[aName];
            result.push({
              key: aName,
              value: reportId
            });
          }
          component.set("v.portalReports", result);
        } else {
          console.log('Problem getting Porta Report Map, response state: '+ state);
        }
      });
      $A.enqueueAction(action);
    }
    // end PD-1320
})