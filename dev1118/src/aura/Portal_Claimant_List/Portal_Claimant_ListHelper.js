({
    // start PD-1277 (move init code to separate helper methods)
    getAction : function (component, helper) {
        var action = component.get('c.getAction');

        action.setParams({'aId': component.get('v.recordId')});

        action.setCallback(this, function getActionCallback(response) {
            var state = response.getState();

            if (component.isValid() && state === 'SUCCESS') {
                component.set('v.action', response.getReturnValue());
                component.set('v.actionLoaded', true);
            } else {
                console.log('ERROR: Problem getting Action, response state: ' + state);
            }
        });

        $A.enqueueAction(action);
    },

    getLFClaimants : function (component, helper) {
        var action = component.get('c.getLFClaimants');

        action.setParams({'aId': component.get('v.recordId')});

        action.setCallback(this, function getLFClaimantsCallback(response) {
            var state = response.getState();

            if(component.isValid() && state === 'SUCCESS') {
                //component.set('v.claimants', response.getReturnValue()); <!-- PD-1277 (comment out unneeded assignment) -->
                component.set('v.allClaimants', response.getReturnValue());
                component.set('v.sortColumn', 'Needs Attention');
                component.set('v.sortAsc', false);

                // start PD-1277 (comment out unneeded code)
                /* records = component.get('v.claimants');  //Is there any needs attention?
                var naCount = 0;
                for (var i=0; i<records.length; i++){
                    var c=records[i];
                    if (c.Needs_Attention_Total__c > 0) {
                        naCount++;  // There are needs attention
                    }
                }
                if(naCount>0){
                    helper.sortBy(component, 'Needs_Attention_Total__c', 'Last_Name__c', 'Needs Attention');
                } else {
                    helper.sortBy(component, 'Last_Name__c', 'Status__c', 'Last Name');
                } */
                // end PD-1277

                component.set('v.lfClaimantsLoaded', true);
            } else {
                console.log('ERROR: Problem getting claimants, response state: ' + state);
            }
        });

        $A.enqueueAction(action);
    },

    getPortalFilter : function (component, helper) {
        // start PD-1258
        var action = component.get('c.getPortalFilter');

        action.setCallback(this, function getPortalFilterCallback(response) {
            var state = response.getState();

            if (component.isValid() && state === 'SUCCESS') {
                var strList = response.getReturnValue();
                var filter  = strList[0];
                var sort    = strList[1];
                var asc     = strList[2] == 'true';

                //console.log('asc = ' + strList[2]);

                component.set('v.sortAsc', asc);

                switch (filter) {
                    case 'allClaimants':
                        helper.updateLabelAuto(component,'All Claimants');
                        helper.filterBy(component,'allClaimants');
                        break;
                    case 'Not_Ready_to_be_Cleared':
                        helper.updateLabelAuto(component,'Not Ready to Be Cleared');
                        helper.filterBy(component,'Not_Ready_to_be_Cleared');
                        break;
                    case 'Released_to_a_Holdback':
                        helper.updateLabelAuto(component,'Released to a Holdback');
                        helper.filterBy(component,'Released_to_a_Holdback');
                        break;
                    case 'Cleared':
                        helper.updateLabelAuto(component,'Cleared');
                        helper.filterBy(component,'Cleared');
                        break;
                    case 'LastWeek':
                        helper.updateLabelAuto(component,'Updated in last 7 days');
                        helper.filterBy(component,'LastWeek');
                        break;
                    case 'NeedsAttention':
                        helper.updateLabelAuto(component,'Needs Attention');
                        helper.filterBy(component,'NeedsAttention');
                        break;
                }

                switch (sort) {
                    case 'Needs_Attention_Total__c':
                        component.set('v.sortField','Last_Name__c'); // PD-1258 - keep the sort order from reversing
                        helper.sortBy(component, 'Needs_Attention_Total__c',  'Last_Name__c', 'Needs Attention');
                        component.set('v.selectedColumn', 'Needs Attention');
                        break;
                    case 'Last_Name__c':
                        component.set('v.sortField','Status__c'); // PD-1258 - keep the sort order from reversing
                        helper.sortBy(component, 'Last_Name__c',  'Status__c', 'Last Name');
                        component.set('v.selectedColumn', 'Name');
                        break;
                    case 'Status__c':
                        component.set('v.sortField','Last_Name__c'); // PD-1258 - keep the sort order from reversing
                        helper.sortBy(component, 'Status__c', 'Last_Name__c', 'Claimant Status');
                        component.set('v.selectedColumn', 'Status');
                        break;
                    case 'Date_Last_Updated__c':
                        component.set('v.sortField','Last_Name__c'); // PD-1258 - keep the sort order from reversing
                        helper.sortBy(component, 'Date_Last_Updated__c',  'Last_Name__c', 'Last Updated');
                        component.set('v.selectedColumn', 'Last Updated');
                        break;
                }
            }
            else {
                console.log('ERROR: Problem getting Filter, response state: ' + state);
            }
        });

        $A.enqueueAction(action);
        // end PD-1258
    },
    // end PD-1277

    // start PD-1277 (new helper function)
    redoSort: function (component, helper) {
        var sortField = component.get('v.sortField');

        switch (sortField) {
            case 'Needs_Attention_Total__c':
                helper.sortBy(component, 'Needs_Attention_Total__c',  'Last_Name__c', 'Needs Attention');
                break;
            case 'Last_Name__c':
                helper.sortBy(component, 'Last_Name__c',  'Status__c', 'Last Name');
                break;
            case 'Status__c':
                helper.sortBy(component, 'Status__c', 'Last_Name__c', 'Claimant Status');
                break;
            case 'Date_Last_Updated__c':
                helper.sortBy(component, 'Date_Last_Updated__c',  'Last_Name__c', 'Last Updated');
                break;
        }
    },
    // end PD-1277

    sortBy: function(component, field, field2, columnName) {
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.filteredClaimants"); // PD-1277 (use filteredClaimants instead of claimants)
        sortAsc = field == sortField? !sortAsc: true;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],

                t3 = a[field2] == b[field2],
                t4 = a[field2] > b[field2];
            if(field == 'Needs_Attention_Total__c'){var t2 = a[field] < b[field]}else{var t2 = a[field] > b[field]};
            return t1? (sortAsc?-1:1)*(t4?-1:1): (sortAsc?-1:1)*(t2?-1:1);
        });

        var currentDir = component.get("v.arrowDirection");

        // set the arrowDirection attribute for conditionally rendred arrow sign
        if (currentDir == 'arrowdown') {
            component.set("v.arrowDirection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }

        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.claimants", records);
        component.set("v.sortColumn", columnName);
// start PD-1258
        var action = component.get("c.setPortalFilter");
        action.setParams({"filterName": component.get("v.filterName"),"sortField": component.get("v.sortField"), "ascending": component.get("v.sortAsc")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var name = response.getReturnValue();
            }
            else {
                console.log('Problem setting Filter, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
// end PD-1258
    },

    updateTriggerLabel: function(cmp, event) {
        var triggerCmp = cmp.find("trigger");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.label", label);
        }
    },
// start PD-1258
    updateLabelAuto: function(cmp,filterName) {
        var triggerCmp = cmp.find("trigger");
        if (triggerCmp) {
            triggerCmp.set("v.label", filterName);
        }
    },

    filterBy: function(component, filterName) {
        records = component.get("v.allClaimants");
// start PD-1258
        var action = component.get("c.setPortalFilter");
        action.setParams({"filterName": filterName,"sortField": component.get("v.sortField"), "ascending": component.get("v.sortAsc")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var name = response.getReturnValue();
            }
            else {
                console.log('Problem setting Filter, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
// end PD-1258
        component.set("v.filterName",filterName);
        switch(filterName) {
            case 'Not_Ready_to_be_Cleared':
            case 'Released_to_a_Holdback':
            case 'Cleared':
                var filter = [];
                var j=0;
                for (var i=0; i<records.length; i++) {
                    var c = records[i];
                    if (filterName == c.Status__c){
                        filter[j]=c;
                        j++;
                    }
                }
                component.set("v.filteredClaimants", filter); // PD-1277 (use filteredClaimants instead of claimants)
                break;
            case 'LastWeek':
                var days =7; // Days you want to subtract
                var date = new Date();
                var lastweek = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));

                var filter = [];
                var j=0;
                for (var i=0; i<records.length; i++) {
                    var c = records[i];
                    var dateupdated= new Date(c.Date_Last_Updated__c);
                    if (dateupdated > lastweek){
                        filter[j]=c;
                        j++;
                    }
                }
                component.set("v.filteredClaimants", filter); // PD-1277 (use filteredClaimants instead of claimants)
                break;
            case 'Needs_Attention':
                var filter = [];
                var j=0;
                for (var i=0; i<records.length; i++) {
                    var c = records[i];
                    if (c.Needs_Attention_Field__c>0 || c.Needs_AttentionPLRP__c > 0){
                        filter[j]=c;
                        j++;
                    }
                }
                component.set("v.filteredClaimants", filter); // PD-1277 (use filteredClaimants instead of claimants)
                break;
            case 'Needs_Attention_Field':
                var filter = [];
                var j=0;
                for (var i=0; i<records.length; i++) {
                    var c = records[i];
                    if (c.Needs_Attention_Field__c>0){
                        filter[j]=c;
                        j++;
                    }
                }
                component.set("v.filteredClaimants", filter); // PD-1277 (use filteredClaimants instead of claimants)
                break;
            case 'Needs_Attention_PLRP':
                var filter = [];
                var j=0;
                for (var i=0; i<records.length; i++) {
                    var c = records[i];
                    if (c.Needs_AttentionPLRP__c>0){
                        filter[j]=c;
                        j++;
                    }
                }

                component.set("v.filteredClaimants", filter); // PD-1277 (use filteredClaimants instead of claimants)
                break;
            case "allClaimants":
                component.set("v.filteredClaimants", records); // PD-1277 (use filteredClaimants instead of claimants)
                break;
        }
    },
})