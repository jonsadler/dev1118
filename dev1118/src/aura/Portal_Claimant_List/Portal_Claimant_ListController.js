/*
    Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-840 - Creation of Claimant List w/Filter Component Part One

History:

    Updated: lmsw - June 2017
    Purpose: PD-809 - Modified lfClaimantsMap

    Updated: Marc Dysart October 2017 for October Release
    Purpose: PD-985 - Portal - Needs Attention Status not changing

    Updated: MJD - November 2017 for December Release
    Purpose: PD-1218 - Portal - Add loading icon to Matter Page

    Updated: RAP - November 2017 for December Release
    Purpose: PD-1258 - FF - Return to Previous Filter setting on Matter page - after viewing Claimant page

    Updated: CPS - Nov 2017 for November Release
    Purpose: PD-1277 - Improve loading speed of portal claimant list
*/
({
    doInit : function(component, event, helper) {
        component.set('v.Spinner', true);

        // start PD-1277 (move init code to separate helper methods)
        helper.getAction(component, helper);
        helper.getLFClaimants(component, helper);
        helper.getPortalFilter(component, helper);
        // end PD-1277
    },

    sortByNeedsAttention: function(component, event, helper) {
        helper.sortBy(component, "Needs_Attention_Total__c",  "Last_Name__c", "Needs Attention");
        component.set("v.selectedColumn", 'Needs Attention');
    },

    sortByName: function(component, event, helper) {
        helper.sortBy(component, "Last_Name__c",  "Status__c",  "Last Name");
        component.set("v.selectedColumn", 'Name');
    },
    sortByStatus: function(component, event, helper) {
        helper.sortBy(component, "Status__c", "Last_Name__c", "Claimant Status");
        component.set("v.selectedColumn", 'Status');
    },
    sortByLastUpdated: function(component, event, helper) {
        helper.sortBy(component, "Date_Last_Updated__c",  "Last_Name__c", "Last Updated");
        component.set("v.selectedColumn", 'Last Updated');
    },

    filterAll: function(component, event, helper){
        // Change label of the filter for dropdown filter
        helper.updateTriggerLabel(component,event);
        helper.filterBy(component,'allClaimants');
        helper.redoSort(component, helper); // PD-1277
    },

    filterNeedsAttention: function(component, event, helper){
        helper.updateTriggerLabel(component,event);
        helper.filterBy(component,'Needs_Attention');
        helper.redoSort(component, helper); // PD-1277
    },

    filterCleared: function(component, event, helper){
        helper.updateTriggerLabel(component,event);
        helper.filterBy(component,'Cleared');
        helper.redoSort(component, helper); // PD-1277
    },

    filterNotReady: function(component, event, helper){
        helper.updateTriggerLabel(component,event);
        helper.filterBy(component,'Not_Ready_to_be_Cleared');
        helper.redoSort(component, helper); // PD-1277
    },

    filterHoldback: function(component, event, helper){
        helper.updateTriggerLabel(component,event);
        helper.filterBy(component,'Released_to_a_Holdback');
        helper.redoSort(component, helper); // PD-1277
    },

    filterLastWeek: function(component, event, helper){
        helper.updateTriggerLabel(component,event);
        helper.filterBy(component,'LastWeek');
        helper.redoSort(component, helper); // PD-1277
    },

    // Start PD-985 //
    handleNeedsAttention: function(component,event,helper) {
        var naField = event.getParam('claimantneedsattentionField');
        var naPLRP = event.getParam('claimantneedsattentionPLRP');
        var triggerCmp = component.find("trigger");
        if (triggerCmp) {
            triggerCmp.set("v.label", 'Needs Attention');
        }
        if (naField) {
            helper.filterBy(component,'Needs_Attention_Field');
        }
        if (naPLRP) {
            helper.filterBy(component,'Needs_Attention_PLRP');
        }
    },
    // End PD-985 //

    // Start PD-1218


    handleRecordsLoaded : function (component, event, helper) {
        // var lienMapPendingLoaded        = component.get('v.lienMapPendingLoaded');
        var actionLoaded                = component.get('v.actionLoaded');
        var lfClaimantsLoaded           = component.get('v.lfClaimantsLoaded');

        // If all records have been loaded, don't show spinner.
        if (actionLoaded &&
            lfClaimantsLoaded
        ) {
            component.set("v.Spinner", false);
        } else {
            component.set("v.Spinner", true);
        }
    },
// End PD-1218
})