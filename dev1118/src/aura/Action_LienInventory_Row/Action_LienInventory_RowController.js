({
    // ################################################################################################# handleInit
    // This method handles the component's init event.
    //
    handleInit : function (component, event, helper) {
        helper.createTileItems(component, helper);
    },

    // ################################################################################################# expandRow
    // This method expands the row when either the row, or the master, expand button is clicked.
    //
    expandRow : function (component, event, helper) {
        component.set('v.collapsed', false);
    },

    // ################################################################################################# collapseRow
    // This method collapses the row when either the row, or the master, collapse button is clicked.
    //
    collapseRow : function (component, event, helper) {
        component.set('v.collapsed', true);
    },

})