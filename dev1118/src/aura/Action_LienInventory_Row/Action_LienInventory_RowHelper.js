({
    // ################################################################################################# createTileItems
    // This method iterates through the row's liens, and creates the tiles for them.
    //
    createTileItems : function (component, helper) {
        var liens           = component.get('v.liens');
        var numLiensPending = 0;
        var numLiensCleared = 0;
        var liensByStageMap = {};
        var lienStages      = component.get('v.configLienStages');
        var tileItems       = [];
        var lienType        = component.get('v.lienType');

        // Iterate through liens and add them to a map
        // where the key is the lien stage, and the value
        // is a list of liens of that stage.
        liens.forEach(function (lien) {
            var lienStage = lien.stageLabel;

            // Increment cleared or pending counter.
            if (lienStage == 'No Lien' || lienStage == 'Final') {
                numLiensCleared++;
            }
            else {
                numLiensPending++;
            }

            // Add the lien to the map.
            if ($A.util.isUndefinedOrNull(liensByStageMap[lienStage])) {
                liensByStageMap[lienStage] = [];
            }

            liensByStageMap[lienStage].push(lien);
        });

        // Set the counters.
        component.set('v.numLiensCleared', numLiensCleared);
        component.set('v.numLiensPending', numLiensPending);
        component.set('v.percentLiensCleared', (numLiensCleared / liens.length * 100).toFixed(0));

        // Iterate through the lien stages to display, and create
        // the stage items data.
        lienStages.forEach(function (lienStage) {
            var tileItem  = {
                'lienStage'   : lienStage,
                'liens'       : [],
                'typeIsModel' : (lienType.includes('Model') && (lienStage == 'Asserted' || lienStage == 'Audit') ? true : false)
            };

            // Populate the stage item with the liens (if any).
            if (!$A.util.isUndefinedOrNull(liensByStageMap[lienStage])) {
                tileItem.liens = liensByStageMap[lienStage];
            }

            // Add the stage item to the list of stage items.
            tileItems.push(tileItem);
        });

        // Set the stage items attribute which will drive
        // the creation of the stage components.
        component.set('v.tileItems', tileItems);
    },

})