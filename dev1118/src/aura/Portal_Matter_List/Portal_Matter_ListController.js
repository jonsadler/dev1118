/* 
	Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-813 - Development of the Portal Home Page

History:

    Updated: lmsw - August 2017
    Purpose: Add Law Firm Name to page

    Updated: lmsw - August 2017
    Purpose: PD-922 - Limit portal users without using permissions.

    Updated: lmsw - October 2017 for October Release
    Purpose: PD-971 - Add Needs Attention on Homepage

    Updated: lmsw - November 2017 for December Release
    Purpose: PD-1264 - Add Legal Terms before access to portal and delete un-used code

    Updated: RAP - November 2017 for December Release
    Purpose: PD-1258 - FF - Return to Previous Filter setting on Matter page - after viewing Claimant page

*/
({
    doInit : function(component, event, helper) {
        helper.getActions(component);
        helper.getCurrentUsers(component);
        helper.getUserLawFirmName(component);
        helper.getActionNAs(component);
  
    },

    chooseMatter: function(component, event, helper) {
        var actionID = event.currentTarget.dataset.id;
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            "matterID": actionID
        }); 
        cmpEvent.fire();
        component.set("v.showMatterList",false);
        var matterlist = document.getElementById("matter-list");
        if (matterlist) {
            matterlist.classList.add("hide-matter-list");
        }
        var action = component.get("c.SetAction");
        action.setParams({"aId": actionID});
        action.setCallback(this, function(response) {
        var state = response.getState();
        if(component.isValid() && state === "SUCCESS") {
                console.log('This set the action ID to '+actionID);
                $A.get("e.force:refreshView").fire();
             } else {
               console.log('Problem setting Action ID, response state: ' + state);
             }
           });
            $A.enqueueAction(action); 
            
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": actionID
        });
        navEvt.fire();
    },

    closeModal: function(component, event, helper) {
        component.set('v.acceptedTerms', false);
        helper.navigateToLogin(component);
    },

    acceptTerms: function(component, event, helper) {
        helper.getActions(component);
        helper.updateCurrentUser(component);
        component.set('v.acceptedTerms', true);
    },

    onCheck: function(component, event, helper) {
        component.set("v.haveRead", component.find("checkBoxRead").get("v.value"));
    }
})