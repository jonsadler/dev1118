/* 
	Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-813 - Development of the Portal Home Page

History:

    Updated: lmsw - August 2017
    Purpose: Add Law Firm Name to page

    Updated: lmsw - October 2017 for October Release
    Purpose: PD-971 - Add Needs Attention on Homepage

    Updated: lmsw - November 2017 for December Release
    Purpose: PD-1264 - Get current user for Accepted Legal Terms
*/
({
	getActions : function(component,event,helper){
        var action = component.get("c.getLFActions");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var results = response.getReturnValue();
                console.log('Get Action: '+ results.length);
                component.set("v.lfActions", response.getReturnValue());
            } else {
                console.log('Problem getting Lawfirm Actions, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);
    },

    getUserLawFirmName : function(component,event,helper){
        var action = component.get("c.getCompanyName");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lawFirmName", response.getReturnValue());
            } else {
                console.log('Problem getting Users Law Firm Name, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);
    },

    getActionNAs : function(component,event,helper){
        var NeedsAttentionTotal = [];
        var action = component.get("c.getLFActionNAs");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                var naMap = response.getReturnValue();
                var result = [];
                var temp = [];
                for (var actionId in naMap) {
                    temp = naMap[actionId];
                    result.push({
                        key: actionId,
                        value: temp
                    });
                } 
                component.set("v.NeedsAttentionTotal", result);
            } else {
                console.log('Problem getting Needs Attention for an Action for this Lawfirm, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    ResetFilter: function(component,event,helper) {
console.log('setting filter');
        var action = component.get("c.setPortalFilter");
console.log('setting params');
        action.setParams({'filterName': null,'sortField': null, 'ascending': null});
console.log('entering callback');
        action.setCallback(this, function(response) {
        	var state = response.getState();
console.log('Filter set, state: ' + state);
            if (component.isValid() && state === "SUCCESS") {
                var name = response.getReturnValue();
                console.log('Filter set, response: ' + name);
            } 
            else {
                console.log('Problem setting Filter, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
// start PD-1258
    getCurrentUsers : function(component,event,helper) {
        var action = component.get("c.getCurrentUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state ==="SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.acceptedTerms", result[0].Has_Accepted_Legal_Terms__c);
                component.set("v.currentUserId", result[0].Id);
            } else {
                console.log('Problem getting current user, response state: '+ state);
             }
        });
        $A.enqueueAction(action);
    },

    updateCurrentUser : function(component,event,helper) {
        var action = component.get("c.updateCurrentUser");
        action.setParams({"uId":component.get("v.currentUserId"),
                         "acceptedTerms":"true"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(compnent.isValid() && state ==="SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.acceptedTerms", result[0].Has_Accepted_Legal_Terms__c);
            } else {
                console.log('Problem updating current user, response state: '+ state);
            }
        });
        $A.enqueueAction(action);
    },
   
    navigateToLogin : function(component, event, helper) {
        //Logout
        window.location.replace($A.get("$Label.c.Portal_Logout"));
        // Login url
        window.location.replace($A.get("$Label.c.Portal_Login"));
    },
// end PD-1258

})