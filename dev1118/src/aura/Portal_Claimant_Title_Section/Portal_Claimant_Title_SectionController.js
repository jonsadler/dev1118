/*  
    Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-841 - Creation of Claimant Title Section Component

History:

    Updated: lmsw - June 2017
    Updated: PD-841 - Modified getLFClaimant

    Updated: MJD - October 2017 for October Release
    Purpose: PD-890 - Portal Users Search for a Claimant

*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLFClaimant");
        action.setParams({"cId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.claimant", response.getReturnValue());
                 var c= component.get("v.claimant");
                 var availableOnPortal = c.Action__r.Available_in_Portal__c;  // PD-890
                console.log('Available on Portal: '+ availableOnPortal);
                // var claimantLawfirm = c.Law_Firm__c;
                // console.log('Claimant Law Firm: '+ claimantLawfirm);
            } else {
                console.log('Problem getting Claimant, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);

  		var action4 = component.get("c.getLastRelease");
        action4.setParams({"cId": component.get("v.recordId")});
        action4.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lastRelease", response.getReturnValue());
            } else {
                console.log('Problem getting Last Eligible Disbursement, response state: ' + state);
            }
        });
        $A.enqueueAction(action4);

        var action6 = component.get("c.getNeedsAttention");
        action6.setParams({"cId": component.get("v.recordId")});
        action6.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.needsAttention", response.getReturnValue());
                var temp = component.get("v.needsAttention");
                var count = 0;
                for (key in temp) {
                    if (key == 'PLRP_Elections'){
                        component.set("v.electionPLRP",true );
                    } else {
                    count++;
                    }
                    // console.log(key);
                }
                component.set("v.alertCount", count);
                // console.log('Count: '+count);
            } else {
                console.log('Problem getting Needs Attention Alerts, response state: ' + state);
            }
        });   
        $A.enqueueAction(action6);


        var action7 = component.get("c.getlawfirmID");
        action7.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lawFirmID", response.getReturnValue());
                var lawFirmID = component.get("v.lawFirmID");
                // console.log('User Law Firm: '+ lawFirmID);
                var c= component.get("v.claimant");
                var claimantLawfirm = c.Law_Firm__c;
                var availableOnPortal = c.Action__r.Available_in_Portal__c;  // PD-890
                console.log('Available on Portal: '+ availableOnPortal);
                // console.log('Claimant Law Firm: '+ claimantLawfirm);

                 if(lawFirmID != claimantLawfirm){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                    "url": "/nopermissions/"
                    });
                    urlEvent.fire();

                } else {
                    /* Start PD-890 */
                        if(availableOnPortal==false){
                            var urlEvent = $A.get("e.force:navigateToURL");
                            urlEvent.setParams({
                            "url": "/not-available/"
                            });
                            urlEvent.fire();
                        }
                   /*  End PD-890 */
                 } 
                

            } else {
                console.log('Problem getting Users Law Firm Name, response state: ' + state);
            }
        });     
        $A.enqueueAction(action7);

        
        
 

}
    }
    },

    handleComponentEvent : function(component, event, helper) {
    	// helper.getMatterID(component, event);
    }
    
})