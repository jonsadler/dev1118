/*  
    Created: lmsw - May 2017
    Purpose: PD-640 Add Claimant totals and Awards totals

History: 

    Updated: lmsw - December for December Release
    Purpose: PD-1296 - Fix display of total and percents in Action Synopsis.
             Removed unused code.
             
    Updated: lmsw - January 2018 for January Release
	Purpose: PD-1020 Make Claimants and Awards link to a report
*/
({
	GetActionRecord : function(component,event,helper){
		var action = component.get("c.getAction");
		action.setParams({"aId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
		    var state = response.getState();
		    if(component.isValid() && state === "SUCCESS") {
		        component.set("v.currentAction", response.getReturnValue());
		    } else {
		        console.log('Problem getting Action, response state: ' + state);
		    }
		});
		$A.enqueueAction(action);
 	}
})