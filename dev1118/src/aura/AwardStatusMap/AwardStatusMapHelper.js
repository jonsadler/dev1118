/* 
    Created: lmsw - February 2017 for March Release
    Purpose: PD-348 - Display Award distribution status for an action

History: 

	Updated: lmsw - December for December Release
	Purpose: PD-1296 - Fix display of total and percents in Action Synopsis
*/
({
	getDPercent : function(component) {
		var value = component.get("v.value");
		var total = component.get("v.totalAwards");
		var percent = 0;
		
    	if (total > 0) { 
            percent = Math.round((value/total)*100); 
        } 
		component.set("v.percentD", percent);                      
    }
})