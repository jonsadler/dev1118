({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLawFirmMetrics");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	component.set("v.filter", $A.get("$Label.c.LawFirmFilter"))
            	console.log('filter = ' + component.get("v.filter"));
	            var lfMap = response.getReturnValue();
	            var result = [];
	            var temp = [];
	            var int;
	            for (var plif in lfMap) {
	            	temp = lfMap[plif];
	       			result.push({
	       				key: plif,
	       				value: temp
	       			});
	            }
	            component.set("v.kludge", result);
            } else {
                console.log('Problem getting law firms, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
       // Start PD-889
       openTabwithSubtab: function(component, event, helper) {
        var newId = event.currentTarget.getAttribute("id");
        var url = '/'+newId;
        var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                    "url": url
                    });
                    urlEvent.fire();
    }
    // PD-889
})