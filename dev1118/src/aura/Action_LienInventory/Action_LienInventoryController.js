({
    // ################################################################################################# handleInit
    // This method handles the component's init event.
    //
    handleInit : function (component, event, helper) {
        helper.initialize(component, helper);
    },

    // ################################################################################################# collapseAllButtonClicked
    // This method handles the event that gets fired when the master
    // collapse button is clicked.
    //
    collapseAllButtonClicked : function (component, event, helper) {
        var rowComponents = component.find('rowComponent');

        if ($A.util.isUndefinedOrNull(rowComponents)) {
            // We did not find any rows (no liens for the action), so do nothing
        }
        else if ($A.util.isUndefinedOrNull(rowComponents.length)) {
            // We found only one row (only one lien type for the action), so invoke its collapse method directly

            rowComponents.collapse();
        }
        else {
            // We found muliple rows (multiple lien types for the action), so iterate through
            // them and call each one's collapse method.
            rowComponents.forEach(function (rowComponent) {
                rowComponent.collapse();
            });
        }
    },

    // ################################################################################################# expandAllButtonClicked
    // This method handles the event that gets fired when the master
    // expand button is clicked.
    //
    expandAllButtonClicked : function (component, event, helper) {
        var rowComponents = component.find('rowComponent');

        if ($A.util.isUndefinedOrNull(rowComponents)) {
            // We did not find any rows (no liens for the action), so do nothing
        }
        else if ($A.util.isUndefinedOrNull(rowComponents.length)) {
            // We found only one row (only one lien type for the action), so invoke its expand method directly

            rowComponents.expand();
        }
        else {
            // We found muliple rows (multiple lien types for the action), so iterate through
            // them and call each one's expand method.
            rowComponents.forEach(function (rowComponent) {
                rowComponent.expand();
            });
        }
    },

})