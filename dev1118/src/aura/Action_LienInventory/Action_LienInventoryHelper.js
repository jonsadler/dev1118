({
    // ################################################################################################# initialize
    // This method calls all the helper methods required to initialize the component.
    //
    initialize : function (component, helper) {
        helper.setAttributeDefaults(component, helper);
        helper.setConfigLienTypes(component, helper);
        helper.setConfigLienStages(component, helper);
        helper.setConfigTileCards(component, helper);
        helper.setConfigReportLinks(component, helper);
        helper.getAction(component, helper);
        helper.getLiens(component, helper, null);
    },

    // ################################################################################################# setAttributeDefaults
    // This method sets all attributes to their default values.  This isn't needed so much
    // for the first time the component is initialized (since the defaults are stored as
    // part of the attribute definition), but is used to start from a blank slate
    // when the component is subsequently refreshed.
    //
    setAttributeDefaults : function (component, helper) {
        component.set('v.configLienTypes',     []);
        component.set('v.configLienStages',    []);
        component.set('v.configTileCards',     {});
        component.set('v.liens',               []);
        component.set('v.numLiensPending',     0);
        component.set('v.numLiensCleared',     0);
        component.set('v.percentLiensCleared', 0);
        component.set('v.rowItems',            []);
    },

    // ################################################################################################# setConfigLienTypes
    // This method sets config data for the order in which to display the lien types (rows).
    // Be sure and use labels (not API names).
    //
    setConfigLienTypes : function (component, helper) {
        component.set('v.configLienTypes', [
            'Medicare Individual Resolution',
            'Medicare Global Model',
            'Non-PLRP',
            'Medicaid',
            'PLRP Claim Detail',
            'PLRP Model',
            'Other Govt']);
    },

    // ################################################################################################# setConfigLienStages
    // This method sets config data for the order in which to display the lien stages (tiles).
    // Be sure and use labels (not API names).
    //
    setConfigLienStages : function (component, helper) {
        // List lien stage labels in the order they are to be displayed.
        // Be sure and use the label (not the API name).
        component.set('v.configLienStages', [
            'To Be Submitted',
            'Submitted',
            'No Lien',
            'Asserted',
            'Audit',
            'Final']);
    },

    // ################################################################################################# setConfigTileCards
    // This method sets config data that describes the cards to be displayed within a tile.
    //
    //     First level key:  Lien type
    //     Second level key: Lien stage
    //     Innermost list:   List of cards displayed
    //
    // The innermost list values consist of up to four elements delimited by double colons, where:
    //     First element (REQUIRED)
    //         'substage'   - Use a lien substage to determine liens for the card
    //         'lienholder' - Use a lienholder name to determine liens for the card
    //         'special'    - Use special logic to determine liens for the card
    //
    //     Second element (REQUIRED)
    //         The substage, lienholder name, or special logic to use
    //
    //     Third element (OPTIONAL)
    //         'draggable' - Makes the card draggable
    //         NOTE: No cards make use of the draggable element yet, since this first
    //               version of code does not include Mass Update functionality.
    //               This is just a placeholder for now.
    //
    //     NOTE: Double colons are used as a delimiter instead of single colons so that if a substage
    //           or lienholder name contains a colon, it won't be treated as a delimiter.
    //
    setConfigTileCards : function (component, helper) {
        component.set('v.configTileCards', {
            'Medicare Individual Resolution' : {
                'To Be Submitted' : [],
                'Submitted'       : ['special::noResponseIn30Days', 'substage::Outlier Situation', 'substage::RAR Received'],
                'No Lien'         : ['substage::Not Eligible'],
                'Asserted'        : ['substage::Received CPL'],
                'Audit'           : ['substage::Returned Audited CPL', 'substage::Requested Final Demand'],
                'Final'           : ['substage::Received Final Demand', 'substage::Lien Paid']
            },
            'Medicare Global Model' : {
                'To Be Submitted' : [],
                'Submitted'       : ['special::noResponseIn30Days', 'substage::Outlier Situation'],
                'No Lien'         : ['substage::Not Eligible'],
                'Final'           : ['substage::Lien Paid']
            },
            'Non-PLRP' : {
                'To Be Submitted' : [],
                'Submitted'       : ['special::noResponseIn30Days', 'substage::First Notice Sent', 'substage::Second Notice Sent', 'substage::Third Notice Sent'],
                'No Lien'         : ['substage::No Interest - $0 claimed/anti-subrogation'],
                'Asserted'        : ['substage::Received Asserted Amount'],
                'Audit'           : ['substage::Returned Initial Audit'],
                'Final'           : ['substage::Received Final Lien Amount', 'substage::Lien Paid']
            },
            'Medicaid' : {
                'To Be Submitted' : [],
                'Submitted'       : ['special::noResponseIn30Days', 'substage::Eligible – waiting for lien itemization', 'substage::Eligible – waiting for settlement info'],
                'No Lien'         : ['substage::No Interest – $0 claimed/anti-subrogation', 'substage::Not Eligible'],
                'Asserted'        : ['substage::Received Asserted Amount'],
                'Audit'           : ['substage::Returned Initial Audit'],
                'Final'           : ['substage::Received Final Lien Amount', 'substage::Completed', 'substage::Lien Paid']
            },
            'PLRP Claim Detail' : {
                'To Be Submitted' : [],
                'Submitted'       : ['special::noResponseIn30Days'],
                'No Lien'         : ['substage::No Interest – $0 claimed/anti-subrogation'],
                'Asserted'        : ['substage::Received Asserted Amount'],
                'Audit'           : ['substage::Contested', 'substage::Returned Initial Audit'],
                'Final'           : ['substage::Waived', 'substage::Received Final Lien Amount', 'substage::Completed', 'substage::Lien Paid']
            },
            'PLRP Model' : {
                'To Be Submitted' : ['special::lienholder1', 'special::lienholder2', 'special::lienholder3'],
                'Submitted'       : ['special::noResponseIn30Days', 'special::lienholder1', 'special::lienholder2', 'special::lienholder3'],
                'No Lien'         : ['substage::No Interest – $0 claimed/anti-subrogation'],
                'Final'           : ['substage::Lien Paid']
            },
            'Other Govt' : {
                'Submitted' : ['special::noResponseIn30Days', 'substage::First Notice Sent', 'substage::Second Notice Sent', 'substage::Third Notice Sent'],
                'No Lien'   : ['substage::Not Eligible'],
                'Asserted'  : ['substage::Received Asserted Amount'],
                'Audit'     : ['substage::Returned Initial Audit'],
                'Final'     : ['substage::Received Final Lien Amount', 'substage::Lien Paid']
            }
        });
    },

    // ################################################################################################# setConfigReportLinks
    // This method sets config data for the report links.
    //
    setConfigReportLinks : function(component, helper) {
        component.set('v.configReportLinks', {
            'lienStage'          : $A.get('$Label.c.Report_LienReportLink'),
            'lienSubstage'       : $A.get('$Label.c.Report_LienSubReportLink'),
            'noResponseIn30Days' : $A.get('$Label.c.Report_LienNoticeReportLink')
        });
    },

    // ################################################################################################# getAction
    // This method gets data for the action.
    //
    getAction : function (component, helper) {
        var apexMethod = component.get('c.getAction');

        apexMethod.setParams({
            'actionId' : component.get('v.recordId')
        });

        apexMethod.setCallback(this, function getActionCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var action = response.getReturnValue();

                component.set('v.action', action);
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (getActionCallback) INCOMPLETE');
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (getActionCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (getActionCallback) ERROR; unknown error');
                }

            }
        });

        $A.enqueueAction(apexMethod);
    },

    // ################################################################################################# getLiens
    // This method gets the liens for the action.  Because actions can contain
    // more than 50,000 liens (which exceeds SOQL query row limits), this method
    // calls itself recursively to get liens in chunks, and then processes
    // them once all have been retrieved.
    //
    getLiens : function (component, helper, prevChunkEndingLienId) {
        var apexMethod = component.get('c.getLiensByActionChunked');

        // We set the chunk row limit to less than the 50,000 SOQL limit,
        // just to be sure we don't ever hit it.
        apexMethod.setParams({
            'actionId'              : component.get('v.recordId'),
            'prevChunkEndingLienId' : prevChunkEndingLienId,
            'chunkRowLimit'         : 20000
        });

        apexMethod.setCallback(this, function getLiensCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var newChunkOfLiens = response.getReturnValue();

                if (newChunkOfLiens.length > 0) {
                    // We found liens, so add them to the existing liens
                    // we've already found, and get the next chunk.
                    var liens = component.get('v.liens');
                    liens     = liens.concat(newChunkOfLiens);
                    component.set('v.liens', liens);

                    var endingLienId = liens[liens.length - 1].Id;

                    helper.getLiens(component, helper, endingLienId);
                }
                else {
                    // We didn't find more liens, so process the ones
                    // we found so far.
                    helper.createRowItems(component, helper);
                }
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (getLiensCallback) INCOMPLETE');
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (getLiensCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (getLiensCallback) ERROR; unknown error');
                }

            }
        });

        $A.enqueueAction(apexMethod);
    },

    // ################################################################################################# createRowItems
    // This method iterates through the action's liens, and creates the types (rows) for them.
    //
    createRowItems : function (component, helper) {
        var liens           = component.get('v.liens');
        var numLiensPending = 0;
        var numLiensCleared = 0;
        var liensByTypeMap  = {};
        var configLienTypes = component.get('v.configLienTypes');
        var rowItems        = [];

        // Iterate through liens and add them to a map
        // where the key is the lien type, and the value
        // is a list of liens of that type.
        liens.forEach(function (lien, index) {
            var lienType  = lien.RecordType.Name;
            var lienStage = lien.stageLabel;

            // Increment cleared or pending counter.
            if (lienStage == 'No Lien' || lienStage == 'Final') {
                numLiensCleared++;
            }
            else {
                numLiensPending++;
            }

            // Add the lien to the map.
            if ($A.util.isUndefinedOrNull(liensByTypeMap[lienType])) {
                liensByTypeMap[lienType] = [];
            }

            liensByTypeMap[lienType].push(lien);
        });

        // Set the counters.
        component.set('v.numLiensCleared', numLiensCleared);
        component.set('v.numLiensPending', numLiensPending);
        component.set('v.percentLiensCleared', (liens.length == 0 ? 0 : (numLiensCleared / liens.length * 100).toFixed(0)));

        // Iterate through the lien types to display, and create
        // the row items data.
        configLienTypes.forEach(function (configLienType, index) {
            if ($A.util.isUndefinedOrNull(liensByTypeMap[configLienType])) {
                // There are no liens of this type, so we
                // don't need to build a row item for this type.
                return;
            }

            // Create a row item for this type and its liens.
            var rowItem = {
                'lienType' : configLienType,
                'liens'    : liensByTypeMap[configLienType]
            };

            // Add the row item to the list of row items.
            rowItems.push(rowItem);
        });

        // Set the row items attribute which will drive
        // the creation of the row components.
        component.set('v.rowItems', rowItems);
    },

})