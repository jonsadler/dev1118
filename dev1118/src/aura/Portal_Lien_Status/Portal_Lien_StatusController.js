({
	/* 
	Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-866 - Creation of the Status Tab Component on the Liens Page
	*/
    doInit : function(component, event, helper) {
        var action = component.get("c.getLienAmtList");
        var lienID = component.get("v.recordId");
        console.log('lienID :'+lienID);
        action.setParams({lId: lienID});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.lienamts", response.getReturnValue());
                var lienamts = component.get("v.lienamts");
                console.log('lienamts:'+lienamts);
                
            } else {
                console.log('Problem getting lien amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);


        var action2 = component.get("c.getInjuries");
        action2.setParams({"lId": lienID});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.injuries", response.getReturnValue());
                // Take out the underscoresin the picklists
                var records = component.get("v.injuries");
                var injuries = [];
                var j = 0;
            	for (var i=0; i<records.length; i++) {
            		var c= records[i];
            		if (c.Compensable__c=='True') {
            			injuries[j]=c;
            			j++;
                        if (c.End_Date__c) {
                            component.set("v.injuryEndDate", true )
                        }
            		}
            	}
            	component.set("v.injuries", injuries);             
            } else {
                console.log('Problem getting injuries, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);


        var action3 = component.get("c.getFees");
        action3.setParams({"lId": lienID});
        action3.setCallback(this, function(response) {
        	var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
	            component.set("v.fees", response.getReturnValue());
	            // Take out the underscores in the picklists
	            var records = component.get("v.fees");
	            var feetype = records;
	            var feetotal = 0;
	            for (var i=0; i<records.length; i++) {
	            	var c = records[i];
	                    if (c.Fee_Type__c.includes("_")){
	                        feetype[i].Fee_Type__c=c.Fee_Type__c.replace(/_/g," ");           
	                    }
	                    if (c.Fee_Subtype__c.includes("_")){
	                    	feetype[i].Fee_Subtype__c=c.Fee_Subtype__c.replace(/_/g," ");
	                    }
	                    var feetotal = feetotal + c.Fee_Amount__c;
	            }
	            component.set("v.feetotal", feetotal);
	            component.set("v.fees", feetype);
            } else {
                console.log('Problem getting Fees, response state: ' + state);
            }
        });


        $A.enqueueAction(action3);
    }

}