({
    // ################################################################################################## handleInit
    handleInit : function (component, event, helper) {
        helper.getTask(component, helper);
        helper.getCurrentUserId(component, helper);
    },

    // ################################################################################################## handleTaskOrCurrentUserIdChanged
    handleTaskOrCurrentUserIdChanged : function (component, event, helper) {
        var task          = component.get('v.task');
        var currentUserId = component.get('v.currentUserId');

        if ($A.util.isUndefinedOrNull(task) || $A.util.isUndefinedOrNull(currentUserId)) {
            return;
        }

        if (task.OwnerId == currentUserId) {
            component.set('v.mode', 'alreadyAssigned');
        }
        else {
            component.set('v.mode', 'confirm');
        }
    },

    // ################################################################################################## confirmOkClicked
    confirmOkClicked : function (component, event, helper) {
        helper.assignTaskToCurrentUser(component, helper);
    },

    // ################################################################################################## confirmCancelClicked
    confirmCancelClicked : function (component, event, helper) {
        var closeQuickAction = $A.get('e.force:closeQuickAction');
        closeQuickAction.fire();
    },

    // ################################################################################################## alreadyAssignedOkClicked
    alreadyAssignedOkClicked : function (component, event, helper) {
        var closeQuickAction = $A.get('e.force:closeQuickAction');
        closeQuickAction.fire();
    },

})