({
    // ################################################################################################## getTask
    getTask : function(component, helper) {
        var taskId     = component.get('v.recordId');
        var apexMethod = component.get('c.getTask');

        apexMethod.setParams({
            taskId : taskId
        });

        apexMethod.setCallback(this, function getTaskCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var task = response.getReturnValue();

                component.set('v.task', task);
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (getTaskCallback) INCOMPLETE');
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (getTaskCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (getTaskCallback) ERROR; unknown error');
                }

            }
        });

        $A.enqueueAction(apexMethod);
    },

    // ################################################################################################## getCurrentUserId
    getCurrentUserId : function(component, helper) {
        var apexMethod = component.get('c.getCurrentUserId');

        apexMethod.setCallback(this, function getCurrentUserIdCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var currentUserId = response.getReturnValue();

                component.set('v.currentUserId', currentUserId);
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (getCurrentUserIdCallback) INCOMPLETE');
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (getCurrentUserIdCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (getCurrentUserIdCallback) ERROR; unknown error');
                }

            }
        });

        $A.enqueueAction(apexMethod);
    },

    // ################################################################################################## assignTaskToCurrentUser
    assignTaskToCurrentUser : function(component, helper) {
        var taskId      = component.get('v.recordId');
        var taskSubject = component.get('v.task.Subject');
        var apexMethod  = component.get('c.assignTaskToCurrentUser');

        apexMethod.setParams({
            taskId : taskId
        });

        apexMethod.setCallback(this, function assignTaskToCurrentUserCallback(response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                // Close quick action window
                var closeQuickAction = $A.get('e.force:closeQuickAction');
                closeQuickAction.fire();

                // Refresh page
                location.reload(true);
            }
            else if (state === 'INCOMPLETE') {
                console.log('ERROR: (assignTaskToCurrentUserCallback) INCOMPLETE');
            }
            else if (state === 'ERROR') {
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('ERROR: (assignTaskToCurrentUserCallback) ERROR; message = ' + errors[0].message);
                    }
                }
                else {
                    console.log('ERROR: (assignTaskToCurrentUserCallback) ERROR; unknown error');
                }

            }
        });

        $A.enqueueAction(apexMethod);
    },

})