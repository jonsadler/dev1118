({
    doInit : function(component, event, helper) {
        var rl = $A.get("$Label.c.Report_Claimants_Needing_Attention");
        component.set("v.reportLink", rl);
        console.log('Report Link: ' + rl);
        var action0 = component.get("c.getShowPlanType");
        action0.setParams({"aId": component.get("v.recordId")});
        action0.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	console.log('planType returns: ' + response.getReturnValue());
            	component.set("v.planType",response.getReturnValue());
            } else {
                console.log('Problem getting PlanType, response state: ' + state);
            }
        });
        $A.enqueueAction(action0);
        var action1 = component.get("c.getNeedsAttentions");
        action1.setParams({"aId": component.get("v.recordId")});
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
// start PD-1150
		        var spt = component.get("v.planType");
		        console.log('spt = ' + spt);
                var nas = response.getReturnValue(); // nas = map<string,map<string,map<string,decimal>>>
                var temp;
                var temp2;
                for (var priority in nas) {
                	var result = [];
                    temp = nas[priority]; // temp = map of apiName => map of label => number of NAs
                    for (var apiName in temp) {
	                    if (apiName != 'No_Plan_Type' || spt === 'true') {
	                    	var result2 = [];
	                    	temp2 = temp[apiName]; // temp2 = map of label => number of NAs
		                    for (var label in temp2) {
			                    var decimal = temp2[label];
			                    result2.push({
			                        key: label,
			                        value: decimal
			                    });
			                }
		                    result.push({
		                        key: apiName,
		                        value: result2
		                    });
		                }
	                }
                    if (priority == '1')
                    	component.set("v.naFirstColumn", result);
                    if (priority == '2')
                    	component.set("v.naSecondColumn", result);
                    if (priority == '3')
                    	component.set("v.naThirdColumn", result);
                }
            } else {
                console.log('Problem getting Needs Attentions, response state: ' + state);
            }
        });
        $A.enqueueAction(action1);
        var action2 = component.get("c.getCurrentAction");
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	var act = response.getReturnValue();
                component.set("v.currentAction", act);
                component.set("v.planType", act);
            }
        });
        $A.enqueueAction(action2);
    },
	handleForceShowToast : function(component, event, helper) {
		var eventMessage = event.getParam('message');
		if (!$A.util.isUndefinedOrNull(eventMessage) && eventMessage.includes('Needs Attention')) {
			var rl = $A.get("$Label.c.Report_Claimants_Needing_Attention");
	        component.set("v.reportLink", rl);
	        console.log('Report Link: ' + rl);
	        var action = component.get("c.getNeedsAttentions");
	        action.setParams({"aId": component.get("v.recordId")});
	        action.setCallback(this, function(response) {
	            var state = response.getState();
	            if (component.isValid() && state === "SUCCESS") {
	                var nas = response.getReturnValue(); // nas = map<string,map<string,map<string,decimal>>>
	                var temp;
	                var temp2;
	                for (var priority in nas) {
	                	var result = [];
	                    temp = nas[priority]; // temp = map of apiName => map of label => number of NAs
	                    for (var apiName in temp) {
	                    	var result2 = [];
	                    	temp2 = temp[apiName]; // temp2 = map of label => number of NAs
		                    for (var label in temp2) {
			                    var decimal = temp2[label];
			                    result2.push({
			                        key: label,
			                        value: decimal
			                    });
			                }
		                    result.push({
		                        key: apiName,
		                        value: result2
		                    });
		                }
	                    if (priority == '1')
	                    	component.set("v.naFirstColumn", result);
	                    if (priority == '2')
	                    	component.set("v.naSecondColumn", result);
	                    if (priority == '3')
	                    	component.set("v.naThirdColumn", result);
	                }
	            } else {
	                console.log('Problem getting Needs Attentions, response state: ' + state);
	            }
	        });
	        $A.enqueueAction(action);
        }
// end PD-1150
    }
})