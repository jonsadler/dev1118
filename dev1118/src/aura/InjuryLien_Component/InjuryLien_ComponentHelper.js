/*
	Created: lmsw - August 2017
	Purpose: PD-965 Create custom component for the InjuryLien Related list on the Lien Page.

History: 

	Updated: lmsw - Sept 2017 for Sept Release
	Purpose: PD-965 Modify drop down menu to follow standard

	Updated: lmsw - Sept 2017 for Sept Release
	Purpose: Add toast for failures
			 Only show valid Injuries to be linked to the Lien.
			 
	Updated: lmsw - Sept 2017 for September Release
    Purpose: PD-965 - Fix Injury list.

    Updated: lmsw - Sept 2017 for September Release
    Purpose: Refactor code and fix refresh for "New" button
*/
({
	CreateItems: function(component,event){
		var items = [
			{label: "Delete", value: "delete"},
			{label: "Edit", value: "edit"}
		];
		component.set("v.actions", items);
	},
	EditIL: function(component,event,helper) {
		var ilID = event.getSource().get("v.name");
		var ere = $A.get("e.force:editRecord");
		ere.setParams({"recordId": ilID});
		ere.fire();
		$A.get("e.force:refreshView").fire();
	},
	GetInjuryLiens : function(component, event) {
		var action = component.get("c.getInjuryLiens");
		action.setParams({"lId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				component.set("v.lienInjuries", response.getReturnValue());
			} else {
				console.log('Problem getting InjuryLiens, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	GetLien : function(component,helper) {
		var action = component.get("c.getLien");
		action.setParams({"lId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				var l = response.getReturnValue();
				component.set("v.lien", l);
				component.set("v.claimant", l.Claimant__c);
				component.set("v.currentAction", l.Action__c);
				component.set("v.recordType", l.Action__r.Name);
				this.GetInjuries(component);
				this.GetLinkedInjuries(component);
				this.GetTypeId(component);
			} else {
				console.log('Problem getting Lien, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	GetInjuries : function(component) {
		var action = component.get("c.getInjuriesMap");
		action.setParams({"cId": component.get("v.claimant"), "lId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				var iMap = response.getReturnValue();
				var result = [];
				var temp = [];
				for(var iId in iMap) {
					temp = iMap[iId];
					result.push({
						key: iId,
						value: temp
					});
				}
				component.set("v.injuries", result);
			} else {
				console.log('Problem getting Injuries Map, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	GetLinkedInjuries : function(component) {
		var action = component.get("c.getInjuryLinkedMap");
		action.setParams({"cId": component.get("v.claimant")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				var iMap = response.getReturnValue();
				var result = [];
				var temp = [];
				for(var iId in iMap) {
					temp = iMap[iId];
					result.push({
						key: iId,
						value: temp
					});
				}
				component.set("v.linkedInjuries", result);
			} else {
				console.log('Problem getting Linked Injuries Map, response state: '+ state);
			}
		});
		$A.enqueueAction(action);
	},
	GetTypeId : function(component) {
        var selectedType = component.get("v.recordType");
        console.log('recordType: '+ selectedType);
        var action = component.get("c.getInjuryTypeId");
        action.setParams({"injuryType": selectedType});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state ==="SUCCESS") {
                component.set("v.newInjuryType", response.getReturnValue());
            } else {
                console.log('Problem getting Injury type Id, response state: '+ state);
            }
        });
        $A.enqueueAction(action);
    },
    showToastDeleteError : function(component, event, helper) {
    	var toastEvent = $A.get("e.force:showToast");
    	toastEvent.setParams({
    		"title": "Fail!",
    		"message": "Deleting an existing InjuryLien was not successful.  Please contact a systems administrator with this information including the Lien Name.",
    		"type": "error",
    		"mode": "sticky"
    	});
    	toastEvent.fire();
    },
    showToastCreateError : function(component, event, helper) {
    	var toastEvent = $A.get("e.force:showToast");
    	toastEvent.setParams({
    		"title": "Fail!",
    		"message": "Creating a new InjuryLien was not successful.  Please contact a systems administrator with this information including the Lien Name.",
    		"type": "error",
    		"mode": "sticky"
    	});
    	toastEvent.fire();
    }
})