/*
	Created: lmsw - August 2017
	Purpose: PD-965 Create custom component for the InjuryLien Related list on the Lien Page.

History:

    Updated: lmsw -Sept 2017 for Sept Release
    Purpose: Modify from code review

    Updated: lmsw - Sept 2017 for September Release
    Purpose: Refactor code and fix refresh for "New" button
*/
({
	doInit : function(component, event, helper) {
        helper.GetInjuryLiens(component);
		helper.GetLien(component);
		helper.CreateItems(component,event);
	},
    onRadio : function(component, event, helper){
        var selected = event.getSource().get("v.text");
        component.set("v.injuryId", selected);
    },
    openTabwithSubtab: function(component, event, helper) {
        var newId = event.currentTarget.getAttribute("id");
        var url = '/'+newId;
        var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                    "url": url
                    });
                    urlEvent.fire();
    },
    // Create new Injury
    createInjury : function (component, event, helper) {
    	var newType = component.get("v.newInjuryType");
    	var claimantId = component.get("v.claimant");
    	var actionId = component.get("v.currentAction");
    	var createRecordEvent = $A.get("e.force:createRecord");
    	createRecordEvent.setParams({
    		"entityApiName": "Injury__c",
    		"recordTypeId": newType,
    		"defaultFieldValues": {
               'Claimant__c' : claimantId,
               'Action__c' : actionId
            }
    	});
    	createRecordEvent.fire();
    },
    // Delete InjuryLien
    DeleteIL: function(component, event,helper) {

        var ilID = component.get("v.deleteIL");
        var action = component.get("c.DeleteInjuryLien");
        action.setParams({"ilId": ilID});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS") {
                if(response.getReturnValue() === "FAIL")
                    helper.showToastDeleteError(component);
                $A.get("e.force:refreshView").fire();
            } else {
                console.log('Problem deleting InjuryLien, response state: '+ state);
                helper.showToastDeleteError(component);
            }
            component.set('v.activeModal', null);
            component.set('v.deleteIL', null);
        });
        $A.enqueueAction(action);
    }, 
    // Create new InjuryLien
    CreateIL: function(component, event,helper) {
        // Getting InjuryLien information
        var lId = component.get("v.recordId");
        var iId = component.get("v.injuryId");    
        var action = component.get("c.CreateInjuryLien");
        action.setParams({"lId": lId, "iId": iId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS") {
                if(response.getReturnValue() === "FAIL")
                    helper.showToastCreateError(component);
                $A.get("e.force:refreshView").fire();
                console.log('A new InjuryLien has been created.');
            } else {
                console.log('Problem creating/saving InjuryLien, response state: '+ state);
                helper.showToastCreateError(component);
            }
            component.set('v.activeModal', null);
            component.set("v.injuryId",null);
        });
        $A.enqueueAction(action);  
         
    }, 
    handleMenuSelect: function(component, event, helper) {
    	var selected = event.getParam("value");
    	if (selected == 'edit') 
    		helper.EditIL(component, event, helper);
    	else if (selected == 'delete') {
            var ilID = event.getSource().get("v.name");
            component.set("v.deleteIL", ilID);
            component.set('v.activeModal', 'deleteConfirm');
        }     
        $A.get("e.force:refreshView").fire();
    },
    newInjuryLienClick: function(component, event, helper) {
        component.set('v.activeModal', 'newInjuryLien');
        helper.GetInjuryLiens(component);
        helper.GetLien(component);
    },
    closeModal: function(component, event, helper) {
        component.set('v.activeModal', null);
    }
})