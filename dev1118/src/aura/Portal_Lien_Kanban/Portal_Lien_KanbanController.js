/* 
	Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-843 - Creation of the Claimant Kanban Status Component
    */ 
({
	doInit : function(component, event, helper) {

        var action4 = component.get("c.getStages");
        action4.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.stages", response.getReturnValue());
            } else {
                console.log('Problem getting stages, response state: ' + state);
            }
        });
        $A.enqueueAction(action4);

        var action5 = component.get("c.getLienMap2");
        action5.setParams({"cId": component.get("v.recordId")});
        action5.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lienMap", response.getReturnValue());
            } else {
                console.log('Problem getting Lien Maps 2, response state: ' + state);
            }
        });
        $A.enqueueAction(action5);

        var action6 = component.get("c.getLienTypeMap");
        action6.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var clMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.lienTypes", result);
            } else {
                console.log('Problem getting Lien Maps 2, response state: ' + state);
            }
        });
        $A.enqueueAction(action6);

        var action7 = component.get("c.getLienSubstageMap");
        action7.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var clMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.lienSubstages", result);
            } else {
                console.log('Problem getting Lien Substage Maps, response state: ' + state);
            }
        });
        $A.enqueueAction(action7);


        var action8 = component.get("c.getLienNonPLRPtype");
        action8.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var clMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.lienNonPLRPtype", result);
            } else {
                console.log('Problem getting Lien Non-PLRP type Maps, response state: ' + state);
            }
        });
        $A.enqueueAction(action8);
    } ,

     viewLienDetails: function(component, event, helper) {
        var newId = event.currentTarget.getAttribute("id");
        var url = '/lien/'+newId;
        var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                    "url": url
                    });
                    urlEvent.fire();
}

})