({
	updateNeedsAttention : function(component, naId) {
		var action7 = component.get("c.updateNeedsAttention");
                            action7.setParams({
                                "naId"  : naId});
                        action7.setCallback(component, function(response) {
                        var state = response.getState();
                        if (component.isValid() && state ==="SUCCESS") {
                            component.set("v.needsAttention", response.getReturnValue());
                        } else {
                            console.log('Problem updating Needs Attention, response state: ' + state);
                        }
                        });
                        $A.enqueueAction(action7);
	},

    checkBlanks : function(originaldata, claimantField) {
        if ($A.util.isUndefined(originaldata[claimantField])){
            originaldata[claimantField]=null;
            console.log('It worked!')
        };
    }
	
})