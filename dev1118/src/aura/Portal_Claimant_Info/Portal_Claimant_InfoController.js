/*  
	Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-862 - Creation of the Claimant Info Tab Component

History:

	Updated: lmsw - June 2017
	Purpose: PD-862 - Modified getLFClaimant

	Updated: lmsw - June 2017
	Purpose: PD-879 - Modified update function

    Updated: Marc Dysart - October 2017 for October Release
    Purpose: PD-1067 - Radio Button Not Displaying Properly on Edit Claimant Details Page


*/
({
	doInit : function(component, event, helper) {
		var action = component.get("c.getLFClaimant");
		action.setParams({"cId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.originalClaimant", response.getReturnValue());
            } else {
                console.log('Problem getting Claimant, response state: ' + state);
            }
        });   
        $A.enqueueAction(action);



        var action2 = component.get("c.getOptedInPLRP");
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var clMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        label: plif,
                        value: temp
                    });
                }
                component.set("v.optedInPLRP", result);
            } else {
                console.log('Problem getting Opted in PLRP Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);


        var action3 = component.get("c.gethandlePrivateLiens");
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var pMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in pMap) {
                    temp = pMap[plif];
                    result.push({
                        label: plif,
                        value: temp
                    });
                }
                component.set("v.handlePrivateLiens", result);
            } else {
                console.log('Problem getting Handle Private Liens Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action3);

        var action4 = component.get("c.getStates");
        action4.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var sMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var stif in sMap) {
                    temp = sMap[stif];
                    result.push({
                        label: stif,
                        value: temp
                    });
                }
                component.set("v.statesList", result);
            } else {
                console.log('Problem getting States Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action4);

//Start PD-1067
        var action5 = component.get("c.getGender");
        action5.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var sMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var stif in sMap) {
                    temp = sMap[stif];
                    result.push({
                        label: stif,
                        value: temp
                    });
                }
                component.set("v.genderList", result);
            } else {
                console.log('Problem getting Gender Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action5);
//End PD-1067

    },
    editInfo : function(component, event, helper) {
		component.set('v.editInfoShow', true);
	 }, 

	 saveClaimantInfo : function(component, event, helper) {
		component.set('v.editInfoShow', false);

        var originaldata = component.get("v.originalClaimant");

        // Apex does not include null values so in order to compare null to changes we need to add a null to the list

        helper.checkBlanks(originaldata, 'First_Name__c');
        helper.checkBlanks(originaldata, 'Middle__c');
        helper.checkBlanks(originaldata, 'Last_Name__c');
        helper.checkBlanks(originaldata, 'Opted_in_PLRP__c');
        helper.checkBlanks(originaldata, 'Providio_handling_private_liens__c');
        helper.checkBlanks(originaldata, 'SSN__c');
        helper.checkBlanks(originaldata, 'DOB__c');
        helper.checkBlanks(originaldata, 'DOD__c');
        helper.checkBlanks(originaldata, 'Address_1__c');
        helper.checkBlanks(originaldata, 'Address_2__c');
        helper.checkBlanks(originaldata, 'Gender__c');
        helper.checkBlanks(originaldata, 'City__c');
        helper.checkBlanks(originaldata, 'State__c');
        helper.checkBlanks(originaldata, 'Zip__c');
        helper.checkBlanks(originaldata, 'Phone__c');

        
		var claimantId = component.get("v.recordId"); 

        // This gets the new values that have been saved
        var firstName = component.find("firstName").get("v.value");
        var middleName = component.find("middleName").get("v.value");
        var optedInPLRP = component.find("optedInPLRP").get("v.value");
        var handlePrivateLiens = component.find("handlePrivateLiens").get("v.value");
        var lastName = component.find("lastName").get("v.value");
        var ssn = component.find("SSN").get("v.value");
        var dob = component.find("dob").get("v.value");
        var address1 = component.find("address1").get("v.value");
        var dod = component.find("dod").get("v.value");
        var address2 = component.find("address2").get("v.value");
        var gender = component.find("gender").get("v.value");
        var city = component.find("city").get("v.value");
        var state = component.find("state").get("v.value");
        var zip = component.find("zip").get("v.value");
        var phone = component.find("phone").get("v.value");

        var needsAttention = component.get("v.needsAttention");
        
        // This compares the new values with the original values
        for (key in originaldata) {
                    var value = originaldata[key];
                    
                    if ((key == 'First_Name__c') && (value != firstName)){
                        var na = needsAttention.Claimant_First_Name;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('First Name changed');
                    } 
                    // console.log('Original Name:'+value);


                    if ((key == 'Middle__c') && (value != middleName)){
                        var na = needsAttention.Claimant_Middle_Name;

                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('Middle Name changed');
                    } 
                    if ((key == 'Last_Name__c') && (value != lastName)){
                        var na = needsAttention.Claimant_Last_Name;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('Last Name changed');
                    } 
                    if ((key == 'Opted_in_PLRP__c') && (value != optedInPLRP)){
                        var na = needsAttention.Opted_in_PLRP;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('optedInPLRP changed');
                    } 
                    if ((key == 'Providio_handling_private_liens__c') && (value != handlePrivateLiens)){
                        var na = needsAttention.Providio_handling_private_liens;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('Providio handling private liens changed');
                    } 
                    if ((key == 'SSN__c') && (value != ssn)){
                        var na = needsAttention.SSN;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('SSN changed');
                    } 
                    if ((key == 'DOB__c') && (value != dob)){
                        var na = needsAttention.DOB;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('DOB changed');
                    } 
                    if ((key == 'DOD__c') && (value != dod)){
                        var na = needsAttention.DOD;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('DOD changed');
                    } 
                    if ((key == 'Address_1__c') && (value != address1)){
                        var na = needsAttention.Address_1;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('Address 1 changed');
                    } 
                    if ((key == 'Address_2__c') && (value != address2)){
                        var na = needsAttention.Address_2;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('Address 2 changed');
                    } 
                    if ((key == 'Gender__c') && (value != gender)){
                        var na = needsAttention.Gender;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('Gender changed');
                    } 
                    if ((key == 'City__c') && (value != city)){
                        var na = needsAttention.City;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('City changed');
                    }
                    if ((key == 'State__c') && (value != state)){
                        var na = needsAttention.State;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('State changed');
                    }  
                    if ((key == 'Zip__c') && (value != zip)){
                        var na = needsAttention.Zip_Code;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('Zip Code changed');
                    }
                    if ((key == 'Phone__c') && (value != phone)){
                        var na = needsAttention.Phone;
                        if(na){
                            var naId = na[0].Id;
                            helper.updateNeedsAttention(component, naId);
                        }
                        console.log('Phone Number changed');
                    }  
                }

        
        var action5 = component.get("c.UpdateLFClaimant");
        action5.setParams({
            "cId"  : claimantId,
            "firstName"  : firstName,
            "middleName"  : middleName,
            "optedInPLRP" : optedInPLRP,
            "handlePrivateLiens" : handlePrivateLiens,
            "lastName" : lastName,
            "ssn" : ssn,
            "dob" : dob,
            "address1" : address1,
            "dod" : dod,
            "address2" : address2,
            "gender" : gender,
            "city" : city,
            "state" : state,
            "zip" : zip,
            "phone" : phone
        });

        action5.setCallback(component, function(response) {
            var state = response.getState();
            if (component.isValid() && state ==="SUCCESS") {
                component.set("v.claimant", response.getReturnValue());
                
            } else {
                console.log('Problem updating claimant, response state: ' + state);
            }


            $A.get("e.force:refreshView").fire();
            component.set("v.selTabId", 'claimantinfo');
                var tab=component.get("v.selTabId");
        });
        $A.enqueueAction(action5);
	 },
	 
	 cancelSaveClaimantInfo : function(component, event, helper) {
		component.set('v.editInfoShow', false);
	 },

     // selectGender : function (component, event, helper) {
     //    var gender = event.target.value;
     //    component.set("v.gender", gender);
     // }

})