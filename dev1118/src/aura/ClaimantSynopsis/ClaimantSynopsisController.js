/* 
	Created: lmsw - January 2017 for March Release
    Purpose: PD-170 - Synopsis Panel Component for Claimant
*/
({
	doInit : function(component, event, helper) {
		var action = component.get("c.getLastRelease");
        action.setParams({"cId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lastRelease", response.getReturnValue());
            } else {
                console.log('Problem getting Last Eligible Disbursement, response state: ' + state);
            }
        });
        $A.enqueueAction(action);

	},
    openTabwithSubtab: function(component, event, helper) {
        var newId = event.currentTarget.getAttribute("id");
        var url = '/'+newId;
        var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                    "url": url
                    });
                    urlEvent.fire();
    }
})