({
	doInit: function (component, event, helper) {
		helper.claimantInfoDoInit(component, event);
	}
    , 
    tabSelected: function(component,event,helper) {
        // event.preventDefault();
        var tab = event.getSource();

        switch (tab.get('v.id')) {
            case 'liens' :
                console.log('lien tab selected');
                component.set('v.selTabId','liens');
                return false;
                break;
            case 'claimantinfo' :
                console.log('claimantinfo tab selected');
                component.set('v.selTabId','claimantinfo');
                break;
        }
        
    },

    handleActive: function (cmp, event) {
        var tab = event.getSource();
        switch (tab.get('v.id')) {
            case 'liens' :
            	console.log('lien tab selected');
                this.injectComponent('c:Portal_Lien_Kanban', tab);
                break;
            case 'claimantinfo' :
            	console.log('claimantinfo tab selected');
                this.injectComponent('c:Portal_Claimant_Info', tab);
                break;
        }
    },
    injectComponent: function (name, target) {
        $A.createComponent(name, {
        }, function (contentComponent, status, error) {
            if (status === "SUCCESS") {
                target.set('v.body', contentComponent);
            } else {
                // throw new Error(error);
                console.log('Failure');
            }
        });
    }
})