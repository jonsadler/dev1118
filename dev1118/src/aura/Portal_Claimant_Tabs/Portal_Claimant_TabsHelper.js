({
	claimantInfoDoInit : function(component, event) {
		var action = component.get("c.getLFClaimant");
		action.setParams({"cId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.claimant", response.getReturnValue());
            
                 console.log('Hello');
                var c= component.get("v.claimant");
                 var claimantLawfirm = c.Law_Firm__c;
                console.log(claimantLawfirm);
                 var availableOnPortal = c.Action__r.Available_in_Portal__c;  // PD-890
                console.log('Availabe'+availableOnPortal);
                    
                
                
            } else {
                console.log('Problem getting Claimant, response state: ' + state);
            }
        });   
        $A.enqueueAction(action);


        var action6 = component.get("c.getNeedsAttention");
        action6.setParams({"cId": component.get("v.recordId")});
        action6.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.needsAttention", response.getReturnValue());
                var temp = component.get("v.needsAttention");
                var count = 0;
                for (key in temp) { 
                    console.log('NA: '+key);                  
                    count++;
                }
                if(count>0){
                	component.set("v.alertTab", true);
                    component.set("v.selTabId", 'claimantinfo');
                } else {
                	component.set("v.alertTab", false);
                    component.set("v.selTabId", 'liens');
                    // This sets the tab if the claimant has just been saved
                        var claimant = component.get("v.claimant");
                        var lastupdated = claimant.LastModifiedDate;              
                        var min =.5; //It was saved in the last .5 min
                        var date = new Date();
                        var lastmin = new Date(date.getTime() - (min * 60 * 1000))
                        var dateupdated= new Date(lastupdated);
                        console.log(dateupdated +' _ '+ lastmin)
                        
                        if (dateupdated > lastmin){
                            component.set("v.selTabId", 'claimantinfo');
                            var temp = component.get("v.selTabId");
                            console.log(temp);
                        }  
                }

            } else {
                console.log('Problem getting Needs Attention Alerts, response state: ' + state);
            }
        });         
        $A.enqueueAction(action6);

        var action7 = component.get("c.getlawfirmID");
        action7.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lawFirmID", response.getReturnValue());   
            } else {
                console.log('Problem getting Users Law Firm Name, response state: ' + state);
            }
        });     
        $A.enqueueAction(action7);
    }
})