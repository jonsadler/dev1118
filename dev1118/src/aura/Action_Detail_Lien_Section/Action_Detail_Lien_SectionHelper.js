/*  
    Created: RAP - March 2017 for March Release
    Purpose: PD-454 - Action - Lien Tab - Lien Inventory Component

History: 

    Updated: lmsw - August 2017
    Purpose: PD-644 - Modify Header links to go to a report

    Updated: lmsw - Sept 2017 for September Release
    Purpose: Modify to open report in subtab. 
             Commented out, re-visit when the API has been released.
*/
({
    showMedicaid : function(component, event, helper) {
        var cmpMsg = component.find("Medicaid");
        var showButton = component.find("Medicaid_1");
        var hideButton = component.find("Medicaid_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideMedicaid : function(component,event) {
        var cmpMsg = component.find("Medicaid");
        var showButton = component.find("Medicaid_2");
        var hideButton = component.find("Medicaid_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showMedTrad : function(component, event, helper) {
        var cmpMsg = component.find("MedTrad");
        var showButton = component.find("MedTrad_1");
        var hideButton = component.find("MedTrad_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideMedTrad : function(component,event) {
        var cmpMsg = component.find("MedTrad");
        var showButton = component.find("MedTrad_2");
        var hideButton = component.find("MedTrad_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showMedMod : function(component, event, helper) {
        var cmpMsg = component.find("MedMod");
        var showButton = component.find("MedMod_1");
        var hideButton = component.find("MedMod_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideMedMod : function(component,event) {
        var cmpMsg = component.find("MedMod");
        var showButton = component.find("MedMod_2");
        var hideButton = component.find("MedMod_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showOtherGovt : function(component, event, helper) {
        var cmpMsg = component.find("OtherGovt");
        var showButton = component.find("OtherGovt_1");
        var hideButton = component.find("OtherGovt_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideOtherGovt : function(component,event) {
        var cmpMsg = component.find("OtherGovt");
        var showButton = component.find("OtherGovt_2");
        var hideButton = component.find("OtherGovt_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showNonP : function(component, event, helper) {
        var cmpMsg = component.find("NonP");
        var showButton = component.find("NonP_1");
        var hideButton = component.find("NonP_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideNonP : function(component,event) {
        var cmpMsg = component.find("NonP");
        var showButton = component.find("NonP_2");
        var hideButton = component.find("NonP_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showPLRPCS : function(component, event, helper) {
        var cmpMsg = component.find("PLRPCS");
        var showButton = component.find("PLRPCS_1");
        var hideButton = component.find("PLRPCS_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hidePLRPCS : function(component,event) {
        var cmpMsg = component.find("PLRPCS");
        var showButton = component.find("PLRPCS_2");
        var hideButton = component.find("PLRPCS_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showPLRPMod : function(component, event, helper) {
        var cmpMsg = component.find("PLRPMod");
        var showButton = component.find("PLRPMod_1");
        var hideButton = component.find("PLRPMod_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hidePLRPMod : function(component,event) {
        var cmpMsg = component.find("PLRPMod");
        var showButton = component.find("PLRPMod_2");
        var hideButton = component.find("PLRPMod_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    getCurrentAction : function(component){
        var action = component.get("c.getAction");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.currentAction", response.getReturnValue());
            } else {
                console.log('Problem getting Action, response state: ' + state);
            }
        });
        $A.enqueueAction(action); 
    },
    /*getReportLink : function(component) {
        var reportLink = component.get('v.reportLink');
        var currentAction = component.get('v.currentAction.Name');
        
        var type_stage = event.currentTarget.getAttribute("id");
        console.log("id: " + type_stage);
        var lienType = component.get(type_stage.substring(0, type_stage.indexOf('-')));
        var stage = type_stage.substring(type_stage.indexOf('-')+1);
        // Generate report url
        var url = reportLink+'fv0='+currentAction+'&fv1='+lienType+'&fv2='+stage;
        component.set('v.reportURL', url);
    }*/
})