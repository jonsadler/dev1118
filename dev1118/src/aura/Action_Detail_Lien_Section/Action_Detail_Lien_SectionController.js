/*  
    Created: RAP - March 2017 for March Release
    Purpose: PD-454 - Action - Lien Tab - Lien Inventory Component

History: 

    Updated: lmsw - August 2017
    Purpose: PD-644 - Modify Header links to go to a report

    Updated: lmsw - Sept 2017 for September Release
    Purpose: Modify to open report in subtab.
             Commented out, re-visit when the API has been released.

*/
({
    doInit : function(component, event, helper) {
        // Get report links from custom labels
        var rl = $A.get("$Label.c.Report_LienReportLink");
        var rsl = $A.get("$Label.c.Report_LienSubReportLink");
        var rnl = $A.get("$Label.c.Report_LienNoticeReportLink");
        component.set("v.reportLink", rl);
        component.set("v.reportSubLink", rsl);
        component.set("v.reportNoticeLink", rnl);

        var action = component.get("c.getLienMetrics");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	component.set("v.lienMap", response.getReturnValue());
            } else {
                console.log('Problem getting lien map, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        var action1 = component.get("c.getFilterMap");
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
	            var fMap = response.getReturnValue();
	            var temp;
            	var result = [];
	            for (var pung in fMap) {
	            	temp = fMap[pung];
	       			result.push({
	       				key: pung,
	       				value: temp
	       			});
	       		}
	            component.set("v.filterMap", result);
            } else {
                console.log('Problem getting filters, response state: ' + state);
            }
        });
        $A.enqueueAction(action1);    
        var action1a = component.get("c.getLienSubstageMap");
        action1a.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
	            var fMap = response.getReturnValue();
	            var temp;
            	var result = [];
	            for (var pung in fMap) {
	            	temp = fMap[pung];
	       			result.push({
	       				key: pung,
	       				value: temp
	       			});
	       		}
       			result.push({
       				key: 'No_Response_in_30_Days',
       				value: 'No Response in 30 Days'
       			});
	            component.set("v.subStageMap", result);
            } else {
                console.log('Problem getting substages, response state: ' + state);
            }
        });
        $A.enqueueAction(action1a);    
        var action2 = component.get("c.getStagesMap");
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
	            var stMap = response.getReturnValue();
	            var temp;
            	var result = [];
	            for (var flit in stMap) {
	            	temp = stMap[flit];
	       			result.push({
	       				key: temp,
	       				value: flit
	       			});
	       		}
	            component.set("v.stagesMap", result);
            } else {
                console.log('Problem getting stages, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);    
        var action3 = component.get("c.getStageMetrics");
        action3.setParams({"aId": component.get("v.recordId")});
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
	            var smMap = response.getReturnValue();
	            var temp = [];
	            var temp2 = [];
	            var temp3 = [];
	            for (var lienType in smMap) {
	            	temp = smMap[lienType]; // temp keyset is lien type
	       			var result = [];
	            	for (var stage in temp) {
		       			temp2 = temp[stage]; // temp2 keyset is stage
		       			var result2 = [];
	            		for (var filbin in temp2) {
		            		temp3 = temp2[filbin]; // temp3 keyset is substage
			       			result2.push({
			       				key: filbin,
			       				value: temp3
			       			});
			       		}
		       			result.push({
		       				key: stage,
		       				value: result2
		       			});
		       		}
		       		if (lienType === 'All')
		       			component.set("v.allMap", result);
		       		else if (lienType === 'Medicaid')
		       			component.set("v.mcMap", result);
		       		else if (lienType === 'Medicare_Traditional')
		       			component.set("v.mtMap", result);
		       		else if (lienType === 'Medicare_Modeled')
		       			component.set("v.mmMap", result);
		       		else if (lienType === 'Other_Govt')
		       			component.set("v.ogMap", result);
		       		else if (lienType === 'Non_PLRP')
		       			component.set("v.npMap", result);
		       		else if (lienType === 'PLRP_Claims_Submission')
		       			component.set("v.pcsMap", result);
		       		else if (lienType === 'PLRP_Modeled')
		       			component.set("v.pmMap", result);
	            }
            } else {
                console.log('Problem getting stageMap, response state: ' + state);
            }
        });
        $A.enqueueAction(action3); 
        helper.getCurrentAction(component); 
    },
    showMedicaid : function(component, event, helper) {
        helper.showMedicaid(component,event);
    },
    hideMedicaid : function(component, event, helper) {
        helper.hideMedicaid(component,event);
    },
    showMedTrad : function(component, event, helper) {
        helper.showMedTrad(component,event);
    },
    hideMedTrad : function(component, event, helper) {
        helper.hideMedTrad(component,event);
    },
    showMedMod : function(component, event, helper) {
        helper.showMedMod(component,event);
    },
    hideMedMod : function(component, event, helper) {
        helper.hideMedMod(component,event);
    },
    showOtherGovt : function(component, event, helper) {
        helper.showOtherGovt(component,event);
    },
    hideOtherGovt : function(component, event, helper) {
        helper.hideOtherGovt(component,event);
    },
    showNonP : function(component, event, helper) {
        helper.showNonP(component,event);
    },
    hideNonP : function(component, event, helper) {
        helper.hideNonP(component,event);
    },
    showPLRPCS : function(component, event, helper) {
        helper.showPLRPCS(component,event);
    },
    hidePLRPCS : function(component, event, helper) {
        helper.hidePLRPCS(component,event);
    },
    showPLRPMod : function(component, event, helper) {
        helper.showPLRPMod(component,event);
    },
    hidePLRPMod : function(component, event, helper) {
        helper.hidePLRPMod(component,event);
    },
    showAll : function(component, event, helper) {
    	helper.showMedicaid(component,event);
    	helper.showMedTrad(component,event);
    	helper.showMedMod(component,event);
    	helper.showOtherGovt(component,event);
    	helper.showNonP(component,event);
    	helper.showPLRPCS(component,event);
    	helper.showPLRPMod(component,event);
        var showButton = component.find("hideAll");
        var hideButton = component.find("showAll");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideAll : function(component, event, helper) {
    	helper.hideMedicaid(component,event);
    	helper.hideMedTrad(component,event);
    	helper.hideMedMod(component,event);
    	helper.hideOtherGovt(component,event);
    	helper.hideNonP(component,event);
    	helper.hidePLRPCS(component,event);
    	helper.hidePLRPMod(component,event);
        var hideButton = component.find("hideAll");
        var showButton = component.find("showAll");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
/***************************/
/* Open Reports in Subtabs */
/***************************/
/*    openReportinSubtab: function(component, event, helper) {
       // helper.resetURL(component);

       // $A.get("$Site.absCoreUrl") + url;
        helper.getReportLink(component);

        var url = component.get('v.reportURL');

        
        console.log('url: '+ url);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect": false
        });
        urlEvent.fire();
       
    }*/
})