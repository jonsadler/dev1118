({
	/* 
	Created: Marc Dysart - June 2017 for July Release
    Purpose: PD-866 - Creation of the Status Tab Component on the Liens Page
	*/
    doInit : function(component, event, helper) {
        var action = component.get("c.getLienAmtList");
        var lienID = component.get("v.recordId");
        console.log('lienID :'+lienID);
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('Lien one!');
            if (component.isValid() && state === "SUCCESS") {
            	console.log('Lien two!');
                component.set("v.lienamts", response.getReturnValue());
                var lienamts = component.get("v.lienamts");
                console.log('lienamts:'+lienamts);
                
            } else {
                console.log('Problem getting lien amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);

    }

}