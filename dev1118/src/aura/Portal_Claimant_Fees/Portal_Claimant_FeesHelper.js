/*
	Created: lmsw - November 2017 for December Release
	Purpose: PD-1268 - Change Fee language for CBF
*/
({
	GetClaimantInfo : function(component,event,helper) {
		var action = component.get("c.getClaimant");
		action.setParams({"cId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				component.set("v.claimant", response.getReturnValue());
				component.set("v.havePaidBy", true);
			} else {
			console.log('Problem getting Claimant Information, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	}
})