/*  
    Created: lmsw - June 2017 for June Release
    Purpose: PD-865 - Claimant Fees for the portal

History:

    Updated: lmsw - November 2017 for December Release
    Purpose: PD-1268 - Change Fee language for CBF
*/
({
    doInit : function(component, event, helper) {
        // var action1 = component.get("c.getFeesForClaimant");
        // action1.setParams({"cId": component.get("v.recordId")});
        // action1.setCallback(this, function(response) {
        //     var state = response.getState();
        //     if (component.isValid() && state === "SUCCESS") {
        //         var fMap = response.getReturnValue(); 
        //         var result = [];
        //         var temp = [];
        //         for (var plif in fMap) {
        //             temp = fMap[plif];
        //             result.push({
        //                 key: plif,
        //                 value: temp
        //             });
        //             // console.log('fmap:'+temp.Date_Invoiced__c);
        //         }
        //         console.log('result:'+result[0].Date_Invoiced__c);
	       //      component.set("v.fees", result);
        //     } else {
        //         console.log('Problem getting feeMap, response state: ' + state);
        //     }
        // });
        // $A.enqueueAction(action1);

        
        // var action2 = component.get("c.getFeeAwards");
        // action2.setParams({"cId": component.get("v.recordId")});
        // action2.setCallback(this, function(response) {
        //     var state = response.getState();
        //     if (component.isValid() && state === "SUCCESS") {
        //         component.set("v.awards", response.getReturnValue());
        //     } else {
        //         console.log('Problem getting awards, response state: ' + state);
        //     }
        // });
        // $A.enqueueAction(action2);


        var action3 = component.get("c.getAllFees");
        action3.setParams({"cId": component.get("v.recordId")});
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var fMap = response.getReturnValue(); 
                var temp = [];
                var fees = [];
                var feeMap = [];
                var feesTotal = 0;
                for (var plif in fMap) { // plif is a type/subtype combination
                    var middle = [];
                    temp = fMap[plif]; // temp is a map of Award IDs to Amounts
                    var feeTotal = 0;
                    for (var plig in temp) { // plig is an awardId
                        vipl = temp[plig]; // vipl is an amount
                        feeTotal += vipl;
                        middle.push({
                            key: plig,
                            value: vipl
                        });
                    }
                    feesTotal += feeTotal;
                    fees.push({
                        key: plif,
                        value: middle
                    });
                    feeMap.push({
                        key: plif,
                        value: feeTotal                        
                    })
                }
                component.set("v.fees", fees);
                // console.log('Fees'+ fees[0].key);
                component.set("v.feesTotal", feesTotal);
                component.set("v.feeMap", feeMap);
            } else {
                console.log('Problem getting fees, response state: ' + state);
            }
        });
        $A.enqueueAction(action3);
        var action1a = component.get("c.getFeeTypes");
        action1a.setParams({"cId": component.get("v.recordId")});
        action1a.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var feeTypes = response.getReturnValue();
                console.log('feeTypes: ' + feeTypes);
                var result = [];
                var temp;
                for (var plif in feeTypes) { // plif is a type/subtype pair
                    temp = feeTypes[plif]; // temp is plif with spaces instead of underscores
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.feeEnum", result);
            } else {
                console.log('Problem getting feeTypes, response state: ' + state);
            }
        });
        $A.enqueueAction(action1a);
        var action1b = component.get("c.getACDs");
        action1b.setParams({"cId": component.get("v.recordId")});
        action1b.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var temp = response.getReturnValue();
                var mapping;
                var acdTotal = 0;
                var acdsTotal = 0;
                var discTotal = 0;
                var acds = [];
                var result = [];
                var mapper = [];
                var totsMap = [];
                for (var acdType in temp) {
                    var first = true;
                    var mapitem = acdType.split('_');
                    var wamp = temp[acdType]; // wamp is a list of award Id / amount pairs
                    discTotal = 0;
                    acdTotal = 0;
                    var middle = [];
                    for (var blub in wamp) {
                        var wonk = wamp[blub];
                        middle.push({
                            key: blub, // action Id
                            value: wonk // amount
                        });
                        if (first) {
                            mapper.push({
                                key: acdType,
                                value: mapitem.join(' ')
                            });
                            first = false;
                        }
                        if (acdType == 'Fee_Reduction') {
                            discTotal += wonk;
                        } else {
                            acdTotal += wonk;
                            acdsTotal += wonk;
                        }
                    }
                    totsMap.push({
                        key: acdType,
                        value: acdTotal
                    });
                    result.push({
                        key: acdType,
                        value: middle
                    });
                }
                component.set("v.totsMap", totsMap);
                component.set("v.acdTotal", acdTotal);
                component.set("v.acdsTotal", acdsTotal);
                component.set("v.acds", result);
                component.set("v.discTotal",discTotal);
                component.set("v.acdMap", mapper);
            } else {
                console.log('Problem getting acds, response state: ' + state);
            }
        });
        $A.enqueueAction(action1b);
        helper.GetClaimantInfo(component);
    }
})