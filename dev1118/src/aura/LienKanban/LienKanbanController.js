/*
    Created: Marc Dysart - February 2017 for first release
    Purpose: PD-115 -Kanban Lien - Claimant

 History:

    Updated: Marc Dysart - March 2017 for first release
    Purpose: PRODSUPT-10 - Add substage in Kanban View

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 - Modify to use Lien Record Types

    Updated: Marc Dysart - April 2017
    Purpose: PRODSUPT-83 - Kanban: Adjustments to cards for PLRP and Non PLRP liens.

    Updated: lmsw - May 2017
    Purpose: PD-615 - Add methods for the "New" button to add a new Lien

    Updated: lmsw - July 2017
    Purpose: PD-895 - Pre-populate Claimant Name on new liens created from Claimant page.

    Updated: lmsw - Sept 2017 for Sept Release
    Purpose: PD-985 - Pre-populate Action on new liens created from Claimant page.

    Updated: CPS - October 2017
    Purpose: PD-1169 - Refresh lien kanban when Lien, LNA, or Override Final Lien Amount are updated

    Updated: lmsw - December 2017 for January Release
    Purpose: PD-1214 - Fix getting correct Lien record type bug
*/
({
    doInit : function(component, event, helper) {
        // Start PD-1169 (moved Apex calls from controller function into their own helper functions, and added new initialize function)
        helper.initialize(component, helper);
    },

    editLien : function(component, event, helper) {
        var lienID = event.target.id;
        var editRecordEvent = $A.get("e.force:editRecord");
            editRecordEvent.setParams({
          "recordId": lienID
       });
        editRecordEvent.fire();
    },

// start PD-614
    showModal : function(component, event, helper) {
        helper.showPopup(component, 'recordTypeModal', 'slds-fade-in-');
        helper.showPopup(component, 'backdrop', 'slds-backdrop--');
// PD-1214      //helper.getTypeId(component);
    },

    hideModal : function(component, event, helper) {
        helper.hidePopup(component, "recordTypeModal", 'slds-fade-in-');
        helper.hidePopup(component, 'backdrop', 'slds-backdrop--');
    },
// start PD-1214
    onRadio : function(component, event, helper ){
        var selected = event.getSource().get("v.text");
        //component.set("v.selectedType", selected);
        component.set("v.newLienType", selected);
        //helper.getTypeId(component);
    },
// end PD-1214
    createLien : function (component, event, helper) {
        var newType = component.get("v.newLienType");
    // start PD-895
        var claimant = component.get("v.claimant");
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Lien__c",
            "recordTypeId": newType,
            "defaultFieldValues": {
               'Claimant__c' : component.get("v.recordId"),
               'Action__c' : claimant.Action__c
            }
// end PD-895
        });
        createRecordEvent.fire();
        helper.hidePopup(component, "recordTypeModal", 'slds-fade-in-');
        helper.hidePopup(component, 'backdrop', 'slds-backdrop--');
    },
// end PD-614

    allowDrop: function(cmp, event, helper){
        event.preventDefault();
    },

    drag: function(cmp, ev, helper){
        ev.dataTransfer.setData("text", ev.target.id);
        var draggedLienID = ev.target.id;
        ev.target.classList.add("draggedCard");
    },

    drop: function(cmp, ev, helper){
        ev.preventDefault();
        var el = ev.srcElement;
        if ( ev.target.className == "listcontent" ) {
            var newstage = ev.target.id;
            var cards = document.getElementsByClassName("draggedCard");
            for(var i = 0; i < cards.length; ++i){
                var draggedLienID = cards.item(i).id
                helper.saveNewStage(cmp, newstage, draggedLienID);
// start PRODSUPT-10
                var onecard = cards.item(i);
                var substage_label = onecard.getElementsByClassName("lien-substage")[0];
                if (substage_label != null) {
                   substage_label.classList.add("hide-substage");
                 }
// end PRODSUPT-10
                cards.item(i).classList.remove("draggedCard")
            }
            var data = ev.dataTransfer.getData("text");
            el.appendChild(document.getElementById(data));
        }
    },

    showLienType: function(component, event, helper){
         var lienNames = document.getElementsByClassName("lienTypeName");
         for(var i = 0; i < lienNames.length; ++i){
            var selected = lienNames.item(i).get("v.value");
            selected = selected.replace("_",/ /g);
        }

    },

    medicaidFilter: function(component, event, helper){
        helper.hideCards(component);
        // This shows only the appropriate cards
        helper.showCorrectCards(component, "Medicaid");
        // This shows the button as selected
        // helper.showButtonSelected(component, event);

        // Change label of the filter for dropdown filter
        helper.updateTriggerLabel(component,event);

    },

     medicareFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "Medicare Individual Resolution");
        helper.updateTriggerLabel(component,event);
    },

    medicareModeledFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "Medicare Global Model");
        helper.updateTriggerLabel(component,event);
    },

    nonPFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "Non-PLRP");
        helper.updateTriggerLabel(component,event);
    },

    plrpFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "PLRP Claim Detail");
        helper.updateTriggerLabel(component,event);
    },

    plrpModeledFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "PLRP Model");
        helper.updateTriggerLabel(component,event);
    },

    othergovtFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "Other Govt");
        helper.updateTriggerLabel(component,event);
    },

    allLienFilter: function(component, event, helper){
        helper.hideCards(component);
        var cards = document.getElementsByClassName("lien-card");
        for(var i = 0; i < cards.length; ++i){
            cards.item(i).classList.remove("hideCard")
        }
        helper.updateTriggerLabel(component,event);
    },

    updateTriggerLabel: function(cmp, event) {
        var triggerCmp = cmp.find("trigger");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.label", label);
        }
    },
    updateLabel: function(cmp, event) {
        var triggerCmp = cmp.find("mytrigger");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.label", label);
        }
    },
    getMenuSelected: function(cmp) {
        var menuItems = cmp.find("checkbox");
        var values = [];
        for (var i = 0; i < menuItems.length; i++) {
            var c = menuItems[i];
            if (c.get("v.selected") === true) {
                values.push(c.get("v.label"));
            }
        }
        var resultCmp = cmp.find("result");
        resultCmp.set("v.value", values.join(","));
    },
    getRadioMenuSelected: function(cmp) {
        var menuItems = cmp.find("radio");
        var values = [];
        for (var i = 0; i < menuItems.length; i++) {
            var c = menuItems[i];
            if (c.get("v.selected") === true) {
                values.push(c.get("v.label"));
            }
        }
        var resultCmp = cmp.find("radioResult");
        resultCmp.set("v.value", values.join(","));
    },
// Start PD-905
   openTabwithSubtab: function(component, event, helper) {
        var newId = event.currentTarget.getAttribute("id");
        var url = '/'+newId;
        var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                    "url": url
                    });
                    urlEvent.fire();
    },
// PD-905

// Start PD-1169
    handleLienUpdated : function (component, event, helper) {
        var eventSource        = event.getSource();
        var lienId             = eventSource.get('v.recordId');
        var lienIdsInitialized = component.get('v.lienIdsInitialized');

        if (lienIdsInitialized.includes(lienId)) {
            // This is a "real" update to a Lien, which we need to refresh the kanban on.
            console.log('DEBUG: (handleLienUpdated) Detected update to lienId = ' + lienId + '.  Refreshing kanban.');

            helper.initialize(component, helper);
        }
        else {
            // This is the first recordUpdated event for this Lien, which means
            // it's being loaded by force:recordData for the first time after an initialization,
            // and we don't need to refresh the kanban for it.
            lienIdsInitialized.push(lienId);
            component.set('v.lienIdsInitialized', lienIdsInitialized);
        }
    },

    handleLienNegotiatedAmountUpdated : function (component, event, helper) {
        var eventSource                        = event.getSource();
        var lienNegotiatedAmountId             = eventSource.get('v.recordId');
        var lienNegotiatedAmountIdsInitialized = component.get('v.lienNegotiatedAmountIdsInitialized');

        if (lienNegotiatedAmountIdsInitialized.includes(lienNegotiatedAmountId)) {
            // This is a "real" update to a Lien Negotiated Amount, which we need to refresh the kanban on.
            console.log('DEBUG: (handleLienNegotiatedAmountUpdated) Detected update to lienNegotiatedAmountId = ' + lienNegotiatedAmountId + '.  Refreshing kanban.');

            helper.initialize(component, helper);
        }
        else {
            // This is the first recordUpdated event for this Lien Negotiated Amount, which means
            // it's being loaded by force:recordData for the first time after an initialization,
            // and we don't need to refresh the kanban for it.
            lienNegotiatedAmountIdsInitialized.push(lienNegotiatedAmountId);
            component.set('v.lienNegotiatedAmountIdsInitialized', lienNegotiatedAmountIdsInitialized);
        }
    },

    handleForceShowToast : function (component, event, helper) {
        var eventMessage = event.getParam('message');

        if ($A.util.isUndefinedOrNull(eventMessage)) {
            return;
        }

        if (eventMessage.includes('Lien was created')) {
            console.log('DEBUG: (LienKanbanController.handleForceShowToast) Detected Lien creation.  Refreshing kanban.');
            helper.initialize(component, helper);
        }
        else if (eventMessage.includes('Lien Negotiated Amount was created')) {
            console.log('DEBUG: (LienKanbanController.handleForceShowToast) Detected Lien Negotiated Amount creation.  Refreshing kanban.');
            helper.initialize(component, helper);
        }
    },

    handleRecordsLoaded : function (component, event, helper) {
        var lienMapPendingLoaded        = component.get('v.lienMapPendingLoaded');
        var liensLoaded                 = component.get('v.liensLoaded');
        var stagesLoaded                = component.get('v.stagesLoaded');
        var lienTypesLoaded             = component.get('v.lienTypesLoaded');
        var lienTypesIdsLoaded          = component.get('v.lienTypesLoaded');
        var lienSubstagesLoaded         = component.get('v.lienSubstagesLoaded');
        var lienNonPLRPtypeLoaded       = component.get('v.lienNonPLRPtypeLoaded');
        var lienNegotiatedAmountsLoaded = component.get('v.lienNegotiatedAmountsLoaded');

        // If all records have been loaded, move the lienMapPending
        // to lienMap.  This will drive the aura:iteration and aura:if
        // elements in the markup to begin rendering the data.
        if (lienMapPendingLoaded &&
            liensLoaded &&
            stagesLoaded &&
            lienTypesLoaded &&
            lienTypesIdsLoaded &&
            lienSubstagesLoaded &&
            lienNonPLRPtypeLoaded &&
            lienNegotiatedAmountsLoaded) {

            component.set('v.lienMap', component.get('v.lienMapPending'));
        }
    },
// End PD-1169

})