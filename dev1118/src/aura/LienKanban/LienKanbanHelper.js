/*
    Created: Marc Dysart - February 2017 for first release
    Purpose: PD-115 -Kanban Lien - Claimant

History:

    Updated: lmsw - May 2017
    Purpose: PD-614 - Add method to get Lien record type ID for the "New" button to add a new Lien

    Updated: lmsw - July 2017
    Purpose: PD-895 - Pre-populate Claimant Name on new liens created from Claimant page.

    Updated: lmsw - Sept 2017 for Sept Release
    Purpose: PD-985 - Pre=populate Action Name on new liens created from Claimant page.

    Updated: CPS - October 2017
    Purpose: PD-1169 - Refresh lien kanban when Lien, LNA, or Override Final Lien Amount are updated

    Updated: lmsw - December 2017 for January Release
    Purpose: PD-1214 - Fix getting correct Lien record type bug
*/
({
	hideCards : function(component) {
		 // This shows all the cards first if they were hidden
        var cards = document.getElementsByClassName("hideCard");
        for(var i = 0; i < cards.length; ++i){
            cards.item(i).classList.remove("hideCard")
        }

        // Then hides all the cards

        var cards = document.getElementsByClassName("lien-card");
        for(var i = 0; i < cards.length; ++i){
            cards.item(i).classList.add("hideCard")
        }
	},

	saveNewStage : function(component, newstage, lienId) {
        var action5 = component.get("c.updateLienCard");
        action5.setParams({
            "lId" : lienId,
            "stage"  : newstage
        });
        $A.enqueueAction(action5);
    },

     // This shows only the appropriate cards
    showCorrectCards : function(component, cardName) {
    	var cards = document.getElementsByClassName(cardName);
	        for(var i = 0; i < cards.length; ++i){
	            cards.item(i).classList.remove("hideCard");
	        };
    },

    // This changes the button to look selected
    // This can be deleted if it is confirmed that no Filter buttons are needed
    showButtonSelected: function(component, event) {
        var cards = document.getElementsByClassName("slds-button--brand");
            for(var i = 0; i < cards.length; ++i){
                cards.item(i).classList.remove("slds-button--brand")
                cards.item(i).classList.add("slds-not-selected")
            }
        var thisbutton= event.target;
        $A.util.removeClass(thisbutton, 'slds-not-selected');
        $A.util.addClass(thisbutton, 'slds-button--brand');
    },

    updateTriggerLabel: function(cmp, event) {
        var triggerCmp = cmp.find("trigger");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.label", label);
        }
    },
// start PD-614 PD-1214
/*    getTypeId : function(component) {
        var selectedType = component.get("v.selectedType");
        console.log('recordType: '+ selectedType);
        var action = component.get("c.getLienTypeId");
        action.setParams({"lienType": selectedType});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state ==="SUCCESS") {
                component.set("v.newLienType", response.getReturnValue());
                console.log('newLienType: '+ response.getReturnValue());
            } else {
                console.log('Problem getting lien type Id, response state: '+ state);
            }
        });
        $A.enqueueAction(action);
    },
*/
// end PD-1214
    showPopup : function(component, divId, className) {
        var modal = component.find(divId);
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal, className+'open')
    },

    hidePopup : function(component, divId, className) {
        var modal = component.find(divId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },
// end PD-614
// start PD-895
    getClaimant : function(component, helper) {
        var action = component.get("c.getClaimant");
        action.setParams({"cId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.claimant", response.getReturnValue());
            } else {
                console.log('Problem getting claimant, response state: '+ state);
            }
        });
        $A.enqueueAction(action);
    },
// end PD-895

// Start PD-1169
    initialize : function (component, helper) {
        helper.clearRecordsAttributes(component, helper);
        helper.clearRecordsLoadedAttributes(component, helper);

        // Reset the list of Lien IDs initialized, so we don't get into a loop refreshing the kanban.
        component.set('v.lienIdsInitialized', []);
        component.set('v.lienNegotiatedAmountIdsInitialized', []);

        // Get the lien related records.
        helper.getLiens(component, helper);
        helper.getStages(component, helper);
        helper.getLienMap2(component, helper);
        helper.getLienTypeMap(component, helper);
        helper.getLienTypeIDMap(component, helper);     //PD-1214
        helper.getLienSubstageMap(component, helper);
        helper.getLienNonPlrpType(component, helper);
        helper.getLienNegotiatedAmounts(component, helper);

        helper.getClaimant(component, helper);      //PD-895
    },

    clearRecordsAttributes : function (component, helper) {
        // Make sure that lienMap is cleared first, because it's the
        // highest-level attribute used by aura:iteration in the markup.
        component.set('v.lienMap', {});
        component.set('v.liens', []);
        component.set('v.stages', []);
        component.set('v.lienTypes', []);
        component.set('v.lienTypesIds',[]);     //PD-1214
        component.set('v.lienSubstages', []);
        component.set('v.lienNonPLRPtype', []);
        component.set('v.lienNegotiatedAmounts', []);
    },

    clearRecordsLoadedAttributes : function (component, helper) {
        component.set('v.lienMapPendingLoaded', false);
        component.set('v.liensLoaded', false);
        component.set('v.stagesLoaded', false);
        component.set('v.lienTypesLoaded', false);
        component.set('v.lienTypesIdsLoaded', false);       //PD-1214
        component.set('v.lienSubstagesLoaded', false);
        component.set('v.lienNonPLRPtypeLoaded', false);
        component.set('v.lienNegotiatedAmountsLoaded', false);
    },

    getLiens : function (component, helper) {
        var action = component.get('c.getLiens');
        action.setParams({'cId': component.get('v.recordId')});
        action.setCallback(this, function getLiensCallback(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS') {
                component.set('v.liens', response.getReturnValue());
                component.set('v.liensLoaded', true);
            } else {
                console.log('ERROR: (getLiensCallback) Problem getting liens, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },

    getStages : function (component, helper) {
        var action = component.get('c.getStages');
        action.setCallback(this, function getStagesCallback(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS') {
                component.set('v.stages', response.getReturnValue());
                component.set('v.stagesLoaded', true);
            } else {
                console.log('ERROR: (getStagesCallback) Problem getting stages, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },

    getLienMap2 : function (component, helper) {
        var action = component.get('c.getLienMap2');
        action.setParams({'cId': component.get('v.recordId')});
        action.setCallback(this, function getLienMap2Callback(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS') {
                component.set('v.lienMapPending', response.getReturnValue());
                component.set('v.lienMapPendingLoaded', true);
            } else {
                console.log('ERROR: (getLienMap2Callback) Problem getting Lien Maps 2, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },

    getLienTypeMap : function (component, helper) {
        var action = component.get('c.getLienTypeMap');
        action.setCallback(this, function getLienTypeMapCallback(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS') {
                var clMap = response.getReturnValue();
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set('v.lienTypes', result);
                component.set('v.lienTypesLoaded', true);
            } else {
                console.log('ERROR: (getLienTypeMap) Problem getting Lien Maps 2, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
// start PD-1214
    getLienTypeIDMap : function (component, helper) {
        var action = component.get('c.getLienTypeIDMap');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS') {
                var clMap = response.getReturnValue();
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    if(plif == "Medicaid") {
                        component.set('v.MedicaidId', clMap[plif]);
                        component.set('v.newLienType', clMap[plif]);
                    }
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set('v.lienTypesIds', result);
                component.set('v.lienTypesIdsLoaded', true);
            } else {
                console.log('ERROR: (getLienTypeIdMap) Problem getting Lien Type Record Ids, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
// end PD-1214
    getLienSubstageMap : function (component, helper) {
        // start PRODSUPT-10
        var action = component.get('c.getLienSubstageMap');
        action.setCallback(this, function getLienSubstageMapCallback(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS') {
                var clMap = response.getReturnValue();
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set('v.lienSubstages', result);
                component.set('v.lienSubstagesLoaded', true);
            } else {
                console.log('ERROR: (getLienSubstageMapCallback) Problem getting Lien Substage Maps, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        // end PRODSUPT-10
    },

    getLienNonPlrpType : function (component, helper) {
        // start PRODSUPT-83
        var action = component.get('c.getLienNonPLRPtype');
        action.setCallback(this, function getLienNonPlrpTypeCallback(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS') {
                var clMap = response.getReturnValue();
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set('v.lienNonPLRPtype', result);
                component.set('v.lienNonPLRPtypeLoaded', true);
            } else {
                console.log('ERROR: (getLienNonPlrpTypeCallback) Problem getting Lien Non-PLRP type Maps, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        // end PRODSUPT-83
    },

    getLienNegotiatedAmounts : function (component, helper) {
        var action = component.get('c.getLienNegotiatedAmountsByClaimant');
        action.setParams({'claimantId': component.get('v.recordId')});
        action.setCallback(this, function getLienNegotiatedAmountsCallback(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS') {
                component.set('v.lienNegotiatedAmounts', response.getReturnValue());
                component.set('v.lienNegotiatedAmountsLoaded', true);
            } else {
                console.log('ERROR: (getLienNegotiatedAmountsCallback) Problem getting Lien Negotiated Amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },

// End PD-1169
})