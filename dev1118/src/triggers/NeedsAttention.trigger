/*
	Created: RAP - September 2017 for September Release
	Purpose: PD-995 - Create Needs Attention Component for Back-End

	Updated: RAP October 2017 for October Release
	Purpose: PD-1101 - Populate Claimant Needs_AttentionPLRP__c and Needs_Attention_Field__c fields
	
*/
trigger NeedsAttention on NeedsAttention__c (after insert, before update, after update, after delete) { // PD-1101 - added after delete

    if (trigger.isBefore) {
   		if (trigger.isUpdate)
	   		NAUtility.BeforeUpdate();
    }
    
    if (trigger.isAfter) {
   		if (trigger.isInsert)
	   		NAUtility.AfterInsert();
   		if (trigger.isUpdate)
	   		NAUtility.AfterUpdate();
   		if (trigger.isDelete) // PD-1101
	   		NAUtility.AfterDelete();
    }
}