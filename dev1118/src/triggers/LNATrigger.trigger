/*
	Created: RAP - August 2017 for August Release
    Purpose: PD-657 - Restrict Lien Negotiated Amounts after a phase is Final
*/

trigger LNATrigger on Lien_Negotiated_Amounts__c (before insert, before update) {

	LNATriggerHandler handler = new LNATriggerHandler();
	
	if (trigger.isBefore && trigger.isInsert) {
		handler.onBeforeInsert();
	}
	
	if (trigger.isBefore && trigger.isUpdate) {
		handler.onBeforeUpdate();
	}
}