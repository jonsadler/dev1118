/*
    Created: RAP - May 2017 for May22 Patch
    Purpose: Receive trigger events and pass the handling of those events to the AccountTriggerHandler class

History:

	Updated: RAP August 2017 for August Release
	Purpose: techDebt - remove unnecessary lists/maps - class runs in trigger context

	Updated: CPS - October 2017 for October Release
	Purpose: PD-918 - Enable "Billing Address same as Shipping Address" functionality
*/

trigger Account on Account (before insert, after insert, before update) {

AccountTriggerHandler handler = new AccountTriggerHandler();

    if (trigger.isBefore && trigger.isInsert) {
        handler.onBeforeInsert();
    }

    if (trigger.isAfter && trigger.isInsert) {
        handler.onAfterInsert();
    }

    if(trigger.isBefore && trigger.isUpdate) {
        handler.onBeforeUpdate();
    }

//    if (trigger.isAfter && trigger.isUpdate) {
//        handler.onAfterUpdate(trigger.new, trigger.oldMap);
//    }
}