/*
    Created: MEF - January 2017 for first release
    Purpose: Receive trigger events and pass the handling of those events
             to the ActionTriggerHandler class
History: 
    
    Updated: lmsw - November 2017 for December Release
    Purpose: PD-1264 - Update Portal users to show Legal Terms before access 
             to portal when an action is updated or created to be avaiable
             in the portal.
*/

trigger Action on Action__c (after insert, after update, before delete, 
before insert) {
    
    public static final string OBJNAME = 'Action';
    
    if(TriggerTools.bypass(OBJNAME)){
        system.debug('Object response for '+ OBJNAME + ' is: '+TriggerTools.bypass(OBJNAME));
        return;
    }
    
    system.debug('Object response for '+ OBJNAME + ' is: '+TriggerTools.bypass(OBJNAME) +' Trigger continued.');
    
    
    

ActionTriggerHandler handler = new ActionTriggerHandler();
    
//    if(Trigger.isBefore && Trigger.isInsert) {
//        handler.onBeforeInsert(Trigger.new, Trigger.newMap);
//    }
    
//    if(Trigger.isBefore && Trigger.isUpdate) {
//        handler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
//    }
    
    if(Trigger.isAfter && Trigger.isInsert) {
        handler.onAfterInsert(Trigger.new, Trigger.newMap);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate) {
        handler.onAfterUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
    }
}