/*
    Created: lmsw - July 2017 
    Purpose: PD-844 Create an Activity on an Event or Call.

History:

	Updated: lmsw - August 2017
	Purpose: Populate Action on Task.
*/

trigger Task on Task (before insert, after insert, after update) {

	TaskUtility handler = new TaskUtility();
    
    if (trigger.isBefore && trigger.isInsert) {
        handler.onBeforeInsert(trigger.new, trigger.newMap);
    }
    if (trigger.isAfter && trigger.isInsert) {
        handler.afterInsert();
    }
    
//    if (trigger.isAfter && trigger.isUpdate) {
//        handler.onAfterUpdate(trigger.new, trigger.oldMap);
//    }
}