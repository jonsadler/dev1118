/*
	Created: MEF - January 2017 for first release
	Purpose: Receive trigger events and pass the handling of those events
			 to the AwardTriggerHandler class

    Updated: RAP - December 2017 for December Release
    Purpose: PD-1308 - Add Ability to Drop Claimant from Action
*/
trigger Award on Award__c (before insert, after insert, before update, after update) {

// start PD-1308
	if (trigger.new[0].get('Action__c') == null)
		return;
// end PD-1308

AwardTriggerHandler handler = new AwardTriggerHandler();
	
	if (trigger.isBefore && trigger.isInsert)
		handler.onBeforeInsert();
	if (trigger.isBefore && trigger.isUpdate){
		handler.onBeforeUpdate();
	}
	if (trigger.isAfter && trigger.isInsert){}
//		handler.onAfterInsert();
	if (trigger.isAfter && trigger.isUpdate)
		handler.onAfterUpdate();
}