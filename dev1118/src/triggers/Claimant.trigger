/*
    Created: MEF - January 2017 for first release
    Purpose: Receive trigger events and pass the handling of those events
             to the ClaimantTriggerHandler class
History:

	Updated: RAP - March 2017 for May Release
	Purpose: PRODSUPT-47 - Claimant Name field is inserted as record ID

	Updated: RAP April 2017 for May Release
	Purpose: techDebt - Operations Flow Control Prep
	
	Updated: RAP August 2017 for August Release
	Purpose: PD-872 - Creation of the Alert System
			 Also refactor - remove unnecessary lists and maps - Handler class runs in the trigger context

    Updated: RAP - December 2017 for December Release
    Purpose: PD-1308 - Add Ability to Drop Claimant from Action
	
*/

trigger Claimant on Claimant__c (before insert, after insert, before update, after update) {

// start PD-1308
	if (trigger.new[0].get('Action__c') == null)
		return;
// end PD-1308

	ClaimantTriggerHandler handler = new ClaimantTriggerHandler();
	    
    if (trigger.isBefore && trigger.isInsert)
        handler.onBeforeInsert();
    if (trigger.isBefore && trigger.isUpdate) // techDebt
        handler.onBeforeUpdate();
    if (trigger.isAfter && trigger.isInsert)
        handler.onAfterInsert();
    if (trigger.isAfter && trigger.isUpdate) // PD-872
        handler.onAfterUpdate();
}