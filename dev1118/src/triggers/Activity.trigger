/*
	Created: RAP October 2017 for October Release
	Purpose: PD-1100 - Claimant Last Updated Field
	
*/
trigger Activity on Activity__c (after insert, after update) {
    
    if (trigger.isAfter) {
    	if (trigger.isInsert)
    		ActivityTriggerHandler.AfterInsert();
    	if (trigger.isUpdate)
    		ActivityTriggerHandler.AfterUpdate();
    }
}