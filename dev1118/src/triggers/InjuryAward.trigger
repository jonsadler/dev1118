/*
    Created: RAP - November 2017 for November Release
    Purpose: PD-1103 - Run AwardLien Batch Trigger Upon Insert of InjuryAward

History:
	Updated: RAP - January 2018 fr January Release
	Purpose: PD-1311 - Modified Report - with specified objects
*/

trigger InjuryAward on InjuryAward__c (after insert, after update) {

    if (trigger.isAfter && !trigger.isDelete) {
    	if (AwardLienUtility.hasRun)
    		return;
    	AwardLienUtility alu = new AwardLienUtility();
    	set<Id> awardIds = new set<Id>();
    	for (SObject s : trigger.new)
    		awardIds.add((Id)s.get('Award__c'));
		alu.ProcessAwards(awardIds);
// start PD-1311
    	set<Id> ilIds = new set<Id>();
		map<Id,InjuryLien__c> ilMap = new map<Id,InjuryLien__c>([SELECT Id, Name FROM InjuryLien__c WHERE Injury__c IN (SELECT Injury__c FROM InjuryAward__c WHERE Award__c IN :awardIds)]);
		ReporterBatchTool rbt = new ReporterBatchTool();
		rbt.CreateReporters(trigger.newMap.keySet(), ilMap.keySet());
// end PD-1311
    }    	
}