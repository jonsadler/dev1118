/*
    Created: lmsw - June 2017
    Purpose: PD-806 - Create Action sharing for Portal users

History: 

    Updated: lmsw - August 2017
	Purpose: No longer using permissions for fortal users.
*/

trigger User on User (after insert, after update) {
/*
	UserTriggerHandler handler = new UserTriggerHandler();
    
    if (trigger.isAfter && trigger.isInsert) {
//        handler.onAfterInsert(trigger.new, trigger.newMap);
    }
    if (trigger.isAfter && trigger.isUpdate) {
//       handler.onAfterUpdate(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
    }
*/
}