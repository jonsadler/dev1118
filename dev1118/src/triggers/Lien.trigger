/*
	Created: RAP - December 2016 for first release
	Purpose: PD-319 - Create Trigger for Lien Insert

	Updated: RAP - January 2017 for first release
	Purpose: Add setting of recordtype, move processing to utility class 

	Updated: lmsw - March 2017
	Purpose: Modify to allow Holdback entry to be Percent or Amount

    Updated: RAP April 2017 for May Release
    Purpose: PD-566 - Automation - Create Lien Res Fees from Fee Model Object
    
    Updated: RAP May 2017 for May15 Release
    Purpose: PD-527 - Automation - calculate Final Payable

    Updated: lmsw May 2017
    Purpose: Combine handlers 

	Updated: RAP September 2017 for September Patch
	Purpose: PD-1042 - Create an Email-able Report Detailing Deleted Liens

    Updated: RAP - December 2017 for December Release
    Purpose: PD-1308 - Add Ability to Drop Claimant from Action
*/
trigger Lien on Lien__c (before insert, before update, after insert, after update, after delete) {

// start PD-1308
	if (!trigger.isDelete && trigger.new[0].get('Action__c') == null)
		return;
// end PD-1308

    if (trigger.isBefore) {
   		if (trigger.isInsert) {
	   		LienUtility.BeforeInsert(); 
   		}
   		if (trigger.isUpdate) {
	   		LienUtility.BeforeUpdate();
   		}
    }
// start PD-566
    if (trigger.isAfter) {
    	if (trigger.isInsert)
    		LienUtility.AfterInsert();
    	if (trigger.isUpdate)
			LienUtility.AfterUpdate();
    	if (trigger.isDelete) // PD-1042
			LienUtility.AfterDelete();
    }
// end PD-566
}