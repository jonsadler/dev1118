/*
	Created: RAP - March 2017 for May Release
	Purpose: PRODSUPT-53 - Add automatic naming for Injuries

History:

	Updated: RAP - July 2017 for July Release
	Purpose: PD-891 - Creation of an Injury Lien Junction
			 Also refactored trigger and utility

    Updated: RAP - December 2017 for December Release
    Purpose: PD-1308 - Add Ability to Drop Claimant from Action
*/
trigger Injury on Injury__c (before insert, after insert, after update) {

// start PD-1308
	if (trigger.new[0].get('Action__c') == null)
		return;
// end PD-1308
    
	if (trigger.isBefore && trigger.isInsert)
		InjuryUtility.onBeforeInsert();
	if (trigger.isAfter && trigger.isInsert)
		InjuryUtility.onAfterInsert();
	if (trigger.isAfter && trigger.isUpdate)
		InjuryUtility.onAfterUpdate();
}