/*
    Created: RAP - September 2017 for September Release
    Purpose: PD-995 - need to update NeedsAttention objects after Health Plan changes.

    Updated: RAP - January 2017 for January Release
    Purpose: PD-1346 - Update QA Batch to Remove Dependency on Award Data

*/

trigger HealthPlan on Health_Plan__c (after insert, after update, after delete) {

	set<Id> cIds = new set<Id>();
	if (trigger.isDelete) {
		for (SObject s : trigger.old)
			cIds.add((Id)s.get('Claimant__c'));
	}
	else {
		for (SObject s : trigger.new)
			cIds.add((Id)s.get('Claimant__c'));
	}
	QAEngine.ProcessClaimants(cIds); // PD-1346
}