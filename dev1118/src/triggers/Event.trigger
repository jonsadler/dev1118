/*
    Created: lmsw - July 2017 
    Purpose: PD-844 Create an Activity on an Event or Call

History:

	Updated: RAP August 2017 for August Release
	Purpose: techDebt - remove unnecessary lists and maps - Utility class runs in the trigger context
*/

trigger Event on Event (after insert, after update) {

	EventUtility handler = new EventUtility();
    
    if (trigger.isAfter && trigger.isInsert) {
        handler.AfterInsert();
    }
    
//    if (trigger.isAfter && trigger.isUpdate) {
//        handler.AfterUpdate();
//    }
}