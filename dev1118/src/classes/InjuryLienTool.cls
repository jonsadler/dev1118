/*
    Created: RAP - August 2017 for August Release
    Purpose: PD-892 - Back-Fill Missing Injury Lien Junctions for Existing Liens (test class)
*/

public without sharing class InjuryLienTool {

	public static void AssociateLiens(list<Id> cList) {
    	map<Id,list<Injury__c>> injuMap = new map<Id,list<Injury__c>>();
    	map<Id,list<Lien__c>> lienMap = new map<Id,list<Lien__c>>();
    	list<Injury__c> injuList = [SELECT Id, Claimant__c
    								FROM Injury__c 
    								WHERE Claimant__c IN :cList 
    								AND Compensable__c = 'True'];
    	list<Lien__c> lienList = [SELECT Id, Claimant__c, (SELECT Injury__c FROM Injuries__r) 
    							  FROM Lien__c 
    							  WHERE Claimant__c IN :cList 
    							  AND Stages__c != 'No_Lien'];
    	for (Injury__c i : injuList) {
    		if (injuMap.containsKey(i.Claimant__c))
    			injuMap.get(i.Claimant__c).add(i);
    		else
    			injuMap.put(i.Claimant__c, new list<Injury__c>{i});
    	}
    	for (Lien__c l : lienList) {
    		if (lienMap.containsKey(l.Claimant__c))
    			lienMap.get(l.Claimant__c).add(l);
    		else
    			lienMap.put(l.Claimant__c, new list<Lien__c>{l});
    	}
    	list<InjuryLien__c> insertList = new list<InjuryLien__c>();
    	for (Id c : cList) {
   			if (injuMap.containsKey(c)) {
	   			for (Injury__c i : injuMap.get(c)) {
		   			if (lienMap.containsKey(c)) {
			    		for (Lien__c l : lienMap.get(c)) {
			    			boolean add = true;
			    			for (InjuryLien__c il : l.Injuries__r) {
		    					if (il.Injury__c == i.Id)
		    						add = false;
			    			}
			    			if (add) {
				    			InjuryLien__c il = new InjuryLien__c(Injury__c = i.Id,
				    												 Lien__c = l.Id);
				    			insertList.add(il);
			    			}
			    		}
		   			}
	    		}
   			}
    	}
    	if (!insertList.isEmpty())
    		insert insertList;
    	CheckAssociations(cList);
	}   

	private static void CheckAssociations(list<Id> cList) {
		map<Id,Injury__c> iMap = new map<Id,Injury__c>([SELECT Id, Name FROM Injury__c 
														WHERE Claimant__c in :cList 
														AND Compensable__c != 'True']);
	    list<InjuryLien__c> deleteList = [SELECT Id FROM InjuryLien__c WHERE Injury__c IN :iMap.keySet()];
    	if (!deleteList.isEmpty())
    		delete deleteList;
	}
}