/*
    Created: RAP - September 2017 for September Release
    Purpose: PD-995 - Create Needs Attention Component for Back-End

	Updated: RAP October 2017 for October Release
	Purpose: PD-1101 - Populate Claimant Needs_AttentionPLRP__c and Needs_Attention_Field__c fields
	
	Updated: RAP November 2017 for December Release
	Purpose: PD-1177 - Claimant NOT moving to 'Cleared' based on specified situations
	
*/
public with sharing class NAUtility {

    public static Id rtId1 = Schema.SObjectType.NeedsAttention__c.getRecordTypeInfosByName().get('Portal').getRecordTypeId();
    public static Id rtId2 = Schema.SObjectType.NeedsAttention__c.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
    public static boolean hasRun = false;
    public static boolean isTrigger;
    
    public static void AfterInsert() {
        CalcFields();
		Recalculate();
    }

	public static void BeforeUpdate() {
		CheckForValidationErrors();
    }

    public static void AfterUpdate() {
        CalcFields();
		Recalculate();
    }
// start PD-1101
    public static void AfterDelete() {
        CalcFields();
    }
// end PD-1101
    public static void CheckForValidationErrors() {
        for (SObject s : trigger.new) {
            if (s.get('RecordTypeId') == rtId2 && s.get('Internal_Status__c') == 'Special Case - Ignore' && s.get('Special_Case_Explanation__c') == null)
                s.addError('Cannot set status to "Special Case - Ignore" without entering an explanation.');
        }
    }
// start PD-1101
    public static void CalcFields() {
        list<Id> cIds = new list<Id>();
        list<string> actionList = new list<string>(); // PD-1082
        list<NeedsAttention__c> searchList = trigger.isDelete ? trigger.old : trigger.new;
        for (SObject s : searchList) {
            if (s.get('RecordTypeId') == rtId1) {
                if (s.get('Claimant__c') != null)
                    cIds.add((Id)s.get('Claimant__c'));
// start PD-1082
                if (s.get('Action__c') != null)
                	actionList.add((string)s.get('Action__c'));
// end PD-1082
            }
        }
        if (!cIds.isEmpty()) {
            list<Claimant__c> cList= [SELECT Id FROM Claimant__c WHERE Id IN :cIds];
            update cList;
        }
// start PD-1082
        list<AggregateResult> aggRes = [SELECT Action__c, COUNT(Id) FROM Claimant__c WHERE Action__r.Name IN :actionList AND Needs_Attention_Total__c > 0 group by Action__c];
        map<Id,Action__c> actionMap = new map<Id,Action__c>([SELECT Id, NeedsAttentionTotal__c FROM Action__c WHERE Name IN :actionList]);
        for (AggregateResult agg : aggRes)
        	actionMap.get((Id)agg.get('Action__c')).NeedsAttentionTotal__c = (integer)agg.get('expr0');
        update actionMap.values();
// end PD-1082
    }
// end PD-1101
    public static void Recalculate() {
        list<Id> cIds = new list<Id>();
        list<Id> lIds = new list<Id>();
        for (SObject s : trigger.new) {
            if (s.get('RecordTypeId') == rtId2) {
                if (s.get('Claimant__c') != null)
                    cIds.add((Id)s.get('Claimant__c'));
                else if (s.get('Lien__c') != null)
                    lIds.add((Id)s.get('Lien__c'));
            }
        }
        if (!lIds.isEmpty()) {
            for (Lien__c l : [SELECT Claimant__c FROM Lien__c WHERE Id IN :lIds])
                cIds.add(l.Claimant__c);
        }
// start PD-1177
		list<Claimant__c> cList = [SELECT Id, Status__c, 
										  (SELECT Internal_Status__c, Internal_Type__c 
										   FROM Needs_Attentions__r 
										   WHERE RecordType.Name = 'Internal')
								   FROM Claimant__c 
								   WHERE Id in :cIds];
		map<string,NeedsAttentionMap__c> holdMap = NeedsAttentionMap__c.getAll();
		list<Claimant__c> updateList = new list<Claimant__c>();
		for (Claimant__c c : cList) {
			boolean skip = false;
			if (c.Needs_Attentions__r != null && !c.Needs_Attentions__r.isEmpty()) {
				for (NeedsAttention__c na : c.Needs_Attentions__r) {
					if (na.Internal_Status__c == 'Needs Attention' && holdMap.get(na.Internal_Type__c).Priority__c == '1') {
						skip = true;
						break;
					}
				}
			}
			if (skip) {
				c.Status__c = 'Not_Ready_to_be_Cleared';
				updateList.add(c);
				continue;
			}
		}
		if (!updateList.isEmpty()) {
			hasRun = true;
			update updateList;
		}
    }
// end PD-1177
}