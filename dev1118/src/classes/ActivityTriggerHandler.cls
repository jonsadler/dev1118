/*
	Created: RAP October 2017 for October Release
	Purpose: PD-1100 - Claimant Last Updated Field
	
*/

public with sharing class ActivityTriggerHandler {

    public static void AfterInsert() {
		UpdateClaimant();
    }

    public static void AfterUpdate() {
		UpdateClaimant();
    }

	private static void UpdateClaimant() {
    	list<Id> idList = new list<Id>();
    	for (SObject s : trigger.new)
    		idList.add((Id)s.get('Associated_Claimant__c'));
    	list<Claimant__c> cList = [SELECT Id, Name FROM Claimant__c WHERE Id IN :idList];
    	update cList;
	}
}