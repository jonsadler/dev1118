/*
    Created: RAP - May 2017 for May22 Patch
    Purpose: PD-524/525/526/527 - Automation - Handler for Account trigger events - first pass - create Action_Accounts after insert.

History:

	Updated: RAP August 2017 for August Release
	Purpose: techDebt - remove unnecessary lists/maps - class runs in trigger context

	Updated: CPS - October 2017 for October Release
	Purpose: PD-918 - Enable "Billing Address same as Shipping Address" functionality

*/

public with sharing class AccountTriggerHandler {

    public void onBeforeInsert() {
		setBillingAddress();    // Added by PD-918
    }

    public void onAfterInsert() {
   		CreateActionAccounts();
	}

    public void onBeforeUpdate() {
		setBillingAddress();    // Added by PD-918
    }

	private static void CreateActionAccounts() {
    	list<Action_Account__c> aaList = [SELECT Account__c, Action__c FROM Action_Account__c];
    	map<Id, set<Id>> aaMap = new map<Id,set<Id>>();
    	for (Action_Account__c aa : aaList) {
		    if (aaMap.containsKey(aa.Action__c))
    			aaMap.get(aa.Action__c).add(aa.Account__c);
    		else
    			aaMap.put(aa.Action__c, new set<Id>{aa.Account__c});
    	}
		Id actionId = ActionAssignment2__c.getInstance(userInfo.getUserId()).Action__c;
		list<Action_Account__c> insertList = new list<Action_Account__c>();
		for (Id aId : trigger.newMap.keySet()) {
			if (!aaMap.containsKey(actionId) || !aaMap.get(actionId).contains(aId)) {
				Action_Account__c aa = new Action_Account__c(Action__c = actionId,
															 Account__c = aId);
				insertList.add(aa);
			}
		}
		if (!insertList.isEmpty())
			insert insertList;
	}

// start PD-918
	// ################################################################################################### setBillingAddress
	// This method sets the Account's Billing Address fields to the same as the Shipping Address fields
	// if the "Same as Shipping Address" checkbox is checked/true.
    private static void setBillingAddress() {
		for (Account newAccount : (List<Account>) Trigger.new) {
			if (newAccount.Same_as_Shipping_Address__c == true) {
				newAccount.BillingStreet          = newAccount.ShippingStreet;
				newAccount.BillingCity            = newAccount.ShippingCity;
				newAccount.BillingState           = newAccount.ShippingState;
				newAccount.BillingPostalCode      = newAccount.ShippingPostalCode;
				newAccount.BillingCountry         = newAccount.ShippingCountry;
				newAccount.BillingGeocodeAccuracy = newAccount.ShippingGeocodeAccuracy;
				newAccount.BillingLatitude        = newAccount.ShippingLatitude;
				newAccount.BillingLongitude       = newAccount.ShippingLongitude;
			}
		}
    }
// end PD-918
}