/*
	Created: RAP - August 2017 for August Release
    Purpose: PD-657 - Restrict Lien Negotiated Amounts after a phase is Final
*/
public with sharing class LNATriggerHandler {

	public void onBeforeInsert() {
		CheckForFinal();
	}

	public void onBeforeUpdate() {
		CheckForFinal();
	}

	private void CheckForFinal() {
		list<Id> idList = new list<Id>();
		for (SObject s: trigger.new)
			idList.add((Id)s.get('Lien__c'));
		list<Lien_Negotiated_Amounts__c> checkList = [SELECT Lien__c FROM Lien_Negotiated_Amounts__c 
													  WHERE Lien__c IN :idList 
													  AND Phase__c = 'Final'];
		if (checkList == null || checkList.isEmpty())
			return;
		set<Id> lnaSet = new set<Id>();
		for (Lien_Negotiated_Amounts__c lna : checkList)
			lnaSet.add(lna.Lien__c);
		for (SObject s: trigger.new) {
			if (lnaSet.contains((Id)s.get('Lien__c')))
				s.addError('A Final Lien Negotiated Amount already exists on this lien.');
		}
	}
}