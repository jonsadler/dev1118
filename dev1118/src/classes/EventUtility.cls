/*
    Created: lmsw - July 2017
    Purpose: PD-844 Create an Activity on an Event or Call

History: 
  
	Updated: lmsw - August 2017 for August Release
	Purpose: Add class static variable for Event Helper.

	Updated: RAP August 2017 for August Release
	Purpose: techDebt - remove unnecessary lists and maps - Utility class runs in the trigger context
			 Also bulkify - remove SOQL query from FOR loop

    Updated: lmsw - October 2017 for October Release
    Purpose: PD-981 - Fix Activities for Liens and Modify Subject and Description for Portal Activity.

    Updated: lmsw - October 2017 for October Release
    Purpose: PD-981 - Modify to allow Lien Event types = Events to show on Claimant page.

    Updated: RAP - October 2017 for October Release
    Purpose: PD-981 - refactor - remove method call inside loop that makes a SOQL query, remove duplication
    				  make method public so can call from both batch and trigger - no need for code in two places.

*/
public with sharing class EventUtility {
	
	public static map<Id,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Lien__c.getRecordTypeInfosById();
	public static string lPrefix = Lien__c.sObjectType.getDescribe().getKeyPrefix();
	public static string cPrefix = Claimant__c.sObjectType.getDescribe().getKeyPrefix();
    
	public void AfterInsert() {
		CreateActivities(trigger.new);
	}

	public static void CreateActivities(list<Event> events) {
		list<Activity__c> insertList = new list<Activity__c>();
		list<Id> idList = new list<Id>();
		string description;
		string subject;

		for (Event e : events) {
			if (string.valueOf(e.WhatId).startsWith(lPrefix))
				idList.add(e.WhatId);
		}
		map<Id,Lien__c> eMap = new map<Id,Lien__c>([SELECT Id, Claimant__c, Account__r.Name, RecordType.Name FROM Lien__c WHERE Id IN :idList]);
		string lienholderName;
		for (Event e : events) {
			if (eMap.containsKey(e.WhatId)) {
				lienholderName = eMap.get(e.WhatId).Account__r.Name;
				if (e.Description.contains(' - ')) {
					subject = e.Description.substringBefore(' - ') + ' - ' + lienholderName;
					description = e.Description.substringAfter(' - ').trim().replaceAll('/NULL','');
				}
				else {
					subject = eMap.get(e.WhatId).RecordType.Name + ' - ' + lienholderName;
					description = e.Description.trim().replaceAll('/NULL','');
				}
	// Create Activity for Lien Event
				Activity__c a = new Activity__c(Activity_Date__c = e.ActivityDateTime, 
												Associated_Claimant__c = null,
												Associated_Lien__c = e.WhatId, 
												Description__c = description, 
												Subject__c = subject,
												Type__c = 'Lien update');
				insertList.add(a);
	// Create Activity for Claimant Event
				Activity__c b = new Activity__c(Activity_Date__c = e.ActivityDateTime, 
												Associated_Claimant__c = eMap.get(e.WhatId).Claimant__c,
												Associated_Lien__c = null, 
												Description__c = description, 
												Subject__c = subject,
												Type__c = 'Lien update');
				insertList.add(b);
			}
			else if (string.valueOf(e.WhatId).startsWith(cPrefix) && e.Activity_Type__c != 'Lien update') {
				Activity__c c = new Activity__c(Activity_Date__c = e.ActivityDateTime, 
												Associated_Claimant__c = e.WhatId,
												Associated_Lien__c = null, 
												Description__c = e.Description, 
												Subject__c = e.Subject,
												Type__c = e.Activity_Type__c);
				insertList.add(c);
			}
			else
				system.debug(loggingLevel.INFO, 'RAP --->> activity not created - event = ' + e);
		}
// Insert Activities
		if (!insertList.isEmpty())
			insert insertList;
    }	
// end PD-981
}