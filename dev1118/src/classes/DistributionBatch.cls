/*
    Created - RAP February 2017 for March Release
    Purpose - PD-431 - Release Dates Auto-Move (Batch processor grabs all Awards belonging to an Action with a Distribution
    		  Date of the day it runs, collects amounts available for disbursement, and moves those funds out of availability
History:

    Updated: RAP - May 2017 for May15 Release
    Purpose: PD-571 - Quality Control implementation - 12 causes for no distribution

    Updated: RAP - September 2017 for October Release
    Purpose: techDebt - all exceptions now calculated in QAEngine - just check Needs Attentions

*/
global class DistributionBatch implements Database.Batchable<Award__c>, Database.Stateful {

    public Date queryDate{get;set;}
	public string action{get;set;}
	public boolean apply{get;set;}
	public map<Id,Claimant__c> cMap{get;set;}
	public integer numReleases{get;set;}
	public list<string> flowString = new list<string>();
	public decimal totalReleased = 0.00;

    public DistributionBatch(string act, boolean test) {
    	queryDate = system.today();
		numReleases = 0;
    	action = act;
    	apply = !test;
    }

	global Iterable<Award__c> start(Database.BatchableContext BC) {
		DisbursementDates__c ddc = DisbursementDates__c.getInstance(action);
		if (ddc == null || ddc.Date__c != queryDate) return new list<Award__c>();
		set<Id> cSet = new set<Id>();
		list<Id> mList = new list<Id>();
		list<Award__c> awards = [SELECT Id, Amount_Remaining__c, Claimant__c, Name
								 FROM Award__c
								 WHERE Action__r.Name = :action
								 AND (Distribution_Status__c = 'Cleared'
								 OR Distribution_Status__c = 'Released_to_a_Holdback')];
		system.debug(loggingLevel.INFO, 'RAP --->> awards = ' + awards);
		for (Award__c aw : awards)
			cSet.add(aw.Claimant__c);

		map<Id,Claimant__c> claimants = new map<Id,Claimant__c>([SELECT Id, Status__c,
																	    (SELECT Id FROM Needs_Attentions__r WHERE Internal_Status__c = 'Needs Attention')
																 FROM Claimant__c
																 WHERE Id in :cSet]);
system.debug('RAP --->> claimants = ' + claimants);
		list<Lien__c> lList = [SELECT Id, Claimant__c, (SELECT Id FROM Needs_Attention__r WHERE Internal_Status__c = 'Needs Attention')
							   FROM Lien__c
							   WHERE Claimant__c IN :claimants.keySet()];
system.debug('RAP --->> liens = ' + lList);
		for (Lien__c l : lList) {
			if (l.Needs_Attention__r != null && !l.Needs_Attention__r.isEmpty())
				cSet.remove(l.Claimant__c);
		}
		cMap = new map<Id,Claimant__c>();
		list<Award__c> awList = new list<Award__c>();
		for (Award__c a : awards) {
			Claimant__c c = claimants.get(a.Claimant__c);
			if (cSet.contains(c.Id) && (c.Needs_Attentions__r == null || c.Needs_Attentions__r.isEmpty())) {
				cMap.put(a.Id, c);
				awList.add(a);
			}
		}
		return awList;
	}

	public void execute(Database.BatchableContext BC, list<Award__c> awards) {
		if (awards == null || awards.isEmpty())	return;
		list<Release__c> releases = new list<Release__c>();
		for (Award__c a : awards) {
			flowString.add('\r\nProcessing Award ' + a.Name + '\r\n');
			Claimant__c claimant = cMap.get(a.Id);
			if (a.Amount_Remaining__c > 0) {
				decimal sendAmt = a.Amount_Remaining__c;
				Release__c send = new Release__c(Award__c = a.Id,
												 Amount__c = sendAmt,
												 Status__c = claimant.Status__c,
												 Date__c = queryDate);
				releases.add(send);
				flowString.add('        release added - ' + send);
				totalReleased += sendAmt;
			}
		}
		if (!releases.isEmpty()) {
			if (apply) insert releases;
			numReleases += releases.size();
		}
	}

	public void finish(Database.BatchableContext BC) {
		flowString.add('Number of Releases created: ' + numReleases + ', totalling $' + totalReleased);
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		string[] toAddresses = new string[] {userInfo.getUserEmail()};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Release Batch Complete');
		mail.setPlainTextBody(string.Join(flowString, '\r\n'));
		if (!test.isRunningTest())
			Messaging.sendEmail(new Messaging.Email[] { mail });
	}
}