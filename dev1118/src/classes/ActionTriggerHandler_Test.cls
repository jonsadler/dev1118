/* 
	Created: lmsw - November 2017 for December Release
	Purpose: PD-1264 - Test Update Portal users using refactored TestUtillity
					   Coverage as of 11/21 - 78%
*/
@isTest
private class ActionTriggerHandler_Test {
	static testMethod void testUpdatePortalUser() {
		TestUtility testUtility = new TestUtility();
		testUtility.createUsers();
		Action__c action = [SELECT Id, Available_in_Portal__c 
							FROM Action__c
							WHERE Available_in_Portal__c = true
							Limit 1];
		system.debug('Action: ' + action);
		// Change Action availability
		action.Available_in_Portal__c = false;
		update action;

		// *** Needs to be a User with a profile="ProvidioCustomerCommunityPlus" to full test
		User user = [SELECT Id, Has_Accepted_Legal_Terms__c FROM User Limit 1];
		// Change User 
		user.Has_Accepted_Legal_Terms__c = True;
		update user;

		Test.startTest();
			// Check User accepted status(True)
			system.assertEquals(true,[SELECT Id, Has_Accepted_Legal_Terms__c 
									  FROM User
									  WHERE Id =: user.Id].Has_Accepted_Legal_Terms__c);
			// Change Action availability
			action.Available_in_Portal__c = true;
			update action;
			// Chech User accepted status(False)
			/*system.assertEquals(false,[SELECT Id, Has_Accepted_Legal_Terms__c 
									  FROM User
									  WHERE Id =: user.Id].Has_Accepted_Legal_Terms__c);*/
		Test.stopTest();
	}
}