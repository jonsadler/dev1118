/*
    Created: RAP - March 2017 for March Release
    Purpose: Test functions of List Controllers

History: 

	Updated: lmsw - March 2017 for March Release
    Purpose: Change Claimant__c.Address__c to Claimant__c.Address_1__c

    Updated: lmsw - March 2017 for May Release
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c

    Updated: RAP - March 2017 for May Release
    Purpose: PD-520 - Extraneous Field Clean-up (change to Description2__c) + minor refactor

    Updated: RAP - April 2017 for May Release
    Purpose: Fix failing testMethod

    Updated: RAP - May 2017 for May15 Release
    Purpose: PD-571 - Quality Control Implementation
    		 Coverage as of 5/5 - LienBatch: 100%
    		 					  DistributionBatch: 100%
    Updated: lmsw - July 2017
    Purpose: Increase test coverage
    		Coverage as of 7/26 - AwardLienBatch: 97%
    							  DistributionBatch: 91%
    							  LienBatchByClaimant: 95%

    Updated: RAP - September 2017 for September Release
    Purpose: PD-995 - test QABatch processor
    
    Updated: RAP - September 2017 for September Release
    Purpose: PD-872 - test ClaimantBatch
    
    Updated: RAP - January 2018 for January Release
    Purpose: techDebt - expand ClaimantBatch test

*/
@isTest
private class Batch_Test {
    
@testSetup
	static void CreateData() {
		TestUtility.createTestData();
	}
// start PD-872	
	static testMethod void TestClaimantBatch() {
		map<Id,Claimant__c> cMap = new map<Id,Claimant__c>([SELECT Id, Name FROM Claimant__c]);
		test.startTest();
		database.executeBatch(new ClaimantBatch('TestAction1'));
		database.executeBatch(new ClaimantBatch(cMap.keySet())); // techDebt
		test.stopTest();
	}
// end PD-872
// start PD-995
	static testMethod void TestQABatch() {
		list<Award__c> awards = [SELECT Amount_Remaining__c, Id, Claimant__r.DOB__c, Name, Claimant__c, Claimant__r.Total_Applied__c, 
										Claimant__r.Num_Liens__c, Claimant__r.Negative_Balance__c, Claimant__r.Date_SSN_Changed__c, 
										Description__c, Amount__c, Claimant__r.Status__c, Distribution_Status__c,
										(SELECT Lien__c, Lien__r.HBAmount__c, Lien__r.Total_Fees_Applied__c, Lien__r.Total_Fees_Paid__c, 
						 						Lien__r.Name__c, Lien__r.RecordType.Name, Lien__r.Stages__c, Lien__r.Substage__c, 
												Final_Payable__c, Lien__r.PayerType__c, Lien__r.Account__c, Lien__r.Date_No_Interest__c, 
												Lien__r.Subrogation_Rights__c, Lien__r.State__c, Lien__r.Date_Submitted__c
					 					 FROM Liens__r)
				  				 FROM Award__c];
		test.startTest();
		database.executeBatch(new QABatch('TestAction2'));
		Id rtId = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('PLRP Model').getRecordTypeId();
		Account a = [SELECT Id FROM Account WHERE Name = 'Lienholder'];
		Claimant__c c = [SELECT Id, Action__c, SSN__c FROM Claimant__c WHERE Last_Name__c = 'RAPPER' limit 1];
		c.SSN__c = '987654321'; 
		update c;
        Lien__c lien1 = new Lien__c(Account__c = a.Id, 
									Action__c = c.Action__c,
									RecordTypeId = rtId, 
									Claimant__c = c.Id, 
									Cleared__c = false,
									Date_Submitted__c = system.today().addDays(-12),
									Notes__c = 'This is a note', 
									Stages__c = 'Submitted', 
									Substage__c = 'First_Notice_Sent',
									Eligible__c = 'Eligible_after_DOL_but_within_timeframe',
									PayerType__c = 'Primary_Group_Health_Plan',
									State__c = 'CO', 
									Final_Payable__c = 100000,
									Submitted__c = true);
		insert lien1;
		list<AwardLien__c> alList = new list<AwardLien__c>();
		for (Award__c aw : awards) {
	        AwardLien__c al = new AwardLien__c(PercentToAward__c = 1.0, 
	                                           Lien__c = lien1.Id, 
	                                           Award__c = aw.Id);
	        alList.add(al);
		}
		insert alList;
		database.executeBatch(new QABatch('TestAction1'));
		QABatchScheduler qab = new QABatchScheduler ('TestAction1');
		qab.scheduleJob(null, 'TestAction1');
		system.schedule('QA Batch Processor', '0 0 4 * * ?', new QABatchScheduler ('TestAction1')); 
		test.stopTest();
	}
// end PD-995
	static testMethod void TestAwardLienBatch() {
		test.startTest();
		database.executeBatch(new AwardLienBatch('TestAction1'));
		AwardLienScheduler als = new AwardLienScheduler ('TestAction1');
		als.scheduleJob(null, 'TestAction1', 'noon');
		system.schedule('AwardLien Processor', '0 0 4 * * ?', new AwardLienScheduler ('TestAction1')); 
		Health_Plan__c hp = [SELECT Id FROM Health_Plan__c limit 1];
		delete hp;
		test.stopTest();
	}
	
	static testMethod void TestLienBatchByClaimant() {
		set<Id> cIds = new set<Id>();
		Id claimantId;
		for (Claimant__c c : [SELECT Id FROM Claimant__c]) {
			cIds.add(c.Id);
			claimantId = c.Id;
		}
		test.startTest();
		database.executeBatch(new LienBatchByClaimant('TestAction1',false));
		database.executeBatch(new LienBatchByClaimant('TestAction1',true,cIds));
		database.executeBatch(new LienBatchByClaimant('TestAction1',true,claimantId));
		test.stopTest();
	}
}