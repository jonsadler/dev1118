/*
    Created - RAP May 2017 for May22 Patch
    Purpose - Creation of AwardLien junctions given InjuryLiens and InjuryAwards

	Updated: RAP - October 2017 for October Release
	Purpose: techDebt - Create AwardLien Junction ONLY for those associated to injuries where Compensable = True

	Updated: RAP - November 2017 for November Release
	Purpose: PD-1103 - Run AwardLien Batch Trigger Upon Insert of InjuryAward - expand to take a list of award ids to limit scope
    
*/
global class AwardLienBatch implements Database.Batchable<SObject>, Database.Stateful {

	public static final string QUERY = 'SELECT Award__c, Id, Injury__c FROM InjuryAward__c ';
	public map<Id,set<Id>> iaMap{get;set;}
	public map<Id,set<Id>> ilMap{get;set;}
	public map<Id,map<Id, AwardLien__c>> alMap2{get;set;}
	public map<Id,set<Id>> alMap{get;set;} // multiple liens per award
	public map<Id,set<Id>> laMap{get;set;} // multiple awards per lien
	public list<AwardLien__c> upsertList = new list<AwardLien__c>();
	public set<AwardLien__c> upsertSet = new set<AwardLien__c>();
	public string action{get;set;}
	public set<Id> awards{get;set;}
	public map<Id,Award__c> awardMap{get;set;}
	public string queryStr;
	
    global AwardLienBatch(string act) {
    	action = act;
    	queryStr = QUERY + 'WHERE Award__r.Action__r.Name = \'' + action + '\'';
    }

    global AwardLienBatch(set<Id> awds) { // PD-1103
    	awards = awds;
    	queryStr = QUERY + 'WHERE Award__c IN :awards';
    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
		system.debug(loggingLevel.INFO, 'RAP --->> queryStr = ' + queryStr);
		iaMap = new map<Id,set<Id>>();
		ilMap = new map<Id,set<Id>>();
		alMap = new map<Id,set<Id>>();
		laMap = new map<Id,set<Id>>();
		alMap2 = new map<Id,map<Id,AwardLien__c>>();
// start PD-1103
		set<Id> claimants = new set<Id>();
		if (action != null)
			awardMap = new map<Id,Award__c>([SELECT Id, Name, Claimant__c, Settlement_Category__c FROM Award__c WHERE Action__r.Name = :action]);
		else {
			awardMap = new map<Id,Award__c>([SELECT Id, Name, Claimant__c, Settlement_Category__c FROM Award__c WHERE Id IN :awards]);
			for (Award__c a : awardMap.values())
				claimants.add(a.Claimant__c);
		}
		list<InjuryLien__c> ilList;
		if (action != null) {
			ilList = [SELECT Injury__c, Lien__c, Id, Lien__r.Name__c, Lien__r.Claimant__c 
					  FROM InjuryLien__c 
					  WHERE Lien__r.Action__r.Name = :action
					  AND Lien__r.Stages__c != 'No_Lien'
					  AND Injury__r.Compensable__c = 'true'];
		}
		else {
			ilList = [SELECT Injury__c, Lien__c, Id, Lien__r.Name__c, Lien__r.Claimant__c 
					  FROM InjuryLien__c 
					  WHERE Lien__r.Claimant__c IN :claimants
					  AND Lien__r.Stages__c != 'No_Lien'
					  AND Injury__r.Compensable__c = 'true'];
		}
		for (InjuryLien__c il : ilList) {
			if (ilMap.containsKey(il.Injury__c))
				ilMap.get(il.Injury__c).add(il.Lien__c);
			else
				ilMap.put(il.Injury__c, new set<Id>{il.Lien__c});
		}
		list<AwardLien__c> alList;
		if (action != null) {
			alList = [SELECT PercentToLien__c, PercentToAward__c, Award__c, Id, Lien__c, Lien_Record_Type__c, Lien__r.RecordTypeId, Lien__r.Account__c
					  FROM AwardLien__c
					  WHERE Lien__r.Action__r.Name = :action
					  AND Lien__r.Stages__c != 'No_Lien'];
		}
		else {
			alList = [SELECT PercentToLien__c, PercentToAward__c, Award__c, Id, Lien__c, Lien_Record_Type__c, Lien__r.RecordTypeId, Lien__r.Account__c
					  FROM AwardLien__c
					  WHERE Lien__r.Claimant__c IN :claimants
					  AND Lien__r.Stages__c != 'No_Lien'];
		}
// end PD-1103			
		for (AwardLien__c al : alList) {
			if (alMap.containsKey(al.Award__c)) {
				alMap.get(al.Award__c).add(al.Lien__c);
				alMap2.get(al.Award__c).put(al.Lien__c, al);
			}
			else {
				alMap.put(al.Award__c, new set<Id>{al.Lien__c});
				alMap2.put(al.Award__c, new map<Id,AwardLien__c>{al.Lien__c => al});
			}
		}
		list<InjuryAward__c> iaList;
		if (action != null) {
			 iaList = [SELECT Award__c, Id, Injury__c, Award__r.Name, Award__r.Settlement_Category__c
					   FROM InjuryAward__c 
					   WHERE Award__r.Action__r.Name = :action
					   AND Injury__r.Compensable__c = 'true']; // techDebt
		}
		else {
			 iaList = [SELECT Award__c, Id, Injury__c, Award__r.Name, Award__r.Settlement_Category__c
					   FROM InjuryAward__c 
					   WHERE Award__r.Claimant__c IN :claimants
					   AND Injury__r.Compensable__c = 'true']; // techDebt
		}
		for (InjuryAward__c ia : iaList) {
			if (iaMap.containsKey(ia.Award__c))
				iaMap.get(ia.Award__c).add(ia.Injury__c);
			else
				iaMap.put(ia.Award__c, new set<Id>{ia.Injury__c});
		}
		return Database.getQueryLocator(queryStr);
	}

	public void execute(Database.BatchableContext BC, list<InjuryAward__c> injuryAwards) {
		map<Id,set<AwardLien__c>> upsertMap = new map<Id,set<AwardLien__c>>();
		for (InjuryAward__c iawd : injuryAwards) {
			Id aId = iawd.Award__c;	
			for (Id iId : iaMap.get(aId)) { // loop through InjuryAwards by award
				if (ilMap.containsKey(iId)) { // if we have a corresponding InjuryLien
					for (Id lId : ilMap.get(iId)) { // loop through InjuryLiens by injury
						AwardLien__c al;
						if (!alMap.containsKey(aId) || !alMap.get(aId).contains(lId)) {
							al = new AwardLien__c(Award__c = aId,
												  Lien__c = lId);
						}
						else
							al = alMap2.get(aId).get(lId);

						if (upsertMap.containsKey(aId))
							upsertMap.get(aId).add(al);
						else 
							upsertMap.put(aId, new set<AwardLien__c>{al});
						if (alMap.containsKey(al.Award__c)) {
							alMap.get(al.Award__c).add(al.Lien__c);
							alMap2.get(al.Award__c).put(al.Lien__c, al);
						}
						else {
							alMap.put(al.Award__c, new set<Id>{al.Lien__c});
							alMap2.put(al.Award__c, new map<Id,AwardLien__c>{al.Lien__c => al});
						}
						if (laMap.containsKey(al.Lien__c))
							laMap.get(al.Lien__c).add(al.Award__c);
						else
							laMap.put(al.Lien__c, new set<Id>{al.Award__c});
					}
				}
			}
		}
		for (set<AwardLien__c> alSet : upsertMap.values()) {
			for (AwardLien__c al : alSet) {
				al.Lien_Record_Type__c = al.Lien__r.RecordTypeId;
				set<Id> lls = laMap.get(al.Lien__c);
				set<string> typeSet = new set<string>();
				for (Id i : lls)
					typeSet.add(awardMap.get(i).Settlement_Category__c);
				if (typeSet.size() == 1) {
					decimal percent2 = 1/decimal.valueOf(lls.size()) * 100;
					al.PercentToAward__c = percent2.setScale(2, system.roundingMode.HALF_UP);
				}
				else 
					al.PercentToAward__c = 100;
				if (alMap.containsKey(al.Award__c) && alMap.get(al.Award__c).size() > 1) {
					map<Id,AwardLien__c> awsMap = alMap2.get(al.Award__c);
					integer i = 0;
					for (AwardLien__c aw : awsMap.values()) {
						if (aw.Lien__r.Account__c == al.Lien__r.Account__c)
							i++;
					}
					decimal percent3 = 1/decimal.valueOf(i) * 100;
					al.PercentToLien__c = percent3.setScale(2, system.roundingMode.HALF_UP);
				}
				else
					al.PercentToLien__c = 100;
				upsertSet.add(al);
				if (upsertSet.size() > 9900) {
					upsertList.addAll(upsertSet);
					upsert upsertList;
					upsertSet.clear();
					upsertList.clear();
				}
			}
		}
	}
	
	public void finish(Database.BatchableContext BC) {
		if (!upsertSet.isEmpty()) {
			upsertList.addAll(upsertSet);
			upsert upsertList;
		}
		list<AwardLien__c> deleteList = [SELECT Id FROM AwardLien__c WHERE Lien__r.Stages__c = 'No_Lien'];
		delete deleteList;
		list<InjuryLien__c> deleteList2 = [SELECT Id FROM InjuryLien__c WHERE (Lien__r.Stages__c = 'No_Lien' OR Injury__r.Compensable__c = 'false') limit 5000]; // PD-1103
		delete deleteList2;
	}
}