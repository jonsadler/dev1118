/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* A utility class for adding functionality to triggers, globally. Recursion, deactivation, etc. can
* be handled here and called from each trigger.
* 
* For trigger deactivation, the bypass() method is called at the start of each trigger to determine
* whether the trigger should exit or continue to execute. Is uses the 'Trigger Bypass' custom
* metadata type to accomplish this.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jon Sadler   <jsadler@providiollc.com>
* @modifiedBy     Jon Sadler   <jsadler@providiollc.com>
* @maintainedBy   Jon Sadler   <jsadler@providiollc.com>
* @version        1.0
* @created        2018-11-14
* @modified       2018-11-14
* @systemLayer    Utility
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            email@domain.com
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented until left-aligned with the previous description text.
*
* vX.X            email@domain.com
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
**/

public with sharing class TriggerTools {
    
    private static final string ALL = 'All';
    private static final string NOCMDFOUNDERROR = 'No trigger bypass Custom Metadata record found for type of: ';
    
    //Custom metadata map to determine whether the object's trigger should be active or inactive
    private static map<string,boolean> bypassSettingsMap = new map<string,boolean>();
    
    //Called from triggers. The calling object's label is passed in as a constant from the trigger.
    public static boolean bypass(string callingObj){
        
  		for(Trigger_Bypass__mdt tb : [SELECT DeveloperName, bypass__c FROM Trigger_Bypass__mdt LIMIT 300])
  	  		bypassSettingsMap.put(tb.DeveloperName,tb.bypass__c);
    		
    		//If all triggers are set to be bypassed, or this object's triggers are to be bypassed, return TRUE.
    		//This will stop the trigger from continuing,
    		//If not, return FALSE and the trigger will continue.
    		if(bypassAll() || bypassThis(callingObj)){
	    		return true;
    		}
    		else
    			return false;
    }
    
    //Check if all triggers have been set to bypass
    private static boolean bypassAll(){    	
    	return bypassThis(ALL);
    }
    
    //Check if this object's triggers have been set to bypass
    private static boolean bypassThis(string callingObj){
    	if(bypassSettingsMap.containsKey(callingObj)){
    		return bypassSettingsMap.get(callingObj);
    	}
    	else
    		//In case this object has no corresponding custom metadata record
    		system.debug(NOCMDFOUNDERROR+callingObj);
    		return false;
    }
}