/*
    Created: RAP - May 2017 for May22 Patch
    Purpose: PD-524/525/526/527 - Automation - Handler for Account trigger events - first pass - create Action_Accounts after insert.
			 Coverage as of 5/16 - 100%

History:

	Updated: lmsw - October 2017
	Purpose: Pd-1102 Refactor using test methods within TestUtility
			 Coverage as of 10/4 - 100%

	Updated: CPS - October 2017 for October Release
	Purpose: PD-918 - Enable "Billing Address same as Shipping Address" functionality
*/
@isTest
private class AccountTriggerHandler_Test {
    static testMethod void TestActionAccountCreation() {
    	// Instanciate Test Utility
    	TestUtility testUtl = new TestUtility();

    	// Create test data
    	// Account Record Types
    	testUtl.CreateAccountRecordTypes();
    	Id rtId1 = testUtl.rtAcct_Lawfirm;
		Id rtId2 = testUtl.rtAcct_Lienholder;
		// User
		testUtl.CreateUser();
		User u = testUtl.user1;
		// Actions
		testUtl.CreateActions();
		Action__c action1 = testUtl.action1;
		Action__c action2 = testUtl.action2;

		// Start test
		test.startTest();
		system.runAs(u) {
			ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
																Action__c = action1.Id);
			insert aac;
			testUtl.CreateAccounts();

			aac.Action__c = action2.Id;
			update aac;
	        Account a1 = new Account(Name = 'Referring Law Firm',
	        						 RecordTypeId = rtId1,
	                                 BillingStreet = '124 Main St',
	                                 BillingCity = 'Denver',
	                                 BillingState = 'CO',
	                                 BillingPostalCode = '80202');
	        Account b1 = new Account(Name = 'LRP Provider',
	        						 RecordTypeId = rtId2,
	                                 BillingStreet = '124 Main St',
	                                 BillingCity = 'Denver',
	                                 BillingState = 'CO',
	                                 BillingPostalCode = '80202');
	        insert new list<Account>{a1,b1};
		}
		test.stopTest();
		// Number of Accounts created in TestUtility.CreateAccounts();
		Integer expectedAccts = 6;

		list<Action_Account__c> checklist1 = [SELECT Id FROM Action_Account__c WHERE Action__c = :action1.Id];
		list<Action_Account__c> checklist2 = [SELECT Id FROM Action_Account__c WHERE Action__c = :action2.Id];
		system.assertEquals(expectedAccts, checklist1.size());
		system.assertEquals(2, checklist2.size());
    }

// start PD-918
	// ################################################################################################### testSetBillingAddress_Insert_Checked
	static testMethod void testSetBillingAddress_Insert_Checked() {
		TestUtility          testUtility = new TestUtility();
		Id                   lawfirmRecordTypeId;
		User                 user;
		Action__c            action;
		ActionAssignment2__c actionAssignment;
		Account              testAccount;
		List<Account>        accounts;

		// Get Account record types
		testUtility.CreateAccountRecordTypes();
		lawfirmRecordTypeId = testUtility.rtAcct_LawFirm;

		// Create User
		testUtility.CreateUser();
		user = testUtility.user1;

		// Create Actions
		testUtility.CreateActions();
		action = testUtility.action1;

		Test.startTest();

		System.runAs(user) {
			// Create Action Assignment
			actionAssignment = new ActionAssignment2__c(Name      = userInfo.getUserId(),
					                                    Action__c = action.Id);
			insert actionAssignment;

			// Create Account
			testAccount = new Account(Name                        = 'Alpha Lawfirm',
					                  RecordTypeId                = lawfirmRecordTypeId,
					                  ShippingStreet              = '5613 DTC Parkway',
					                  ShippingCity                = 'Greenwood Village',
					                  ShippingState               = 'CO',
					                  ShippingPostalCode          = '80111',
					                  ShippingCountry             = 'United States',
					                  BillingStreet               = '123 Main St',
					                  BillingCity                 = 'Toronto',
					                  BillingState                = 'ON',
					                  BillingPostalCode           = 'M4B 1B3',
					                  BillingCountry              = 'Canada',
					                  Same_As_Shipping_Address__c = true
			                         );
			insert testAccount;
		}

		Test.stopTest();

		// Get the Account that was created
		accounts = [SELECT Id,
				           BillingStreet,
				           BillingCity,
				           BillingState,
				           BillingPostalCode,
				           BillingCountry,
				           BillingGeocodeAccuracy,
				           BillingLatitude,
				           BillingLongitude,
				           ShippingStreet,
				           ShippingCity,
				           ShippingState,
				           ShippingPostalCode,
				           ShippingCountry,
				           ShippingGeocodeAccuracy,
				           ShippingLatitude,
		                   ShippingLongitude
		            FROM   Account];

		// Assert we found the Account
		System.assertEquals(1, accounts.size());

		// Assert the billing address was set to the shipping address
		System.assertEquals(accounts[0].ShippingStreet,          accounts[0].BillingStreet);
		System.assertEquals(accounts[0].ShippingCity,            accounts[0].BillingCity);
		System.assertEquals(accounts[0].ShippingState,           accounts[0].BillingState);
		System.assertEquals(accounts[0].ShippingPostalCode,      accounts[0].BillingPostalCode);
		System.assertEquals(accounts[0].ShippingCountry,         accounts[0].BillingCountry);
		System.assertEquals(accounts[0].ShippingGeocodeAccuracy, accounts[0].BillingGeocodeAccuracy);
		System.assertEquals(accounts[0].ShippingLatitude,        accounts[0].BillingLatitude);
		System.assertEquals(accounts[0].ShippingLongitude,       accounts[0].BillingLongitude);
	}

	// ################################################################################################### testSetBillingAddress_Insert_NotChecked
	static testMethod void testSetBillingAddress_Insert_NotChecked() {
		TestUtility          testUtility = new TestUtility();
		Id                   lawfirmRecordTypeId;
		User                 user;
		Action__c            action;
		ActionAssignment2__c actionAssignment;
		Account              testAccount;
		List<Account>        accounts;

		// Get Account record types
		testUtility.CreateAccountRecordTypes();
		lawfirmRecordTypeId = testUtility.rtAcct_LawFirm;

		// Create User
		testUtility.CreateUser();
		user = testUtility.user1;

		// Create Actions
		testUtility.CreateActions();
		action = testUtility.action1;

		Test.startTest();

		System.runAs(user) {
			// Create Action Assignment
			actionAssignment = new ActionAssignment2__c(Name      = userInfo.getUserId(),
					                                    Action__c = action.Id);
			insert actionAssignment;

			// Create Account
			testAccount = new Account(Name                        = 'Alpha Lawfirm',
					                  RecordTypeId                = lawfirmRecordTypeId,
					                  ShippingStreet              = '5613 DTC Parkway',
					                  ShippingCity                = 'Greenwood Village',
					                  ShippingState               = 'CO',
					                  ShippingPostalCode          = '80111',
					                  ShippingCountry             = 'United States',
					                  BillingStreet               = '123 Main St',
					                  BillingCity                 = 'Toronto',
					                  BillingState                = 'ON',
					                  BillingPostalCode           = 'M4B 1B3',
					                  BillingCountry              = 'Canada',
					                  Same_As_Shipping_Address__c = false
			                         );
			insert testAccount;
		}

		Test.stopTest();

		// Get the Account that was created
		accounts = [SELECT Id,
				           BillingStreet,
				           BillingCity,
				           BillingState,
				           BillingPostalCode,
				           BillingCountry
		            FROM   Account];

		// Assert we found the Account
		System.assertEquals(1, accounts.size());

		// Assert the billing address was not set to the shipping address
		System.assertEquals('123 Main St', accounts[0].BillingStreet);
		System.assertEquals('Toronto',     accounts[0].BillingCity);
		System.assertEquals('ON',          accounts[0].BillingState);
		System.assertEquals('M4B 1B3',     accounts[0].BillingPostalCode);
		System.assertEquals('Canada',      accounts[0].BillingCountry);
	}

	// ################################################################################################### testSetBillingAddress_Update_Checked
	static testMethod void testSetBillingAddress_Update_Checked() {
		TestUtility          testUtility = new TestUtility();
		Id                   lawfirmRecordTypeId;
		User                 user;
		Action__c            action;
		ActionAssignment2__c actionAssignment;
		Account              testAccount;
		List<Account>        accounts;

		// Get Account record types
		testUtility.CreateAccountRecordTypes();
		lawfirmRecordTypeId = testUtility.rtAcct_LawFirm;

		// Create User
		testUtility.CreateUser();
		user = testUtility.user1;

		// Create Actions
		testUtility.CreateActions();
		action = testUtility.action1;

		Test.startTest();

		System.runAs(user) {
			// Create Action Assignment
			actionAssignment = new ActionAssignment2__c(Name      = userInfo.getUserId(),
					                                    Action__c = action.Id);
			insert actionAssignment;

			// Create Account
			testAccount = new Account(Name                        = 'Alpha Lawfirm',
					                  RecordTypeId                = lawfirmRecordTypeId,
					                  ShippingStreet              = '5613 DTC Parkway',
					                  ShippingCity                = 'Greenwood Village',
					                  ShippingState               = 'CO',
					                  ShippingPostalCode          = '80111',
					                  ShippingCountry             = 'United States'
			                         );
			insert testAccount;

			// Update Account
			testAccount.Same_As_Shipping_Address__c = true;
			update testAccount;
		}

		Test.stopTest();

		// Get the Account that was updated
		accounts = [SELECT Id,
				           BillingStreet,
				           BillingCity,
				           BillingState,
				           BillingPostalCode,
				           BillingCountry,
				           BillingGeocodeAccuracy,
				           BillingLatitude,
				           BillingLongitude,
				           ShippingStreet,
				           ShippingCity,
				           ShippingState,
				           ShippingPostalCode,
				           ShippingCountry,
				           ShippingGeocodeAccuracy,
				           ShippingLatitude,
		                   ShippingLongitude
		            FROM   Account];

		// Assert we found the Account
		System.assertEquals(1, accounts.size());

		// Assert the billing address was set to the shipping address
		System.assertEquals(accounts[0].ShippingStreet,          accounts[0].BillingStreet);
		System.assertEquals(accounts[0].ShippingCity,            accounts[0].BillingCity);
		System.assertEquals(accounts[0].ShippingState,           accounts[0].BillingState);
		System.assertEquals(accounts[0].ShippingPostalCode,      accounts[0].BillingPostalCode);
		System.assertEquals(accounts[0].ShippingCountry,         accounts[0].BillingCountry);
		System.assertEquals(accounts[0].ShippingGeocodeAccuracy, accounts[0].BillingGeocodeAccuracy);
		System.assertEquals(accounts[0].ShippingLatitude,        accounts[0].BillingLatitude);
		System.assertEquals(accounts[0].ShippingLongitude,       accounts[0].BillingLongitude);
	}

	// ################################################################################################### testSetBillingAddress_Update_NotChecked
	static testMethod void testSetBillingAddress_Update_NotChecked() {
		TestUtility          testUtility = new TestUtility();
		Id                   lawfirmRecordTypeId;
		User                 user;
		Action__c            action;
		ActionAssignment2__c actionAssignment;
		Account              testAccount;
		List<Account>        accounts;

		// Get Account record types
		testUtility.CreateAccountRecordTypes();
		lawfirmRecordTypeId = testUtility.rtAcct_LawFirm;

		// Create User
		testUtility.CreateUser();
		user = testUtility.user1;

		// Create Actions
		testUtility.CreateActions();
		action = testUtility.action1;

		Test.startTest();

		System.runAs(user) {
			// Create Action Assignment
			actionAssignment = new ActionAssignment2__c(Name      = userInfo.getUserId(),
					                                    Action__c = action.Id);
			insert actionAssignment;

			// Create Account
			testAccount = new Account(Name                        = 'Alpha Lawfirm',
					                  RecordTypeId                = lawfirmRecordTypeId,
					                  ShippingStreet              = '5613 DTC Parkway',
					                  ShippingCity                = 'Greenwood Village',
					                  ShippingState               = 'CO',
					                  ShippingPostalCode          = '80111',
					                  ShippingCountry             = 'United States'
			                         );
			insert testAccount;

			// Update Account
			testAccount.BillingStreet     = '123 Main St';
			testAccount.BillingCity       = 'Toronto';
			testAccount.BillingState      = 'ON';
			testAccount.BillingPostalCode = 'M4B 1B3';
			testAccount.BillingCountry    = 'Canada';

			update testAccount;
		}

		Test.stopTest();

		// Get the Account that was updated
		accounts = [SELECT Id,
				           BillingStreet,
				           BillingCity,
				           BillingState,
				           BillingPostalCode,
				           BillingCountry
		            FROM   Account];

		// Assert we found the Account
		System.assertEquals(1, accounts.size());

		// Assert the billing address was not set to the shipping address
		System.assertEquals('123 Main St', accounts[0].BillingStreet);
		System.assertEquals('Toronto',     accounts[0].BillingCity);
		System.assertEquals('ON',          accounts[0].BillingState);
		System.assertEquals('M4B 1B3',     accounts[0].BillingPostalCode);
		System.assertEquals('Canada',      accounts[0].BillingCountry);
	}
// end PD-918
}