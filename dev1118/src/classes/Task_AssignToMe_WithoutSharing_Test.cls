/*
    Created: CPS - August 2017 for September Release
    Purpose: PD-966 - Test class for Task_AssignToMe_WithoutSharing
*/

@isTest
public class Task_AssignToMe_WithoutSharing_Test {
    // ################################################################################################## testSetup
    @testSetup
    static void testSetup() {
        Action__c  testAction01;
        List<User> testUsers;
        Task       testTask01;
        Task       testTask02;

        // Create test Action
        testAction01 = new Action__c(
            Name                   = 'Test Action 01',
            Holdback_Percentage__c = 0.5,
            Fees_Paid_By__c        = 'Claimant',
            Active__c              = true
        );

        insert testAction01;

        // Get two active users in PLC Management profile
        testUsers = [SELECT Id
                     FROM   User
                     WHERE  Profile.Name = 'PLC Management'
                            AND IsActive = true
                     LIMIT  2];

        System.assertEquals(2, testUsers.size(), 'DEBUG: (testSetup) Did not find two users in PLC Management profile');

        // Create two Tasks, one for each user
        testTask01 = new Task(
            Subject      = 'Test Task 01',
            ActivityDate = System.today(),
            OwnerId      = testUsers[0].Id,
            WhatId       = testAction01.Id,
            Priority     = 'Normal'
        );

        testTask02 = new Task(
            Subject      = 'Test Task 02',
            ActivityDate = System.today(),
            OwnerId      = testUsers[1].Id,
            WhatId       = testAction01.Id,
            Priority     = 'Normal'
        );

        insert new List<Task>{testTask01, testTask02};
    }

    // ################################################################################################## testGetTask
    // This test method asserts the getTask method returns the correct Task data.
    static testMethod void testGetTask() {
        Task expectedTask;
        Task returnedTask;

        // Get the expected Task
        expectedTask = [SELECT Id,
                               OwnerId,
                               Subject
                        FROM   Task
                        LIMIT  1];

        // Get the returned Task
        returnedTask = Task_AssignToMe_WithoutSharing.getTask(expectedTask.Id);

        // Assert that the expected and returned fields are the same
        System.assertEquals(expectedTask.Id,      returnedTask.Id);
        System.assertEquals(expectedTask.OwnerId, returnedTask.OwnerId);
        System.assertEquals(expectedTask.Subject, returnedTask.Subject);
    }

    // ################################################################################################## testGetTask_NullTaskId
    // This test method asserts the getTask method returns null if passed a null Task ID.
    static testMethod void testGetTask_NullTaskId() {
        System.assertEquals(null, Task_AssignToMe_WithoutSharing.getTask(null));
    }

    // ################################################################################################## testGetCurrentUserId
    // This test method asserts the getCurrentUserId method returns the correct ID.
    static testMethod void testGetCurrentUserId() {
        User user;
        Id   expectedUserId;
        Id   returnedUserId;

        // Get the user ID from one of the Tasks
        expectedUserId = [SELECT Id,
                                 OwnerId
                          FROM   Task
                          LIMIT  1].OwnerId;

        // Get the user record
        user = [SELECT Id
                FROM   User
                WHERE  Id = :expectedUserId];

        // Assert the user ID returned is valid when run as this user
        System.runAs(user) {
            returnedUserId = Task_AssignToMe_WithoutSharing.getCurrentUserId();
        }

        System.assertEquals(expectedUserId, returnedUserId);
    }

    // ################################################################################################## testUserCannotUpdateOtherTask
    // This test method asserts that a user cannot normally update another user's Task.
    static testMethod void testUserCannotUpdateOtherTask() {
        List<Task> tasks;
        User       user;
        Boolean    exceptionCaught = false;

        // Get both tasks
        tasks = [SELECT Id,
                        OwnerId
                 FROM   Task];

        // Get user assigned to first task
        user = [SELECT Id
                FROM   User
                WHERE  Id = :tasks[0].OwnerId];

        // Have this user try to update the other user's task.  This should raise a DML exception.
        System.runAs(user) {
            try {
                tasks[1].Subject = 'Subject UPDATED';

                update tasks[1];
            }
            catch (DmlException e) {
                exceptionCaught = true;
            }
        }

        System.assert(exceptionCaught, 'DEBUG: (testUserCannotUpdateOtherTask) User was able to update other users task; exception not caught.');
    }

    // ################################################################################################## testAssignTaskToCurrentUser
    // This test method asserts the assignTaskToCurrentUser method does reassign another user's task
    // to the current user.
    static testMethod void testAssignTaskToCurrentUser() {
        List<Task> tasks;
        User       user;
        Boolean    exceptionCaught = false;
        Task       updatedTask;

        // Get both tasks
        tasks = [SELECT Id,
                OwnerId
        FROM   Task];

        // Get user assigned to first task
        user = [SELECT Id
        FROM   User
        WHERE  Id = :tasks[0].OwnerId];

        // Have this user try to reassign the other user's task to themself.  This should succeed.
        System.runAs(user) {
            try {
                Task_AssignToMe_WithoutSharing.assignTaskToCurrentUser(tasks[1].Id);
            }
            catch (DmlException e) {
                exceptionCaught = true;
            }
        }

        // Assert no exception was caught
        System.assert(!exceptionCaught, 'DEBUG: (testAssignTaskToCurrentUser) Exception was caught.');

        // Assert the task was updated with the new owner
        updatedTask = [SELECT Id,
                OwnerId
        FROM   Task
        WHERE  Id = :tasks[1].Id];

        System.assertEquals(user.Id, updatedTask.OwnerId);
    }

    // ################################################################################################## testAssignTaskToCurrentUser_NullTaskId
    // This test method asserts the assignTaskToCurrentUser method returns false if passed a null Task ID.
    static testMethod void testAssignTaskToCurrentUser_NullTaskId() {
        System.assertEquals(false, Task_AssignToMe_WithoutSharing.assignTaskToCurrentUser(null));
    }

    // ################################################################################################## testAssignTaskToCurrentUser_NonexistantTaskId
    // This test method asserts the assignTaskToCurrentUser method returns false if passed a nonexistant Task ID.
    static testMethod void testAssignTaskToCurrentUser_NonexistantTaskId() {
        System.assertEquals(false, Task_AssignToMe_WithoutSharing.assignTaskToCurrentUser('00Tc000000IuPCi'));
    }

}