/*
    Created - RAP September 2017 for September Release
	Purpose: PD-872 - Create Internal Needs Actions

*/
global class ClaimantBatch implements Database.Batchable<Claimant__c> {

	public static string claimantStr = 'SELECT First_Name__c, Last_Name__c, SSN__c, DOB__c, Gender__c, Address_1__c, City__c, State__c, Zip__c, Phone__c, Opted_in_PLRP__c ' +
										'FROM Claimant__c';
	public string action;
	public set<Id> claimantIds;
	public boolean isTest;
		
    public ClaimantBatch() {}

    public ClaimantBatch(string act) {
    	action = act;
    }
    
    public ClaimantBatch(set<Id> cIds) {
		claimantIds = cIds;
    }
    
	global Iterable<Claimant__c> start(Database.BatchableContext BC) {
		isTest = test.isRunningTest();
		if (action != null)
			claimantStr += isTest ? '' : ' WHERE Action__r.Name = :action'; 
		else if (claimantIds != null)
			claimantStr += isTest ? '' : ' WHERE Id IN :claimantIds'; 
		list<Claimant__c> claimants = database.query(claimantStr);
		return claimants;
	}

	public void execute(Database.BatchableContext BC, list<Claimant__c> claimants) {
		if (claimants == null || claimants.isEmpty())	
			return;
		update claimants;
	}
	
	public void finish(Database.BatchableContext BC) {}
}