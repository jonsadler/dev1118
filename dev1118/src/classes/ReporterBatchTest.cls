/*
	Created: RAP - January 2018 for January Release
	Purpose: PD-1311 - Modified Report - with specified objects 
	
 */
@isTest
private class ReporterBatchTest {

	public static TestUtility tu = new TestUtility();

    static testMethod void TestReporterBatchTool() {
		test.startTest();
		tu.createInjuryLiens();
		tu.createInjuryAwards();
        test.stopTest();
    }

    static testMethod void TestReporterBatchProcessor() {
		tu.createInjuryAwards();
		tu.createInjuryLiens();
		test.startTest();
		database.executeBatch(new ReporterBatchProcessor('testAction01'),200);
        test.stopTest();
    }
}