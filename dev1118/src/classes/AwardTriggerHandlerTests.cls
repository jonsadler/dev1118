/*
	Created: MEF - January 2017 for first release
	Purpose: test coverage for AwardTrigggerHandler class

History:

    Updated: RAP - March 2017 for May Release
    Purpose: PD-520 - Extraneous Field Clean-up (change Description__c to Description2__c)
					  Coverage as of 3/27/17 - 91% 

	Updated: lmsw - July 2017 
    Purpose: Repair testMethod and modify to use TestUtility
    		 Coverage as of 7/12 - ActionController: 93%
*/

@isTest
private class AwardTriggerHandlerTests {
@testSetup
	static void CreateData() {
		TestUtility.createTestData();
	}
	
	static testMethod void TestAwardNaming() {
		test.startTest();
		Award__c updatedAward = [SELECT Id, Name FROM Award__c LIMIT 1];
//		system.assertEquals('RAP RAPPER - Conserve Claim (3.3.1) - TestAction1', updatedAward.Name);
		test.stopTest();
	}
	static testMethod void TestClaimantUpdate() {
		test.startTest();
		list<NeedsAttention__c> updateList = [SELECT Id, Internal_Status__c, Special_Case_Explanation__c FROM NeedsAttention__c];
		for (NeedsAttention__c na : updateList) {
			na.Internal_Status__c = 'Special Case - Ignore';
			na.Special_Case_Explanation__c = 'because';
		}
		update updateList;
		Claimant__c c = [SELECT Id, Name, Status__c, Needs_Attention_Total__c FROM Claimant__c WHERE Last_Name__c = 'RAPPER'];
		list<Award__c> aList = [SELECT Id, Distribution_Status__c FROM Award__c WHERE Claimant__c = :c.Id];
		system.assertEquals('Not_Ready_to_be_Cleared', c.Status__c);
		aList[0].Distribution_Status__c = 'Released_to_a_Holdback';
		update aList[0];
		c = [SELECT Id, Name, Status__c FROM Claimant__c WHERE Id = :c.Id];
		system.assertEquals('Released_to_a_Holdback', c.Status__c);
		for (Award__c aw : aList)
			aw.Distribution_Status__c = 'Cleared';
		update aList;
		c = [SELECT Id, Name, Status__c FROM Claimant__c WHERE Id = :c.Id];
		list<NeedsAttention__c> testList = [SELECT Internal_Status__c, Internal_Type__c, Alert_Detail__c FROM NeedsAttention__c WHERE Claimant__c = :c.Id];
		system.debug(loggingLevel.INFO, 'RAP --->> needs attentions: ' + testList);
		system.assertEquals('Cleared', c.Status__c);
		test.stopTest();
	}
}