/*
    Created - RAP January 2018 for January Release
    Purpose - PD-1311 - Modified Report - with specified objects
    
*/

global class ReporterBatchProcessor implements Database.Batchable<SObject>, Database.Stateful {

	public static string QUERY = 'SELECT Id, Injury__c FROM InjuryAward__c ';
	public string action{get;set;}
	
    global ReporterBatchProcessor(string act) {
    	action = act;
    	if (!test.isRunningTest() && action != 'All')
			QUERY += 'WHERE Award__r.Action__r.Name = \'' + action + '\'';
    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(QUERY);
	}

	public void execute(Database.BatchableContext BC, list<InjuryAward__c> injuryAwards) {
		ReporterBatchTool rbt = new ReporterBatchTool();
		list<Id> iIds = new list<Id>();
		set<Id> iaIds = new set<Id>();
		set<Id> ilIds = new set<Id>();
		for (InjuryAward__c ia : injuryAwards) {
			iaIds.add(ia.Id);
			iIds.add(ia.Injury__c);
		}
		list<InjuryLien__c> ilList = [SELECT Id FROM InjuryLien__c WHERE Injury__c IN :iIds];
		for (InjuryLien__c il : ilList)
			ilIds.add(il.Id);
		rbt.CreateReporters(iaIds, ilIds);
	}
	
	public void finish(Database.BatchableContext BC) {}

}