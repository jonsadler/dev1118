/*
    Created: lmsw - July 2017
    Purpose: PD-844 Create an Activity on an Event or Call.

History: 
 	
 	Updated: lmsw - Aug 2017
 	Purpose: Update Action on Task.
*/
public with sharing class TaskUtility {
	public static map<Id,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Lien__c.getRecordTypeInfosById();
    
	public void onBeforeInsert(list<Task> newTaskRecs, map<Id, Task> newTasksMap) {
		AddAction(newTaskRecs);
	}

	public void afterInsert() {
		CreateCalls();
	}

	private static void CreateCalls() {
		//Create on Activities object so that it will show up on portal
        list<Activity__c> insertActivityList = new list<Activity__c>();
        for (SObject s : trigger.new) {
        	Task taskRecord = (Task)s;
        	if (taskRecord.TaskSubtype == 'Call') {
        		DateTime now = system.now();
	        	string relatedTo = string.valueOf(taskRecord.whatId);
	        	
	        	if (relatedTo.startsWithIgnoreCase('a0V')) {
	        		// For call related to a Claimant
	        		system.debug('Add event related to Claimant');
	        		Activity__c evC = new Activity__c(Associated_Claimant__c = relatedTo,
													  Activity_Date__c = now,
													  Description__c = taskRecord.description,
													  Subject__c = taskRecord.subject,
													  Type__c = 'Call');
	        		insertActivityList.add(evC);
	        	}
	        	else if (relatedTo.startsWithIgnoreCase('a08')) {
	        		// For call related to a Lien
	        		lien__c l = [SELECT Id, Claimant__c, RecordTypeId FROM Lien__c WHERE Id =: relatedTo LIMIT 1];
	        		string rtName = rtMap.get(l.RecordTypeId).getName();
	        		Activity__c ev1 = new Activity__c(Associated_Lien__c = relatedTo,
													  Activity_Date__c = now,
													  Description__c = taskRecord.description,
													  Subject__c = taskRecord.subject,
													  Type__c = 'Call');
	            	Activity__c ev2 = new Activity__c(Associated_Claimant__c = l.Claimant__c,
													  Activity_Date__c = now,
													  Description__c = rtName +' - '+ taskRecord.description,
													  Subject__c = taskRecord.subject,
													  Type__c = 'Call');
	            	insertActivityList.addAll(new list<Activity__c>{ev1,ev2});
	        	}
	        }
        }
        if (!insertActivityList.isEmpty())
        	insert insertActivityList;	
    } 

    private static void AddAction(list<Task> newTasks) {
    	User currentUser =  [SELECT Id, CurrentAction__c FROM User WHERE Id = : UserInfo.getUserId()];
    	
    	// Get Action ID/Name map
    	map<Id,String> actionMap = new map<Id,String>();
    	for(Action__c a : [SELECT Id, Name from Action__c WHERE Id =: currentUser.CurrentAction__c]){
    		actionMap.put(a.Id,a.Name);
    	}

    	// Add Action Name to new Task
    	for (Task t : newTasks) {
    		t.Action__c = actionMap.get(currentUser.CurrentAction__c);
    	}
    }      	
}