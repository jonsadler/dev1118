/*
    Created - RAP August 2017 for August Release
    Purpose - Set flag on Claimants that have issues keeping them from getting funds
    
History:

	Updated: RAP August 2017 for August Release
	Purpose: QA enhancement - run batch on claimant update - only for trigger contents

	Updated: RAP September 2017 for September Patch
	Purpose: PD-975 - Fix Medicare / Medicaid Lien Calculations, QA batch process

	Updated: RAP September 2017 for September Release
	Purpose: PD-998 - Create QC Component (update processor to create Needs Attention objects)

	Updated: RAP September 2017 for September Release
	Purpose: PD-995 - Move processing out of batch processor for multi-point access

    Updated: RAP - January 2017 for January Release
    Purpose: PD-1346 - Update QA Batch to Remove Dependency on Award Data

*/
global class QABatch implements Database.Batchable<Claimant__c>, Database.Stateful {

	private static string claimantStr = 'SELECT Id FROM Claimant__c ';
	public string action;
	public boolean isTest;
		
    public QABatch(string act) {
    	action = act;
		isTest = test.isRunningTest();
    }
    
	global Iterable<Claimant__c> start(Database.BatchableContext BC) {
// start PD-975 	
		claimantStr += isTest ? '' : 'WHERE Action__r.Name = :action'; 
// end PD-975
		list<Claimant__c> claimants = database.query(claimantStr);
		return claimants;
	}

	public void execute(Database.BatchableContext BC, list<Claimant__c> claimants) {
		if (claimants == null || claimants.isEmpty())	
			return;
		set<Id> cIds = new set<Id>();
		for (Claimant__c c : claimants)
			cIds.add(c.Id);
		boolean result = QAEngine.ProcessClaimants(cIds); // PD-1346
		if (!result)
			system.debug(loggingLevel.INFO, 'RAP --->> QA batch failed on Action' + action);
	}
	
	public void finish(Database.BatchableContext BC) {}
}