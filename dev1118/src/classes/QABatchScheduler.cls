/*
Created: RAP August 2017 for August Release
Purpose: Update Claimant information with Release issues
*/

global class QABatchScheduler implements Schedulable {

	global string actionName;
	
	global QABatchScheduler (string action) {
		actionName = action;
	}
	
	global void execute(SchedulableContext sc) {
        integer batchSize = 200;
        database.executeBatch(new QABatch(actionName), batchSize);
    }
    
    public void scheduleJob(string scheduleString, string action) {
        if (scheduleString == null)
            scheduleString = '0 0 4 * * ?'; // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        actionName = action;
        system.schedule(getUniqueJobName(), scheduleString, new QABatchScheduler (actionName));
    }
      
    public string getUniqueJobName() {
        //construct unique job name by appending timestamp
        string uniqueJobName = 'QABatch - ' + actionName;
        system.debug('job name : ' + uniqueJobName);
        return uniqueJobName;
    }    
}