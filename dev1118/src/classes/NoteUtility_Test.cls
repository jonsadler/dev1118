/*
    Created: RAP - March 2017 for May Release
    Purpose: PD-465 - Lien Detail Page - Notes, Tasks, and Phone calls

History:

	Updated: RAP - April 2017 for May Release
	Purpose: Add Fee Model to CreateData method
    		 Code Coverage as of 4/25: CDNoteTrigger 100%
			    		 			   NoteUtility 100%
  Updated: lmsw - July 2017 
  Purpose: Repair testMethod and modify to use TestUtility
         Coverage as of 7/12 - CDNoteTrigger 100%
                               NoteUtility 100%
    
*/
@isTest
private class NoteUtility_Test {

  @testSetup
  	static void CreateData() {
      TestUtility.createTestData();
	}
	
  static testMethod void TestNoteLinking() {
	Lien__c l = [SELECT Id, Claimant__c FROM Lien__c WHERE Stages__c = 'To_Be_Submitted' limit 1];
	ApexPages.currentPage().getParameters().put('id', l.Id);
  	test.startTest();
  	ContentNote cn = new ContentNote(Title = 'Test Note',
  									 Content = BLOB.valueOf('This is a test note'));
  	insert cn;
  	ContentDocumentLink cdl = new ContentDocumentLink(Visibility = 'AllUsers',
													  ShareType = 'V',
													  LinkedEntityId = l.Id,
													  ContentDocumentId = cn.Id);
	insert cdl;
  	ContentDocument cd = [SELECT Title, Description FROM ContentDocument WHERE Id = :cn.Id];
  	cd.Description = 'Something';
  	update cd;
  	test.stopTest();
  	list<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink WHERE ContentDocumentId = :cd.Id AND LinkedEntityId = :l.Claimant__c];
  	system.assertEquals(1, links.size());
  }
}