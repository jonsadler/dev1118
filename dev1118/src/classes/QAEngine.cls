/*
    Created: RAP - September 2017 for September Release
    Purpose: PD-995 - Create Needs Attention Component for Back-End

	Updated: RAP - October 2017 for October Release
	Purpose: PD-1114 - Run a Calculation that will Compare Total Final Payable on Liens vs Award Liens

    Updated: RAP - October 2017 for October Release
    Purpose: PD-1171 - Create Needs Attention if PLRP health plan type required and not present

    Updated: RAP - November 2017 for November Release
    Purpose: PD-1235 - QC should only create Subrogation Needs Attentions for PLRP and non-PLRP Liens

    Updated: RAP - December 2017 for immediate Release
    Purpose: PD-1262 - Liens not Clearing when no longer outstanding - Awards and Final Payable are correct

    Updated: RAP - January 2017 for January Release
    Purpose: PD-1346 - Update QA Batch to Remove Dependency on Award Data
*/
public without sharing class QAEngine {

    public static map<Id,list<NeedsAttention__c>> naMap = new map<Id,list<NeedsAttention__c>>();
    public static map<Id, list<string>> portalMap{get;set;}
    public static final set<string> SUBROSTATES = new set<string>{'NY', 'NJ', 'VA', 'MO', 'CT', 'AZ', 'KS', 'NC'};
    public static Id rtId = Schema.SObjectType.NeedsAttention__c.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
    public static boolean isTrigger;
// start PD-1346
    public static boolean ProcessAwards(list<Award__c> awards) {
		if (awards == null || awards.isEmpty())
			return false;
        try {
			isTrigger = trigger.new != null;
        }
        catch (exception e) {
			isTrigger = false;
        }
        return false;
    }
    public static boolean ProcessClaimants(set<Id> cIds) {
		if (cIds == null || cIds.isEmpty())
			return false;
        if (NAUtility.hasRun)
        	return true;
        try {
        	isTrigger = trigger.new != null;
        }
        catch (exception e) {
        	isTrigger = false;
        }
        string action;
		list<NeedsAttention__c> deleteList = new list<NeedsAttention__c>();
        map<Id,string> checkMap = new map<Id,string>();
        map<Id,Id> cMap = new map<Id,Id>();
        map<Id,list<Model_Detail__c>> modelMap = new map<Id,list<Model_Detail__c>>();
        map<Id,Lien__c> ledMap;
        set<Claimant__c> cSet = new set<Claimant__c>();
        map<Id,list<Award__c>> awardMap = new map<Id,list<Award__c>>(); 
        boolean isTest;
        set<Id> claimants;
        map<Id,Claimant__c> claimMap;
        list<Id> mList = new list<Id>();
        list<Id> lList = new list<Id>();
        claimMap = new map<Id,Claimant__c>([SELECT Id, DOB__c, Date_SSN_Changed__c, Action__r.Fees_Paid_By__c, Negative_Balance__c, Num_Liens__c, Total_Applied__c, Status__c,
                                                   Date_Released__c, Action__c, Name, Action__r.Name,
                                                   (SELECT Id, PLC_Determination__c FROM HealthPlans__r),
                                                   (SELECT Id, Account__c, Stages__c FROM Liens__r),
                                                   (SELECT Internal_Status__c, Internal_Type__c, Id FROM Needs_Attentions__r WHERE RecordType.Name = 'Internal')
                                            FROM Claimant__c
                                            WHERE Id in :cIds]);
		action = claimMap.values()[0].Action__r.Name;
        for (Claimant__c c : claimMap.values()) {
            naMap.put(c.Id, new list<NeedsAttention__c>());
            for (NeedsAttention__c na : c.Needs_Attentions__r) {
                checkMap.put(na.Id,na.Internal_Status__c);
                if (na.Internal_Status__c != 'Special Case - Ignore')
                    na.Internal_Status__c = 'Resolved';
                naMap.get(c.Id).add(na);
            }
            for (Lien__c l : c.Liens__r)
            	mList.add(l.Account__c);
        }
        list<Model_Detail__c> modelList = [SELECT Payable_Amount__c, Name, Eligibility__c, Award_Type__c, Provider__c, RecordType.Name, PayerType__c
                                           FROM Model_Detail__c
                                           WHERE Action__r.Name = :action
                                           AND Provider__c IN :mList];
		decimal medicareAmount = 0;
        for (Model_Detail__c md : modelList) {
            if (md.RecordType.Name == 'Medicare Global Model' && md.Payable_Amount__c > medicareAmount)
            	medicareAmount = md.Payable_Amount__c;
            if (modelMap.containsKey(md.Provider__c))
                modelMap.get(md.Provider__c).add(md);
            else
                modelMap.put(md.Provider__c, new list<Model_Detail__c>{md});
        }
        map<string,LRP_Plan_Type__c> lrpMap = LRP_Plan_Type__c.getAll(); // PD-1171

		list<Award__c> awards = [SELECT Amount_Remaining__c, Id, Claimant__r.DOB__c, Name, Claimant__c, Claimant__r.Total_Applied__c, Claimant__r.Num_Liens__c, Amount_Disbursed__c, 
										Claimant__r.Negative_Balance__c, Claimant__r.Date_SSN_Changed__c, Description__c, Amount__c, Claimant__r.Status__c, Distribution_Status__c, 
										(SELECT Lien__c, Lien__r.HBAmount__c, Lien__r.Total_Fees_Applied__c, Lien__r.Total_Fees_Paid__c, Lien__r.Name__c, Lien__r.RecordType.Name, 
												Lien__r.Stages__c, Lien__r.Substage__c, Final_Lien_Amount__c, Lien__r.PayerType__c, Lien__r.Account__c, Lien__r.Date_No_Interest__c, 
												Lien__r.Subrogation_Rights__c, Lien__r.State__c, Lien__r.Date_Submitted__c, Amount_Applied__c  
										 FROM Liens__r 
										 WHERE Lien__r.Substage__c != 'Not_Eligible') 
								 FROM Award__c 
								 WHERE Claimant__c IN :cIds];
        for (Award__c a : awards) {
        	if (awardMap.containsKey(a.Claimant__c))
        		awardMap.get(a.Claimant__c).add(a);
        	else
        		awardMap.put(a.Claimant__c, new list<Award__c>{a});
	    }
        ledMap = new map<Id,Lien__c>([SELECT Id, Name__c, RecordType.Name, Non_PLRP_Type__c, PayerType__c, Name, Date_Submitted__c, State__c, Health_Plan_Type__c,
        									 Subrogation_Rights__c, Account__c, Stages__c, Date_No_Interest__c, Account__r.Name, Action__r.Name, 
        									 Claimant__c, Final_Payable__c, HBAmount2__c, // PD-1114
        									 cHICN__c, // PD-1235
                                             (SELECT Date_Start__c, Date_End__c, LienType__c FROM LienEligibilityDates__r),
                                             (SELECT Injury__r.DOL__c, Injury__r.Compensable__c FROM Injuries__r),
                                             (SELECT Internal_Status__c, Internal_Type__c, Id FROM Needs_Attention__r WHERE RecordType.Name = 'Internal')
                                      FROM Lien__c
                                      WHERE Claimant__c IN :cIds]);
       	decimal tempAdd = 0;
        for (Lien__c l : ledMap.values()) {
            naMap.put(l.Id, new list<NeedsAttention__c>());
            for (NeedsAttention__c na : l.Needs_Attention__r) {
                checkMap.put(na.Id,na.Internal_Status__c);
                if (na.Internal_Status__c != 'Special Case - Ignore')
                    na.Internal_Status__c = 'Resolved';
                naMap.get(l.Id).add(na);
            }
        }
// start PD-1114
		list<AggregateResult> aggRes1 = [SELECT Lien__c, SUM(Final_Lien_Amount__c) fTotal, SUM(Amount_Applied__c) hTotal FROM AwardLien__c WHERE lien__c IN :ledMap.keySet() AND Lien__r.Stages__c != 'No_Lien' group by Lien__c];
		decimal check1;
		decimal check2;
		decimal check3;
		decimal check4;
        for (AggregateResult agg1 : aggRes1) {
        	Lien__c l = ledMap.get((Id)agg1.get('Lien__c'));
        	check1 = (decimal)agg1.get('fTotal') == null ? 0 : (decimal)agg1.get('fTotal');
        	check2 = (decimal)agg1.get('hTotal') == null ? 0 : (decimal)agg1.get('hTotal');
        	check3 = l.Final_Payable__c == null ? 0 : l.Final_Payable__c;
        	check4 = l.HBAmount2__c == null ? 0 : l.HBAmount2__c;

        	if (check1.setScale(2) != check3.setScale(2))
        		CreateLNA(l,'Final_Mismatch');
        	if (check2.setScale(2) != check4.setScale(2))
        		CreateLNA(l,'Holdback_Mismatch');
        }
// end PD-1114		
        for (Id cId : cIds) {
	        boolean eligible = false;
	        boolean medicareLien = false;
	        boolean partc = false;
	        boolean needPartC = false;
	        boolean plrpFound = false;
	        boolean needPLRP = false;
            integer plrp = 0;
            string rtName;
            Claimant__c claimant = claimMap.get(cId);
	        if (claimant.Negative_Balance__c)   					// #1
	            CreateCNA(claimant,'Negative_balance');
	        if (claimant.Num_Liens__c <= 0)         				// #2
	            CreateCNA(claimant,'No_liens');
	        if (claimant.Action__r.Fees_Paid_By__c == 'Claimant' && claimant.Total_Applied__c <= 0)
	            CreateCNA(claimant, 'No_fees_assessed');
	        if (claimant.Status__c == 'Released_to_a_Holdback' && claimant.Date_Released__c != null && claimant.Date_Released__c.daysBetween(system.today()) > 90)
	            CreateCNA(claimant,'Holdback_more_than_90');
	        for (Health_Plan__c hp : claimant.HealthPlans__r) {
	            if (string.isBlank(hp.PLC_Determination__c))        // #5
	                CreateCNA(claimant,'Health_plan_no_determination');
	            else if (hp.PLC_Determination__c == 'Created_Non_PLRP_Lien')
	                needPLRP = true;
	        }
	        for (Lien__c l : claimant.Liens__r) {
                Lien__c lien = ledMap.get(l.Id);
                rtName = lien.RecordType.Name;
                if (claimant.Date_SSN_Changed__c != null && claimant.Date_SSN_Changed__c > lien.Date_Submitted__c)  // #12
                    CreateCNA(claimant,'SSN_changed');
                if (lien.Non_PLRP_Type__c == 'Part_C' || lien.PayerType__c == 'Primary_Part_C')
                    partc = true;
                for (InjuryLien__c ilc : lien.Injuries__r) {
                    if (needPartC)
                        continue;
                    if (eligible == false && ilc.Injury__r.Compensable__c == 'True') 	// Over 65 and no match with Medicare
                        eligible = claimant.DOB__c.monthsBetween(ilc.Injury__r.DOL__c) >= 774;
                    for (LienEligibilityDate__c led : lien.LienEligibilityDates__r) {   // Part C range covers a DOL and no Part C lien for that Injury
                        if (led.LienType__c == 'PartC' &&
                            led.Date_Start__c < ilc.Injury__r.DOL__c && 
                            (led.Date_End__c > ilc.Injury__r.DOL__c || led.Date_End__c == null)) {
                            needPartC = true;
                            break;
                        }
                    }
                }
                if (rtName == 'Medicare Global Model')  // Over 65 and no match with Medicare
                    medicareLien = true;
                if (rtName == 'Non-PLRP' || rtName == 'Other Govt')   // Determination = Created Non-PLRP lien, but the Non-PLRP Lien has not been established
                    plrpFound = true;
                if (lien.PayerType__c != null && lien.PayerType__c.startsWith('Primary')) {
                    if (lien.Final_Payable__c < medicareAmount) 		// #6
                        CreateLNA(lien,'Primary_but_<_medicare');
                    if (rtName.startsWith('PLRP')) {
                        if (plrp == 1)                                                                      // #7
                            CreateLNA(lien,'Double_PLRP');
                        plrp = 1;
                    }
                }
                if (rtName.startsWith('PLRP')) {
                    if (lien.Stages__c == 'No_Lien' && lien.Date_No_Interest__c == null)                // #11
                        CreateLNA(lien,'PLRP_no_lien_no_date');
                }
                if (rtName == 'Non-PLRP' && SUBROSTATES.contains(lien.State__c) && string.isBlank(lien.Subrogation_Rights__c))  	// #9 - edited for PD-1235
                    CreateLNA(lien,'No_Subrogation');
// start PD-1171
				if (rtName == 'PLRP Claim Detail' && lrpMap.containsKey(lien.Action__c) && lrpMap.get(lien.Action__c).Providers__c.contains(lien.Account__r.Name) && 
					string.isBlank(lien.Health_Plan_Type__c) && (lien.Stages__c == 'Asserted' || lien.Stages__c == 'Audit' || lien.Stages__c == 'Final')) // Production feedback on stages
                    CreateLNA(lien,'No_Plan_Type');
// end PD-1171
            }
            if (awardMap.containsKey(cId)) {
	            for (Award__c a : awardMap.get(cId)) {
		            decimal liens = 0.00;
		            for (AwardLien__c al : a.Liens__r) {
		                if (al.Final_Lien_Amount__c > 0)
		                	liens += al.Final_Lien_Amount__c;
		            }
			        if (a.Amount_Disbursed__c > 0 && claimant.Status__c == 'Not_Ready_to_be_Cleared')
			            CreateCNA(claimant,'Prior_distribution_not_ready');
			        if (liens > a.Amount__c * 0.33)                         // #8
			            CreateCNA(claimant,'Exceeds_33_Percent');
		        }
            }
	        if (needPartC && !partc)
	            CreateCNA(claimant,'Part_C_No_Lien');
	        if (eligible && !medicareLien)
	            CreateCNA(claimant,'65_no_medicare');
	        if (needPLRP && !plrpFound)
	            CreateCNA(claimant,'Determination_created_no_lien');
        }
        if (!naMap.isEmpty()) {
            set<NeedsAttention__c> naSet = new set<NeedsAttention__c>(); 
            for (list<NeedsAttention__c> nas : naMap.values()) {
                for (NeedsAttention__c na : nas) {
                    if (na.Id == null || // created new NA OR
                    	((!isTrigger || !trigger.newMap.containsKey(na.Id)) && // not going to auto-save AND 
                    	  checkMap.get(na.Id) != na.Internal_Status__c)) // status has changed
                        naSet.add(na);
                }
            }
            try {
            	NAUtility.hasRun = true;
				if (!naSet.isEmpty()) {
system.debug(loggingLevel.INFO, 'RAP --->> naSet = ' + naSet);
					list<NeedsAttention__c> naList = new list<NeedsAttention__c>();
					naList.addAll(naSet);
                	upsert naList;
				}
                return true;
            }
            catch (exception e) {
                return false;
            }
        }
        return true;
    }
// end PD-1346    
    public static void CreateCNA(Claimant__c c, string type_x) {
        NeedsAttention__c na;
        boolean add = true;
        if (!naMap.get(c.Id).isEmpty()) {
            for (NeedsAttention__c nac : naMap.get(c.Id)) {
                if (nac.Internal_Type__c == type_x) {
                    if (nac.Internal_Status__c == 'Special Case - Ignore')
                        return;
                    add = false;
                    if (trigger.isExecuting && (!trigger.isDelete && trigger.newMap.containsKey(nac.Id))) {
                        nac = (NeedsAttention__c)trigger.newMap.get(nac.Id);
                        if (nac.Internal_Status__c == 'Resolved')
                            trigger.newMap.get(nac.Id).addError('Cannot change status to "Resolved" - conditions resulting in alert still exist.');
	                    else if (nac.Internal_Status__c == 'Special Case - Ignore')
	                        return;
                    }
                    nac.Internal_Status__c = 'Needs Attention';
                }
            }
        }
        if (add) {
            na = new NeedsAttention__c(Claimant__c = c.Id,
                                       Internal_Status__c = 'Needs Attention',
                                       RecordTypeId = rtId,
                                       Internal_Type__c = type_x);
            naMap.get(c.Id).add(na);
        }
    }

    public static void CreateLNA(Lien__c l, string type_x) {
        NeedsAttention__c na;
        boolean add = true;
        if (!naMap.get(l.Id).isEmpty()) {
            for (NeedsAttention__c nac : naMap.get(l.Id)) {
                if (nac.Internal_Type__c == type_x) {
                    if (nac.Internal_Status__c == 'Special Case - Ignore')
                        return;
                    add = false;
                    if (trigger.isExecuting && (!trigger.isDelete && trigger.newMap.containsKey(nac.Id))) {
                        nac = (NeedsAttention__c)trigger.newMap.get(nac.Id);
                        if (nac.Internal_Status__c == 'Resolved')
                            trigger.newMap.get(nac.Id).addError('Cannot change status to "Resolved" - conditions resulting in alert still exist.');
	                    else if (nac.Internal_Status__c == 'Special Case - Ignore')
	                        return;
                    }
                    nac.Internal_Status__c = 'Needs Attention';
                }
            }
        }
        if (add) {
            na = new NeedsAttention__c(Lien__c = l.Id,
                                       Internal_Status__c = 'Needs Attention',
                                       RecordTypeId = rtId,
                                       Internal_Type__c = type_x);
            naMap.get(l.Id).add(na);
        }
    }
// start PD-872
    public static boolean AssessNAs(list<Claimant__c> claimants) {
        list<string> naList;
        portalMap = new map<Id,list<string>>();
        for (Claimant__c c : claimants) {
            naList = new list<string>();
            if (string.isBlank(c.First_Name__c))
                naList.add('Claimant_First_Name');
            if (string.isBlank(c.Last_Name__c))
                naList.add('Claimant_Last_Name');
            if (string.isBlank(c.SSN__c))
                naList.add('SSN');
            if (c.DOB__c == null)
                naList.add('DOB');
            if (string.isBlank(c.Gender__c))
                naList.add('Gender');
            if (string.isBlank(c.Address_1__c))
                naList.add('Address_1');
            if (string.isBlank(c.City__c))
                naList.add('City');
            if (string.isBlank(c.State__c))
                naList.add('State');
            if (string.isBlank(c.Zip__c))
                naList.add('Zip_Code');
            if (!naList.isEmpty())
                portalMap.put(c.Id,naList);
        }
        if (!portalMap.isEmpty())
            return CreateNAs();
        return true;
    }

    private static boolean CreateNAs() {
        Id rtId = Schema.SObjectType.NeedsAttention__c.getRecordTypeInfosByName().get('Portal').getRecordTypeId();
        list<NeedsAttention__c> insertList = new list<NeedsAttention__c>();
        for (Id cId : portalMap.keySet()) {
            for (string s : portalMap.get(cId)) {
                insertList.add(new NeedsAttention__c(Claimant__c = cId,
                                                     Alert_Type__c = 'Missing Claimant Info',
                                                     Alert_Status__c = 'Law_Firm_Notified',
                                                     RecordTypeId = rtId,
                                                     Alert_Detail__c = s)); 
            }
        }
        if (!insertList.isEmpty()) {
            try {
                insert insertList;
                return true;
            }
            catch (exception e) {
                return false;
            }
        }
        return true;
    }
// end PD-872
}