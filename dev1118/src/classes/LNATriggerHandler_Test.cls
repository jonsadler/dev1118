/*
    Created: lmsw - September 2017 for September Release
    Purpose: Test functions of LNA Trigger Handler Class - PD-657
    		 Coverage as of 9/28 - LNATriggerHandler: 100%

History:

	Updated: lmsw - October 2017
	Purpose: Pd-1102 Refactor using test methods within TestUtility
			 Coverage as of 10/5 - LNATriggerHandler: 100%
*/
@isTest
private class LNATriggerHandler_Test {
	static testMethod void TestUpdateLNA() {
		// Instanciate Test Utility
    	TestUtility testUtl = new TestUtility();
        
        // Create test data
        // Updated amount
		Decimal NEWAMOUNT = 2500.00;
        // User
		testUtl.CreateUser();
    	// Actions
		testUtl.CreateActions();
		testUtl.CreateActionAssignments();
		// Lawfirm
		testUtl.CreateAccounts();
		testUtl.CreateActionAccounts();
		// Model Details
		testUtl.CreateModelDetails();
		// Claimant
		testUtl.CreateClaimants();
		// Lien action= action1ID, claimant=claimant1ID
		testUtl.CreateLiens();
		//Lien Negotiated Amount
		testUtl.CreateLNAs();
		Lien_Negotiated_Amounts__c lna = testUtl.lienNegAmt1;
		
		// Start test - Update Lien Negotiated Amount
		test.startTest();
			lna.Lien_Amount__c = NEWAMOUNT;
			update lna;

			// Assert the updated Amount of the Lien Negotiated Amount is the same as the number of expected Lien Negotiated Amount
			system.assertEquals(NEWAMOUNT, lna.Lien_Amount__c);
		test.stopTest();
	}

	static testMethod void TestCheckforFinal() {
		// Instanciate Test Utility
    	TestUtility testUtl = new TestUtility();
        
        // Create test data
        // Updated amount
		Decimal NEWAMOUNT = 2500.00;
		Date today = system.today();
        // User
		testUtl.CreateUser();
    	// Actions
		testUtl.CreateActions();
		testUtl.CreateActionAssignments();
		// Lawfirm
		testUtl.CreateAccounts();
		testUtl.CreateActionAccounts();
		// Model Details
		testUtl.CreateModelDetails();
		// Claimant
		testUtl.CreateClaimants();
		// Lien action= action1ID, claimant=claimant1ID
		testUtl.CreateLiens();
		//Lien Negotiated Amount
		Lien__c lien = testUtl.lien2;
		testUtl.CreateLNAs();
		Lien_Negotiated_Amounts__c lna = testUtl.lienNegAmtF1;
		
		// Try to create new Lien Negotiated Amount for this Lien
		test.startTest();
			Lien_Negotiated_Amounts__c newLNA = new Lien_Negotiated_Amounts__c(
															Lien__c = lien.Id,
															Phase__c = 'Asserted',
															Lien_Amount__c = NEWAMOUNT,
															Lien_Amount_Date__c = today);
			try{
				insert newLNA;	
			}catch(Exception e) {
				Boolean expectedExceptionThrown = e.getMessage().contains(
					'A Final Lien Negotiated Amount already exists on this lien.') ? true : false;
				
				// Assert the expected Exception message is the same as the expected Exception message throwns.
				system.assertEquals(expectedExceptionThrown,true);
			}
		test.stopTest();
	}
}