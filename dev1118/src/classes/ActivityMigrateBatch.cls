/*
    Created - RAP July 2017 for July Release
    Purpose - PD-844 - Migrate Event data to new Activity__c object

History: 
    
    Updated - lmsw October 2017 for October Release
    Purpose - PD-981 - Modify batch to include new Portal Activity Subject/Description.
              Assumption: All events are lien updates, activity__c.Type__c = 'Lien update'
    
    Updated - RAP October 2017 for October Release
    Purpose - PD-981 - Modify batch to use method in EventUtility

*/
global without sharing class ActivityMigrateBatch implements Database.Batchable<SObject> {

//	public string QUERY = 'SELECT Subject, Description, ActivityDateTime, WhatId, Activity_Type__c, Lienholder__c, Lienholder__r.Name FROM Event WHERE WhatId IN (SELECT Id FROM Lien__c)';
//	public boolean tester;
//	public map<datetime,string> lienLinkMap;
  
    global ActivityMigrateBatch() {}
//    	tester = false;
//    }

// Call batch with a parameter = true to limit entry.
//    global ActivityMigrateBatch(boolean test) {
//		tester = test;
//    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
//		if (tester)
//			QUERY += ' limit 10';
    	return null;
  	}

	public void execute(Database.BatchableContext BC, list<Event> events) {}
//		EventUtility.CreateActivities(events);
//	}
 
	public void finish(Database.BatchableContext BC) {} 
}