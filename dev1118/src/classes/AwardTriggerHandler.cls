/*
	Created: MEF - January 2017 for first release
	Purpose: Handler for Award__c trigger events
	ToDo: (proposed) create utility methods possibly to separate classes as appropriate (getActionNames, getClaimantNames, etc...)

    Updated: RAP - March 2017 for May Release
    Purpose: PD-520 - Extraneous Field Clean-up (change Description__c to Description2__c)

    Updated: RAP - May 2017 for May29 Patch
    Purpose: PD-604 - Change Award & Claimants Status to 'Released to a Holdback' Automatically (Phase One)

    Updated: RAP - August 2017 for August Release
    Purpose: PD-872 - Creation of the Alert System

    Updated: RAP - October 2017 for October Release
    Purpose: PD-1114 - Run a Calculation that will Compare Total Final Payable on Liens vs Award Liens

    Updated: RAP - December 2017 for December Release
    Purpose: PD-1308 - Add Ability to Drop Claimant from Action
*/
public with sharing class AwardTriggerHandler {

	public map<string,string> categories {
		get {
			if (categories == null) {
	            categories = new map<string,string>();
	            list<Schema.PicklistEntry> pickList = Award__c.Settlement_Subcategory__c.getDescribe().getPicklistValues();
	            for (Schema.PicklistEntry spe : pickList)
	                categories.put(spe.getValue(),spe.getLabel());
			}
			return categories;
		}
		set;
	}
	
	public void onBeforeInsert() {
		BuildAwardName();
	}
//	public void onAfterInsert(list<Award__c> newAwardRecs, map<Id, Award__c> newAwardsMap) {
//	}
	     
	public void onBeforeUpdate() {
		BuildAwardName();
	}
	public void onAfterUpdate() {
// start PD-604
		UpdateClaimant();
	}

	private void UpdateClaimant() {
		list<Id> idList = new list<Id>();
		list<Claimant__c> updateList = new list<Claimant__c>();
		for (SObject s : trigger.new)
			idList.add((Id)s.get('Claimant__c'));
		map<Id,Claimant__c> claimantMap = new map<Id,Claimant__c>([SELECT Id, Status__c, Needs_Attention_Total__c, // PD-872
																		  (SELECT Internal_Status__c, Internal_Type__c FROM Needs_Attentions__r WHERE RecordType.Name = 'Internal') // PD-1114
																   FROM Claimant__c 
																   WHERE Id in :idList]);
		list<Award__c> aList = [SELECT Id, Claimant__c, Claimant__r.Status__c, Claimant__r.Name, Distribution_Status__c
								FROM Award__c 
								WHERE Claimant__c in :idList];
		map<Id,list<Award__c>> awardMap = new map<Id,list<Award__c>>();
		for (Award__c aw : aList) {
			if (awardMap.containsKey(aw.Claimant__c))
				awardMap.get(aw.Claimant__c).add(aw);
			else
				awardMap.put(aw.Claimant__c, new list<Award__c>{aw});
		}
		map<string,NeedsAttentionMap__c> holdMap = NeedsAttentionMap__c.getAll(); // PD-1114
system.debug(loggingLevel.INFO, 'RAP --->> holdMap = ' + holdMap);
		for (Id cId: awardMap.keySet()) {
			boolean clear = true;
			boolean rtah = false;
			Claimant__c c = claimantMap.get(cId);
// start PD-1114
			boolean skip = false;
system.debug(loggingLevel.INFO, 'RAP --->> Needs Attentions = ' + c.Needs_Attentions__r);
			if (c.Needs_Attentions__r != null && !c.Needs_Attentions__r.isEmpty()) {
				for (NeedsAttention__c na : c.Needs_Attentions__r) {
					if (na.Internal_Status__c == 'Needs Attention' && holdMap.get(na.Internal_Type__c).Priority__c == '1') {
						skip = true;
						break;
					}
				}
			}
			if (skip) {
				c.Status__c = 'Not_Ready_to_be_Cleared';
				updateList.add(c);
				continue;
			}
// end PD-1114
			if (c.Needs_Attention_Total__c > 0) // PD-872
				continue;
			for (Award__c aw : awardMap.get(cId)) {
				if (aw.Distribution_Status__c != 'Cleared')
					clear = false;
				if (aw.Distribution_Status__c == 'Released_to_a_Holdback' || aw.Distribution_Status__c == 'Cleared')
					rtah = true;
			}
			if (clear)
				c.Status__c = 'Cleared';
			else if (rtah)
				c.Status__c = 'Released_to_a_Holdback';
			else
				c.Status__c = 'Not_Ready_to_be_Cleared';
			updateList.add(c);
		}
		if (!updateList.isEmpty())
			update updateList;
	}
// end PD-604
	private void BuildAwardName() {
		map<Id,Action__c> actionMap = new map<Id,Action__c>([SELECT Id, Name FROM Action__c]);
		list<Id> idList = new list<Id>();
		for (SObject s :trigger.new)
			idList.add((Id)s.get('Claimant__c'));
		map<Id,Claimant__c> cMap = new map<Id,Claimant__c>([SELECT Id, First_Name__c, Last_Name__c FROM Claimant__c WHERE Id IN :idList]);
		Date today = system.today();
		for (SObject s : trigger.new) {
			Award__c aw = (Award__c)s;
			if (aw.Date_of_Award__c == null)
				aw.Date_of_Award__c = today;
			string description = aw.Settlement_Category__c == 'Base' ? categories.get(aw.Settlement_Subcategory__c.substringBefore(';')) : 'ENH- ' + categories.get(aw.Settlement_Subcategory__c.substringBefore(';'));
system.debug(loggingLevel.INFO, 'RAP --->> aw.Action__c = ' + aw.Action__c);
			string name = cMap.get(aw.Claimant__c).First_Name__c + ' ' + cMap.get(aw.Claimant__c).Last_Name__c + '- ' + description + '- ' + (aw.Action__c != null && actionMap.containsKey(aw.Action__c) ? actionMap.get(aw.Action__c).Name : 'Dropped'); // PD-1308
			if (name.length() > 80 && name.contains('ENH- '))
				name = name.replace('ENH- ','');
            if(name.length() > 80)
                name = name.substring(0,79);
			s.put('Name', name);
			if (aw.Settlement_Subcategory__c == 'Spousal_Award')
				s.put('Distribution_Status__c', 'Cleared');
		}
	}
}