/*
    Created: lmsw - March for March Release
    Purpose: PD-466 - Lien Detail Page - Documents Tab - Claimant Box Folders

History: 

    Updated: lmsw - May 2017
    Purpose: PD-596 - Show box if Claimant folder exists else show instructions.
*/

public with sharing class ClaimantBoxControllerExtension {

    private final Lien__c lien;
    public System.PageReference claimantFolderUrl {get; set;}
    public Boolean hasClaimant {get; set;}
    public String claimantName {get; set;}      //PD-596
    public Id claimantId {get; set;}        //PD-596

    public ClaimantBoxControllerExtension(ApexPages.StandardController stdController) {
        this.lien = (Lien__c)stdController.getRecord();
        Boolean isMobileContext = true;
        getClaimantFolderUrl(lien,isMobileContext);
    }

    public System.PageReference getClaimantFolderUrl(Lien__c l, Boolean isMobileContext) {
       // Id claimantId;
        hasClaimant = false;
        Lien__c lien = [SELECT Id, Claimant__c, Claimant__r.Name
                        FROM Lien__c
                        WHERE Id =: l.Id];
        claimantId = lien.claimant__c;
        claimantName = lien.Claimant__r.Name;

        // Instantiate the Toolkit object
        box.Toolkit boxToolkit = new box.Toolkit();
        if(claimantId != null){
            claimantFolderUrl = boxToolkit.getFolderUrl(claimantId, isMobileContext);
            if(claimantFolderUrl != null) hasClaimant = true;
        }
        return claimantFolderUrl;
    }

}