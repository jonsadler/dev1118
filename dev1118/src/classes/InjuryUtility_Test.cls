/*
	Created: RAP - March 2017 for May Release
	Purpose: PRODSUPT-53 - Add automatic naming for Injuries
			 Test Coverage as of 3/29: Injury trigger: 100%
			 						   InjuryUtility: 100%
History:

    Updated: lmsw - July 2017 
    Purpose: Repair testMethod and modify to use TestUtility
         Coverage as of 7/12 - Injury trigger: 100%
                               InjuryUtility: 100%
*/
@isTest
private class InjuryUtility_Test {

@testSetup
	static void CreateData() {
        TestUtility.createTestData();
	}
	
    static testMethod void TestNaming() {
		Claimant__c c = [SELECT Id, Name FROM Claimant__c WHERE First_Name__c = 'RAP' limit 1];
		Id aId = [SELECT Id FROM Action__c WHERE Name = 'TestAction1'].Id;
		Injury__c i1 = new Injury__c(Name = 'Injury1', 
									 Action__c = aId,
									 Claimant__c = c.Id,
									 Injury_Description__c = 'Broken Mandible',
                                     Compensable__c = 'True', 
									 DOL__c = system.today().addDays(-45));
    	test.startTest();
    	insert i1;
    	test.stopTest();
    	i1 = [SELECT Name FROM Injury__c WHERE Id = :i1.Id];
    	system.assertEquals('RAP RAPPER - Broken Mandible', i1.Name);
    }
}