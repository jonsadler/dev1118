/*
    Created: RAP - March 2017 for March Release
    Purpose: Test functions of ActionBoxControllerExtension

History
	Updated: lmsw - March 2017 for March Release
	Purpose: Add test functions for ClaimantBoxControllerExtension

	Updated: lmsw - March 2017
    Purpose: Change Claimant__c.Address__c to Claimant__c.Address_1__c

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c
    		 					  
    Created: RAP - April 2017 for May Release
    Purpose: Repair testMethod
    		 Coverage as of 4/25 - ActionBoxControllerExtension: 100%
    		 					   ClaimantBoxControllerExtension: 100%

    Updated: lmsw - July 2017 
    Purpose: Repair testMethod to use TestUtility
    		 Coverage as of 7/11 - ActionBoxControllerExtension: 93%
    		 					   ClaimantBoxControllerExtension: 100%
    Updated: lmsw - October 2017
	Purpose: Pd-1102 Refactor using test methods within TestUtility
			 Coverage as of 10/5 - ActionBoxControllerExtension: 93%
			 					   ClaimantBoxControllerExtension: 100% 

*/
@isTest
private class ActionBoxController_Test {
	static testMethod void TestExtension() {
		// Instanciate Test Utility
    	TestUtility testUtl = new TestUtility();
    	
    	// Create Test data
    	// User
		testUtl.CreateUser();
    	// Actions
		testUtl.CreateActions();
		testUtl.CreateActionAssignments();
		// Lawfirm
		testUtl.CreateAccounts();
		testUtl.CreateActionAccounts();
		// Model Details
		testUtl.CreateModelDetails();
		// Claimant
		testUtl.CreateClaimants();
		Claimant__c c = testUtl.claimant1;
		// Lien
		testUtl.CreateLiens();
		Lien__c l = testUtl.lien1;
		
		// Start test
		test.startTest();
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(c);
			ActionBoxControllerExtension ctrl = new ActionBoxControllerExtension(stdCtrl);
			ApexPages.currentPage().getParameters().put('sidtp', 'p1');
			ctrl = new ActionBoxControllerExtension(stdCtrl);
			
			ApexPages.StandardController stdCtrl_lien = new ApexPages.StandardController(l);
			ClaimantBoxControllerExtension cctrl = new ClaimantBoxControllerExtension(stdCtrl_lien);
			ApexPages.currentPage().getParameters().put('isdtp','p1');
			cctrl = new ClaimantBoxControllerExtension(stdCtrl_lien); 
		test.stopTest();
	}
}