/*
    Created - lmsw October 2017
    Purpose - PD-981 Batch to delete Activites
    		  database.executeBatch(new ActivityDeleteBatch(true));
    
*/
global without sharing class ActivityDeleteBatch implements Database.Batchable<SObject> {

  public string QUERY = 'SELECT Id FROM Activity__c';
  public boolean tester = false;
  
    global ActivityDeleteBatch() {}

    // Call batch with a parameter = true to limit entry.
//    global ActivityDeleteBatch(boolean test) {
//      tester = test;
//    }

  global Database.QueryLocator start(Database.BatchableContext BC) {
//    if (tester)
//      QUERY += ' limit 100';
    return null;
  }

  public void execute(Database.BatchableContext BC, list<Activity__c> activities) {}
//   delete(activities);
//  }

  public void finish(Database.BatchableContext BC) {} 
}