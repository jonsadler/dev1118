/*
	Created: RAP - March 2017 for May Release
	Purpose: PRODSUPT-53 - Add automatic naming for Injuries

History:

	Updated: RAP - July 2017 for July Release
	Purpose: PD-891 - Creation of an Injury Lien Junction
			 Also refactored trigger and utility
*/
public without sharing class InjuryUtility { 

	public static void OnBeforeInsert() {
		NameInjuries();
	}

	public static void OnAfterInsert() {
		AssociateLiens();
	}

	public static void OnAfterUpdate() {
		CheckAssociations();
	}

	private static void NameInjuries() { 	   
    	list<Id> cList = new list<Id>();
    	for (SObject s : trigger.new)
    		cList.add((Id)s.get('Claimant__c'));
	    map<Id,Claimant__c> cMap = new map<Id,Claimant__c>([SELECT Id, Name FROM Claimant__c WHERE Id IN :cList]);
    	for (SObject s : trigger.new)
			s.put('Name', cMap.get((Id)s.get('Claimant__c')).Name + ' - ' + s.get('Injury_Description__c'));
	}
	
	private static void AssociateLiens() {
    	list<Id> cList = new list<Id>();
    	for (SObject s : trigger.new)
    		cList.add((Id)s.get('Claimant__c'));
    	map<Id,list<Lien__c>> lienMap = new map<Id,list<Lien__c>>();
    	list<Lien__c> lienList = [SELECT Id, Claimant__c FROM Lien__c WHERE Claimant__c IN :cList];
    	for (Lien__c l : lienList) {
    		if (lienMap.containsKey(l.Claimant__c))
    			lienMap.get(l.Claimant__c).add(l);
    		else
    			lienMap.put(l.Claimant__c, new list<Lien__c>{l});
    	}
    	list<InjuryLien__c> insertList = new list<InjuryLien__c>();
    	for (SObject s : trigger.new) {
    		Id checkId = (Id)s.get('Claimant__c');
    		if (lienMap.containsKey(checkId)) {
	    		for (Lien__c l : lienMap.get(checkId)) {
	    			InjuryLien__c il = new InjuryLien__c(Injury__c = (Id)s.get('Id'),
	    												 Lien__c = l.Id);
	    			insertList.add(il);
	    		}
    		}
    	}
    	if (!insertList.isEmpty())
    		insert insertList;
	}   
	private static void CheckAssociations() { 	   
    	list<Id> tempList = new list<Id>();
    	for (SObject s : trigger.new) {
    		if ((string)s.get('Compensable__c') == 'False')
    			tempList.add((Id)s.get('Id'));
    	}
	    list<InjuryLien__c> deleteList = [SELECT Id FROM InjuryLien__c WHERE Injury__c IN :tempList];
    	if (!deleteList.isEmpty())
    		delete deleteList;
	}
}