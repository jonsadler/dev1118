/*
    Created: lmsw - July 2017
    Purpose: Test functions of Task Trigger Utility Class - PD-844
    		Coverage as of 7/19 - TaskUtility: 100%
*/
@isTest
private class TaskUtility_Test {
	
@testSetup
	static void CreateData() {
		TestUtility.createTestData();
	}
	
	static testMethod void TestTaskUtility() {
		Lien__c l = [SELECT Id, Claimant__c, CreatedById FROM Lien__c WHERE Submitted__c = false LIMIT 1];
		Id cId = l.Claimant__c;
		Id uId = l.CreatedById;
		test.startTest();
		Task t = new Task(OwnerId = uId,
						  Subject = 'Test Call on a Claimant',
						  Status = 'Not Started',
						  Priority = 'Normal',
						  TaskSubtype = 'Call',
						  WhatId = cId);
    	insert t;  
    	system.assertEquals('Test Call on a Claimant', [SELECT Id, Subject__c FROM Activity__c].Subject__c);
    	Task t2 = new Task(OwnerId = uId,
						   Subject = 'Test Call on a Lien',
						   Status = 'Not Started',
						   Priority = 'Normal',
						   TaskSubtype = 'Call',
						   WhatId = l.Id);
    	insert t2;  
    	system.assertEquals(3, [SELECT count() FROM Activity__c]); 
		test.stopTest();
	}
}