/*
    Created: MEF - January 2017 for first release
    Purpose: Handler for Action trigger events
    ToDo: move utility methods to separate classes as appropriate 

 History: 

 	Updated: lmsw - March 2017 for March Release
 	Purpose: Add Method to create Box Folder when an Action is created.

    Updated: lmsw - November 2017 for December Release
    Purpose: PD-1264 - Update Portal users to show Legal Terms before access 
             to portal when an action is updated or created to be avaiable
             in the portal.
*/

public with sharing class ActionTriggerHandler {

    public void onAfterInsert(list<Action__c> newActions, map<Id, Action__c> newMap) {
    	set<Id> actionIds = newMap.keySet();
    	CreateBoxFolder(actionIds);
	}
         
//	public void onBeforeUpdate(List<Action__c> newActionRecs, Map<Id, Action__c> newActionsMap, Map<Id, Action__c> oldActionsMap) {
//	}

//	public void onBeforeInsert(List<Action__c> newActionRecs, Map<Id, Action__c> newActionsMap) {
//	}

	public void onAfterUpdate(List<Action__c> newActionRecs, List<Action__c> oldActionRecs, Map<Id, Action__c> newActionsMap, Map<Id, Action__c> oldActionsMap) {
        set<Id> pactionIds = GetUpdatePortalActions(newActionsMap,oldActionsMap);
        if(pactionIds.size()>0)
            UpdatePortalUser(pactionIds);
    }
// start PD-1264    
    private static set<Id> GetUpdatePortalActions(Map<Id, Action__c> newActionsMap, Map<Id, Action__c> oldActionsMap){
        // Get Actions that updated action.available_in_portal__c = yes;
        set<Id> actionIds = new set<Id>();
        for(Id aId : newActionsMap.keySet()){
            if(newActionsMap.get(aId).Available_in_Portal__c == true &&
              (newActionsMap.get(aId).Available_in_Portal__c != oldActionsMap.get(aId).Available_in_Portal__c)){
                actionIds.add(aId);
            }
        }
        system.debug('Actions changed available in portal: '+ actionIds);
        return actionIds;
    }
    private static void UpdatePortalUser(set<Id> aIds){
        Id portalProfileId = [SELECT Id FROM Profile WHERE Name =: 'ProvidioCustomerCommunityPlus'].Id;
        // Get all Accounts in ActionAccount that have this Action. Create list of Portal User role names
        list<string> portalRoleNames = new list<string>();
        map<string,Id> accts = new map<string,Id>();
        for(Action_Account__c aa : [SELECT Id, Account__c, Account__r.Name, 
                                   Action__c
                                   FROM Action_Account__c
                                   WHERE Action__c IN: aIds])
        {
          accts.put(aa.Account__r.Name, aa.Account__r.Id);
            string portalRoleName = aa.Account__r.Name + ' Customer User';
            portalRoleNames.add(portalRoleName);
        }
        // Get list of portal users that have the Profile = ProvidioCustomerCommunityPlus
        // and have a Role = Lawfirm Name + Customer User
        list<User> updateUsers = new list<User>();
        for(user u : [SELECT Id, Name, UserRoleId, UserRole.Name,
                      ProfileId
                      FROM User
                      WHERE ProfileId =: portalProfileId 
                      AND UserRole.Name IN: portalRoleNames])
        {
            // Change accepted Terms status for users
            u.Has_Accepted_Legal_Terms__c = false;
            updateUsers.add(u);
        }
        // Update accepted terms for these portal users
        If(updateUsers.size()>0)
            update updateUsers;
    }
// end PD-1264

@future (callout=true)
    private static void CreateBoxFolder(set<Id> aIds) {
        box.Toolkit boxToolkit = new box.Toolkit(); // Instantiate the Toolkit object
        for (Id aId : aIds) // Create a folder and associate it with an Action
        	string actionFolderId = boxToolkit.createFolderForRecordId(aId,null,true);
        if (!test.isRunningTest()) boxToolkit.commitChanges();
    }
}