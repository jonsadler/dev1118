/*
    Created: lmsw - March 2017 for March release
    Purpose: Handler for Action trigger events
    		 Add method to create a box folder when a Claimant is created
History:

	Updated: RAP March 2017 for May Release
	Purpose: PRODSUPT-47 - Claimant Name field is inserted as record ID

	Updated: RAP April 2017 for May Release
	Purpose: techDebt - Operations Flow Control Prep

    Updated: lmsw - April 2017
    Purpose: PRODSUPT - 121 Modify to allow inserting a Claimant name.

	Updated: RAP May 2017 for May15 Release
	Purpose: PD-527 - Automation (rule #12)

    Updated: lmsw - May 2017
    Purpose: PD-656 Add new ActionClaimant when a new Claimant is added
	
	Updated: RAP May 2017 for May15 Release
	Purpose: PD-524/525/526/527 - automation - create appropriate liens on Claimant insert
								- automation - remove duplicate Claimants and create new ActionClaimant junction only
	
	Updated: RAP May 2017 for May15 Release
	Purpose: PD-524/525/526/527 - automation - fix failed lien creation
								- automation - remove creation of ActionClaimant junction

	Updated: RAP August 2017 for August Release
	Purpose: PD-872 - Creation of the Alert System
			 Also refactor - remove unnecessary lists and maps - Handler class runs in the trigger context

	Updated: RAP August 2017 for August Release
	Purpose: QA enhancement - run batch on claimant update - only for trigger contents

	Updated: RAP September 2017 for September Release
	Purpose: PD-998 - Create QC Component (update processor to create Needs Attention objects)

	Updated: RAP October 2017 for October Release
	Purpose: PD-1101 - Populate Claimant Needs_AttentionPLRP__c and Needs_Attention_Field__c fields

	Updated: RAP October 2017 for October Release
	Purpose: PD-1100 - Claimant Last Updated Field

	Updated: lmsw - October 2017
	Purpose: PD-940 - Auto create Claimant folder structure when a Claimant is created

	Updated: cps October 2017 for October Release
	Purpose: PD-1100 Claimant last updated field (Fixed soql)

    Updated: RAP - January 2017 for January Release
    Purpose: PD-1346 - Update QA Batch to Remove Dependency on Award Data
	
*/

public with sharing class ClaimantTriggerHandler {

	public static map<string, Claimant__c> ssnMap{get;set;}
	public static set<Id> actions{get;set;}
	
// start PRODSUPT-47
    public void onBeforeInsert() {
		NameClaimants();
    }
// end PRODSUPT-47
    public void onAfterInsert() {
    	CreateBoxFolder(trigger.newMap.keySet()); // Have to pass Ids in - future method
    	boolean result = QAEngine.AssessNAs(trigger.new); // PD-872
    	actions = new set<Id>();
    	for (SObject s : trigger.new)
    		actions.add((string)s.get('Action__c'));
    	CreateLiens();
    }
// start techDebt
    public void onBeforeUpdate() {
		for (SObject s : trigger.new) {
			if (s.get('PreviousAction__c') != null)
				s.addError('This claimant has been dropped from the action. To edit, first move back to the Action, then select \'Edit\'.');
		}
		CalculateFormulaFields();
		CalculateNAFields(); // PD-1100, PD-1101
		CheckForSSNChange(); // PD-527
		NameClaimants();
    } 
//end techDebt
    public void onAfterUpdate() {
		if (NAUtility.hasRun)
			return;
		QAEngine.ProcessClaimants(trigger.newMap.keySet()); // PD-1346
    }
// start PD-1100/1101
    private static void CalculateNAFields() {
        set<string> checkSet = new set<string>{'Claimant info needs to be updated', 'Missing Claimant Info'};
        list<Claimant__c> cList= [SELECT Id, Needs_Attention_Field__c, Needs_AttentionPLRP__c, LastModifiedDate, Date_Last_Updated__c, 
		                                 (SELECT Alert_Type__c, Alert_Status__c FROM Needs_Attentions__r WHERE Alert_Status__c = 'Law_Firm_Notified'), // PD-1101
		                                 (SELECT Id, Activity_Date__c FROM Activities__r order by Activity_Date__c desc limit 1) // PD-1100
		                          FROM Claimant__c
		                          WHERE Id IN :trigger.newMap.keySet()];
        for (Claimant__c c : cList) {
// start PD-1101
            integer field = 0;
            integer plrp = 0;
            datetime temp;
            for (NeedsAttention__c na : c.Needs_Attentions__r) {
                if (na.Alert_Type__c == 'Missing PLRP Elections') {
                    plrp++;
                }
                else if (checkSet.contains(na.Alert_Type__c)) {
                    field++;
                }
            }
            trigger.newMap.get(c.Id).put('Needs_AttentionPLRP__c',plrp);
            trigger.newMap.get(c.Id).put('Needs_Attention_Field__c',field);
// end PD-1101
// start PD-1100
			if (c.Activities__r.isEmpty())
				temp = system.now();
			else
				temp = c.LastModifiedDate > c.Activities__r[0].Activity_Date__c ? system.now() : c.Activities__r[0].Activity_Date__c;
            trigger.newMap.get(c.Id).put('Date_Last_Updated__c',temp);
// end PD-1100
        }
    }
// end PD-1100/1101
// start techDebt    
    private static void CalculateFormulaFields() {
    	list<Lien__c> lienList = [SELECT Claimant__c, Total_Fees_Applied__c, Outstanding_Amount__c, Stages__c, Substage__c 
    							  FROM Lien__c 
    							  WHERE Claimant__c IN :trigger.newMap.keySet()
    							  AND Stages__c != 'No_Lien'];
    	map<Id,integer> lienMap = new map<Id,integer>();
    	map<Id,decimal> feeMap = new map<Id,decimal>();
    	map<Id,decimal> outMap = new map<Id,decimal>();
    	map<Id,integer> pendingMap = new map<Id,integer>();
    	decimal fee = 0.00;
    	decimal out = 0.00;
    	integer pending = 0;
    	integer total = 0;
		for (Id key : trigger.newMap.keySet()) {
   			feeMap.put(key, fee);
   			outMap.put(key, out);
   			lienMap.put(key, total);
    		pendingMap.put(key, pending);
		}
		for (Lien__c l : lienList) {
			fee = l.Total_Fees_Applied__c;
			out = l.Outstanding_Amount__c;
			pending = 1;
			total = 1;
   			fee += feeMap.get(l.Claimant__c);
   			feeMap.put(l.Claimant__c, fee);
   			out += outMap.get(l.Claimant__c);
   			outMap.put(l.Claimant__c, out);
   			total += lienMap.get(l.Claimant__c);
   			lienMap.put(l.Claimant__c, total);
    		if (l.Stages__c != 'Final' || l.Substage__c == 'Awaiting_Model_Determination') {
    			pending += pendingMap.get(l.Claimant__c);
    			pendingMap.put(l.Claimant__c, pending);
    		}
    	}
    	for (Id c : trigger.newMap.keySet()) {
    		Claimant__c cl = (Claimant__c)trigger.newMap.get(c);
// start PD-998
    		Claimant__c oldC = (Claimant__c)trigger.oldMap.get(c);
    		if (cl.Status__c == 'Released_to_a_Holdback' && oldC.Status__c == 'Not_Ready_to_be_Cleared' && cl.Date_Released__c == null)
    			cl.Date_Released__c = system.today();
// end PD-998
    		cl.Lien_Amount_Left__c = outMap.get(c);
    		cl.Liens_Pending__c = pendingMap.get(c);
    		cl.Num_Liens__c = lienMap.get(c);
    		cl.Total_Applied__c = feeMap.get(c);
    	}
    }
// end techDebt
// start PD-527
	private static void CheckForSSNChange() {
		for (SObject s : trigger.new) {
			Claimant__c c = (Claimant__c)s;
			Claimant__c oldC = (Claimant__c)trigger.oldMap.get(c.Id);
			if (c.SSN__c != oldC.SSN__c)
				s.put('Date_SSN_Changed__c', system.today());
		}
	}
// end PD-527
// start PD-524/525/526/527 (automation - create appropriate liens on Claimant insert)
	private static void CreateLiens() {
    	map<Id,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Model_Detail__c.getRecordTypeInfosById();
    	map<string,Schema.RecordTypeInfo> trMap = Schema.SObjectType.Lien__c.getRecordTypeInfosByName();
		map<Id,Action__c> actionMap = new map<Id,Action__c>([SELECT Id, Medicaid_Eligibility_Verification__c, Medicare_Individual_Resolution__c,
																	Medicare_Eligibility_Verification__c, Medicare_Model__c, PLRP__c,
																   (SELECT Id, Account__c, PLRP_Type__c FROM Actions_Accounts__r WHERE LRP_Provider__c = true)
															 FROM Action__c
															 WHERE Id IN :actions]);
		list<Account> medAccts = [SELECT Id, Name, BillingState FROM Account WHERE (Name = 'First Recovery Group' 
																				OR Name = 'HMS'
																				OR Name = 'Centers for Medicare & Medicaid Services'
																				OR Name like '% - Medicaid')];
		map<string,Id> medicacctMap = new map<string,Id>();
		for (Account a : medAccts) {
			if (a.Name == 'First Recovery Group' || a.Name == 'HMS' || a.Name == 'Centers for Medicare & Medicaid Services')
				medicacctMap.put(a.Name, a.Id);
			else
				medicacctMap.put(a.BillingState, a.Id);
		}
		map<string,list<Model_Detail__c>> medicaidMap = new map<string,list<Model_Detail__c>>();
		map<string,list<Model_Detail__c>> plrpMap = new map<string,list<Model_Detail__c>>();
		map<Id,list<Model_Detail__c>> medicareMap = new map<Id,list<Model_Detail__c>>();
		list<Lien__c> insertList = new list<Lien__c>();
		list<Model_Detail__c> models = [SELECT Action__c, Medicaid_Aggregate__c, Medicare_Aggregate__c, Apply_Terms__c, Award_Type__c, 
											   Calculate_Final_Using__c, Cap_Holdback__c, Comments__c, Covered_Awards__c, Eligibility__c, 
											   FRG__c, HMS__c, Name, Payable_Amount__c, PayerType__c, Provider__c, Id, RecordTypeId, 
											   Reduction__c, State__c, State_Agency__c, RecordType.Name
										FROM Model_Detail__c
										WHERE Action__c IN :actionMap.keySet()
										AND Award_Type__c != 'Enhanced'];
		for (Model_Detail__c model : models) {
			string rtName = rtMap.get(model.RecordTypeId).getName();
			if (rtName == 'Medicaid') {
				if (medicaidMap.containsKey(model.State__c))
					medicaidMap.get(model.State__c).add(model);
				else
					medicaidMap.put(model.State__c, new list<Model_Detail__c>{model});
			}
			else if (rtName == 'PLRP Claim Detail' ||
					 rtName == 'PLRP Model') {
				if (plrpMap.containsKey(model.Provider__c))
					plrpMap.get(model.Provider__c).add(model);
				else
					plrpMap.put(model.Provider__c, new list<Model_Detail__c>{model});
			}
			else if (rtName == 'Medicare Global Model') {
				if (medicareMap.containsKey(model.Provider__c))
					medicareMap.get(model.Provider__c).add(model);
				else
					medicareMap.put(model.Provider__c, new list<Model_Detail__c>{model});
			}
		}
		for (SObject s : trigger.new) {
			Claimant__c c = (Claimant__c)s;
			Id rtId;
			Action__c action = actionMap.get(c.Action__c);
			boolean medicaid = action.Medicaid_Eligibility_Verification__c;
			boolean medmodel = action.Medicare_Model__c;
			boolean medtrad = action.Medicare_Individual_Resolution__c;
			boolean plrp = action.PLRP__c;
			if (medicaidMap.containsKey(c.State__c) && medicaid) {
				rtId = trMap.get('Medicaid').getRecordTypeId();
				for (Model_Detail__c model : medicaidMap.get(c.State__c)) {
					if (model.Action__c != c.Action__c)
						continue;
					if (model.FRG__c) {
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtId,
												Account__c = medicacctMap.get('First Recovery Group'),
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
					if (model.HMS__c) {
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtId,
												Account__c = medicacctMap.get('HMS'),
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
					if (model.State_Agency__c) {
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtId,
												Account__c = medicacctMap.get(c.State__c),
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
				}
			}
			if (medmodel) {
				rtId = trMap.get('Medicare Global Model').getRecordTypeId();
				Lien__c l = new Lien__c(Claimant__c = c.Id,
										Action__c = action.Id,
										RecordTypeId = rtId,
										Account__c = medicacctMap.get('Centers for Medicare & Medicaid Services'),
										Stages__c = 'To_Be_Submitted',
										State__c = c.State__c);
				insertList.add(l);
			}
			if (medtrad) {
				rtId = trMap.get('Medicare Individual Resolution').getRecordTypeId();
				Lien__c l = new Lien__c(Claimant__c = c.Id,
										Action__c = action.Id,
										RecordTypeId = rtId,
										Account__c = medicacctMap.get('Centers for Medicare & Medicaid Services'),
										Stages__c = 'To_Be_Submitted');
				insertList.add(l);
			}
			if (plrp && c.Opted_in_PLRP__c == 'Yes') {
				for (Action_Account__c aac : action.Actions_Accounts__r) {
					if (aac.PLRP_Type__c == 'Claim_Detail') {
						rtId = trMap.get('PLRP Claim Detail').getRecordTypeId();
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtId,
												Account__c = aac.Account__c,
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
					else {
						rtId = trMap.get('PLRP Model').getRecordTypeId();
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtId,
												Account__c = aac.Account__c,
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
				}
			}
		}
		if (!insertList.isEmpty())
			insert insertList;
	}
// end PD-524/525/526/527

@future (callout=true)
    private static void CreateBoxFolder(set<Id> cIds) {
    	system.debug('Create Claimant Box folder');
        // Instantiate the Toolkit object
        box.Toolkit boxToolkit = new box.Toolkit();
        // Create a folder and associate it with a Claimant
        for (Id cId : cIds) {
        	string claimantFolderId = boxToolkit.createFolderForRecordId(cId,null,true);
        	//system.debug('Claimant Folder ID: '+ claimantFolderId);

        	// Create two sub-folders in the newly created claimant folder
        	//string plcFolderId = boxToolkit.createFolder('PLC',claimantFolderId,null);
        	//string portalFolderId = boxToolkit.createFolder('Portal',claimantFolderId,null);
        }
        if (!test.isRunningTest()) 
        	boxToolkit.commitChanges();
    }
// start PRODSUPT-47
    private static void NameClaimants() {
		for (SObject s : trigger.new) {
            string name;
            if (s.get('First_Name__c') != null) {       // PRODSUPT-121
        		name = (string)s.get('First_Name__c');
        		if (s.get('Middle__c') != null)
    				name += ' ' +  (string)s.get('Middle__c');
    			name += ' ' + (string)s.get('Last_Name__c');
            }
            s.put('Name', name);
    	}
    }
// end PRODSUPT-47
}