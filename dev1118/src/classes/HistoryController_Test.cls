/*
    Created: RAP - March 2017 for March Release
    Purpose: test functions of History Controllers

History: 

    Updated: lmsw - March 2017
    Purpose: Change Claimant__c.Address__c to Claamant__c.Address_1__c

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c

    Updated: RAP - April 2017 for May Release
    Purpose: Fix failing testMethod
			 Coverage as of 4/25 - ActionHistoryController: 100%
	                               ClaimantHistoryController: 100%
	                               LienHistoryController: 100%
    Updated: lmsw - July 2017 
    Purpose: Repair testMethod and modify to use TestUtility
    		 Coverage as of 7/12 - ActionHistoryController: 100%
                                   ClaimantHistoryController: 100%
                                   LienHistoryController: 100%

    Updated: lmsw - October 2017
	Purpose: Pd-1102 Refactor using test methods within TestUtility
			 Coverage as of 10/5 - ActionHistoryController: 100%
                                   ClaimantHistoryController: 100%
                                   LienHistoryController: 100%
*/
@isTest
private class HistoryController_Test {
	static testMethod void TestHistories() {
		// Instanciate Test Utility
    	TestUtility testUtl = new TestUtility();
        
        // Create test data
        // User
		testUtl.CreateUser();
    	// Actions
		testUtl.CreateActions();
		testUtl.CreateActionAssignments();
		// Lawfirm
		testUtl.CreateAccounts();
		testUtl.CreateActionAccounts();
		// Model Details
		testUtl.CreateModelDetails();
		// Claimant
		testUtl.CreateClaimants();
		// Lien action= action1ID, claimant=claimant1ID
		testUtl.CreateLiens();
		Lien__c l = testUtl.lien1;
		Action__c lienAction = testUtl.action1;
		Claimant__c lienClaimant = testUtl.claimant1;

		// Start test
		test.startTest();
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(lienAction);
			ActionHistoryController ctrl = new ActionHistoryController(stdCtrl);
			list<Action__History> aH = ctrl.actionH;
			
			stdCtrl = new ApexPages.StandardController(lienClaimant);
			ClaimantHistoryController ctrl2 = new ClaimantHistoryController(stdCtrl);
			list<Claimant__History> cH = ctrl2.claimantH;

			stdCtrl = new ApexPages.StandardController(l);
			LienHistoryController ctrl3 = new LienHistoryController(stdCtrl);
			list<Lien__History> lH = ctrl3.lienH;
	    test.stopTest();
	}
}