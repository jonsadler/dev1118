/*
    Created: RAP - March 2017 for May Release
    Purpose: PD-465 - Lien Detail Page - Notes, Tasks, and Phone calls
    
*/

public with sharing class NoteUtility {
    
    public static void onAfterInsert(set<Id> noteIds) {
    	LinkToClaimant(noteIds);
    }
    public static void onAfterUpdate(set<Id> noteIds, map<Id,ContentDocument> oldMap) {
    	LinkToClaimant(noteIds);
    }
//    public static void onBeforeDelete(set<Id> noteIds) {
    	// not yet needed
//    }

    private static void LinkToClaimant (set<Id> noteIds) {
		list<Id> lienIds = new list<Id>();
		set<Id> existing = new set<Id>();
		list<ContentDocumentLink> insertList = new list<ContentDocumentLink>();
		list<ContentDocumentLink> links = [SELECT Visibility, ShareType, LinkedEntityId, Id, ContentDocumentId 
										   FROM ContentDocumentLink
										   WHERE ContentDocumentId IN :noteIds];
    	for (ContentDocumentLink cdl : links) {
system.debug(loggingLevel.INFO, 'RAP --->> cdl.LinkedEntityId = ' + cdl.LinkedEntityId);
			if (string.valueOf(cdl.LinkedEntityId).startsWith('a08'))
    			lienIds.add(cdl.LinkedEntityId);
    		else
    			existing.add(cdl.LinkedEntityId);
    	}
    	map<Id,Lien__c> lienMap = new map<Id,Lien__c>([SELECT Id, Claimant__c FROM Lien__c WHERE Id IN :lienIds]);	
    	for (ContentDocumentLink cdl : links) {
	   		if (lienMap.containsKey(cdl.LinkedEntityId)) {
		   		ContentDocumentLink cdl1 = new ContentDocumentLink(Visibility = cdl.Visibility,
																   ShareType = cdl.ShareType,
																   LinkedEntityId = lienMap.get(cdl.LinkedEntityId).Claimant__c,
																   ContentDocumentId = cdl.ContentDocumentId);
    			if (!existing.contains(cdl1.LinkedEntityId))
    				insertList.add(cdl1);
	   		}
    	}
    	if (!insertList.isEmpty())
    		insert insertList;
    }
}