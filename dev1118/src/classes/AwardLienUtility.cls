/*
    Created - RAP January 2018 for January Release
    Purpose - techDebt - get processing out of batch processor body for multiple access points

*/

public class AwardLienUtility {

	public static boolean hasRun = false;

	public static final string QUERY = 'SELECT Award__c, Id, Injury__c FROM InjuryAward__c WHERE Award__c IN :awards';
	public map<Id,set<Id>> iaMap{get;set;}
	public map<Id,set<Id>> ilMap{get;set;}
	public map<Id,map<Id, AwardLien__c>> alMap2{get;set;}
	public map<Id,set<Id>> alMap{get;set;} // multiple liens per award
	public map<Id,set<Id>> laMap{get;set;} // multiple awards per lien
	public list<AwardLien__c> upsertList = new list<AwardLien__c>();
	public set<AwardLien__c> upsertSet = new set<AwardLien__c>();
	public string action{get;set;}
	public set<Id> awards{get;set;}
	public map<Id,Award__c> awardMap{get;set;}
	public string queryStr;
	
    public void ProcessAwards(set<Id> awds) { // PD-1103
    	awards = awds;
		list<InjuryAward__c> injuryAwards = database.query(QUERY);
		iaMap = new map<Id,set<Id>>();
		ilMap = new map<Id,set<Id>>();
		alMap = new map<Id,set<Id>>();
		laMap = new map<Id,set<Id>>();
		alMap2 = new map<Id,map<Id,AwardLien__c>>();

		set<Id> claimants = new set<Id>();
		awardMap = new map<Id,Award__c>([SELECT Id, Name, Claimant__c, Settlement_Category__c FROM Award__c WHERE Id IN :awards]);
		for (Award__c a : awardMap.values())
			claimants.add(a.Claimant__c);

		list<InjuryLien__c> ilList;
		ilList = [SELECT Injury__c, Lien__c, Id, Lien__r.Name__c, Lien__r.Claimant__c, Lien__r.RecordTypeId 
				  FROM InjuryLien__c 
				  WHERE Lien__r.Claimant__c IN :claimants
				  AND Lien__r.Stages__c != 'No_Lien'
				  AND Injury__r.Compensable__c = 'true'];
		for (InjuryLien__c il : ilList) {
			if (ilMap.containsKey(il.Injury__c))
				ilMap.get(il.Injury__c).add(il.Lien__c);
			else
				ilMap.put(il.Injury__c, new set<Id>{il.Lien__c});
		}

		list<AwardLien__c> alList;
		alList = [SELECT PercentToLien__c, PercentToAward__c, Award__c, Id, Lien__c, Lien_Record_Type__c, Lien__r.RecordTypeId, Lien__r.Account__c, Lien__r.State__c
				  FROM AwardLien__c
				  WHERE Lien__r.Claimant__c IN :claimants
				  AND Lien__r.Stages__c != 'No_Lien'];
		for (AwardLien__c al : alList) {
			if (alMap.containsKey(al.Award__c)) {
				alMap.get(al.Award__c).add(al.Lien__c);
				alMap2.get(al.Award__c).put(al.Lien__c, al);
			}
			else {
				alMap.put(al.Award__c, new set<Id>{al.Lien__c});
				alMap2.put(al.Award__c, new map<Id,AwardLien__c>{al.Lien__c => al});
			}
		}

		list<InjuryAward__c> iaList;
		iaList = [SELECT Award__c, Id, Injury__c, Award__r.Name, Award__r.Settlement_Category__c
				  FROM InjuryAward__c 
				  WHERE Award__r.Claimant__c IN :claimants
				  AND Injury__r.Compensable__c = 'true']; // techDebt
		for (InjuryAward__c ia : iaList) {
			if (iaMap.containsKey(ia.Award__c))
				iaMap.get(ia.Award__c).add(ia.Injury__c);
			else
				iaMap.put(ia.Award__c, new set<Id>{ia.Injury__c});
		}

		map<Id,set<AwardLien__c>> upsertMap = new map<Id,set<AwardLien__c>>();
		for (InjuryAward__c iawd : injuryAwards) {
			Id aId = iawd.Award__c;	
			for (Id iId : iaMap.get(aId)) { // loop through InjuryAwards by award
				if (ilMap.containsKey(iId)) { // if we have a corresponding InjuryLien
					for (Id lId : ilMap.get(iId)) { // loop through InjuryLiens by injury
						AwardLien__c al;
						if (!alMap.containsKey(aId) || !alMap.get(aId).contains(lId)) {
							al = new AwardLien__c(Award__c = aId,
												  Lien__c = lId);
						}
						else
							al = alMap2.get(aId).get(lId);
system.debug(loggingLevel.INFO, 'RAP --->> al = ' + al);
						if (upsertMap.containsKey(aId))
							upsertMap.get(aId).add(al);
						else 
							upsertMap.put(aId, new set<AwardLien__c>{al});
						if (alMap.containsKey(al.Award__c)) {
							alMap.get(al.Award__c).add(al.Lien__c);
							alMap2.get(al.Award__c).put(al.Lien__c, al);
						}
						else {
							alMap.put(al.Award__c, new set<Id>{al.Lien__c});
							alMap2.put(al.Award__c, new map<Id,AwardLien__c>{al.Lien__c => al});
						}
						if (laMap.containsKey(al.Lien__c))
							laMap.get(al.Lien__c).add(al.Award__c);
						else
							laMap.put(al.Lien__c, new set<Id>{al.Award__c});
					}
				}
			}
		}
		map<Id,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Lien__c.getRecordTypeInfosById();
		map<Id,Lien__c> lienMap = new map<Id,Lien__c>([SELECT Id, RecordTypeId FROM Lien__c WHERE Id in :laMap.keySet()]);
		for (set<AwardLien__c> alSet : upsertMap.values()) {
			for (AwardLien__c al : alSet) {
				al.Lien_Record_Type__c = al.Lien__r.RecordTypeId;
				set<Id> lls = laMap.get(al.Lien__c);
				set<string> typeSet = new set<string>();
				for (Id i : lls)
					typeSet.add(awardMap.get(i).Settlement_Category__c);
				if (typeSet.size() == 1) {
					decimal percent2 = 1/decimal.valueOf(lls.size()) * 100;
					al.PercentToAward__c = percent2.setScale(2, system.roundingMode.HALF_UP);
				}
				else 
					al.PercentToAward__c = 100;
				if (alMap.containsKey(al.Award__c) && alMap.get(al.Award__c).size() > 1) {
					map<Id,AwardLien__c> awsMap = alMap2.get(al.Award__c);
					integer i = 0;
					for (AwardLien__c aw : awsMap.values()) {
						string key1 = rtMap.get(lienMap.get(aw.Lien__c).RecordTypeId).getName() == 'Medicaid' ? aw.Lien__r.State__c : aw.Lien__r.Account__c;
						string key2 = rtMap.get(lienMap.get(al.Lien__c).RecordTypeId).getName() == 'Medicaid' ? al.Lien__r.State__c : al.Lien__r.Account__c;
						if (key1 == key2)
							i++;
					}
					decimal percent3 = 1/decimal.valueOf(i) * 100;
					al.PercentToLien__c = percent3.setScale(2, system.roundingMode.HALF_UP);
				}
				else
					al.PercentToLien__c = 100;
				upsertSet.add(al);
				if (upsertSet.size() > 9900) {
					upsertList.addAll(upsertSet);
					upsert upsertList;
					upsertSet.clear();
					upsertList.clear();
				}
			}
		}
		hasRun = true;
	}
}