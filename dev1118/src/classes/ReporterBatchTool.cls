/*
    Created - RAP January 2018 for January Release
    Purpose - PD-1311 - Modified Report - with specified objects
    
*/

public class ReporterBatchTool {
    
    public void CreateReporters(set<Id> awards, set<Id> liens) {
    	
		map<Id,InjuryAward__c> iaMap = new map<Id,InjuryAward__c>([SELECT Id, Injury__c FROM InjuryAward__c WHERE Id IN :awards]);
		map<Id,InjuryLien__c> ilMap = new map<Id,InjuryLien__c>([SELECT Id, Injury__c FROM InjuryLien__c WHERE Id IN :liens]);
		list<Reporter__c> checkList = [SELECT InjuryLien__c, InjuryAward__c 
									   FROM Reporter__c 
									   WHERE InjuryLien__c IN :ilMap.keySet() 
									   AND InjuryAward__c IN :iaMap.keySet()];
		map<Id,Id> checkMap = new map<Id,Id>();
		for (Reporter__c r : checkList)
			checkMap.put(r.InjuryLien__c, r.InjuryAward__c);
		list<Reporter__c> rList = new list<Reporter__c>();
		for (InjuryAward__c ia : iaMap.values()) {
		    for (InjuryLien__c il : ilMap.values()) {
		        if (il.Injury__c == ia.Injury__c) {
		            if (!checkMap.containsKey(il.Id)) {
			            Reporter__c r = new Reporter__c(InjuryLien__c = il.Id,
			                                            InjuryAward__c = ia.Id);
			            rList.add(r);
		            }
		        }
		    }
		}
		insert rList;
	}
}