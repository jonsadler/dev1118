/*
	Created: RAP - April 2017 for May Release
    Purpose: PD-208 - View Multiple Actions

History:

	Updated: lmsw - July 2017 
    Purpose: Repair testMethod to use TestUtility
    		 Coverage as of 7/11 - ActionController: 86%

    Updated: lmsw - October 2017
	Purpose: Pd-1102 Refactor using test methods within TestUtility
			 Coverage as of 10/6 - ActionController: 87%

	Updated: RAP - November 2017 for November Release
    Purpose: techDebt - increase code coverage

 */
@isTest
private class ActionController_Test {	
	
	static testMethod void TestStandardRedirect() {
		// Instanciate Test Utility
    	TestUtility testUtl = new TestUtility();

    	// Create test data
    	// User
		testUtl.CreateUser();
		User u = testUtl.currentUser;
		// Actions
		testUtl.CreateActions();
		Action__c a = testUtl.action1;
		// App to Action
		testUtl.CreateApptoAction();

		PageReference pageRef = new PageReference('/apex/ActionRedirect');
		ApexPages.currentPage().getParameters().put('sfdc.tabName','app2TA1');
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(a);

		// Start test
		test.startTest();
			system.runAs(u) {
				ActionController ctrl = new ActionController(stdCtrl);
		        ctrl.RedirectToAction();
			}
        test.stopTest();
    }
// start techDebt    
    static testMethod void TestActionShare() {
    	TestUtility testUtl = new TestUtility();
		testUtl.CreateUser();
		User u = testUtl.currentUser;
		testUtl.CreateActions();
		Action__c a = testUtl.action1;
		try {
	        list<Action__Share> shareList1 = [SELECT ParentId, UserOrGroupId
	                                         FROM Action__Share 
	                                         WHERE UserOrGroupId = :u.Id
	                                         AND ParentId = :a.Id];
			delete shareList1;
		}
		catch (exception e) {}
		system.runAs(u) {
			string lawfirmId = Controller_Aura1.getLawfirmID();
		}
    }
// end techDebt
}