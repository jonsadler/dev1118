/*  
    Created: RAP - April 2017 for May Release
    Purpose: PD-208 - View Multiple Actions

    Updated: RAP April 2017 for May Release
    Purpose: techDebt - Operations Flow Control Prep

    Updated: RAP October 2017 for October Release
    Purpose: PD-1082 - Portal Search Results
    
*/

public without sharing class ActionController {

    public static Id userId{get;set;}
    public static string aId{get;set;}

// constructor
    public ActionController(ApexPages.StandardController stdCtrl) {
        string actionTab;
        string actionName;
        userId = test.isRunningTest() ? [SELECT Id FROM User WHERE LastName = 'User' limit 1].Id : userInfo.getUserId();
        actionTab = ApexPages.currentPage().getParameters().get('sfdc.tabName');
        system.debug(loggingLevel.INFO, 'RAP --->> actionTab = ' + actionTab);
        actionName = AppToAction__c.getInstance(actionTab).Action__c;
        system.debug(loggingLevel.INFO, 'RAP --->> actionName = ' + actionName);
        if (actionName != 'All')
            aId = [SELECT ActionID_18__c FROM Action__c WHERE Name = :actionName limit 1].ActionID_18__c;
        else
            aId = 'All';
    }
// page auto-run method
    public pageReference RedirectToAction() {
        Controller_Aura1.SetAction(aId);
        if (aId == 'All')
            return new pageReference('/a02/o');
        return new pageReference('/' + aId);
    }
// start PD-1082
    public static void CreateActionShare(string lawFirm) {
        Id uId = userInfo.getUserId();
        map<Id,Action__c> actionMap = new map<Id,Action__c>([SELECT Id FROM Action__c 
                                                             WHERE Available_in_Portal__c = true
                                                             AND Id IN (SELECT Action__c FROM Action_Account__c WHERE Account__c = :lawFirm)]);
        list<Action__Share> shareList0 = [SELECT ParentId, UserOrGroupId
                                          FROM Action__Share 
                                          WHERE UserOrGroupId = :uId
                                          AND ParentId IN :actionMap.keySet()];
        if (!test.isRunningTest() && actionMap.size() == shareList0.size())
            return;
        list<Action__Share> shareList2 = new list<Action__Share>();
        for (Action__c a : actionMap.values()) {
            boolean add = true;
            for (Action__Share existing : shareList0) {
                if (existing.ParentId == a.Id)
                    add = false;
            }
            if (add) {
                Action__Share aShare = new Action__Share(ParentId = a.Id,
                                                         UserOrGroupId = uId,
                                                         AccessLevel = 'Read',
                                                         RowCause = Schema.Action__Share.RowCause.Manual);
                shareList2.add(aShare);
            }
        }
        if (!shareList2.isEmpty())
            insert shareList2;
        list<Id> acctList = new list<Id>();
        list<Lien__c> lList = [SELECT Account__c FROM Lien__c WHERE Claimant__c IN (SELECT Id FROM Claimant__c WHERE Law_Firm__c = :lawFirm)];
        for (Lien__c l : lList)
            acctList.add(l.Account__c);
        map<Id,Account> acctMap = new map<Id,Account>([SELECT Id FROM Account 
                                                       WHERE Id IN :acctList]);
        list<AccountShare> shareList1 = [SELECT AccountId, UserOrGroupId
                                         FROM AccountShare 
                                         WHERE UserOrGroupId = :uId
                                         AND AccountId IN :acctMap.keySet()];
        if (acctMap.size() == shareList1.size())
            return;
        list<AccountShare> shareList3 = new list<AccountShare>();
        for (Account a : acctMap.values()) {
            boolean add = true;
            for (AccountShare existing : shareList1) {
                if (existing.AccountId == a.Id)
                    add = false;
            }
            if (add) {
                AccountShare actShare = new AccountShare(AccountId = a.Id,
                                                         UserOrGroupId = uId,
                                                         AccountAccessLevel = 'Read',
                                                         OpportunityAccessLevel = 'Read',
                                                         RowCause = Schema.Action__Share.RowCause.Manual);
                shareList3.add(actShare);
            }
        }
        if (!shareList3.isEmpty())
            insert shareList3;
    }
    public static list<Claimant__c> GetAllClaimants(Id aId) {
        return [SELECT Id, Action__c, Status__c FROM Claimant__c WHERE Action__c = :aId];
    }
// end PD-1082
}