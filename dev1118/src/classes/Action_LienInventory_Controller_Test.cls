/*
    Created: CPS - March 2018 for Version 1.1 Release
    Purpose: PD-2287 - Refactoring Lien Inventory (to handle more than 50,000 liens)
*/

@IsTest
public with sharing class Action_LienInventory_Controller_Test {

    @TestSetup
    static void CreateData() {
        TestUtility.CreateTestData();
    }

    static testMethod void testGetAction() {
        Action__c expectedAction;
        Action__c actualAction;

        // Get an expected action.
        expectedAction = [SELECT Id,
                                 Name
                          FROM   Action__c
                          LIMIT  1];

        // Assert we found an expected action.
        System.assert(expectedAction != null);

        // Get the actual action from the controller.
        actualAction = Action_LienInventory_Controller.getAction(expectedAction.Id);

        // Assert we found an actual action and it matches the expected action.
        System.assert(actualAction != null);
        System.assertEquals(expectedAction.Id,   actualAction.Id);
        System.assertEquals(expectedAction.Name, actualAction.Name);
    }

    static testMethod void testGetLiensByActionChunked() {
        Lien__c       lien;
        List<Lien__c> expectedLiens;
        List<Lien__c> actualLiens;

        // Get a lien.
        lien = [SELECT Id,
                       Action__c
                FROM   Lien__c
                WHERE  Action__c != NULL
                LIMIT  1];

        // Assert we found a lien.
        System.assert(lien != null);

        // Get all the expected liens for an action.
        expectedLiens = [SELECT Id
                         FROM   Lien__c
                         WHERE  Action__c = :lien.Action__c];

        // Assert we found some expected liens.
        System.assert(expectedLiens.size() > 0);

        // Get all the actual liens from the controller
        actualLiens = Action_LienInventory_Controller.getLiensByActionChunked(lien.Action__c, null, 45000);

        // Assert we found the same number of actual liens.
        System.assertEquals(expectedLiens.size(), actualLiens.size());
    }

}