/*
	Created: lmsw - July 2017
	Purpose: Test function for User Trigger Handler - PD-806/879
			 Test Coverage as of 7/11/17: 88%
*/
@isTest
private class UserTriggerHandler_Test {
	
@testSetup
	static void test_Setup() {
		TestUtility.createTestData();
	}
		
	static testMethod void TestPortalUsers() {
	/*	UserRole portalRole = [SELECT Id FROM UserRole WHERE PortalType = 'None' LIMIT 1];
		Id acctID = [SELECT Id FROM Account WHERE Name ='LawFirm' LIMIT 1].Id;
		Id lhID = [SELECT Id FROM Account WHERE Name = 'Lienholder' LIMIT 1].Id;
		Id actionID = [SELECT Id FROM Action__c WHERE Name = 'TestAction1' LIMIT 1].Id;
		Profile profile = [SELECT Id FROM Profile WHERE name = 'System Administrator'];
		User portalAcctOwner = new User(UserRoleId = portalRole.Id,
										ProfileId = profile.Id,
										Username = 'profileuser@testorg.com',
										Alias = 'prof',
										Email='profileuser@testorg.com',
										EmailEncodingKey='UTF-8',
										Firstname = 'Fname',
										LastName = 'Lname', 
										LanguageLocaleKey='en_US',
										LocaleSidKey='en_US',
										TimeZoneSidKey='America/Los_Angeles');
		database.insert(portalAcctOwner);

		system.runAs(portalAcctOwner){
			// Create a Contact
			Contact contact1 = new Contact(FirstName ='Fname',
										LastName = 'Lname',
										AccountId = acctID,
										Email = 'Lname@test.com');
			Database.insert(contact1);
		}
		test.startTest(); 
		Id contactID = [SELECT Id FROM Contact WHERE LastName = 'Lname' LIMIT 1].Id;
		Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'ProvidioCustomerCommunityPlus'];
		// Create User
		User portalUser = new User(UserName = 'PoralUser@test.com',
									ContactId = contactID,
									ProfileId = portalProfile.Id,
									Alias = 'pu123',
									Email = 'pu123@test.com',
									EmailEncodingKey='UTF-8',
									LastName = 'PortalLname', 
									CommunityNickname = 'pu12345',
									TimeZoneSidKey='America/Los_Angeles',
									LocaleSidKey='en_US',
									LanguageLocaleKey='en_US'); 
		Database.insert(portalUser);
		map<Id,Id> userToAcct = new map<Id,Id>();
		userToAcct.put(portalUser.Id,acctID);
		map<Id,set<Id>> userToActionMap = UserTriggerHandler.getUserToActions(userToAcct);
		system.assertEquals(1, userToActionMap.size());
		string userToAction = JSON.serialize(userToActionMap);
		map<Id,set<Id>> userToLHMap = UserTriggerHandler.getUserToLienholders(userToActionMap);
		system.assertEquals(1, userToLHMap.size());
		string userToLH = JSON.serialize(userToLHMap);
		UserTriggerHandler.addActionSharing(userToAction);
		UserTriggerHandler.addAcctSharing(userToLH,userToAcct);
		test.stopTest();
		
		// Verify sharing rules have been created.
		User pu1 = [SELECT Id FROM User WHERE Alias = 'pu123'];
		system.assertEquals(6, [SELECT count() FROM AccountShare WHERE UserOrGroupId =: pu1.Id]);
		system.assertEquals('Edit', [SELECT AccessLevel FROM Action__Share WHERE UserOrGroupId = : pu1.Id LIMIT 1].AccessLevel);*/
	}	
}