/*
    Created - RAP June 2017 for June Release
    Purpose - PD-434 - Group Liens by Claimant to ensure all context present

*/
global class LienBatchByClaimant implements Database.Batchable<Claimant__c> {

	public string action{get;set;}
	public boolean recalc{get;set;}
	public set<Id> claimants{get;set;}
	public Id claimant{get;set;}
		
    public LienBatchByClaimant(string act, boolean snarf) {
    	action = act;
    	recalc = snarf;
    }
    
    public LienBatchByClaimant(string act, boolean snarf, set<Id> cList) {
    	action = act;
    	recalc = snarf;
    	claimants = cList;
    }

    public LienBatchByClaimant(string act, boolean snarf, Id c) {
    	action = act;
    	recalc = snarf;
    	claimant = c;
    }

	global Iterable<Claimant__c> start(Database.BatchableContext BC) {
		string queryString = 'SELECT Action__c, Address_1__c, Address_2__c, Alert3__c, City__c, Claim_Admin_Id__c, ClaimantID_18__c, ' +
							 'Name, DOB__c, DOB_text__c, DOD__c, Date_SSN_Changed__c, Email__c, First_Name__c, Flat_Fee_Paid__c, Gender__c, ' +
							 'Last_Name__c, Law_Firm__c, Lawfirm_ID__c, Lien_Amount_Left__c, Liens_Pending__c, M_M_Resolution__c, ' +
							 'Medicare_Resolution__c, Middle__c, Negative_Balance__c, Num_Liens__c, Opted_in_PLRP__c, ' +
						 	 'Other_Govt_Lien_Resolution__c, Part_C_Resolution__c, Phone__c, Primary_Attorney__c, ' +
							 'Private_Lien_Resolution__c, ProvidioID__c, Id, SSN__c, SSN_only__c, State__c, Status__c, ' +
							 'Status_Circle_Color__c, SystemModstamp, Third_Party_ID__c, Total_Applied__c, ' +
							 'Total_Surgery_Per_Defendant__c, VenueTxt__c, Wrongful_Death__c, Zip__c ' +
							 'FROM Claimant__c WHERE Action__r.Name = :action';
		if (claimant != null) {
			queryString += ' AND Id = :claimant';
		}
		if (claimants != null) {
			list<Id> cIds = new list<Id>();
			cIds.addAll(claimants);
			queryString += ' AND Id IN :cIds';
		}
		list<Claimant__c> batchList = database.query(queryString);
		return batchList;
	}

	public void execute(Database.BatchableContext BC, list<Claimant__c> claimants) {
		if (claimants != null && !claimants.isEmpty()) {
			list<Id> cList = new list<Id>();
			for (Claimant__c c : claimants)
				cList.add(c.Id);
			list<Lien__c> liens = [SELECT Action__c, Billing_Contact__c, Claimant__c, Cleared__c, Contact__c, County__c, CreatedById, CreatedDate, Date_No_Interest__c, Date_Submitted__c, 
										  Date_Payment_Made__c, Date_Overridden__c, Date_Payment__c, Date_Second_Sent__c, Date_Third_Sent__c, Deal_Made__c, IsDeleted, Eligible__c, Employer__c, 
										  Final_Agreed_Amount__c, Final_Lien_Amount_Value__c, Final_Demand_Received__c, Final_Payable__c, Group_Name__c, Group_Number__c, HICN__c, Health_Plan__c, 
										  HBAmount2__c, HBAmount__c, HBPercent__c, LastActivityDate, LastModifiedById, LastModifiedDate, Ledger__c, Letter_to_Medicaid_Individual__c, 
										  Letter_to_Private__c, LienAmount__c, LienID_18__c, Name__c, Lien_Type__c, Account__c, Lien_Holder_File_ID__c, Medicare_Contractor__c, 
										  Medicare_Match_Result__c, Military_Branch__c, Non_PLRP_Type__c, Notes__c, OffsetPercent__c, OtherPP_Type__c, Outstanding_Amount__c, Overridden_By_FMLA__c, 
										  Overridden_By__c, Override_Amount__c, OverrideFLAmount__c, Payable_Entity__c, PayerType__c, Percent_Complete__c, Policy__c, Provider_Name__c, 
										  OverrideReason__c, ReceivedSPD__c, Id, RecordTypeId, Name, Second_Notice_Sent__c, Stages__c, State__c, Status__c, Status_ERISA__c, Submitted__c, 
										  Subro_Rep__c, Subrogation_Rights__c, Substage__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, Time_in_Final__c, Time_in_First__c, 
										  Time_in_Second__c, Total_Fees_Applied__c, Total_Fees_Invoiced__c, Total_Fees_Paid__c, Treatment_Facility__c, Action__r.Name,
										  (SELECT PercentToLien__c, PercentToAward__c, Amount_Applied__c, Amount_Applied2__c, Award__c, Name, Fees_Applied__c, Final_Lien_Amount__c, 
										  		  Final_Payable__c, Lien__c, LienAmountCovered__c, Id, Total_Lien_Fees_Applied__c FROM Awards__r),
										  (SELECT Lien_Amount__c, Lien_Amount_Date__c FROM LienAmounts__r order by Lien_Amount_Date__c desc)
									FROM Lien__c
									WHERE Claimant__c IN :cList
									AND Stages__c != 'No_Lien'];
			list<Lien__c> lienList = new list<Lien__c>();
			for (Lien__c l : liens) {
				if (l.Awards__r != null && !l.Awards__r.isEmpty()) {
					if (l.LienAmounts__r != null && !l.LienAmounts__r.isEmpty())
						l.LienAmount__c = l.LienAmounts__r[0].Lien_Amount__c;
					if (recalc) {
						if (l.Stages__c == 'Final')
							l.Substage__c = 'Awaiting_Model_Determination';
						l.Final_Payable__c = null;
					}
					lienList.add(l);
				}
			}
			update lienList;
		}
	}
	
	public void finish(Database.BatchableContext BC) {
		
	}
}