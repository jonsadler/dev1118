/*
    Created: RAP - February 2017 for March Release
    Purpose: PD-172 - Claimant - Injury Tab

History:

	Updated: RAP - February 2017 for March Release
	Purpose: PD-405 - Claimant - Injury Tab (change start date to DOL, keep title)

    Updated: RAP - March 2017 for May Release
    Purpose: PD-520 - Extraneous Field Clean-up (change to Description2__c)

    Updated: lmsw - April 2017
    Purpose: PRODSUPT-99 - Sort Compensable = True, Undetermined, False and then DOL

    Updated: RAP - June 2017 for June Release
    Purpose: techDebt - refactor update for PRODSUPT-99

    Updated: lmsw - July 2017
    Purpose: Modified injuries

    Updated: lmsw - August 2017
    Purpose: Change to Description2__c
*/
public with sharing class InjuryListController {

    public Id claimantId{get;set;}
    public list<InjuryWrapper> iwList{get;set;}
    public list<Injury__c> injuries {
    	get {
    		if (injuries == null && claimantId != null) {
// start PRODSUPT-99
				injuries = new list<Injury__c>();
	        	list<Injury__c> temp = [SELECT Compensable__c, Id, Injury_Category__c, Injury_Description__c, DOL__c, End_Date__c,
				                               (SELECT Award__c, Award__r.Description__c FROM Awards__r),
				                               (SELECT ICDGroup__c, ICDGroup__r.Name FROM ICDCodes__r),
				                               (SELECT Lien__c, Lien__r.Name, Lien__r.Final_Payable__c FROM Liens__r)
				                        FROM Injury__c 
				                        WHERE Claimant__c = :claimantId
				                        order by DOL__c asc, Injury_Category__c asc, Injury_Description__c asc];

				map<string, list<Injury__c>> injMap = new map<string, list<Injury__c>>();
				injMap.put('True',new list<Injury__c>());
				injMap.put('Undetermined',new list<Injury__c>());
				injMap.put('False',new list<Injury__c>());

				for (Injury__c inj : temp)
					injMap.get(inj.Compensable__c).add(inj);
				for (string key : new list<string>{'True','Undetermined','False'})
				if (!injMap.get(key).isEmpty())
					injuries.addAll(injMap.get(key));
// end PRODSUPT-99
    		}
    		return injuries;
    	}
    	set;
    }
// constructor
	public InjuryListController(ApexPages.StandardController stdCtrl) {
		claimantId = stdCtrl.getId();
		iwList = new list<InjuryWrapper>();
 		for (Injury__c i : injuries) {
			InjuryWrapper iw = new InjuryWrapper(i);
			iwList.add(iw);
   		}
	}
	public class InjuryWrapper {
		public Id idVal{get;set;}
		public string category_x{get;set;}
		public string description{get;set;}
		public string start{get;set;}
		public string end_x{get;set;}
		public string comp{get;set;}
		public list<InjuryAward__c> awards{get;set;}
		public list<InjuryLien__c> liens{get;set;}
		public list<InjuryICDCode__c> icds{get;set;}
		public boolean showAwards{get;set;}
		public boolean showLiens{get;set;}
		public InjuryWrapper(Injury__c injury) {
			idVal = injury.Id;
			category_x = injury.Injury_Category__c;
			description = injury.Injury_Description__c;
			start = injury.DOL__c == null ? '' : string.valueOf(injury.DOL__c);
			end_x = injury.End_Date__c == null ? '' : string.valueOf(injury.End_Date__c);
			comp = injury.Compensable__c;
			awards = injury.Awards__r;
			liens = injury.Liens__r;
			icds = injury.ICDCodes__r;
			showAwards = injury.Awards__r != null && !injury.Awards__r.isEmpty();
			showLiens = injury.Liens__r != null && !injury.Liens__r.isEmpty();
		}
	}
}