/*
    Created: RAP - March 2017 for March Release
    Purpose: Test functions of List Controllers

History:

	Updatede: lmsw - March 2017
    Purpose: Change Claimant__c.Address__c to Claamant__c.Address_1__c

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c

    Updated: RAP - March 2017 for May Release
    Purpose: PD-520 - Extraneous Field Clean-up
			 Coverage as of 3/27 - AwardListController: 98%
    		 					   InjuryListController: 97%

    Purpose: Modify to use TestUtility
    		 Coverage as of 7/13 - AwardListController: 97%
    		 					   InjuryListController: 97%
*/
@isTest
private class ListController_Test {

@testSetup
	static void CreateData() {
		TestUtility.createTestData();
	}
	
	static testMethod void TestListControllers() {
		test.startTest();
		Claimant__c c = [SELECT Id FROM Claimant__c WHERE Last_Name__c = 'RAPPER' limit 1];
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(c);
		AwardListController ctrl1 = new AwardListController(stdCtrl);
		InjuryListController ctrl2 = new InjuryListController(stdCtrl);
		test.stopTest();
	}
}