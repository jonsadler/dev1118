/*
    Created: CPS - August 2017 for September Release
    Purpose: PD-966 - Allow users to assign other users' tasks to themselves for editing
             NOTE: This logic was placed into this separate class because "without sharing" is
                   needed to allow users to take ownership of tasks owned by other users.  Since
                   extreme care must be taken when using "without sharing", this class is very
                   granular and contains methods that only satisfy this user story, and can be
                   locked down by assigning only those user profiles to it that need this ability.
*/

global without sharing class Task_AssignToMe_WithoutSharing {
    // ################################################################################################## getTask
    @AuraEnabled
    public static Task getTask(Id taskId) {
        Task task;

        if (taskId == null) {
            return null;
        }

        task = [SELECT Id,
                       OwnerId,
                       Subject
                FROM   Task
                WHERE  Id = :taskId];

        return task;
    }

    // ################################################################################################## getCurrentUserId
    @AuraEnabled
    public static String getCurrentUserId() {
        return UserInfo.getUserId();
    }

    // ################################################################################################## assignTaskToCurrentUser
    @AuraEnabled
    public static Boolean assignTaskToCurrentUser(Id taskId) {
        List<Task> tasks;
        Id         currentUserId = UserInfo.getUserId();

        if (taskId == null) {
            return false;
        }

        tasks = [SELECT Id,
                        OwnerId
                 FROM   Task
                 WHERE  Id = :taskId];

        if (tasks.size() != 1 || tasks[0].OwnerId == currentUserId) {
            return false;
        }

        tasks[0].OwnerId = currentUserId;
        update tasks[0];
        return true;
    }
}