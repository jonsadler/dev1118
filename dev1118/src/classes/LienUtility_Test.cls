/*
    Created: RAP - March 2017 for March Release
    Purpose: Test functions of Lien Trigger Utility Class - PD-434 and techDebt

History: 

    Updated: lmsw - March 2017
    Purpose: Change Claimant__c.Address__c to Claamant__c.Address_1__c

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c

    Updated: RAP April 2017 for May Release
    Purpose: Repair failing testMethod

    Updated: RAP May 2017 for May15 Release
    Purpose: PD-527 - Automation adjustments
			 Coverage as of 3/21 - LienUtility: 94%
			 					   LienTrigger: 100%
			 Coverage as of 5/21 - LienUtility: 94%
			 					   LienTrigger: 100%
	Updated: lmsw - July 2017
	Purpose: Modify to use common test utility
			Coverage as of 7/11 - LienUtility: 71%
			 					  LienTrigger: 100%

    Updated: RAP August 2017 for August Release
    Purpose: PD-868 - Update Test Classes to Reflect Changes

    Updated: RAP August 2017 for September Release
    Purpose: PD-902 - Validation of Mismatched Stage/Substage Combinations

*/
@isTest
private class LienUtility_Test {

@testSetup
	static void CreateData() {
		TestUtility.CreateTestData();
	}
	
	static testMethod void TestStageCalcs() {
		test.startTest();
		Lien__c lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
							   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
							   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, PayerType__c
						FROM Lien__c
						WHERE Submitted__c = false
						limit 1];
		lien.Stages__c = 'Submitted';
		lien.Substage__c = 'First_Notice_Sent';
        update lien;
		lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
					   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
					   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, PayerType__c
				FROM Lien__c
				WHERE Id = :lien.Id
				limit 1];
        lien.Date_Submitted__c = system.today().addDays(-15);
        update lien;
        lien.Date_Submitted__c = system.today().addDays(-30);
        update lien;
		lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
					   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
					   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, PayerType__c
				FROM Lien__c
				WHERE Id = :lien.Id
				limit 1];
	}
// start PD-525/526
	static testMethod void TestFinalPayable() {
		list<Lien_Negotiated_Amounts__c> lienAmtList = new list<Lien_Negotiated_Amounts__c>();
		list<Id> lienIds = new list<Id>();
		list<Lien__c> liens = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, PayerType__c, 
									  Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
									  Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, RecordType.Name, 
									  Override_Holdback__c, Directed_By__c
							   FROM Lien__c
							   WHERE Claimant__r.Name = 'RAP RAPPER'
							   AND Submitted__c = true];
		for (Lien__c l : liens) {
	        l.Stages__c = 'Final';
	        l.Substage__c = 'Awaiting_Model_Determination';
	        lienIds.add(l.Id);
		}
		liens[0].Override_Holdback__c = true;
		test.startTest();
        try {
        	update liens[0];
        }
        catch(exception e){}
       	liens[0].Description__c = 'Just Because';
        try {
        	update liens[0];
        }
        catch(exception e){}
       	liens[0].Directed_By__c = userInfo.getUserId();
        try {
        	update liens[0];
        }
        catch(exception e){}
		liens[0].HBAmount2__c = 22000.00;
       	update liens[0];
       	list<Lien_Negotiated_Amounts__c> lnaList = [SELECT Id, Lien__c FROM Lien_Negotiated_Amounts__c WHERE Phase__c = 'Final'];
       	set<Id> dontdoit = new set<Id>();
       	for (Lien_Negotiated_Amounts__c lna : lnaList)
       		dontdoit.add(lna.Lien__c);
       	for (Id lId : lienIds) {
	        if (dontdoit.contains(lId))
	        	continue;
	        lienAmtList.add(new Lien_Negotiated_Amounts__c (Lien__c = lId, 
	        												Lien_Amount__c = 8000, 
	        												Lien_Amount_Date__c = system.today(), 
	        												Phase__c = 'Final'));
       	}
       	insert lienAmtList;
        test.stopTest();
	}
// end PD-525/526
	static testMethod void TestOutlyers() {
		map<string,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Lien__c.getRecordTypeInfosByName();
		Id rtId = rtMap.get('PLRP Model').getRecordTypeId();
		test.startTest();
		Lien__c lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
							   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
							   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, PayerType__c
						FROM Lien__c
						WHERE Submitted__c = true
						limit 1];
		lien.RecordTypeId = rtId;
        lien.Second_Notice_Sent__c = true;
        lien.Date_Second_Sent__c = system.today().addDays(-25);
        update lien;
        lien.Stages__c = 'Final';
        lien.Substage__c = 'Awaiting_Model_Determination';
        update lien;
        test.stopTest();
	}
// start PD-566
	static testMethod void TestOverride() {
        Id rtId7 = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('Other Govt').getRecordTypeId();
		list<Lien__c> liens = [SELECT Id, RecordTypeId, Action__c, Account__c, Stages__c, Account__r.Lienholder_Contact__c, Substage__c, Claimant__c, PayerType__c,
									  Date_Overridden__c, OverrideReason__c, OverrideFLAmount__c, Override_Amount__c, Override_Holdback__c, 
									  (SELECT Id, Fee_Amount__c, Fee_Type__c, Fee_Subtype__c FROM Lien_Res_Fees__r),
									  (SELECT Id, Award__r.Settlement_Category__c, Award__r.Amount__c, Award__r.NetAmount__c, Award__r.Settlement_SubCategory__c, 
									 		  PercentToAward__c, PercentToLien__c, Final_Lien_Amount__c 
									   FROM Awards__r)
							   FROM Lien__c
							   WHERE Submitted__c = true];
		test.startTest();
        Lien__c l = liens[0];
        l.Date_Overridden__c = system.today();
        l.OverrideReason__c = 'Because';
        l.OverrideFLAmount__c = true;
        l.Override_Amount__c = 54667;
        l.RecordTypeId = rtId7;
        for (Lien__c lien : liens) {
        	lien.Stages__c = 'Final';
        	lien.Substage__c = 'Awaiting_Model_Determination';
        }
        update liens;
        test.stopTest();
	}
	static testMethod void TestSendEmail() {
		Lien__c lien = [SELECT Id, RecordTypeId, Action__c, Account__c, Stages__c, Account__r.Lienholder_Contact__c, Substage__c, Claimant__c, PayerType__c,
							   (SELECT Id, Fee_Amount__c, Fee_Type__c, Fee_Subtype__c FROM Lien_Res_Fees__r),
							   (SELECT Id, Award__r.Settlement_Category__c, Award__r.Amount__c, Award__r.NetAmount__c, Award__r.Settlement_SubCategory__c, 
									   PercentToAward__c, PercentToLien__c 
								FROM Awards__r)
						FROM Lien__c
						limit 1];
		delete lien;
	}
	static testMethod void TestStageSubstage() {
		Lien__c lien = [SELECT Id, RecordTypeId, Action__c, Account__c, Stages__c
						FROM Lien__c
						limit 1];
		lien.Stages__c = 'Whatever';
		try {
			update lien;
		}
		catch (exception e) {}
	}
}