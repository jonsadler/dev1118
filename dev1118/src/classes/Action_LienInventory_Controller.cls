/*
    Created: CPS - March 2018 for Version 1.1 Release
    Purpose: PD-2287 - Refactoring Lien Inventory (to handle more than 50,000 liens)
*/

public class Action_LienInventory_Controller {
    // ################################################################################################ getAction
    // This method gets information about an action.
    //
    @AuraEnabled
    public static Action__c getAction(Id actionId) {
        if (actionId == null) {
            return null;
        }

        return [SELECT   Id,
                         Name
                FROM     Action__c
                WHERE    Id = :actionId];
    }

    // ################################################################################################ getLiensByActionChunked
    // This method gets the Liens in an Action.  Because Actions can contain
    // more than 50,000 Liens (which exceeds SOQL query row limits), the caller
    // will call this method multiple times to get a chunk of Liens at a time
    // based on a starting Lien ID and a row limit.
    //
    @AuraEnabled
    public static List<Lien__c> getLiensByActionChunked(Id actionId, Id prevChunkEndingLienId, Integer chunkRowLimit) {
        // We have to reassign Integers coming from Lightning components back to themselves
        // to avoid an "Internal Salesforce.com Error" at runtime when they're accessed.
        // More info at: https://developer.salesforce.com/forums/?id=906F00000005FxqIAE
        chunkRowLimit = Integer.valueOf(chunkRowLimit);

        if (actionId      == null ||
            chunkRowLimit == null ||
            chunkRowLimit  > 50000) {

            System.debug('ERROR: (getLiensByActionChunked) Bad parms; actionId = ' + actionId + '; chunkRowLimit = ' + chunkRowLimit);
            return new List<Lien__c>();
        }

        List<Lien__c> liens = [SELECT   Id,
                                        Name,
                                        RecordType.Name,
                                        Account__r.Name,
                                        daysSinceLastNoticeSent__c,
                                        Claimant__r.Name,
                                        State__c,
                                        Stages__c,
                                        Substage__c,
                                        toLabel(Stages__c) stageLabel,
                                        toLabel(Substage__c) substageLabel
                               FROM     Lien__c
                               WHERE    Action__c = :actionId AND
                                        Id        > :prevChunkEndingLienId
                               ORDER BY Id ASC
                               LIMIT    :chunkRowLimit];

        return liens;
    }

}