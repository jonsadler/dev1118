/*
    Created: lmsw - June 2017
    Purpose: PD-806 - Create Action sharing for Portal users

History:

	Updated: lmsw - June 2017
	Purpose: PD-879 - Modified AccessLevel = Edit

	Updated: lmsw - July 2017
	Purpose: PD-879 - Modify to add AccessLevel = Read for Lienholder Accounts.

	Updated: lmsw - July 2017
	Purpose: - Modified future methods for test coverage

	Updated: lmsw - August 2017
	Purpose: No longer using permissions for fortal users.

*/

public with sharing class UserTriggerHandler {
/*
    public void onAfterInsert(list<User> newUsers, map<Id, User> newUsersMap) {
   	//	createSharing(newUsersMap);
	}
 
	public void onAfterUpdate(list<User> newUsersRecs, list<User> oldUsersRecs, map<Id, User> newUsersMap, map<Id, User> oldUserssMap) {
//		updateSharing(newUsersMap);
	}

	private static void createSharing(map<Id, User> newUsersMap) {
		// Get portal user (userId) with company Name/acctId
		map<Id,Id> userToAcct = getPortalUsers(newUsersMap);
		// Create user to Action map	
		map<Id,set<Id>> userToAction = getUserToActions(userToAcct);
		// Create user to Lienholder map
		map<Id,set<Id>> userToLienholders = getUserToLienholders(userToAction);
	  	
		// Add Sharing - serialize data before calling the future method
		if(!Test.isRunningTest()) {
			addActionSharing(JSON.serialize(userToAction));
			addAcctSharing(JSON.serialize(userToLienholders),userToAcct);
		}				
	}

	private static void updateSharing(map<Id, User> newUsersMap) {
		// Get portal user (userId) with company Name/acctId
		map<Id,Id> userToAcct = getPortalUsers(newUsersMap);
		// Create user to Action map	
		map<Id,set<Id>> userToAction = getUserToActions(userToAcct);
		// Create user to Lienholder map
		map<Id,set<Id>> userToLienholders = getUserToLienholders(userToAction);
	  		  	
		// Add Sharing - serialize data before calling the future method
		if(!Test.isRunningTest()) {
			addActionSharing(JSON.serialize(userToAction));
			addAcctSharing(JSON.serialize(userToLienholders),userToAcct);
		}
	}
*/
/*	public static map<Id,Id> getPortalUsers(map<Id, User> newUsersMap) {
		// Get profile Id for "ProvidioCustomerCommunityPlus"
		map<Id,Id> userToAcct = new map<Id,Id>();
		if (!test.isRunningTest()) {
			Id communityProfileId = [SELECT Id FROM Profile WHERE Name = 'ProvidioCustomerCommunityPlus'].Id;
	
			// Get portal user (userId) with account ID	
			for(User u : newUsersMap.values()) {
			    if(u.ProfileId == communityProfileId) 
			    	userToAcct.put(u.Id,u.accountId);
			}
			system.debug('userToAcct: '+ userToAcct);
		}
		return userToAcct;
	}

	public static map<Id,Set<Id>> getUserToActions(map<Id,Id> portalUsers) {
		map<Id,set<Id>> userToActions = new map<Id,set<Id>>();
		map<Id,set<Id>> acctToActions = new map<Id,set<Id>>();
		
		for(Claimant__c c : [SELECT Id, Action__c, Law_Firm__c
							 FROM Claimant__c
							 WHERE Law_Firm__c IN :portalUsers.values()])
		{
		    if(acctToActions.containsKey(c.law_firm__c)) 
		        acctToActions.get(c.law_firm__c).add(c.action__c);
		    else
		       acctToActions.put(c.law_firm__c,new set<Id>{c.action__c}); 
		}
		                        
   		for(Id uId : portalUsers.keySet()){
		    Id acctId = portalUsers.get(uId);
		    set<Id> actions = acctToActions.get(acctId);
		    userToActions.put(uId,actions);    
		}
		system.debug('userToActions: '+ userToActions);     
		return userToActions;
	}
	
	public static map<Id,Set<Id>> getUserToLienholders(map<Id,set<Id>> userToAction){
		// Create map action-lienholders
		map<Id,set<Id>> userToLienholders = new map<Id,set<Id>>();

		for(Id uId : userToAction.keyset()) {
			set<Id> actionIds = userToAction.get(uId);
			// Get Lienhlders for user's Actions
			list<Action_Account__c> lhList = ([SELECT Account__c, Account__r.RecordType.Name, 
												Action__c, Action__r.Active__c
												FROM Action_Account__c
												WHERE Account__r.RecordType.Name =: 'Lienholder'
												AND Action__c IN : actionIds]);
			// Create map user-lienholders
			for(Action_Account__c aa : lhList) {
				if(userToLienholders.containsKey(uId))
					userToLienholders.get(uId).add(aa.Account__c);
				else
					userToLienholders.put(uId, new set<Id>{aa.Account__c});
			}
		}
		system.debug('userToLienholder: '+ usertoLienholders);
		return userToLienholders;
	}

	@future
	public static void addActionSharing(string userToActionSerialized) {
		map<Id,list<Id>> userToAction = (map<Id, list<Id>>)JSON.deserialize(userToActionSerialized, map<Id, list<Id>>.class);
		list<Action__Share> actionShareList = new list<Action__Share>();
		
		// Add Action sharing for user
		for(Id uId : userToAction.keySet()){
		    list<Id> aids = userToAction.get(uId);
		    for(Id aId : aids){
		    	Action__Share actionShare = new Action__Share(ParentId = aId,
		                                        AccessLevel = 'Edit',
		                                        UserOrGroupId = uId);
				system.debug('actionShare: '+ actionShare);
				actionShareList.add(actionShare);
		    }
		}
		if(!actionShareList.isEmpty()) insert actionShareList;
	}
	
	@future
	public static void addAcctSharing(string userLienholderAccessSerialized, map<Id,Id> userToAcct) {
		map<Id,list<Id>> userLHAccess = (map<Id, list<Id>>)JSON.deserialize(userLienholderAccessSerialized, map<Id,list<Id>>.class);
		list<AccountShare> acctShareList = new list<AccountShare>();
		
		// Add Account sharing for the user
		for(Id uId : userToAcct.keySet()) {
			Id aId = userToAcct.get(uId);
			AccountShare acctShare = new AccountShare(AccountId = aId,
											UserOrGroupId = uId,
											AccountAccessLevel = 'Edit',
											OpportunityAccessLevel = 'None');
			system.debug('account Share: '+ acctShare);
			acctShareList.add(acctShare);

			// Add Lienholder sharing for the user
			list<Id> lhIds = userLHAccess.get(uId);
			for(Id lhId : lhIds){
				AccountShare lhShare = new AccountShare(AccountId = lhId,
											UserOrGroupId = uId,
											AccountAccessLevel = 'Read',
											OpportunityAccessLevel = 'None');
				system.debug('lienholder share: '+ lhShare);
				acctShareList.add(lhShare);
			}
		}		
		if(!acctShareList.isEmpty()) insert acctShareList;
 	}	*/
}