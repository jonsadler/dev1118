/*
	Created: lmsw - January 2018 for January Release
	Purpose: Test Class for methods in Controller_Aura1 for Portal Matter Reports
				Coverage of as of 1/11/18:	Controller_Aura1.getPortalReportMap - 100%
											Controller_Aura1.getPortalMatterReportId - 100%
*/
@isTest
private class Portal_Matter_Report_Test {
	
	@isTest(SeeAllData='true') 
	public static void testPortalMatterReport() {
		string reportTitle = ' Client Status Report';
		string portalFolder = 'Portal Reports';
		// Get a list of Portal Actions
		map<string,Action__c> portalActions = new map<string,Action__c>();
		Action__c portalAction;
		list<string> portalReportNames = new list<string>();
		for(Action__c a : [SELECT Id, Name, Available_in_Portal__c, Active__c
							FROM Action__c
							WHERE Active__c = true
							AND Available_in_Portal__c = true])
		{	
			portalActions.put(a.Name,a);
			portalReportNames.add(a.Name + reportTitle);
			// Get one action
			portalAction = a;	
		}
		
		// Get a list of reports in the Portal Reports folder that are a Matter Report
		list<Report> reportList = [SELECT Id, Name, FolderName 
									FROM Report
									WHERE FolderName =: portalFolder
									AND Name IN: portalReportNames];
		integer expectedReportCount = reportList.size();
			
		test.startTest();
			string expectedReportName;
			map<string,string> portalReportMap = Controller_aura1.getPortalReportMap(portalActions.values());
			integer actualReportCount = portalReportMap.size();
			system.assertEquals(expectedReportCount,actualReportCount);

			if(portalActions.size() > 0)
				expectedReportName = portalAction.Name + reportTitle;
			
			string reportId = Controller_aura1.getPortalMatterReportId(portalAction);
			if(!string.isBlank(reportId) && !string.isBlank(expectedReportName))
				system.assertEquals(expectedReportName, [SELECT Id, Name
														FROM Report
														WHERE Id =: reportId].Name);
		test.stopTest();
		
	}
	
}