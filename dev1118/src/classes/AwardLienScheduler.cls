/*
Created: RAP September 2017 for September Release
Purpose: Schedule AwardLienBatch twice per day

*/

global class AwardLienScheduler implements Schedulable {

	global string actionName;
	global string noonOrMidnight;
	
	global AwardLienScheduler (string action) {
		actionName = action;
	}
	
	global void execute(SchedulableContext sc) {
        integer batchSize = 200;
        database.executeBatch(new AwardLienBatch(actionName), batchSize);
    }
    
    public void scheduleJob(string scheduleString, string action, string time_x) {
        if (scheduleString == null)
            scheduleString = '0 0 0 * * ?'; // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        actionName = action;
        noonOrMidnight = time_x;
        system.schedule(getUniqueJobName(), scheduleString, new AwardLienScheduler (actionName));
    }
      
    public string getUniqueJobName() {
        //construct unique job name by appending timestamp
        string uniqueJobName = 'AwardLienBatch - ' + actionName + ' - ' + noonOrMidnight;
        system.debug('job name : ' + uniqueJobName);
        return uniqueJobName;
    }    
}