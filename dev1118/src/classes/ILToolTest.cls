/*
    Created: RAP - August 2017 for August Release
    Purpose: PD-892 - Back-Fill Missing Injury Lien Junctions for Existing Liens (test class)
*/

@isTest
private class ILToolTest {
    
@testSetup
	static void test_Setup() {
		TestUtility.createTestData();
		list<InjuryLien__c> deleteList = [SELECT Id FROM InjuryLien__c];
		delete deleteList;
	}
	
	static testMethod void testTool() {
		map<Id,Claimant__c> cMap = new map<Id,Claimant__c>([SELECT Id, Name FROM Claimant__c]);
		list<Id> cList = new list<Id>();
		cList.addAll(cMap.keySet());
		injuryLienTool.AssociateLiens(cList);
	}
}