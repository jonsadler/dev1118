/*
    Created: RAP - February 2017 for March Release
    Purpose: PD-82 - MVP - History Tracking
*/

public with sharing class ClaimantHistoryController { 
    
    public string claimantId{get;set;}
    
    public ClaimantHistoryController(ApexPages.StandardController stdCtrl) {
		claimantId = stdCtrl.getId();
    }
    
    public list<Claimant__History> claimantH {
    	get {
   			return [SELECT OldValue, NewValue, Field, CreatedById, CreatedDate, Id, ParentId, CreatedBy.Name 
   					FROM Claimant__History
   					WHERE ParentId = :claimantId
   					order by CreatedDate desc];
    	}
    }
}