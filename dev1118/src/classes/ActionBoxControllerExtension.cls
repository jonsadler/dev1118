/*
    Created: lmsw - February for Discovry/Design
    Purpose: PD-395 Create Controller extension to get Action Box Folder on Claimant page

History: 

    Updated: lmsw - February for Discovery/Design
    Purpose: PD-395 Change to handle classic and lightning, lightning is currently hardcoded

    Updated: lmsw - March for March Release
    Purpose: PD-449 - Modified to use box Toolkit - Action Box Folder

    Updated: RAP - March 2017 for March Release
    Purpose: refactor - pull action name from custom setting, some efficiency updates

    Updated: lmsw - March 2017
    Purpose: remove unused lines of code.

    Updated: lmsw - August 2017
    Purpose: PD-964 - Fix Action box on Claimant page to show Claimant Action
    
*/

public with sharing class ActionBoxControllerExtension {
    
    //private final Claimant__c claimant;
    public system.PageReference actionFolderUrl {get; set;}
    public boolean hasAction {get; set;}
    
    public ActionBoxControllerExtension(ApexPages.StandardController stdCtrl) {
        Claimant__c c = (Claimant__c)stdCtrl.getRecord();
        boolean isMobileContext = true;
        hasAction = false;
        getActionFolderUrl(c,isMobileContext);
    }

    public system.PageReference getActionFolderUrl(Claimant__c c, boolean isMobileContext) {
        Claimant__c cRecord = [SELECT Id, Action__c FROM Claimant__c WHERE Id =: c.Id];
        string act = cRecord.Action__c;
        
        if (act != null) {
            // Instantiate the Toolkit object
            box.Toolkit boxToolkit = new box.Toolkit();
            actionFolderUrl = boxToolkit.getFolderUrl(act, isMobileContext);
            if (actionFolderUrl != null) 
            	hasAction = true;
        }
        return actionFolderUrl;
	}
}